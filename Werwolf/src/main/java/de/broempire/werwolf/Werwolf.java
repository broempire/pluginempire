/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf;

import de.broempire.werwolf.APIs.ActionAPI;
import de.broempire.werwolf.Commands.Check;
import de.broempire.werwolf.Commands.SetDorfplatz;
import de.broempire.werwolf.Commands.SetLobby;
import de.broempire.werwolf.Commands.SetSpawn;
import de.broempire.werwolf.Commands.Start;
import de.broempire.werwolf.Listener.BlockListener;
import de.broempire.werwolf.Listener.ChatListener;
import de.broempire.werwolf.Listener.ChestListener;
import de.broempire.werwolf.Listener.DeathListener;
import de.broempire.werwolf.Listener.Listeners;
import de.broempire.werwolf.Listener.MoveListener;
import de.broempire.werwolf.Listener.NatureListener;
import de.broempire.werwolf.Listener.PlayerListener;
import de.broempire.werwolf.Manager.RoleManager;
import de.broempire.werwolf.Manager.ScoreboardManager;
import de.broempire.werwolf.Manager.ScoreboardTeams;
import de.broempire.werwolf.Manager.InteractManager;
import de.broempire.werwolf.MySQL.Bro;
import de.broempire.werwolf.MySQL.MySQL;
import de.broempire.werwolf.MySQL.MySQLStats;
import de.broempire.werwolf.Utils.GameStatus;
import de.broempire.werwolf.Utils.Methods;
import java.util.ArrayList;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Instrument;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * 
 * @author Juli
 */
public class Werwolf extends JavaPlugin{

    public static String prefix = "§8▎ §6Werwolf §8» ";
    public static String noPerms = prefix + "§8▎ §6BroEmpire§8 ❘ §cDu besitzt nicht die nötigen Rechte für diesen Befehl!";
    public static String nospieler = prefix + "§8▎ §6BroEmpire§8 ❘ §cDu bist kein Spieler!";
    
    public ArrayList<Player> town = new ArrayList<>();
    
    public ArrayList<Player> werewolf = new ArrayList<>();
    public ArrayList<Player> villager = new ArrayList<>();
    public ArrayList<Player> mayor = new ArrayList<>();
    public ArrayList<Player> witch = new ArrayList<>();
    public ArrayList<Player> executioner = new ArrayList<>();
    public ArrayList<Player> jester = new ArrayList<>();
    
    public static String teamplayerprefix;
    public static String teampremiumprefix;
    public static String teamvipprefix;
    public static String teambuilderprefix;
    public static String teamleadbuilderprefix;
    public static String teamjrmoderatorprefix;
    public static String teammoderatorprefix;
    public static String teamleadmoderatorprefix;
    public static String teamforenmodprefix;
    public static String teamleadforenmodprefix;
    public static String teamdeveloperprefix;
    public static String teamleaddeveloperprefix;
    public static String teamadminprefix;
    
    
    public ArrayList<Location> spawn = new ArrayList<>();
    public ArrayList<Location> dorfplatz = new ArrayList<>();
    
    public Werwolf plugin;
    
    public GameStatus status;
    
    public MySQL mySQL;
    private MySQLStats mySQLStats;
    private Bro bro;
    
    private String database;
    private String username;
    private String password;
    private String host;
    
    private RoleManager rolemanger;
    private Methods methods;
    private ScoreboardTeams scoreboardteams;
    private ScoreboardManager scoreboardmanager; 
    private InteractManager interactmanager;
    
    public static int time = 91;
    
    private boolean countdown = false;
    
    @Override
    public void onEnable() {
        for(World w : Bukkit.getWorlds()) {
            
	    w.setThundering(false);
	    w.setStorm(false);
	    w.setTime(1000);
	}
        
        /*ToDo-Liste:
        ~ ChatListener(Servergruppen in der Lobby)
        ~ DeathListener(verschiedene Methoden)
        ~ PlayerListener
        ~ InteractManager(VoteSystem, ...)
        ~ RoleManager(Rollen hinzufügen, einzelne Rollen "ausstatten")
        ~ ScoreboardManager(Beschreibungen für die Rollen schreiben)
        ~ ScoreboardTeams(Rollen hinzufügen)
        ~ MySQL(eventuelle Bugs beheben, Ranking, Stats, ...)
        ~ Methods(den Neustart vom Server coden)
        */
        this.rolemanger = new RoleManager(this);
        this.methods = new Methods(this);
        this.scoreboardteams = new ScoreboardTeams(this);
        this.scoreboardmanager = new ScoreboardManager(this);
        this.interactmanager = new InteractManager(this);
        
        ScoreboardTeams.scoreboardteamsinit();
        
        registerCommands();
	registerEvents();
        
        loadConfig();
        loadConfigData();
        //ConnectToMySQL();
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        
	status = GameStatus.LOBBY;
        System.out.println("[Werwolf] Plugin geladen!");
    }

    private void registerEvents() {
        PluginManager pm = getServer().getPluginManager();
        
        pm.registerEvents(new Listeners(this), this);
        pm.registerEvents(new NatureListener(), this);
        pm.registerEvents(new DeathListener(this), this);
        pm.registerEvents(new PlayerListener(this), this);
        pm.registerEvents(new MoveListener(this), this);
        pm.registerEvents(new BlockListener(this), this);
        pm.registerEvents(new ChestListener(this), this);
        pm.registerEvents(new ChatListener(this), this);
        pm.registerEvents(new InteractManager(this), this);
    }
    
    private void registerCommands() {
        
        this.getCommand("check").setExecutor(new Check(this));
        this.getCommand("start").setExecutor(new Start());
        this.getCommand("setlobby").setExecutor(new SetLobby(this));
        this.getCommand("setspawn").setExecutor(new SetSpawn(this));
        this.getCommand("setdorfplatz").setExecutor(new SetDorfplatz(this));
    }
    
    @Override
    public void onDisable() {
        System.out.println("[Werwolf] Plugin deaktiviert!");
    }
    
    public void loadConfigData() {
        database = this.getConfig().getString("MySQL.Database");
        username = this.getConfig().getString("MySQL.Username");
        password = this.getConfig().getString("MySQL.Password");
        host = this.getConfig().getString("MySQL.Hostname");
        
        teamplayerprefix = this.getConfig().getString("Prefix.Player");
        teampremiumprefix = this.getConfig().getString("Prefix.Premium");
        teamvipprefix = this.getConfig().getString("Prefix.VIP");
        teambuilderprefix = this.getConfig().getString("Prefix.Builder");
        teamleadbuilderprefix = this.getConfig().getString("Prefix.LeadBuilder");
        teamjrmoderatorprefix = this.getConfig().getString("Prefix.JrModerator");
        teammoderatorprefix = this.getConfig().getString("Prefix.Moderator");
        teamleadmoderatorprefix = this.getConfig().getString("Prefix.LeadModerator");
        teamforenmodprefix = this.getConfig().getString("Prefix.Forenmod");
        teamleadforenmodprefix = this.getConfig().getString("Prefix.LeadDeveloper");
        teamdeveloperprefix = this.getConfig().getString("Prefix.Developer");
        teamleaddeveloperprefix = this.getConfig().getString("Prefix.LeadDeveloper");
        teamadminprefix = this.getConfig().getString("Prefix.Admin");
    }
    
    public void loadConfig() {
	FileConfiguration cfg = this.getConfig();
		
        cfg.options().copyDefaults(true);
                
	cfg.addDefault("MySQL.Hostname", "Hostname");
	cfg.addDefault("MySQL.Username", "Username");
	cfg.addDefault("MySQL.Database", "Database");
        cfg.addDefault("MySQL.Password", "Password");
                
        cfg.addDefault("Prefix.Player", "§eSpieler §7| ");
        cfg.addDefault("Prefix.Premium", "§6Premium §7| ");
        cfg.addDefault("Prefix.VIP", "§dVIP §7| ");
        cfg.addDefault("Prefix.LeadBuilder", "§9LeadBuild §7| ");
        cfg.addDefault("Prefix.Builder", "§9Build §7| ");
        cfg.addDefault("Prefix.LeadModerator", "§cLeadMod §7| ");
        cfg.addDefault("Prefix.Moderator", "§cMod §7| ");
        cfg.addDefault("Prefix.JrModerator", "§cJrMod §7| ");
        cfg.addDefault("Prefix.LeadForenmod", "§2LeadF-Mod §7| ");
        cfg.addDefault("Prefix.Forenmod", "§2F-Mod §7| ");
        cfg.addDefault("Prefix.LeadDeveloper", "§bLeadDev §7| ");
        cfg.addDefault("Prefix.Developer", "§bDev §7| ");
        cfg.addDefault("Prefix.Admin", "§4Admin §7| ");
		
        saveConfig();
    }
    
    public void ConnectToMySQL() {
            mySQL = new MySQL (host, database, username, password);
            mySQL.update("CREATE TABLE IF NOT EXISTS WerwolfStats (UUID varchar (64) PRIMARY KEY, played VARCHAR(100), wins VARCHAR(100), points VARCHAR(100), kills VARCHAR(100), deaths VARCHAR(100))");
            
            mySQLStats = new MySQLStats(mySQL);
            bro = new Bro(this, mySQL);
    }
    
    public Location getLocation(String path) {
        String world = this.getConfig().getString("Locations." + path + ".World");
        double x = this.getConfig().getDouble("Locations." + path + ".X");
        double y = this.getConfig().getDouble("Locations." + path + ".Y");
        double z = this.getConfig().getDouble("Locations." + path + ".Z");
        float yaw = this.getConfig().getInt("Locations." + path + ".Yaw");
        float pitch = this.getConfig().getInt("Locations." + path + ".Pitch");
        
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }
    
    public Location getLocations(String path, int number) {
        String world = this.getConfig().getString("Locations." + path + "." + number + ".World");
        double x = this.getConfig().getDouble("Locations." + path + "." + number + ".X");
        double y = this.getConfig().getDouble("Locations." + path + "." + number + ".Y");
        double z = this.getConfig().getDouble("Locations." + path + "." + number + ".Z");
        float yaw = this.getConfig().getInt("Locations." + path + "." + number + ".Yaw");
        float pitch = this.getConfig().getInt("Locations." + path + "." + number + ".Pitch");
        
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
        
    }
    
    public GameStatus getStatus() {
        return status;
    }
    
    public MySQLStats getMySQLStats() {
        return mySQLStats;
    }
    
    public ArrayList<Player> getTown() {
        return town;
    }
    
    public ArrayList<Player> getWerewolf() {
        return werewolf;
    }
    
    public ArrayList<Player> getVillager() {
        return villager;
    }
    
    public ArrayList<Player> getMayor() {
        return mayor;
    }
    
    public ArrayList<Player> getWitch() {
        return witch;
    }
    
    public ArrayList<Player> getExecutioner() {
        return executioner;
    }
    
    public ArrayList<Player> getJester() {
        return jester;
    }
    
    public ScoreboardTeams getScoreboardTeams() {
        return scoreboardteams;
    }
    
    public ScoreboardManager getScoreboardManager() {
        return scoreboardmanager;
    }
    
    public RoleManager getRoles() {
        return rolemanger;
    }
    
    public InteractManager getInteract() {
        return interactmanager;
    }
    
    public Bro getBro() {
        return bro;
    }
    
    public Methods getMethods() {
        return methods;
    }
    
    public boolean isCountdown() {
        return countdown;
    }
    
    public void StartCountdown() {
        countdown = true;
	new BukkitRunnable() {
            
            int s = 0;
            String[] players = new String[]{"Juligum", "Juligum", "Juligum", "Juligum", "Juligum", "Juligum", "Juligum", "Juligum", "Juligum", "Juligum", "Juligum", "Juligum"};
            
	    @Override
	    public void run() {
                if(Bukkit.getOnlinePlayers().isEmpty()) {
                    countdown = false;
                    cancel();
                }
                
                time--;
                
                if(Bukkit.getOnlinePlayers().size() >= 2 && time > 60) {
                    time = 61;
                    Bukkit.broadcastMessage(prefix + "§aDie Mindestanzahl an Spielern ist erreicht!");
                }else if (Bukkit.getOnlinePlayers().size() < 2 && time <= 60) {
                    time = 91;
                    Bukkit.broadcastMessage(prefix + "§cEs sind nicht genug Spieler online!");
                }
                for(Player all : Bukkit.getOnlinePlayers()) {
		    all.setLevel(time);
                
                    if(time > 5) {
                        ActionAPI.sendActionBar(all, "§7Spieler: §6" + Bukkit.getOnlinePlayers().size() + "§7/§612  §7Mindestens: §64");
                    }
                }
                
               switch(time) {
                    case 60:
                    case 50:
                    case 40:
                    case 30:
                    case 20:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Noch §b" + time + " §7Sekunden bis zum Start!");
                        }
                        break;
                    case 10:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.BLOCK_ANVIL_USE, 3F, 3F);
                            all.sendMessage(prefix + "§7Noch §b" + time + " §7Sekunden bis zum Start!");
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Noch §b" + time + " §7Sekunden bis zum Start!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Noch §b" + time + " §7Sekunde bis zum Start!");
                        }
                        break;
                    case 0:
                        
                        
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            if(s == 0) {
                                Bukkit.broadcastMessage("S = " + s + " Player = " + all.getDisplayName());
                                players[0] = all.getDisplayName();
                            }else{
                                Bukkit.broadcastMessage("S = " + s + " Player = " + all.getDisplayName());
                                players[s] = all.getDisplayName();
                            }
                            s++;
                            
                            all.getInventory().clear();
                            all.teleport(getLocations("Dorfplatz", s));
                            //mySQLStats.addPlayed(all.getUniqueId().toString(), 1);
                            all.setGameMode(GameMode.SURVIVAL);
                        }
                        
                        switch(Bukkit.getOnlinePlayers().size()) {
                            case 1:
                                
                                getRoles().Villager(Bukkit.getPlayerExact(players[0]));
                                break;
                            case 2:
                                    
                                getRoles().Villager(Bukkit.getPlayerExact(players[0]));
                                getRoles().Werewolf(Bukkit.getPlayerExact(players[1]));
                                break;
                            case 3:
                                
                                getRoles().Villager(Bukkit.getPlayerExact(players[0]));
                                getRoles().Werewolf(Bukkit.getPlayerExact(players[1]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[2]));
                            case 4:
                                    
                                getRoles().Villager(Bukkit.getPlayerExact(players[0]));
                                getRoles().Werewolf(Bukkit.getPlayerExact(players[1]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[2]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[3]));
                                break;
                            case 5:
                                    
                                getRoles().Werewolf(Bukkit.getPlayerExact(players[0]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[1]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[2]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[3]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[4]));
                                break;
                            case 6:
                                    
                                getRoles().Villager(Bukkit.getPlayerExact(players[0]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[1]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[2]));
                                getRoles().Werewolf(Bukkit.getPlayerExact(players[3]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[4]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[5]));
                                break;
                            case 7:
                                    
                                getRoles().Villager(Bukkit.getPlayerExact(players[0]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[1]));
                                getRoles().Werewolf(Bukkit.getPlayerExact(players[2]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[3]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[4]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[5]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[6]));
                                break;
                            case 8:
                                    
                                getRoles().Villager(Bukkit.getPlayerExact(players[0]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[1]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[2]));
                                getRoles().Werewolf(Bukkit.getPlayerExact(players[3]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[4]));
                                getRoles().Werewolf(Bukkit.getPlayerExact(players[5]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[6]));
                                getRoles().Villager(Bukkit.getPlayerExact(players[7]));
                                    break;
                            /*case 9:
                                    
                                    getRoles().Villager(players[0]);
                                    getRoles().Villager(players[1]);
                                    getRoles().Werewolf(players[2]);
                                    getRoles().Villager(players[3]);
                                    getRoles().Villager(players[4]);
                                    getRoles().Villager(players[5]);
                                    getRoles().Villager(players[6]);
                                    getRoles().Villager(players[7]);
                                    getRoles().Werewolf(players[8]);
                                    break;
                            case 10:
                                    
                                    getRoles().Villager(players[0]);
                                    getRoles().Villager(players[1]);
                                    getRoles().Villager(players[2]);
                                    getRoles().Werewolf(players[3]);
                                    getRoles().Villager(players[4]);
                                    getRoles().Villager(players[5]);
                                    getRoles().Villager(players[6]);
                                    getRoles().Villager(players[7]);
                                    getRoles().Werewolf(players[8]);
                                    getRoles().Villager(players[9]);
                                    break;
                            case 11:
                                    
                                    getRoles().Werewolf(players[0]);
                                    getRoles().Villager(players[1]);
                                    getRoles().Villager(players[2]);
                                    getRoles().Villager(players[3]);
                                    getRoles().Villager(players[4]);
                                    getRoles().Villager(players[5]);
                                    getRoles().Villager(players[6]);
                                    getRoles().Villager(players[7]);
                                    getRoles().Villager(players[8]);
                                    getRoles().Werewolf(players[9]);
                                    getRoles().Villager(players[10]);
                                    break;
                            case 12:
                                    
                                    getRoles().Villager(players[0]);
                                    getRoles().Villager(players[1]);
                                    getRoles().Villager(players[2]);
                                    getRoles().Werewolf(players[3]);
                                    getRoles().Villager(players[4]);
                                    getRoles().Werewolf(players[5]);
                                    getRoles().Villager(players[6]);
                                    getRoles().Villager(players[7]);
                                    getRoles().Villager(players[8]);
                                    getRoles().Villager(players[9]);
                                    getRoles().Werewolf(players[10]);
                                    getRoles().Villager(players[11]);
                                    break;*/
                        }
                        status = GameStatus.NOMOVE;
                        
                        DisscusionCountdown();
                        cancel();
                        break;
                    }
                }
		}.runTaskTimer(this, 0, 20);
	}
    
    public void NoMoveCountdown() {
	new BukkitRunnable() {
            
            int nomove = 21;
            int s = 0;
            
	    @Override
	    public void run() {
                nomove--;
                
                    switch(nomove) {
                    case 20:
                        town.addAll(villager);
                        town.addAll(mayor);
                    case 10:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Die Runde beginnt in §b" + nomove + " §7Sekunden!");
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Die Runde beginnt in §b" + nomove + " §7Sekunden!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Die Runde beginnt in §b" + nomove + " §7Sekunde!");
                        }
                        break;
                    case 0:
                        status = GameStatus.INGAME;
                        NightCountdown();
                        cancel();
                        break; 
                    }
                }
		}.runTaskTimer(this, 0, 20);
	}
    
    public void NightCountdown() {
	new BukkitRunnable() {
            
            int n = 61;
            int s = 0;
            
	    @Override
	    public void run() {
                n--;
                
                switch(n) {
                    case 60:
                        for(Player villiger : villager) {
                            s++;
                            
                            villiger.teleport(getLocations("Spawn", s));
                            villiger.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 600, 3));
                            villiger.getInventory().setItem(0, new ItemStack(Material.WOOD_SWORD));
                        }
                        for(Player werewolf : getWerewolf()) {
                            werewolf.getInventory().setItem(0, new ItemStack(Material.IRON_SWORD));
                            werewolf.getInventory().setHelmet(new ItemStack(Material.CHAINMAIL_HELMET));
                            werewolf.getInventory().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
                            werewolf.getInventory().setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
                            werewolf.getInventory().setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
                            //werewolf.getInventory().setItem(1, getMethods().createItem(Material.PAPER, 1, 0, "§7↠ §cVoten", "§7↳ §8Rechtsklick auf die Köpfe zum Voten!"));
                        }
                        for(World w : Bukkit.getWorlds()) {
                            w.setTime(13000);
                        }
                        s = 0;
                    case 30:
                    case 10:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Die Nacht endet in §b" + n + " §7Sekunden!");
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Die Nacht endet in §b" + n + " §7Sekunden!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Die Nacht endet in §b" + n + " §7Sekunde!");
                        }
                        break;
                    case 0:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            s++;
                            
                            all.teleport(getLocations("Spawn", s));
                        }
                        for(Player werewolf : getWerewolf()) {
                            werewolf.getInventory().clear();
                            werewolf.getInventory().setArmorContents(null);
                            //werewolf.getInventory().setItem(1, getMethods().createItem(Material.PAPER, 1, 0, "§7↠ §cVoten", "§7↳ §8Rechtsklick auf die Köpfe zum Voten!"));
                        }
                        for(World w : Bukkit.getWorlds()) {
                            w.setTime(1000);
                        }
                        FarmCountdown();
                        cancel();
                        break;
                    }
                }
		}.runTaskTimer(this, 0, 20);
	}
    
    public void FarmCountdown() {
	new BukkitRunnable() {
            
            int f = 31;
            int s = 0;
            
	    @Override
	    public void run() {
                f--;
                    
                    switch(f) {
                    case 240:
                    case 180:
                    case 120:
                    case 60:
                    case 30:
                    case 10:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Die Farmphase endet in §b" + f + " §7Sekunden!");
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Die Farmphase endet in §b" + f + " §7Sekunden!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Die Farmphase endet in §b" + f + " §7Sekunde!");
                        }
                        break;
                    case 0:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            s++;
                            
                            all.teleport(getLocations("Dorfplatz", s));
                        }
                        status = GameStatus.NOMOVE;
                        DisscusionCountdown();
                        cancel();
                        break;
                        
                    }
                }
		}.runTaskTimer(this, 0, 20);
	}
    
    public void DisscusionCountdown() {
	new BukkitRunnable() {
            
            int d = 31; 
            
	    @Override
	    public void run() {
                d--;
                    
                    switch(d) {
                    case 30:
                    case 10:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Die Diskusionsphase endet in §b" + d + " §7Sekunden!");
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Die Diskusionsphase endet in §b" + d + " §7Sekunden!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Die Diskusionsphase endet in §b" + d + " §7Sekunde!");
                        }
                        break;
                    case 0:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            
                            all.getInventory().setItem(0, getMethods().createItem(Material.PAPER, 1, 0, "§7↠ §cVoten", "§7↳ §8Rechtsklick auf die Köpfe zum Voten!"));
                        }
                        VoteCountdown();
                        cancel();
                        break;
                    }
            }
	}.runTaskTimer(this, 0, 20);
    }
    
    public void VoteCountdown() {
	new BukkitRunnable() {
            
            int v = 61;
            int s = 0;
            
	    @Override
	    public void run() {
                v--;
                    
                switch(v) {
                    case 60:
                    case 30:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            Bukkit.broadcastMessage(getInteract().getVotes(all) + ": VOTES1");
                        }
                    case 10:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Ihr könnt noch §b" + v + " §7Sekunden voten!");
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Ihr könnt noch §b" + v + " §7Sekunden voten!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Ihr könnt noch §b" + v + " §7Sekunde voten!");
                        }
                        break;
                    case 0:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.sendMessage(getInteract().getVotes(all) + ": VOTES");
                            all.sendMessage(interactmanager.getWinner() + "");
                            all.sendMessage(prefix + interactmanager.getWinner().getDisplayName() + " §7wurde angeklagt!");
                            
                            interactmanager.getWinner().teleport(getLocation("Death"));
                        }
                        status = GameStatus.NOMOVE;
                        //DefenseCountdown();
                        cancel();
                        break;
                }
                }
		}.runTaskTimer(this, 0, 20);
	}
    
    public void DefenseCountdown() {
	new BukkitRunnable() {
            
            int d = 21;
            
	    @Override
	    public void run() {
                d--;
                    
                switch(d) {
                    case 20:
                    case 10:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                        all.sendMessage(prefix + "§7Der Angeklagte hat noch §b" + d + " §7Sekunden Zeit um die Spieler umzustimmen!");
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                        all.sendMessage(prefix + "§7Der Angeklagte hat noch §b" + d + " §7Sekunden Zeit um die Spieler umzustimmen!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                        all.sendMessage(prefix + "§7Der Angeklagte hat noch §b" + d + " §7Sekunde Zeit um die Spieler umzustimmen!");
                        }
                        break;
                    case 0:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            
                            all.getInventory().setItem(0, getMethods().createItem(Material.BARRIER, 1, 0, "§cSchuldig", ""));
                            all.getInventory().setItem(2, getMethods().createItem(Material.EMERALD, 1, 0, "§aUnschuldig", ""));
                        }
                        status = GameStatus.NOMOVE;
                        EntscheidungsCountdown();
                        cancel();
                        break;
                    }
                }
		}.runTaskTimer(this, 0, 20);
	}
    
    public void EntscheidungsCountdown() {
	new BukkitRunnable() {
            
            int i = 21;
            
	    @Override
	    public void run() {
                i--;
                    
                switch(i) {
                    case 20:
                    case 10:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                        all.sendMessage(prefix + "§7Ihr könnt noch §b" + i + " §7Sekunden voten!");
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                        all.sendMessage(prefix + "§7Ihr könnt noch §b" + i + " §7Sekunden voten!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                        all.sendMessage(prefix + "§7Ihr könnt noch §b" + i + " §7Sekunde voten!");
                        }
                        break;
                    case 0:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.getInventory().remove(Material.PAPER);
                        }
                        if(interactmanager.getInnocent().size() >= interactmanager.getGuilty().size()) {
                            status = GameStatus.INGAME;
                            interactmanager.clearIG();
                            interactmanager.clearVP();
                            NightCountdown();
                        }else{
                            status = GameStatus.NOMOVE;
                            interactmanager.clearIG();
                            DeathCountdown();
                        }
                        cancel();
                        break;
                    }
                }
		}.runTaskTimer(this, 0, 20);
	}
    
    public void DeathCountdown() {
	new BukkitRunnable() {
            
            int d = 6;
            
	    @Override
	    public void run() {
                d--;
                    
                switch(d) {
                    case 5:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(prefix + "§7Irgendwelche letzten Worte?");
                        }
                        break;
                    case 0:
                        interactmanager.getWinner().setHealth(0);
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.sendMessage(prefix + interactmanager.getWinner().getDisplayName() + " §7war ein " + getMethods().getExactTeam(interactmanager.getWinner()));
                        }
                        status = GameStatus.INGAME;
                        cancel();
                        break;
                    }
                }
		}.runTaskTimer(this, 0, 20);
	}
    
}
