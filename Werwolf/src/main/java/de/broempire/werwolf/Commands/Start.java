/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Commands;

import de.broempire.werwolf.Werwolf;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class Start implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Werwolf.nospieler);
			return true;
		}
		
		Player p = (Player)sender;
		if(!p.hasPermission("BroEmpire.start")) {
			p.sendMessage(Werwolf.noPerms);
			return true;
		}
		
		if(args.length != 0) {
			p.sendMessage(Werwolf.prefix + "§cFehler! §9Benutze: /start");
			return true;
		}
                
		if(Bukkit.getOnlinePlayers().size() >= 1) {
		if(Werwolf.time > 11) {
                    
		    for(Player all : Bukkit.getOnlinePlayers()) {
			all.sendMessage(Werwolf.prefix + "§6Die Wartezeit wurde verkürzt!");
		    }
		    Werwolf.time = 11;
		}
                }
		return true;
	}
    
}
