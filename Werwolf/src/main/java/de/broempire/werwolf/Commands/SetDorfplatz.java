/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Commands;

import de.broempire.werwolf.Werwolf;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class SetDorfplatz implements CommandExecutor {
    
    private Werwolf plugin;
    
    public SetDorfplatz(Werwolf plugin) {
		this.plugin = plugin;
	}
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Werwolf.nospieler);
			return true;
		}
		Player p = (Player)sender;
		if(!p.hasPermission("Werwolf.setdorfplatz")) {
			p.sendMessage(Werwolf.noPerms);
		return true;
		}
		if(args.length != 1) {
			p.sendMessage(Werwolf.prefix + "§cFehler! §7Benutze: /setdorfplatz <Anzahl>");
			return true;
		}
		try{
	            int nummber = Integer.parseInt(args[0]);
		    plugin.getConfig().set("Locations.Dorfplatz." + nummber + ".World", p.getWorld().getName());
		    plugin.getConfig().set("Locations.Dorfplatz." + nummber + ".X", p.getLocation().getX());
		    plugin.getConfig().set("Locations.Dorfplatz." + nummber + ".Y", p.getLocation().getY());
		    plugin.getConfig().set("Locations.Dorfplatz." + nummber + ".Z", p.getLocation().getZ());
		    plugin.saveConfig();
	            if(plugin.getConfig().getString("Locations.Dorfplatz." + nummber + ".World") != null)
			p.sendMessage(Werwolf.prefix + "§7Du hast den " + nummber + ". §3Dorfplatz §7erfolgreich gesetzt");
		}catch(NumberFormatException e) {
			p.sendMessage(Werwolf.prefix + "§cBitte Zahl eingeben!");
		}
		return true;
	}
    
}
