/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Commands;

import de.broempire.werwolf.Werwolf;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class SetLobby implements CommandExecutor {
    
    private Werwolf plugin;
    
    public SetLobby(Werwolf plugin) {
		this.plugin = plugin;
	}
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Werwolf.nospieler);
			return true;
		}
		Player p = (Player)sender;
		if(!p.hasPermission("Werwolf.setlobby")) {
			p.sendMessage(Werwolf.noPerms);
			return true;
		}
		
		if(args.length != 0) {
			p.sendMessage(Werwolf.prefix + "§cFehler! §7Benutze: /setlobby");
			return true;
		}
		
		plugin.getConfig().set("Locations.Lobby.World", p.getWorld().getName());
		plugin.getConfig().set("Locations.Lobby.X", p.getLocation().getX());
		plugin.getConfig().set("Locations.Lobby.Y", p.getLocation().getY());
		plugin.getConfig().set("Locations.Lobby.Z", p.getLocation().getZ());
                plugin.getConfig().set("Locations.Lobby.Yaw", p.getLocation().getYaw());
                plugin.getConfig().set("Locations.Lobby.Pitch", p.getLocation().getPitch());
	        plugin.saveConfig();
		if(plugin.getConfig().getString("Locations.Lobby.World") != null) {
		   p.sendMessage(Werwolf.prefix + "§7Du hast die §3Lobby §7erfolgreich gesetzt");
                }
		return true;
  }
    
}
