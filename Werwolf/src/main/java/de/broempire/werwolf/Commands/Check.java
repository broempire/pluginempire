/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Commands;

import de.broempire.werwolf.Werwolf;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class Check implements CommandExecutor {
    
    private Werwolf plugin;
    
    public Check(Werwolf plugin) {
	this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
        Player p = (Player)sender;
        
        if(args.length != 0) {
	    p.sendMessage(Werwolf.prefix + "§cFehler! §7Benutze: /check");
	    return true;
	}
        for(Player all : Bukkit.getOnlinePlayers()) {
            p.sendMessage("§6Votes: §7" + plugin.getInteract().getVotes(all));
        }
        return true;
    }
    
}
