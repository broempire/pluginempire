/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Listener;

import de.broempire.werwolf.Utils.GameStatus;
import de.broempire.werwolf.Werwolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

/**
 *
 * @author Juli
 */
public class Listeners implements Listener {
    
    private Werwolf plugin;
    
    public Listeners(Werwolf plugin) {
        
	this.plugin = plugin;
    }
    
    @EventHandler
	public void onDamage(EntityDamageEvent e) {
		if(plugin.getStatus() != GameStatus.INGAME) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageByBlockEvent e) {
		if(plugin.getStatus() != GameStatus.INGAME) {
		   e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if(plugin.getStatus() != GameStatus.INGAME) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onFood(FoodLevelChangeEvent e) {
		if(plugin.getStatus() == GameStatus.INGAME || plugin.getStatus() == GameStatus.SCHUTZ) {
			e.setCancelled(false);
		}else {
			e.setCancelled(true);
		}
	}
    
}
