/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Listener;

import de.broempire.werwolf.Utils.GameStatus;
import de.broempire.werwolf.Werwolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 *
 * @author Juli
 */
public class MoveListener implements Listener {
    
    private Werwolf plugin;
    
    public MoveListener(Werwolf plugin) {
		this.plugin = plugin;
	}
    
    @EventHandler
	public void onMove(PlayerMoveEvent e) {
	    if(plugin.getStatus() == GameStatus.NOMOVE) {
		if(e.getFrom().getBlockX() != e.getTo().getBlockX() ||e.getFrom().getBlockZ() != e.getTo().getBlockZ()) {
                        
			e.setTo(e.getFrom());
		    }
		}
	}
    
}
