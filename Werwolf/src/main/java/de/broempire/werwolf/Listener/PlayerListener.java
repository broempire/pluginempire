/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Listener;

import de.broempire.werwolf.Utils.GameStatus;
import de.broempire.werwolf.Werwolf;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 *
 * @author Juli
 */
public class PlayerListener implements Listener {
    
    private Werwolf plugin;
    
    public PlayerListener(Werwolf plugin) {
		this.plugin = plugin;
	}
    
    @EventHandler
    public void JoinEvent (PlayerJoinEvent e) {
        
	if(plugin.getStatus() == GameStatus.LOBBY) {
	    Player p = e.getPlayer();    
            readyPlayer(p);
            
            if(!plugin.isCountdown()) {
                plugin.StartCountdown();
            }
            
            plugin.getVillager().add(p);
            Bukkit.broadcastMessage("Villager: " + plugin.getVillager() + " Werewolf: " + plugin.getWerewolf());
            
            p.getInventory().setItem(8, plugin.getMethods().createItem(Material.REDSTONE, 1, 0, "§7↠ §cSpiel verlassen", "§7↳ §8Rechtsklick zum Benutzen!"));
                    
            p.teleport(plugin.getLocation("Lobby"));
            e.setJoinMessage(Werwolf.prefix + "§8[§2+§8] §7" + p.getDisplayName());
	    
            plugin.getScoreboardManager().sendScoreboardLobby(p);
	}
    }
        
    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
	Player p = e.getPlayer();
	
        clearFromArray(p);
        
	e.setQuitMessage(Werwolf.prefix + "§8[§4-§8] §7" + p.getDisplayName());	
    }
        
    
    @EventHandler
    public void onLogin (PlayerLoginEvent e) {
		
	if(plugin.getStatus() != GameStatus.LOBBY) {
	    e.disallow(PlayerLoginEvent.Result.KICK_FULL, "§cDas Spiel hat schon gestartet!");
            
	    if(Bukkit.getOnlinePlayers().size() == Bukkit.getServer().getMaxPlayers()) {
                    
                e.setKickMessage(Werwolf.prefix + "Der Server ist voll!");
            }
	}
		
    }
        
    public void readyPlayer(Player p) {
        	
	p.setHealth(20D);
	p.setFoodLevel(20);
	p.getInventory().clear();
	p.getInventory().setArmorContents(null);
	p.setFireTicks(0);
	p.setAllowFlight(false);
	p.setFlying(false);
	p.setGameMode(GameMode.ADVENTURE);
	p.setExp(0F);
	p.setLevel(0);
	for(PotionEffect effect : p.getActivePotionEffects()) {
		p.removePotionEffect(effect.getType());
	}
		
    }
    
    public void clearFromArray(Player p) {
        plugin.getVillager().remove(p);
        plugin.getWerewolf().remove(p);
        
    }
}
