/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Listener;

import de.broempire.werwolf.MySQL.MySQLStats;
import de.broempire.werwolf.Utils.GameStatus;
import de.broempire.werwolf.Werwolf;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 *
 * @author Juli
 */
public class DeathListener implements Listener {
    
    private Werwolf plugin;
    private MySQLStats mySQLStats;
    
    public DeathListener(Werwolf plugin) {
        this.plugin = plugin;
        this.mySQLStats = plugin.getMySQLStats();
    }
    
    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        
        e.getDrops().clear();
        
        if(p.getKiller() instanceof Player) {
            Player k = p.getKiller();
            
            e.setDeathMessage(null);
            
            TryKickOut(p, k);
	    trySettingWinner();
        }else{
            e.setDeathMessage(null);
        }
    }
    
    public void TryKickOut(Player p, Player k) {
        
    }
    
    public void trySettingWinner() {
        if(plugin.getWerewolf().isEmpty() && plugin.getTown().size() >= 1) {
            for (Player all : Bukkit.getOnlinePlayers()) {
                plugin.status = GameStatus.RESTART;
                all.playSound(all.getLocation(), Sound.ENTITY_FIREWORK_LARGE_BLAST, 5F, 5F);
            }
            for(Player p : plugin.getTown()) {
            Bukkit.broadcastMessage(Werwolf.prefix + "Die §aStadt hat gewonnen!");
            Bukkit.broadcastMessage(Werwolf.prefix + "Folgende §aSpieler haben gewonnen: " + p.getDisplayName());
            }
        }
    }
}
