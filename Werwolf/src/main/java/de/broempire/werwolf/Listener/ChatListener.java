/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Listener;

import de.broempire.werwolf.Utils.GameStatus;
import de.broempire.werwolf.Werwolf;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 *
 * @author Juli
 */
public class ChatListener implements Listener {
    
    private Werwolf plugin;
    
    public ChatListener(Werwolf plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onChat (AsyncPlayerChatEvent e) {
        
        Player p = e.getPlayer();
        
        if(plugin.getStatus() == GameStatus.LOBBY || plugin.getStatus() == GameStatus.RESTART) {
            e.setFormat(p.getCustomName() + " §8>> §f"+ e.getMessage());
        }else{
            e.setFormat("§a" + p.getDisplayName() + " §8>> §f"+ e.getMessage());
        }
        
    }
    
}
