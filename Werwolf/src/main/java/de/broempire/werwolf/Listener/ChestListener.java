package de.broempire.werwolf.Listener;

import de.broempire.werwolf.Utils.GameStatus;
import de.broempire.werwolf.Werwolf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ChestListener implements Listener {
	
	private List<ItemStack> chestitems;
	private Werwolf plugin;
	public static HashMap<Location, Inventory> chest = new HashMap<Location, Inventory>();
	
	public ChestListener(Werwolf plugin) {
		this.plugin = plugin;
		chestitems = new ArrayList<ItemStack>();
		setChestItems();
	}
	
	@EventHandler
	public void onChestOpen(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Block block = e.getClickedBlock();
		
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(block.getType() == Material.OBSIDIAN) {
				if(plugin.getStatus() == GameStatus.INGAME || plugin.getStatus() == GameStatus.SCHUTZ) {
					if(!chest.containsKey(block.getLocation())) {
						Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST);
						Random random = new Random();
						int chestint = random.nextInt(3) + 2;
						while(chestint > 0) {
							chestint--;
							ItemStack itemstack = chestitems.get(random.nextInt(chestitems.size()));
							int slot = random.nextInt(27);
							inv.setItem(slot, itemstack);
						}
						chest.put(block.getLocation(), inv);
						p.openInventory(inv);
						
						return;
					}else{
						p.openInventory(chest.get(block.getLocation()));
							
						return;
					}
				}else{
					e.setCancelled(true);
					return;
				}
			}
		}
	}
	
	private void setChestItems() {
		
		chestitems.add(new ItemStack(Material.WHEAT));
		chestitems.add(new ItemStack(Material.SEEDS));
		chestitems.add(new ItemStack(Material.CARROT_ITEM));
		chestitems.add(new ItemStack(Material.POTATO_ITEM));
		chestitems.add(new ItemStack(Material.MELON_SEEDS));
		chestitems.add(new ItemStack(Material.STONE_HOE));
		chestitems.add(new ItemStack(Material.STONE_HOE));
		chestitems.add(new ItemStack(Material.STONE_HOE));
		chestitems.add(new ItemStack(Material.WHEAT));
		chestitems.add(new ItemStack(Material.WHEAT));
		chestitems.add(new ItemStack(Material.WHEAT));
		chestitems.add(new ItemStack(Material.SEEDS));
		chestitems.add(new ItemStack(Material.SEEDS));
		chestitems.add(new ItemStack(Material.WHEAT));
		chestitems.add(new ItemStack(Material.SEEDS));
		chestitems.add(new ItemStack(Material.CARROT_ITEM));
		chestitems.add(new ItemStack(Material.CARROT_ITEM));
		chestitems.add(new ItemStack(Material.POTATO_ITEM));
		chestitems.add(new ItemStack(Material.POTATO_ITEM));
		
	}

}
