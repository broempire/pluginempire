/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Listener;

import de.broempire.werwolf.Utils.Mapreset;
import de.broempire.werwolf.Werwolf;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

/**
 *
 * @author Juli
 */
public class BlockListener implements Listener {
    
    private Werwolf plugin;
    
    public BlockListener(Werwolf plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        
        Player p = e.getPlayer();
        Block b = e.getBlock();
        
        switch (b.getType()) {
            case CARROT:
            case POTATO:
            case MELON_BLOCK:
            case MELON_SEEDS:    
            case SEEDS:
                Mapreset.locations.add(b.getLocation());
                e.setCancelled(false);
                break;
        }
        if(p.getGameMode() == GameMode.CREATIVE) {
            e.setCancelled(false);
        }else{
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        
        Player p = e.getPlayer();
        Block b = e.getBlock();
        
        switch (b.getType()) {
            case CARROT:
            case POTATO:
            case MELON_BLOCK:
            case MELON_SEEDS:    
            case SEEDS:
                Mapreset.locations.add(b.getLocation());
                e.setCancelled(false);
                break;
        }
        if(p.getGameMode() == GameMode.CREATIVE) {
            e.setCancelled(false);
        }else{
            e.setCancelled(true);
        }
        
    }
    
}
