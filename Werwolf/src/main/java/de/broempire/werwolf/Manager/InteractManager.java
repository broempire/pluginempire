/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Manager;

import de.broempire.werwolf.Werwolf;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

/**
 *
 * @author Juli
 */
public class InteractManager implements Listener {
    
    private Werwolf plugin;
    private Player maxPlayer;
    private ArrayList<Player> innocent = new ArrayList<>();
    private ArrayList<Player> guilty = new ArrayList<>();
    private HashMap<Player, Integer> votes = new HashMap<>();
    private HashMap<Player, Player> players = new HashMap<>();

    public InteractManager(Werwolf plugin) {
        this.plugin = plugin;
        
    }
    
    public void addVote(Player p) {
        int current = votes.get(p);
        
        votes.replace(p, current + 1);
    }
    
    public void removeVote(Player p) {
        int current = votes.get(p);
        
        votes.replace(p, current - 1);
    }
    
    public Player getWinner() {
        int currentMax = 0;
        for(Player all : Bukkit.getOnlinePlayers()) {
        Bukkit.broadcastMessage("Vor der For-Schleife");
        Bukkit.broadcastMessage(votes.get(all) + " §aVotes");
        }
        for(Player p : votes.keySet()) {
            Bukkit.broadcastMessage("In der For-Schleife");
            Bukkit.broadcastMessage(p + " = votes.keySet");
            if(votes.get(p) > currentMax) {
                currentMax = votes.get(p);
                maxPlayer = p;
            }
        }
        return maxPlayer;
    }
    
    public void clearVP() {
        votes.clear();
        players.clear();
    }
    
    public void clearIG() {
        innocent.clear();
        guilty.clear();
    }
    
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
         
        Player p = e.getPlayer();
            
	if(e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR) {
            if(e.getItem() != null && e.getItem().getItemMeta() != null && e.getItem().getItemMeta().getDisplayName() != null) {
		if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7↠ §cVoten")) {
                    Inventory inv = Bukkit.createInventory(null, 54, "§7>> §cAbstimmung");
                    
			for(Player all : Bukkit.getOnlinePlayers()) {
			    if(all != p) {
                                if(votes.containsKey(all)) {
				    ItemStack item = new ItemStack(Material.SKULL_ITEM, votes.get(all), (short) 3);
                                    
                                    SkullMeta meta = (SkullMeta) item.getItemMeta();
				    meta.setDisplayName("§a" + all.getDisplayName());
                                
				    item.setItemMeta(meta);
                                    inv.addItem(item);
                                }else{
                                    ItemStack item = new ItemStack(Material.SKULL_ITEM, 0, (short) 3);
                                    SkullMeta meta = (SkullMeta) item.getItemMeta();
				meta.setDisplayName("§a" + all.getDisplayName());
                                
				item.setItemMeta(meta);
                                inv.addItem(item);
                                }
				}
			}
			p.openInventory(inv);
		    }else if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7↠ §cSpiel verlassen")) {
                        e.setCancelled(true);
                        /*try {
                                ByteArrayOutputStream b = new ByteArrayOutputStream();
                                DataOutputStream out = new DataOutputStream(b);
                                out.writeUTF("Connect");
                                out.writeUTF("lobby01");
                                p.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
                            }catch(IOException ex) {
                                Logger.getLogger(InteractManager.class.getName()).log(Level.SEVERE, null, ex);
                            }*/
                    }else if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cSchuldig")) {
                        if(!guilty.equals(p)) {
                            guilty.add(p);
                            p.sendMessage(Werwolf.prefix + "§7Du hast für §cSchuldig §7gestimmt!");
                        }else{
                            p.sendMessage(Werwolf.prefix + "§cDu hast bereits für §cSchuldig §7gestimmt!");
                        }
                    }else if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aUnschuldig")) {
                        if(!innocent.equals(p)) {
                            innocent.add(p);
                            p.sendMessage(Werwolf.prefix + "§7Du hast für §aUnschuldig §7gestimmt!");
                        }else{
                            p.sendMessage(Werwolf.prefix + "§cDu hast bereits für §aUnschuldig §7gestimmt!");
                        }
                    }
            }
            }
	}
    
    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if(e.getClickedInventory().getName().equalsIgnoreCase("§7>> §cAbstimmung")) {
            e.setCancelled(true);
            if(e.getSlot() == e.getRawSlot()) {
                if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
                    String playername = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
		    Player t = Bukkit.getPlayerExact(playername);
                    
                    if(t != null) {
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        if(!votes.containsKey(all)) {
                                votes.put(all, 0);
                                all.sendMessage(votes + "");
                            }
                        }
                        Bukkit.broadcastMessage("---------------Vor: addVote----------------");
                        Bukkit.broadcastMessage("§a" + p.getDisplayName() + "§7: Votes(Ich): " + votes.get(p) + " Votes(" + t.getDisplayName() + "): " + votes.get(t));
                        Bukkit.broadcastMessage("§a" + p.getDisplayName() + "§7: Players(Ich): " + players.get(p) + " Players(" + t.getDisplayName() + "): " + players.get(t));
                        Bukkit.broadcastMessage("---------------Vor: addVote----------------");
                        Bukkit.broadcastMessage(votes + ": VOTES");
                        if(players.containsKey(p)) {
                            if(!players.get(p).equals(t)) {
                                addVote(t);
                                removeVote(players.get(p));
                                p.sendMessage(Werwolf.prefix + "§7Du hast deinen Vote zu dem Spieler §a" + e.getCurrentItem().getItemMeta().getDisplayName() + " §7geändert!");
                                p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FLAP, 3, 3);
                                players.replace(p, t);
                            }else{
                                p.sendMessage(Werwolf.prefix + "§cDu hast schon für diesen Spieler gestimmt!");
                                p.playSound(p.getLocation(), Sound.ENTITY_VILLAGER_NO, 3, 3);
                            }
                        }else{
                            addVote(t);
                            p.sendMessage(Werwolf.prefix + "§7Du hast für den Spieler §a" + e.getCurrentItem().getItemMeta().getDisplayName() + " §7gevotet!");
                            p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FLAP, 3, 3);
                            players.put(p, t);
                        }
                        p.closeInventory();
                    }
                }
            }
        }
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (players.containsKey(p)) {
            removeVote(players.get(p));
        }
    }
    
    public Integer getVotes(Player p) {
        return votes.get(p);
    }
    
    public ArrayList<Player> getInnocent() {
        return innocent;
    }
    
    public ArrayList<Player> getGuilty() {
        return guilty;
    }
}
