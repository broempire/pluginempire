/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Manager;

import de.broempire.werwolf.APIs.ScoreboardTeam;
import de.broempire.werwolf.Werwolf;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class RoleManager {
    
    private Werwolf plugin;
    
    public RoleManager(Werwolf plugin) {
        this.plugin = plugin;
        
    }
    
    public void Werewolf(Player p) {
        
        plugin.getVillager().remove(p);
        plugin.getWerewolf().add(p);
        
        p.sendMessage(Werwolf.prefix + "§7Deine Rolle: §4Werwolf");
        
        plugin.getScoreboardManager().sendScoreboardWerewolf(p);
    }
    
    public void Villager(Player p) {
            
        p.sendMessage(Werwolf.prefix + "§7Deine Rolle: §aDorfbewohner");
            
        plugin.getScoreboardManager().sendScoreboardVillager(p);
    }
    
    public void Mayor(Player p) {
        
        plugin.getVillager().remove(p);
        plugin.getMayor().add(p);
        
        p.sendMessage(Werwolf.prefix + "§7Deine Rolle: §aBürgermeister");
        
        plugin.getScoreboardManager().sendScoreboardMayor(p);
    }
    
    public void Witch(Player p) {
        
        plugin.getVillager().remove(p);
        plugin.getWitch().add(p);
        
        p.sendMessage(Werwolf.prefix + "§7Deine Rolle: §5Hexe");
        
        plugin.getScoreboardManager().sendScoreboardWitch(p);
    }
    
    public void Executioner(Player p) {
        
        plugin.getVillager().remove(p);
        plugin.getExecutioner().add(p);
        
        p.sendMessage(Werwolf.prefix + "§7Deine Rolle: §8Hänker");
        
        plugin.getScoreboardManager().sendScoreboardExecutioner(p);
    }
    
    public void Jester(Player p) {
        
        plugin.getVillager().remove(p);
        plugin.getJester().add(p);
        
        p.sendMessage(Werwolf.prefix + "§7Deine Rolle: §dNarr");
        
        plugin.getScoreboardManager().sendScoreboardJester(p);
    }
    
    /*public void Jester() {
        final Random rd = new Random();
        
        Player p = plugin.getVillager().get(rd.nextInt(plugin.getVillager().size()));
        
        plugin.getVillager().remove(p);
    }
    
    public void Jester() {
        final Random rd = new Random();
        
        Player p = plugin.getVillager().get(rd.nextInt(plugin.getVillager().size()));
        
        plugin.getVillager().remove(p);
    }
    
    public void Jester() {
        final Random rd = new Random();
        
        Player p = plugin.getVillager().get(rd.nextInt(plugin.getVillager().size()));
        
        plugin.getVillager().remove(p);
    }
    */
}
