/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Manager;

import de.broempire.werwolf.Werwolf;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 *
 * @author Juli
 */
public class ScoreboardManager {
    
    private Werwolf plugin;
    
    public ScoreboardManager (Werwolf plugin) {
		this.plugin = plugin;
	}
    
    public void sendScoreboardLobby(Player p) {
        
        Scoreboard board = p.getScoreboard();
		
	Objective obj = board.getObjective("lobby") != null ? board.getObjective("lobby") : board.registerNewObjective("lobby", "dummy");
	obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	obj.setDisplayName("§8» §6§lWerwolf §8«");
        
        Team online = board.getTeam("online") != null ? board.getTeam("online") : board.registerNewTeam("online");
        online.setPrefix("§8» ");
        online.addEntry(ChatColor.YELLOW.toString());
        online.setSuffix(Bukkit.getOnlinePlayers().size() + "§7/§e" + plugin.getServer().getMaxPlayers() + " §7[4]");
        
        obj.getScore(" ").setScore(10);
        obj.getScore("§6Online:").setScore(9);
        obj.getScore(ChatColor.YELLOW.toString()).setScore(8);
        obj.getScore(" ").setScore(7);
        obj.getScore("§6Team:").setScore(6);
        obj.getScore(ChatColor.BLACK.toString()).setScore(5);
        obj.getScore(" ").setScore(4);
        obj.getScore("§6Map:").setScore(3);
        obj.getScore(ChatColor.BLUE.toString()).setScore(2);
        obj.getScore("§7-------------").setScore(1);
        obj.getScore("§7++ §6§lBroEmpire.de §7++").setScore(0);
        
        p.setScoreboard(board);
    }
    
    public void sendScoreboardWerewolf(Player p) {
        Scoreboard board = p.getScoreboard();
        
        if (board.getObjective("lobby") != null) {
            board.getObjective("lobby").unregister();
        }
        
        Objective obj = board.getObjective("werewolf") != null ? board.getObjective("werewolf") : board.registerNewObjective("werewolf", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	obj.setDisplayName("§8» §4Werwolf §8«");
        
        p.setScoreboard(board);
    }
    
    public void sendScoreboardVillager(Player p) {
        Scoreboard board = p.getScoreboard();
        
        if (board.getObjective("lobby") != null) {
            board.getObjective("lobby").unregister();
        }
        
        Objective obj = board.getObjective("villager") != null ? board.getObjective("villager") : board.registerNewObjective("villager", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName("§8» §6§lWerwolf §8«");
	//obj.setDisplayName("§8» §aDorfbewohner§7(§aVillager§7) §8«");
        
        obj.getScore(" ").setScore(10);
        obj.getScore("§n§6Orientierung: §r§aStadt").setScore(9);
        obj.getScore("§6Attribute:").setScore(8);
        obj.getScore(" ").setScore(7);
        obj.getScore(" ").setScore(6);
        obj.getScore(ChatColor.BLACK.toString()).setScore(5);
        obj.getScore(" ").setScore(4);
        obj.getScore("§6Map:").setScore(3);
        obj.getScore(ChatColor.BLUE.toString()).setScore(2);
        obj.getScore("§7-------------").setScore(1);
        obj.getScore(" ").setScore(0);
        
        /*obj.getScore("§n§eOrientierung: §r§aStadt").setScore(10);
        obj.getScore("§eAttribute:").setScore(9);
        obj.getScore("§8> §7Du bist ein ganz normaler Bewohner.").setScore(8);
        obj.getScore(" ").setScore(7);
        obj.getScore("§8> §7Sonst hast du keine speziellen Fähigkeiten.").setScore(6);
        obj.getScore(" ").setScore(5);
        obj.getScore(" ").setScore(4);
        obj.getScore(" ").setScore(3);
        obj.getScore(" ").setScore(2);
        obj.getScore(" ").setScore(1);
        obj.getScore("§6Ziel: §7Hänge/Töte alle Werwölfe und Bösewichte um zu gewinnen!").setScore(0);
        */
        p.setScoreboard(board);
    }
    
    public void sendScoreboardMayor(Player p) {
        Scoreboard board = p.getScoreboard();
        
        if (board.getObjective("lobby") != null) {
            board.getObjective("lobby").unregister();
        }
        
        Objective obj = board.getObjective("mayor") != null ? board.getObjective("mayor") : board.registerNewObjective("mayor", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	obj.setDisplayName("§8» §aBürgermeister §8«");
        
        p.setScoreboard(board);
    }
    
    public void sendScoreboardWitch(Player p) {
        Scoreboard board = p.getScoreboard();
        
        if (board.getObjective("lobby") != null) {
            board.getObjective("lobby").unregister();
        }
        
        Objective obj = board.getObjective("witch") != null ? board.getObjective("witch") : board.registerNewObjective("witch", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	obj.setDisplayName("§8» §5Hexe §8«");
        
        p.setScoreboard(board);
    }
    
    public void sendScoreboardExecutioner(Player p) {
        Scoreboard board = p.getScoreboard();
        
        if (board.getObjective("lobby") != null) {
            board.getObjective("lobby").unregister();
        }
        
        Objective obj = board.getObjective("executioner") != null ? board.getObjective("executioner") : board.registerNewObjective("executioner", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	obj.setDisplayName("§8» §7Hänker §8«");
        
        p.setScoreboard(board);
    }
    
    public void sendScoreboardJester(Player p) {
        Scoreboard board = p.getScoreboard();
        
        if (board.getObjective("lobby") != null) {
            board.getObjective("lobby").unregister();
        }
        
        Objective obj = board.getObjective("jester") != null ? board.getObjective("jester") : board.registerNewObjective("jester", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	obj.setDisplayName("§8» §dNarr§7(§dJester§7) §8«");
        
        obj.getScore("§n§eOrientierung: §r§7Neutral§f(§9Böse§f)").setScore(10);
        obj.getScore("§eAttribute:").setScore(9);
        obj.getScore("§f> Wenn du gehängt wirst, kannst du jemanden, der gegen dich gestimmt hat, in der nächsten Nacht töten.").setScore(8);
        obj.getScore(" ").setScore(7);
        obj.getScore(" ").setScore(6);
        
        p.setScoreboard(board);
    }
}
