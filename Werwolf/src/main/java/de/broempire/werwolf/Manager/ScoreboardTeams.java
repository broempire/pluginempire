/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Manager;

import de.broempire.werwolf.APIs.ScoreboardTeam;
import de.broempire.werwolf.Werwolf;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class ScoreboardTeams {
    
    private Werwolf plugin;
    
    public static Map<String, ScoreboardTeam> teams = new HashMap<>();
    
    public ScoreboardTeams(Werwolf plugin) {
        this.plugin = plugin;
    }
    
    public static void scoreboardteamsinit() {
        
        ScoreboardTeam admin = new ScoreboardTeam("00ADMIN");
        admin.setPrefix(Werwolf.teamadminprefix);
        teams.put("ADMIN", admin);
        ScoreboardTeam developer = new ScoreboardTeam("01DEVELOPER");
        developer.setPrefix(Werwolf.teamdeveloperprefix);
        teams.put("DEVELOPER", developer);
        ScoreboardTeam leaddeveloper = new ScoreboardTeam("02LEADDEVELOPER");
        leaddeveloper.setPrefix(Werwolf.teamleaddeveloperprefix);
        teams.put("LEADDEVELOPER", leaddeveloper);
        ScoreboardTeam moderator = new ScoreboardTeam("03MODERATOR");
        moderator.setPrefix(Werwolf.teammoderatorprefix);
        teams.put("MODERATOR", moderator);
        ScoreboardTeam jrmoderator = new ScoreboardTeam("04JRMODERATOR");
        jrmoderator.setPrefix(Werwolf.teamjrmoderatorprefix);
        teams.put("JRMODERATOR", jrmoderator);
        ScoreboardTeam leadmoderator = new ScoreboardTeam("05LEADMODERATOR");
        leadmoderator.setPrefix(Werwolf.teamleadmoderatorprefix);
        teams.put("LEADMODERATOR", leadmoderator);
        ScoreboardTeam forenmod = new ScoreboardTeam("06FORENMOD");
        forenmod.setPrefix(Werwolf.teamforenmodprefix);
        teams.put("FORENMOD", forenmod);
        ScoreboardTeam leadforenmod = new ScoreboardTeam("07LEADFORENMOD");
        leadforenmod.setPrefix(Werwolf.teamleadforenmodprefix);
        teams.put("LEADFORENMOD", leadforenmod);
        ScoreboardTeam builder = new ScoreboardTeam("08BUILDER");
        builder.setPrefix(Werwolf.teambuilderprefix);
        teams.put("BUILDER", builder);
        ScoreboardTeam leadbuilder = new ScoreboardTeam("09LEADBUILDER");
        leadbuilder.setPrefix(Werwolf.teamleadbuilderprefix);
        teams.put("LEADBUILDER", leadbuilder);
        ScoreboardTeam vip = new ScoreboardTeam("10VIP");
        vip.setPrefix(Werwolf.teamvipprefix);
        teams.put("VIP", vip);
        ScoreboardTeam premium = new ScoreboardTeam("11PREMIUM");
        premium.setPrefix(Werwolf.teampremiumprefix);
        teams.put("PREMIUM", premium);
        ScoreboardTeam player = new ScoreboardTeam("12PLAYER");
        player.setPrefix(Werwolf.teamplayerprefix);
        teams.put("PLAYER", player);
        
        ScoreboardTeam werewolf = new ScoreboardTeam("twerewolf");
        werewolf.setPrefix("§4");
        teams.put("Werwolf", werewolf);
        
        ScoreboardTeam villiger = new ScoreboardTeam("tvilliger");
        villiger.setPrefix("§a");
        teams.put("Villiger", villiger);
        
        ScoreboardTeam mayor = new ScoreboardTeam("tmayor");
        mayor.setPrefix("§a");
        teams.put("Mayor", mayor);
        
        ScoreboardTeam witch = new ScoreboardTeam("twitch");
        witch.setPrefix("§5");
        teams.put("Witch", witch);
        
        ScoreboardTeam executioner = new ScoreboardTeam("texecutioner");
        executioner.setPrefix("§8");
        teams.put("Executioner", executioner);
        
        ScoreboardTeam jester = new ScoreboardTeam("tjester");
        jester.setPrefix("§d");
        teams.put("Jester", jester);
        
        /*ScoreboardTeam witch = new ScoreboardTeam("twitch");
        witch.setPrefix("§5");
        teams.put("Witch", witch);*/
        
        Bukkit.broadcastMessage("§cScoreboard COMLETE");
    }
    
    public void updatePlayer(Player p) {
        ScoreboardTeam team;
        if(plugin.getMethods().hasTeam(p)) {
            team = teams.get(plugin.getMethods().getExactTeam(p));
            
            p.setPlayerListName(team.getPrefix() + p.getDisplayName());
            p.setDisplayName(team.getPrefix() + p.getDisplayName());
            p.setCustomName(plugin.getMethods().getColor(p) + p.getName());
        }else{
            team = teams.get(plugin.getBro().getGroup(p.getUniqueId().toString()));
            
            p.setPlayerListName(team.getPrefix() + p.getDisplayName());
            p.setDisplayName(team.getPrefix()+ p.getDisplayName());
            p.setCustomName(team.getPrefix() + p.getDisplayName());
        }
    }
}
