/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.werwolf.Utils;

import de.broempire.werwolf.Werwolf;
import java.util.ArrayList;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author Juli
 */
public class Methods {
    
    private Werwolf plugin;
    
    public Methods(Werwolf plugin) {
        this.plugin = plugin;
    }
    
    public ItemStack createItem(Material mat, int amount, int shortid, String DisplayName, String ... lore) {
        ArrayList<String> alore= new ArrayList<>();
        for(String line : lore) {
            alore.add(line);
        }
        short s =  (short) shortid;
	ItemStack i = new ItemStack(mat, amount,s);
        ItemMeta meta = i.getItemMeta();
	meta.setDisplayName(DisplayName);
        meta.setLore(alore);
	i.setItemMeta(meta);
	return i;
    }
    
    public boolean hasTeam(Player p) {

        boolean b;

        if(plugin.getWerewolf().contains(p)) {
            b = true;
            
        }else if(plugin.getVillager().contains(p)) {
            b = true;
            
        }else if(plugin.getMayor().contains(p)) {
            b = true;
            
        }else if(plugin.getWitch().contains(p)) {
            b = true;
        }else{
            b = false;
        }
        return b;
    }
    
    public String getExactTeam (Player p) {

            String s;

            if(plugin.getWerewolf().contains(p)) {
                s = "Werwolf";
                
            }else if(plugin.getVillager().contains(p)) {
                s = "Villiger";
                
            }else if(plugin.getMayor().contains(p)) {
                s = "Mayor";
                
            }else if(plugin.getWitch().contains(p)) {
                s = "Witch";
                
            }else{
                s = "Kein Team";
            }
            return s;
    }
    
    public String getColor(Player p) {

            String s;

            if (plugin.getWerewolf().contains(p)) {
                s = "§c";
                
            }else if (plugin.getVillager().contains(p)) {
                s = "§a";
                
            }else if (plugin.getMayor().contains(p)) {
                s = "§a";
                
            }else if (plugin.getWitch().contains(p)) {
                s = "§5";
                
            }else{
                s = "§a";
            }
            return s;

    }
}
