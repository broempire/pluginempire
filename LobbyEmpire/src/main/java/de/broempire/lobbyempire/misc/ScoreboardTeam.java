/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.misc;

import com.google.common.collect.Lists;
import java.util.Collection;

/**
 *
 * @author Matti
 */
public class ScoreboardTeam {

    private final String name;
    private String prefix;
    private String suffix;
    private Collection<String> players;
    
    public ScoreboardTeam(String name) {
        this.name = name;
        this.prefix = "";
        this.suffix = "";
        this.players = Lists.newArrayList();
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }
    
    public Collection<String> getPlayerNameSet() {
        return players;
    }

    public String getName() {
        return name;
    }

    public void setPlayerNameSet(Collection<String> players) {
        this.players = players;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
    
    
}
