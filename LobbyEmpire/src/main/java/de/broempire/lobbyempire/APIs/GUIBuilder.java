/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.APIs;

import de.broempire.lobbyempire.main;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Matti
 */
public class GUIBuilder implements Listener{

    private final Inventory inv;
    private ArrayList<ItemBuilder> items = new ArrayList<>();
    
    public GUIBuilder(main plugin, int size, String name) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.inv = Bukkit.createInventory(null, size, name);
    }
    
    public GUIBuilder addItem(int slot, ItemStack is) {
        inv.setItem(slot, is);
        return this;
    }
    public GUIBuilder addGUIItem(int slot, ItemBuilder item) {
        items.add(item);
        inv.setItem(slot, item.build());
        return this;
    }
    
    public Inventory open(Player p) {
        p.openInventory(inv);
        return inv;
    }
    public GUIBuilder getInventory() {
        return this;
    }
    public ArrayList<ItemBuilder> getItems() {
        return items;
    }
    @EventHandler
    public void onClickItem(InventoryClickEvent e) {
        if (e.getWhoClicked().getOpenInventory().getTopInventory().equals(inv)) {
            e.setCancelled(true);
            try {
                items.stream().filter((item) -> (e.getCurrentItem().getItemMeta().equals(item.getItemMeta()))).forEach((item) -> {
                    item.click((Player)e.getWhoClicked());
                });
            } catch (Exception ex) {
                
            }
        }
    }
    
    
    
}
