/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire;

import de.broempire.lobbyempire.events.LobbyItems;
import de.broempire.lobbyempire.events.PlayerListeners;
import de.broempire.lobbyempire.manager.ScoreboardTeams;
import de.broempire.lobbyempire.MySQL.MySQL;
import de.broempire.lobbyempire.MySQL.Bro;
import de.broempire.lobbyempire.commands.BuildmodeCommand;
import de.broempire.lobbyempire.commands.LocationCommand;
import de.broempire.lobbyempire.commands.SpawnCommand;
import de.broempire.lobbyempire.commands.WorldCommand;
import de.broempire.lobbyempire.manager.GroupManager;
import de.broempire.lobbyempire.manager.ScoreboardManager;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Matti
 */
public class main extends JavaPlugin {
    MySQL mySQL;
    Bro bro;
    ScoreboardTeams scoreboardTeams;
    ScoreboardManager scoreboardManager;
    GroupManager groupManager;
    
    private final List<Player> silent = new ArrayList<>();
    
    main plugin;
    
    public static String prefix = "§8▎ §6BroEmpire§8 ❘ §7";
    public static String noperm = "§8▎ §6BroEmpire§8 ❘ §cDu besitzt nicht die nötigen Rechte für diesen Befehl!";
    
    public static String teamplayerprefix;
    public static String teampremiumprefix;
    public static String teamvipprefix;
    public static String teambuilderprefix;
    public static String teamleadbuilderprefix;
    public static String teammoderatorprefix;
    public static String teamjrmoderatorprefix;
    public static String teamleadmoderatorprefix;
    public static String teamforenmodprefix;
    public static String teamleadforenmodprefix;
    public static String teamdeveloperprefix;
    public static String teamleaddeveloperprefix;
    public static String teamadminprefix;
    
    String database;
    String username;
    String password;
    String host;

    @Override
    public void onEnable() {
        plugin = this;
        loadConfig();
        scoreboardTeams = new ScoreboardTeams(this);
        scoreboardManager = new ScoreboardManager(this);
        groupManager = new GroupManager(this);
        init();
        
        System.out.println("[" + this.getName() + "] Plugin wurde aktiviert!");
    }
    public void init() {
        
        Bukkit.getPluginManager().registerEvents(new PlayerListeners(this), this);
        Bukkit.getPluginManager().registerEvents(new LobbyItems(this), this);
        
        this.getCommand("setlocation").setExecutor(new LocationCommand(this));
        this.getCommand("buildmode").setExecutor(new BuildmodeCommand());
        this.getCommand("spawn").setExecutor(new SpawnCommand(this));
        this.getCommand("world").setExecutor(new WorldCommand(this));
        
        loadConfigStrings();
        
        ScoreboardTeams.scoreboardteamsinit();
        
        for (World w : Bukkit.getWorlds()) {
            w.setThundering(false);
            w.setStorm(false);
            w.setTime(6000L);
        }
        
        mySQL = new MySQL(host, database, username, password);
        
        bro = new Bro(this, mySQL);
        
        this.getScoreboardTeams().renewScoreboard();
        
        Bukkit.getOnlinePlayers().stream().forEach((p) -> {
            this.getScoreboardManager().updateScoreboard(p);
        });
    }
        
    public void loadConfig() {
        FileConfiguration cfg = this.getConfig();
        
        cfg.options().copyDefaults(true);
        
        cfg.addDefault("MySQL.Hostname", "Hostname");
        cfg.addDefault("MySQL.Username", "Username");
        cfg.addDefault("MySQL.Database", "Database");
        cfg.addDefault("MySQL.Password", "Password");
        
        cfg.addDefault("Prefix.Player", "§eSpieler §7| ");
        cfg.addDefault("Prefix.Premium", "§6Premium §7| ");
        cfg.addDefault("Prefix.VIP", "§dVIP §7| ");
        cfg.addDefault("Prefix.Builder", "§9Build §7| ");
        cfg.addDefault("Prefix.LeadBuilder", "§9LeadBuild §7| ");
        cfg.addDefault("Prefix.Moderator", "§cMod §7| ");
        cfg.addDefault("Prefix.JrModerator", "§cJrMod §7| ");
        cfg.addDefault("Prefix.LeadModerator", "§cLeadMod §7| ");
        cfg.addDefault("Prefix.Forenmod", "§2F-Mod §7| ");
        cfg.addDefault("Prefix.LeadForenmod", "§2LeadF-Mod §7| ");
        cfg.addDefault("Prefix.Developer", "§bDev §7| ");
        cfg.addDefault("Prefix.LeadDeveloper", "§bLeadDev §7| ");
        cfg.addDefault("Prefix.Admin", "§4Admin §7| ");
        
        saveConfig();
    }

    @Override
    public void onDisable() {
        plugin = null;
        System.out.println("[" + this.getName() + "] Plugin wurde deaktiviert!");
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public ScoreboardTeams getScoreboardTeams() {
        return scoreboardTeams;
    }

    public ScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }
    
    public boolean isSilentLobby(Player p) {
        return silent.contains(p);
    }
    
    public void setSilentLobby(Player p) {
        silent.add(p);
    }
    
    public void removeSilentLobby(Player p) {
        silent.remove(p);
    }
    
    public void loadConfigStrings() {
        database = this.getConfig().getString("MySQL.Database");
        username = this.getConfig().getString("MySQL.Username");
        password = this.getConfig().getString("MySQL.Password");
        host = this.getConfig().getString("MySQL.Hostname");
        
        teamplayerprefix = this.getConfig().getString("Prefix.Player");
        teampremiumprefix = this.getConfig().getString("Prefix.Premium");
        teamvipprefix = this.getConfig().getString("Prefix.VIP");
        teambuilderprefix = this.getConfig().getString("Prefix.Builder");
        teamleadbuilderprefix = this.getConfig().getString("Prefix.LeadBuilder");
        teammoderatorprefix = this.getConfig().getString("Prefix.Moderator");
        teamjrmoderatorprefix = this.getConfig().getString("Prefix.JrModerator");
        teamleadmoderatorprefix = this.getConfig().getString("Prefix.LeadModerator");
        teamforenmodprefix = this.getConfig().getString("Prefix.Forenmod");
        teamleadforenmodprefix = this.getConfig().getString("Prefix.LeadForenmod");
        teamdeveloperprefix = this.getConfig().getString("Prefix.Developer");
        teamleaddeveloperprefix = this.getConfig().getString("Prefix.LeadDeveloper");
        teamadminprefix = this.getConfig().getString("Prefix.Admin");
    }
    public void setLocation(Location loc, String path) {
        this.getConfig().set("Locations." + path + ".world", loc.getWorld().getName());
        this.getConfig().set("Locations." + path + ".x", loc.getX());
        this.getConfig().set("Locations." + path + ".y", loc.getY() + 1);
        this.getConfig().set("Locations." + path + ".z", loc.getZ());
        this.getConfig().set("Locations." + path + ".yaw", loc.getYaw());
        this.getConfig().set("Locations." + path + ".pitch", loc.getPitch());
        
        this.saveConfig();
    }
    public Location getLocation(String path) {
        String world = this.getConfig().getString("Locations." + path + ".world");
        double x = this.getConfig().getDouble("Locations." + path + ".x");
        double y = this.getConfig().getDouble("Locations." + path + ".y");
        double z = this.getConfig().getDouble("Locations." + path + ".z");
        float yaw = this.getConfig().getInt("Locations." + path + ".yaw");
        float pitch = this.getConfig().getInt("Locations." + path + ".pitch");
        
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }

    public Bro getBro() {
        return bro;
    }

    public MySQL getMySQL() {
        return mySQL;
    }

    public GroupManager getGroupManager() {
        return groupManager;
    }
    
    public static void sendUsage(String usage, String example, Player p) {
        p.sendMessage("§8§m          §r§7»§6Benutzung§7«§8§m         -");
        p.sendMessage(" ");
        p.sendMessage("§4" + usage);
        if (example != null) {
            p.sendMessage("§2Beispiel§7: §6" + example);
        }
        p.sendMessage(" ");
        p.sendMessage("§8§m          §r§7»§6Benutzung§7«§8§m         -");
    }   
}
