/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.manager;

import de.broempire.lobbyempire.main;
import java.util.concurrent.TimeUnit;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 *
 * @author Matti
 */
public class ScoreboardManager {
    
    main plugin;
    
    public ScoreboardManager(main plugin) {
        this.plugin = plugin;
    }
    
    public void sendScoreboard(Player p) {
        Scoreboard s = Bukkit.getScoreboardManager().getNewScoreboard();
        
        Objective obj = s.getObjective("Lobby") != null ? s.getObjective("Lobby") : s.registerNewObjective("Lobby", "dummy");
        //Objective bearbeiten - Sidebar setzen und Displayname verändern
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName("§7↠ " + p.getCustomName());
        //Bearbeitbare Teams hinzufügen
        Team group = s.getTeam("group") != null ? s.getTeam("group") : s.registerNewTeam("group");
        group.setPrefix("§8§l↳ ");
        group.addEntry(ChatColor.GREEN.toString());
        group.setSuffix(plugin.getGroupManager().getGroup(p).getName());
        
        Team coins = s.getTeam("coins") != null ? s.getTeam("coins") : s.registerNewTeam("coins");
        coins.setPrefix("§8§l↳ ");
        coins.addEntry(ChatColor.AQUA.toString());
        coins.setSuffix(plugin.getBro().getCoins(p.getUniqueId().toString()) + "");
        
        Team onlinetime = s.getTeam("online") != null ? s.getTeam("online") : s.registerNewTeam("online");
        onlinetime.setPrefix("§8§l↳ ");
        onlinetime.addEntry(ChatColor.BLACK.toString());
        onlinetime.setSuffix("§b" + TimeUnit.MILLISECONDS.toHours(plugin.getBro().getOnlineTime(p.getUniqueId().toString())) + " Stunde(n)");
        
        //Scores im Scoreboard setzen
        obj.getScore("§7§m---------------").setScore(13);
        obj.getScore("§9Rang:").setScore(12);
        obj.getScore(ChatColor.GREEN.toString()).setScore(11);
        obj.getScore(" ").setScore(10);
        obj.getScore("§9Brollar:").setScore(9);
        obj.getScore(ChatColor.AQUA.toString()).setScore(8);
        obj.getScore("  ").setScore(7);
        obj.getScore("§9Onlinezeit:").setScore(6);
        obj.getScore(ChatColor.BLACK.toString()).setScore(5);
        obj.getScore("   ").setScore(4);
        obj.getScore("§9Teamspeak:").setScore(3);
        obj.getScore("§8§l↳ §bBroEmpire.de").setScore(2);
        obj.getScore("§7§m---------------§r ").setScore(1);
        obj.getScore("§6§lBroEmpire.de").setScore(0);

        //Spieler das Scoreboard setzen
        p.setScoreboard(s);
        
    }
    public void updateScoreboard(Player p) {
        //Scoreboard des Spielers getten + das zu bearbeitende Objective
        Scoreboard s = p.getScoreboard();
        Objective obj = s.getObjective("Lobby");
        //DisplayName des Scoreboards verändern
        obj.setDisplayName("§7↠ " + p.getCustomName());
        //Suffixe der Teams ändern -> verändern sich auch im Scoreboard
        s.getTeam("group").setSuffix(plugin.getGroupManager().getGroup(p).getName());
        s.getTeam("coins").setSuffix(plugin.getBro().getCoins(p.getUniqueId().toString()) + "");
        s.getTeam("online").setSuffix("§b" + TimeUnit.MILLISECONDS.toHours(plugin.getBro().getOnlineTime(p.getUniqueId().toString())) + " Stunde(n)");
    }
}
