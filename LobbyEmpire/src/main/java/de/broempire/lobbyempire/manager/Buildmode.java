/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.DisplaySlot;

/**
 *
 * @author Fabian
 */
public class Buildmode {

    public static List<String> buildmodelist = new ArrayList<>();
    private static final HashMap<String, ItemStack[]> inv = new HashMap<>();

    public static String notonline = "§cDieser Spieler ist momentan nicht online oder er existiert nicht!";

    public static String isbuildmodeon = "§7Der Spieler [Player] §7ist nun im §aBuildmode§7!";
    public static String isbuildmodeoff = "§7Der Spieler [Player] §7ist nun nicht mehr im §cBuildmode§7!";

    public static void setBuildmodeTrue(Player p) {
        buildmodelist.add(p.getName());
        p.setGameMode(GameMode.CREATIVE);
        inv.put(p.getName(), p.getInventory().getContents());
        p.getInventory().clear();
        p.setFlying(true);
    }

    public static void setBuildmodeFalse(Player p) {
        buildmodelist.remove(p.getName());
        p.setGameMode(GameMode.ADVENTURE);
        p.setFlying(false);
        p.setAllowFlight(true);
        p.getInventory().setContents(inv.get(p.getName()));
    }

    public static boolean isBuildmode(Player p) {
        return buildmodelist.contains(p.getName());
    }
}
