/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.events;

import com.nametagedit.plugin.NametagEdit;
import de.broempire.lobbyempire.APIs.ActionAPI;
import de.broempire.lobbyempire.APIs.ItemBuilder;
import de.broempire.lobbyempire.main;
import static de.broempire.lobbyempire.manager.Buildmode.isBuildmode;
import static de.broempire.lobbyempire.manager.Buildmode.setBuildmodeFalse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.Vector;

/**
 *
 * @author Matti
 */
public class PlayerListeners implements Listener {
    
    main plugin;
    
    private HashMap<Player, Integer> timer = new HashMap<>();

    public PlayerListeners(main plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onJoin (PlayerJoinEvent e) {
        Player p = e.getPlayer();
        
        p.teleport(plugin.getLocation("SPAWN"));
        
        e.setJoinMessage(null);
        
        p.setHealthScale(2D);
        p.setMaxHealth(2D);
        p.setHealth(2D);
        p.setFoodLevel(20);
        
        p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(100D);
        
        ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
        SkullMeta im = (SkullMeta) is.getItemMeta();
        im.setOwner(p.getName());
        im.setDisplayName("§7↠ §bProfil");
        List<String> lore = new ArrayList<>();
        lore.add("§7↳ §8Rechtsklick zum Benutzen!");
        im.setLore(lore);
        is.setItemMeta(im);
        
        p.getInventory().clear();
        p.getInventory().setItem(0, new ItemBuilder(Material.COMPASS).setDisplayName("§7↠ §6Minigames").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
        p.getInventory().setItem(1, new ItemBuilder(Material.MAP).setDisplayName("§7↠ §2Map").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
        p.getInventory().setItem(3, new ItemBuilder(Material.BLAZE_ROD).setDisplayName("§7↠ §eSpielersichtbarkeit").setLore("§7↳ §8Rechtsklick zum Benutzen!", " ", "§7↠ §6Möglichkeiten:", " ","§7- §6Alle Spieler unsichtbar", "§7- §6Nur Teammitglieder & Freunde sichtbar", "§7- §6Alle Spieler sichtbar").build());
        //p.getInventory().setItem(3, new ItemBuilder(Material.CHEST).setDisplayName("§7↠ §aShop").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
        p.getInventory().setItem(5, is);
        
        p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
        
        p.setExp(0.00F);
        p.setLevel(0);
        
        p.setGameMode(GameMode.ADVENTURE);
        
        LivingEntity le = (LivingEntity) p;
        le.setCollidable(false);
        
        if (!plugin.getBro().getGroup(p.getUniqueId().toString()).equals("PLAYER")) {
            if (!plugin.getBro().getGroup(p.getUniqueId().toString()).equals("PREMIUM")) {
                p.getInventory().setItem(7, new ItemBuilder(Material.TNT).setDisplayName("§7↠ §cSilentLobby").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
            }
            if (plugin.getBro().getGroup(p.getUniqueId().toString()).equals("ADMIN")) {
                p.setOp(true);
            } else {
                p.setOp(false);
            }
            p.setExp(0.99F);
            p.setAllowFlight(true);
        }
        //Scoreboard setzen - Spieler
        plugin.getScoreboardTeams().updatePlayer(p);
        
        //Sidebar Scoreboard setzen
        plugin.getScoreboardManager().sendScoreboard(p);
        
        p.getInventory().setItem(8, getBook(p));
        
    }
    @EventHandler
    public void onQuit (PlayerQuitEvent e) {
        if (isBuildmode(e.getPlayer())) {
            setBuildmodeFalse(e.getPlayer());
        }
        NametagEdit.getApi().clearNametag(e.getPlayer());
        e.setQuitMessage(null);
    }
    
    @EventHandler
    public void onWeatherChange(WeatherChangeEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onCreatureSpawn(EntitySpawnEvent e) {
        e.setCancelled(true);
    }
    @EventHandler
    public void onChatEvent(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        if (plugin.isSilentLobby(p)) {
            e.setCancelled(true);
            p.sendMessage(main.prefix + "§cIn der SilentLobby kannst du keine Nachrichten versenden/bekommen!");
        }
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            if (plugin.isSilentLobby(player)) {
                e.getRecipients().remove(player);
            }
        } 
        e.setFormat(p.getDisplayName() + " §8» §r" + e.getMessage());
    }
    @EventHandler 
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }
    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (isBuildmode(e.getPlayer())) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void onBlockPlace (BlockPlaceEvent e) {
        if (isBuildmode(e.getPlayer())) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        e.blockList().clear();
    }
    
    @EventHandler 
    public void onToggleFlight(PlayerToggleFlightEvent e) {
        Player p = e.getPlayer();
        if (p.getGameMode().equals(GameMode.ADVENTURE) || p.getGameMode().equals(GameMode.SURVIVAL)) {
            e.setCancelled(true);
            if (p.getExp() >= 0.99F) {
                Vector v = p.getLocation().getDirection().multiply(3D).setY(1D);
                p.setVelocity(v);
                p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FLAP, 3, 3);
                p.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 10);
                p.setExp(0F);
                p.setAllowFlight(false);
                timer.put(p, Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        p.setExp(p.getExp() + 0.01F);
                        if (p.getExp() >= 0.99F) {
                            Bukkit.getScheduler().cancelTask(timer.get(p));
                            timer.remove(p);
                            p.setAllowFlight(true);
                            ActionAPI.sendActionBar(p, "§aDu kannst den Doppelsprung nun wieder nutzen!");
                        }
                    }
                }, 1, 1));
            }
        }
    }
    
    private ItemStack getBook(Player p) {
        ItemStack is = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta im = (BookMeta) is.getItemMeta();
        im.setAuthor("BroEmpire");
        im.setDisplayName("§7↠ §9BroEmpire Handbuch");
        im.setTitle("§7↠ §9BroEmpire Handbuch");
        im.addPage("§2Willkommen " + p.getCustomName() + "§2!\n\n§aViel Spaß auf unserem Netzwerk. \n\n§7Dies ist ein §6kleines Handbuch §7mit wichtigen Befehlen, Regeln usw.!");
        im.addPage("§aFortsetzung folgt...");
        is.setItemMeta(im);
        return is;
    }
}
