/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.events;

import de.broempire.lobbyempire.APIs.ActionAPI;
import de.broempire.lobbyempire.APIs.GUIBuilder;
import de.broempire.lobbyempire.APIs.ItemBuilder;
import de.broempire.lobbyempire.main;
import static de.broempire.lobbyempire.manager.Buildmode.isBuildmode;
import de.broempire.lobbyempire.misc.VanishType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author Matti
 */
public class LobbyItems implements Listener{
    
    private final GUIBuilder teleporter;
    private final GUIBuilder hidePlayers;
    private final GUIBuilder mapMountainTeleporter;
    private final GUIBuilder mapSeaportTeleporter;
    private final main plugin;
    
    private HashMap<Player, VanishType> vanishlist= new HashMap<>();
    
    private final List<Player> cooldownSilentLobby = new ArrayList<>();
    
    private Consumer<Player> spawnCallback;
    private Consumer<Player> buildCallback;
    private Consumer<Player> virusCallback;
    private Consumer<Player> zombieescapeCallback;
    private Consumer<Player> werwolfCallback;

    private Consumer<Player> vanishShowAllCallback;
    private Consumer<Player> vanishTeamCallback;
    private Consumer<Player> vanishAllCallback;
    
    private Consumer<Player> mountainOpenCallback;
    private Consumer<Player> seaportOpenCallback;
    
    public LobbyItems (main plugin) {
        initCallback();
        this.plugin = plugin;
        this.teleporter = new GUIBuilder(plugin, 27, "§7↠ §6Minigames");
        for (int i = 0; i < 27; i++) {
            this.teleporter.addItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short)0).setDisplayName(" ").build());
        }
        this.teleporter.addGUIItem(13, new ItemBuilder(Material.PRISMARINE_CRYSTALS, spawnCallback).setDisplayName("§5» Spawn «"))
                       .addGUIItem(3, new ItemBuilder(Material.BARRIER, buildCallback).setDisplayName("§6» Folgt... «"))
                       .addGUIItem(23, new ItemBuilder(Material.SPIDER_EYE, virusCallback).setDisplayName("§c» Virus «"))
                       .addGUIItem(5, new ItemBuilder(Material.BARRIER, zombieescapeCallback).setDisplayName("§2» Folgt... «"))
                       .addGUIItem(21, new ItemBuilder(Material.BONE, werwolfCallback).setDisplayName("§4» Werwolf «"))
//                       .addGUIItem(10, new ItemBuilder(Material.IRON_ORE, mountainOpenCallback).setDisplayName("§e» Mountain «"))
//                       .addGUIItem(16, new ItemBuilder(Material.BIRCH_FENCE_GATE, seaportOpenCallback).setDisplayName("§e» Seaport «"))
                       .addItem(4, new ItemBuilder(Material.GOLD_NUGGET).setDisplayName(" ").build())
                       .addItem(22, new ItemBuilder(Material.GOLD_NUGGET).setDisplayName(" ").build())
                       .addItem(12, new ItemBuilder(Material.GOLD_NUGGET).setDisplayName(" ").build())
                       .addItem(14, new ItemBuilder(Material.GOLD_NUGGET).setDisplayName(" ").build())
                       .addItem(11, new ItemBuilder(Material.GOLD_NUGGET).setDisplayName(" ").build())
                       .addItem(9, new ItemBuilder(Material.GOLD_NUGGET).setDisplayName(" ").build())
                       .addItem(15, new ItemBuilder(Material.GOLD_NUGGET).setDisplayName(" ").build())
                       .addItem(17, new ItemBuilder(Material.GOLD_NUGGET).setDisplayName(" ").build());
        this.hidePlayers = new GUIBuilder(plugin, 9, "§7↠ §eSpielersichtbarkeit")
            .addGUIItem(2, new ItemBuilder(Material.GLASS, vanishShowAllCallback).setDisplayName("§a» Alle anzeigen «"))
            .addGUIItem(4, new ItemBuilder(Material.GHAST_TEAR, vanishTeamCallback).setDisplayName("§e» Nur Freunde & Teammitglieder anzeigen «"))
            .addGUIItem(6, new ItemBuilder(Material.REDSTONE, vanishAllCallback).setDisplayName("§4» Alle verstecken «"));
        this.mapMountainTeleporter = new GUIBuilder(plugin, 9, "§7↠ Mountain");
        this.mapSeaportTeleporter = new GUIBuilder(plugin, 9, "§7↠ Seaport");       
    }
    
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            ItemStack is = e.getItem();
            if (is != null) {
                if (is.hasItemMeta()) {
                    if (is.getItemMeta().getDisplayName() != null) {
                        switch (is.getItemMeta().getDisplayName()) {
                            case "§7↠ §6Minigames":
                                teleporter.open(e.getPlayer());
                                break;
                            case "§7↠ §2Map":
                                e.setCancelled(true);
                                e.getPlayer().sendMessage(main.prefix + "§cHier folgt ein Map-Teleporter!");
                                break;
                            case "§7↠ §eSpielersichtbarkeit":
                                hidePlayers.open(e.getPlayer());
                                break;
                            case "§7↠ §cSilentLobby":
                                e.setCancelled(true);
                                if (!cooldownSilentLobby.contains(e.getPlayer())) {
                                    if (!plugin.isSilentLobby(e.getPlayer())) {
                                        plugin.setSilentLobby(e.getPlayer());
                                        for (Player player : plugin.getServer().getOnlinePlayers()) {
                                            player.hidePlayer(e.getPlayer());
                                            e.getPlayer().hidePlayer(player);
                                        }
                                        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
                                        e.getPlayer().playEffect(e.getPlayer().getLocation(), Effect.FLAME, 5);
                                        e.getPlayer().sendMessage(main.prefix + "Du bist nun in der §cSilentLobby§7!");
                                    } else {
                                        plugin.removeSilentLobby(e.getPlayer());
                                        for (Player player : plugin.getServer().getOnlinePlayers()) {
                                            switch (vanishlist.get(e.getPlayer())) {
                                                case ALL:
                                                    break;
                                                case TEAMONLY:
                                                    if (e.getPlayer().hasPermission("BroEmpire.team")) {
                                                        e.getPlayer().showPlayer(player);
                                                    }
                                                    break;
                                                case SHOWALL:
                                                    e.getPlayer().showPlayer(player);
                                                    break;
                                            }
                                            if (vanishlist.containsKey(player)) {
                                                VanishType type = vanishlist.get(player);
                                                switch (type) {
                                                    case ALL:
                                                        break;
                                                    case TEAMONLY:
                                                        if (e.getPlayer().hasPermission("BroEmpire.team")) {
                                                            player.showPlayer(e.getPlayer());
                                                        }
                                                        break;
                                                    case SHOWALL:
                                                        player.showPlayer(e.getPlayer());
                                                        break;
                                                } 
                                            }
                                        }
                                        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
                                        e.getPlayer().playEffect(e.getPlayer().getLocation(), Effect.FLAME, 5);
                                        e.getPlayer().sendMessage(main.prefix + "Du bist nun nicht mehr in der §aSilentLobby§7!");
                                    }
                                    cooldownSilentLobby.add(e.getPlayer());
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            cooldownSilentLobby.remove(e.getPlayer());
                                        }
                                    }.runTaskLater(plugin, 100L);
                                } else {
                                    e.getPlayer().sendMessage(main.prefix + "§cBitte spamme dieses Item nicht!");
                                }
                                break;
                            case "§7↠ §bProfil":
                                e.setCancelled(true);
                                e.getPlayer().sendMessage(main.prefix + "§cDein Profil kannst du noch nicht bearbeiten!");
                                break;
//                            case "§7↠ §aShop":
//                                e.setCancelled(true);
//                                e.getPlayer().sendMessage(main.prefix + "§cDer Shop ist noch nicht verfügbar!");
//                                break;
                }
                }
                }
            }
        }
    }
    
    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        if (isBuildmode(e.getPlayer())) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onItemDrag(InventoryDragEvent e) {
        e.setCancelled(true);
    }
    @EventHandler 
    public void onPickupItem(PlayerPickupItemEvent e) {
        e.setCancelled(true);
    }
    @EventHandler
    public void onItemMove(InventoryMoveItemEvent e) {
        e.setCancelled(true);
    }
    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (isBuildmode((Player) e.getWhoClicked())) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onItemSwitch(PlayerSwapHandItemsEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        vanishlist.put(p, VanishType.SHOWALL);
        for (Player online : plugin.getServer().getOnlinePlayers()) {
            if (vanishlist.containsKey(online)) {
                VanishType type = vanishlist.get(online);
                switch (type) {
                    case ALL:
                        online.hidePlayer(p);
                        break;
                    case TEAMONLY:
                        if (!p.hasPermission("BroEmpire.team")) {
                            online.hidePlayer(p);
                        }
                        break;
                    case SHOWALL:
                        break;
                }
            }
            if (plugin.isSilentLobby(online)) {
                online.hidePlayer(p);
            } else if (plugin.isSilentLobby(p)) {
                p.hidePlayer(online);
            }
        }
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        vanishlist.remove(p);
    }
    
    private void initCallback() {
        //TELEPORTER CALLBACKS -> Teleportiert die Spieler!
        spawnCallback = (Player t) -> {
            ActionAPI.sendActionBar(t, "§7Du bist nun am §aSpawn§7!");
            t.teleport(plugin.getLocation("SPAWN"));
            t.playSound(t.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3, 3);
            t.closeInventory();
        };
        virusCallback = (Player t) -> {
            ActionAPI.sendActionBar(t, "§7Du bist nun bei §cVirus§7!");
            t.teleport(plugin.getLocation("VIRUS"));
            t.playSound(t.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3, 3);
            t.closeInventory();
        };
        zombieescapeCallback = (Player t) -> {
//            ActionAPI.sendActionBar(t, "§7Du bist nun bei §2ZombieEscape§7!");
//            t.teleport(plugin.getLocation("ZOMBIEESCAPE"));
//            t.playSound(t.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3, 3);
//            t.closeInventory();
        };
        werwolfCallback = (Player t) -> {
            ActionAPI.sendActionBar(t, "§7Du bist nun bei §4Werwolf§7!");
            t.teleport(plugin.getLocation("WERWOLF"));
            t.playSound(t.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3, 3);
            t.closeInventory();
        };
        buildCallback = (Player t) -> {
//            ActionAPI.sendActionBar(t, "§7Du bist nun bei §6Build your Empire§7!");
//            t.teleport(plugin.getLocation("BUILDYOUREMPIRE"));
//            t.playSound(t.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3, 3);
//            t.closeInventory();
        };
        
        //VANISH CALLBACKS -> Versteckt bestimmte Spieler!
        vanishAllCallback = (Player t) -> {
            ActionAPI.sendActionBar(t, "§7Die Spieler sind nun §cversteckt§7!");
            plugin.getServer().getOnlinePlayers().stream().forEach((online) -> {
                t.hidePlayer(online);
            });
            vanishlist.put(t, VanishType.ALL);
            t.playSound(t.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
            t.closeInventory();
        };
        vanishShowAllCallback = (Player t) -> {
            ActionAPI.sendActionBar(t, "§7Die Spieler sind nun §asichtbar§7!");
            plugin.getServer().getOnlinePlayers().stream().forEach((online) -> {
                t.showPlayer(online);
            });
            vanishlist.put(t, VanishType.SHOWALL);
            t.playSound(t.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
            t.closeInventory();
        };
        vanishTeamCallback = (Player t) -> {
            ActionAPI.sendActionBar(t, "§7Nur §6Teammitglieder und Freunde §7sind nun §asichtbar§7!");
            plugin.getServer().getOnlinePlayers().stream().forEach((online) -> {
                if (!online.hasPermission("BroEmpire.team")) {
                    t.hidePlayer(online);
                } else {
                    t.showPlayer(online);
                }
                t.playSound(t.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
            });
            vanishlist.put(t, VanishType.TEAMONLY);
            t.closeInventory();
        };
        mountainOpenCallback = (Player t) -> {
            mapMountainTeleporter.open(t);
        };
                seaportOpenCallback = (Player t) -> {
            mapSeaportTeleporter.open(t);
        };
    }
    
}
