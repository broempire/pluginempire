/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.queue;

import de.broempire.lobbyempire.misc.Groups;
import org.bukkit.entity.Player;

/**
 *
 * @author Fabian
 */
public class PlayerUnit {
    
    public final int MAX_UNIT_SIZE = 10;
    
    public Player leader;
    public Player[] members = new Player[MAX_UNIT_SIZE-1];
    public int unitsize;
    public QueueRank lowestrank;
    
    public PlayerUnit(Player leader, Groups leadergroup)
    {
        this.leader = leader; 
        unitsize = 1;
        
        lowestrank = getrank(leadergroup);
    }
    
    public void addmember(Player newmember, Groups newmembergroup)
    {
        if(unitsize < MAX_UNIT_SIZE)
        {
            members[unitsize-1] = newmember;
            unitsize++;
            QueueRank newrank = getrank(newmembergroup);
            
            if(newrank.getranknumber() < lowestrank.getranknumber())
            {
                lowestrank = newrank;
            }
        } else {
            //zu viele Spieler
        }
    }
    
    public QueueRank getlowestrank()
    {
        return lowestrank;
    }
    
    public int getunitsize()
    {
        return unitsize;
    }
    
    public Player getleader()
    {
        return leader;
    }
    
    public Player[] getmembers()
    {
        return members;
    }
    
    public Player[] getplayers()
    {
        Player[] players = new Player[unitsize];
        players[0] = leader;
        for(int i=1;i<unitsize;i++)
        {
            players[i] = members[i-1];
        }
        return players;
    }
    
    public static QueueRank getrank(Groups group)
    {
        switch(group.getGroupname())
            {
                case "Player":{return QueueRank.NORMAL;}
                case "Premium":{return QueueRank.PREMIUM;}
                default:{return QueueRank.SPECIAL;}
            }
    }
    
}
