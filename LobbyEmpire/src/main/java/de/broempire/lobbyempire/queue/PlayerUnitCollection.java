/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.queue;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fabian
 */
public class PlayerUnitCollection {
    List<PlayerUnit> queue = new ArrayList<>();
    int playercount;
    
    public PlayerUnitCollection()
    {
        playercount = 0;
    }
    
    public void addplayerunit(PlayerUnit unit)
    {
        queue.add(unit);
        playercount += unit.getunitsize();
    }
    
    public void removeplayerunit(PlayerUnit unit)
    {
        queue.remove(unit);
        playercount -= unit.getunitsize();
    }
    
    public PlayerUnit getfirstplayerunit(int maxplayercount)
    {
        for(int i=0; i<queue.size(); i++)
        {
            PlayerUnit unit = queue.get(i);
            if(unit.unitsize <= maxplayercount)
            {
                removeplayerunit(unit);
                return unit;
            }
        }
        return null;
    }
    
    public int getplayercount()
    {
        return playercount;
    }
}
