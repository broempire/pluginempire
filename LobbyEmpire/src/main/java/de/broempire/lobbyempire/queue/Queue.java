/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.queue;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.entity.Player;

/**
 *
 * @author Fabian
 */
public class Queue {
    
    public final int premiumspeed = 2;
    
    public PlayerUnitCollection ncollection;
    public PlayerUnitCollection pcollection;
    public PlayerUnitCollection scollection;
    //public PlayerUnitCollection tosend;
    public String name;
    public int minplayer;
    public int slots;
    //Zugang zum Serverversenden muss hier auch noch rein
    //Kanns gern mal erkl�ren, wenn das Server System steht
    
    public void trytosend()
    {
        Player[] tosend;
        int[] amounts = getamounts();
        int playercount = amounts[0] + amounts[1] + amounts[2];
        if(playercount >= minplayer)
        {
            tosend = preparetosend(amounts);
        }
        
        //MATTI WICHTIG FUER DICH MATTI WICHTIG FUER DICH MATTI WICHTIG FUER DICH
        //MATTI WICHTIG FUER DICH MATTI WICHTIG FUER DICH MATTI WICHTIG FUER DICH
        //
        //jetzt muss nur noch jeder Spieler im Player Array tosend auf den der
        //Queue zugewiesenen Server geschickt werden!
        //
        //MATTI WICHTIG FUER DICH MATTI WICHTIG FUER DICH MATTI WICHTIG FUER DICH
        //MATTI WICHTIG FUER DICH MATTI WICHTIG FUER DICH MATTI WICHTIG FUER DICH
    }
    
    public Queue(String name, int minplayer, int slots)
    {
        this.name = name;
        this.minplayer = minplayer;
        this.slots = slots;
    }
    
    public void addunit(PlayerUnit unit)
    {
        switch(unit.getlowestrank())
        {
            case NORMAL:{ncollection.addplayerunit(unit);}
            case PREMIUM:{pcollection.addplayerunit(unit);}
            case SPECIAL:{scollection.addplayerunit(unit);}
        }
    }
    
    public void removeunit(PlayerUnit unit)
    {
        switch(unit.getlowestrank())
        {
            case NORMAL:{ncollection.removeplayerunit(unit);}
            case PREMIUM:{pcollection.removeplayerunit(unit);}
            case SPECIAL:{scollection.removeplayerunit(unit);}
        }
    }
    
    public int getplayercount(QueueRank rank)
    {
        switch(rank)
        {
            case NORMAL:{return ncollection.getplayercount();}
            case PREMIUM:{return pcollection.getplayercount();}
            case SPECIAL:{return scollection.getplayercount();}
        }
        return 0;
    }
    
    public int getallplayercount()
    {
        return ncollection.getplayercount()+
                pcollection.getplayercount()+
                scollection.getplayercount();
    }
    
    public int[] getamounts()
    {
        int[] amounts = new int[3];
        if(getallplayercount()>slots) {
            if(getplayercount(QueueRank.SPECIAL)>slots) {
                amounts[0]=slots;
                amounts[1]=0;
                amounts[2]=0;
                return amounts;
            }
            else {
                amounts[0]=getplayercount(QueueRank.SPECIAL);
                int freeslots = slots - amounts[0];
                if(getplayercount(QueueRank.PREMIUM)==0) {
                    amounts[1]=0;
                    amounts[2]=freeslots;
                    return amounts;
                }
                else if(getplayercount(QueueRank.NORMAL)==0) {
                    amounts[1]=freeslots;
                    amounts[2]=0;
                    return amounts;
                }
                else {
                    amounts[1]=freeslots*premiumspeed*getplayercount(QueueRank.PREMIUM)/((premiumspeed*getplayercount(QueueRank.PREMIUM))+getplayercount(QueueRank.NORMAL));
                    if(amounts[1]==0) {
                        amounts[1]=1;
                        amounts[2]=freeslots-1;
                        return amounts;
                    }
                    else if(amounts[1]==freeslots) {
                        amounts[1]=freeslots-1;
                        amounts[2]=1;
                        return amounts;
                    }
                    else {
                        if(amounts[1]>getplayercount(QueueRank.PREMIUM)) {
                            amounts[1]=getplayercount(QueueRank.PREMIUM);
                            amounts[2]=freeslots-amounts[1];
                            return amounts;
                        }
                        else if(freeslots-amounts[1]>getplayercount(QueueRank.NORMAL)) {
                            amounts[2]=getplayercount(QueueRank.NORMAL);
                            amounts[1]=freeslots-amounts[2];
                            return amounts;
                        }
                        else {
                            amounts[2]=freeslots-amounts[1];
                            return amounts;
                        }
                    }
                }             
            }
        }
        else if(getallplayercount()>=minplayer) {
            amounts[0]=getplayercount(QueueRank.SPECIAL);
            amounts[1]=getplayercount(QueueRank.PREMIUM);
            amounts[2]=getplayercount(QueueRank.NORMAL);
            return amounts;
        }
        else {
            //"Die Mindestspieleranzahl wurde noch nicht erf�llt"
            amounts[0]=0;
            amounts[1]=0;
            amounts[2]=0;
            return amounts;
        }
    }
    
    public Player[] preparetosend(int[] amounts1)
    {
        int[] amounts = amounts1;
        Player[] tosend;
        List<Player> players = new ArrayList<>();
        
        int specialamount = amounts[0];
        while(specialamount>=1)
        {
            PlayerUnit unit = scollection.getfirstplayerunit(specialamount);
            if(unit!=null)
            {
                specialamount-=unit.getunitsize();
                Player[] members = unit.getplayers();
                for(Player p : members)
                {
                    players.add(p);
                }
            } else {
                amounts[1]+=specialamount;
                specialamount=0;
            }
        }
        
        int premiumamount = amounts[1];
        while(premiumamount>=1)
        {
            PlayerUnit unit = pcollection.getfirstplayerunit(premiumamount);
            if(unit!=null)
            {
                premiumamount-=unit.getunitsize();
                Player[] members = unit.getplayers();
                for(Player p : members)
                {
                    players.add(p);
                }
            } else {
                amounts[2]+=premiumamount;
                premiumamount=0;
            }
        }
        
        int normalamount = amounts[2];
        while(normalamount>=1)
        {
            PlayerUnit unit = ncollection.getfirstplayerunit(normalamount);
            if(unit!=null)
            {
                normalamount-=unit.getunitsize();
                Player[] members = unit.getplayers();
                for(Player p : members)
                {
                    players.add(p);
                }
            } else {
                normalamount=0;
            }
        }
        
        tosend = new Player[players.size()];
        for(int i=0;i<players.size();i++)
        {
            tosend[i] = players.get(i);
        }
        return tosend;
        
    }
}
