/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.queue;

/**
 *
 * @author Fabian
 */
public enum QueueRank {
    
    NORMAL("Normal", 0),
    PREMIUM("Premium", 1),
    SPECIAL("Special", 2);
    
    String rankname;
    int ranknumber;
    
    QueueRank(String rankname, int ranknumber)
    {
        this.rankname = rankname;
        this.ranknumber = ranknumber;
    }
    
    public String getrankname()
    {
        return rankname;
    }
    
    public int getranknumber()
    {
        return ranknumber;
    }
}
