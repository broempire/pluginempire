/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.commands;

import de.broempire.lobbyempire.main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Matti
 */
public class LocationCommand implements CommandExecutor{
    main plugin;
    
    public LocationCommand(main plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player player = (Player) sender;
        if (player.hasPermission("LobbyEmpire.setlocations")) {
            if (args.length == 1) {
                plugin.setLocation(player.getLocation(), args[0].toUpperCase());
                player.sendMessage(main.prefix + "Die Location §6" + args[0].toUpperCase() + " §7wurde gesetzt!");
            } else {
                main.sendUsage("/setlocation <Name>", "/setlocation Virus", player);
            }
        } else {
            player.sendMessage(main.noperm);
        }
        return true;
    }
    
}
