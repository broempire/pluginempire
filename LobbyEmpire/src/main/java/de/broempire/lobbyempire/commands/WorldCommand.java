/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.commands;

import de.broempire.lobbyempire.main;
import de.broempire.lobbyempire.manager.Buildmode;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Matti
 */
public class WorldCommand implements CommandExecutor{

    private main plugin;
    
    public WorldCommand(main plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("LobbyEmpire.worlds")) {
                if (args.length == 1) {
                    if (Bukkit.getWorld(args[0]) != null) {
                        p.teleport(Bukkit.getWorld(args[0]).getSpawnLocation());
                        Buildmode.setBuildmodeTrue(p);
                    }
                } else {
                    main.sendUsage("/world <Name>", "/world world", p);
                }
            }
        }
        return true;
    }
    
}
