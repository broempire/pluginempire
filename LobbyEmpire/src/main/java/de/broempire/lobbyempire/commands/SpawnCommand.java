/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.lobbyempire.commands;

import de.broempire.lobbyempire.APIs.ActionAPI;
import de.broempire.lobbyempire.main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Matti
 */
public class SpawnCommand implements CommandExecutor{
    
    main plugin;
    
    public SpawnCommand(main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            switch(args.length) {
                case 0:
                    p.teleport(plugin.getLocation("SPAWN"));
                    ActionAPI.sendActionBar(p, "§7Du bist nun am §aSpawn§7!");
                    break;
                case 1:
                    if (p.hasPermission("LobbyEmpire.spawn")) {
                        if (args[0].equalsIgnoreCase("all")) {
                            plugin.getServer().getOnlinePlayers().forEach((online) ->{
                                online.teleport(plugin.getLocation("SPAWN"));
                                ActionAPI.sendActionBar(online, "§7Du bist nun am §aSpawn§7!");
                            });
                        } else if (Bukkit.getPlayer(args[0]) != null) {
                            Bukkit.getPlayer(args[0]).teleport(plugin.getLocation("SPAWN"));
                            ActionAPI.sendActionBar(Bukkit.getPlayer(args[0]), "§7Du bist nun am §aSpawn§7!");
                        } else {
                            main.sendUsage("/spawn [Spieler|all]", "/spawn Schmidtchen", p);
                        }
                    } else {
                        p.sendMessage(main.noperm);
                    }
            }
        }
        return true;
    }
}
