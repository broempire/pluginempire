/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire;

import de.broempire.bungeeempire.BanManager.BanCommand;
import de.broempire.bungeeempire.BanManager.BanManager;
import de.broempire.bungeeempire.BanManager.CheckCommand;
import de.broempire.bungeeempire.BanManager.KickCommand;
import de.broempire.bungeeempire.BanManager.TempbanCommand;
import de.broempire.bungeeempire.BanManager.UnbanCommand;
import de.broempire.bungeeempire.Commands.BroadcastCommand;
import de.broempire.bungeeempire.Commands.LobbyCommand;
import de.broempire.bungeeempire.Commands.MaintainceCommand;
import de.broempire.bungeeempire.Commands.AllianceCommand;
import de.broempire.bungeeempire.Commands.BrollarCommand;
import de.broempire.bungeeempire.Commands.BuildCommand;
import de.broempire.bungeeempire.Commands.GroupCommand;
import de.broempire.bungeeempire.Commands.HelpCommand;
import de.broempire.bungeeempire.Commands.PartyCommand;
import de.broempire.bungeeempire.Commands.PingCommand;
import de.broempire.bungeeempire.Commands.PluginsCommand;
import de.broempire.bungeeempire.Commands.SupportCommand;
import de.broempire.bungeeempire.Commands.TeamCommand;
import de.broempire.bungeeempire.Commands.TeamchatCommand;
import de.broempire.bungeeempire.Listener.PlayerListener;
import de.broempire.bungeeempire.MySQL.BannedPlayers;
import de.broempire.bungeeempire.MySQL.Bro;
import de.broempire.bungeeempire.MySQL.MySQL;
import de.broempire.bungeeempire.MySQL.PlayerData;
import de.broempire.bungeeempire.Perms.Permissions;
import de.broempire.bungeeempire.TeamspeakManager.TeamspeakManager;
import de.broempire.bungeeempire.TeamspeakManager.VerifyCommand;
import de.broempire.bungeeempire.party.PartyManager;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

/**
 *
 * @author Matti
 */
public class BungeeEmpire extends Plugin{

    private boolean maintaince = true;
    public String prefix = "§8▎ §6BroEmpire§8 ❘ §7";
    public String noPerms = "§8▎ §6BroEmpire§8 ❘ §cDu besitzt nicht die nötigen Rechte für diesen Befehl!";
    int number = 0;
    
    private PartyManager partyManager;
    private Bro bro;
    private MySQL mySQL;
    private PlayerData playerData;
    private Permissions permissions;
    private BanManager banManager;
    private BannedPlayers bannedPlayers;
    private TeamspeakManager teamspeakManager;
    
    @Override
    public void onEnable() {
        this.partyManager = new PartyManager();
        
        sendBroadcasts();
        getProxy().getPluginManager().registerCommand(this, new MaintainceCommand(this));
        getProxy().getPluginManager().registerCommand(this, new PingCommand(this));
        getProxy().getPluginManager().registerCommand(this, new LobbyCommand(this));
        getProxy().getPluginManager().registerCommand(this, new BroadcastCommand(this));
        getProxy().getPluginManager().registerCommand(this, new TeamchatCommand(this));
        getProxy().getPluginManager().registerCommand(this, new TeamCommand(this));
        getProxy().getPluginManager().registerCommand(this, new AllianceCommand(this));
        getProxy().getPluginManager().registerCommand(this, new SupportCommand(this));
        getProxy().getPluginManager().registerCommand(this, new PartyCommand(this));
        getProxy().getPluginManager().registerCommand(this, new BrollarCommand(this));
        getProxy().getPluginManager().registerCommand(this, new GroupCommand(this));
        getProxy().getPluginManager().registerCommand(this, new BuildCommand(this));
        getProxy().getPluginManager().registerCommand(this, new PluginsCommand(this));
        getProxy().getPluginManager().registerCommand(this, new HelpCommand(this));
        getProxy().getPluginManager().registerCommand(this, new BanCommand(this));
        getProxy().getPluginManager().registerCommand(this, new TempbanCommand(this));
        getProxy().getPluginManager().registerCommand(this, new CheckCommand(this));
        getProxy().getPluginManager().registerCommand(this, new UnbanCommand(this));
        getProxy().getPluginManager().registerCommand(this, new VerifyCommand(this));
        getProxy().getPluginManager().registerCommand(this, new KickCommand(this));
        
        getProxy().getPluginManager().registerListener(this, new PlayerListener(this));
        
        this.mySQL = new MySQL("localhost", "BroEmpire", "BroEmpireMC", "!BroEmpireSQL");
        mySQL.update("CREATE TABLE IF NOT EXISTS Players (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, UUID VARCHAR(100), PlayerName VARCHAR(100), PlayerGroup VARCHAR(100), Coins VARCHAR(100), FirstOnline VARCHAR(100), LastLogin VARCHAR(100), BanPoints VARCHAR(100), OnlineTime VARCHAR(100))");
        mySQL.update("CREATE TABLE IF NOT EXISTS BannedPlayers (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, UUID VARCHAR(100), PlayerName VARCHAR(16), End VARCHAR(100), Reason VARCHAR(100))");
        mySQL.update("CREATE TABLE IF NOT EXISTS playerdata (uuid VARCHAR(100) PRIMARY KEY, dataKey VARCHAR(100), dataValue VARCHAR(100))");
        
        this.bro = new Bro(this, mySQL);
        this.banManager = new BanManager(this);
        this.bannedPlayers = new BannedPlayers(mySQL, this);
        this.permissions = new Permissions(this);
        this.playerData = new PlayerData(mySQL, this);
        this.teamspeakManager = new TeamspeakManager(this);
        
    }

    @Override
    public void onDisable() {
        
    }
    
    public boolean isMaintaince() {
        return this.maintaince;
    }
    public void setMaintaince(boolean maintaince) {
        this.maintaince = maintaince;
    }
    
    public void sendMessageToTeam(TextComponent msg) {
        getProxy().getPlayers().stream().filter((player) -> (player.hasPermission("BungeeEmpire.team"))).forEach((player) -> {
            TextComponent message = new TextComponent("§8▎ §6Team§8 ❘ §7");
            message.addExtra(msg);
            player.sendMessage(message);
        });
    }
    
    public void sendMessageToSupporters(TextComponent msg) {
        getProxy().getPlayers().stream().filter((player) -> (player.hasPermission("BungeeEmpire.support"))).forEach((player) -> {
            TextComponent message = new TextComponent("§8▎ §6Support§8 ❘ §7");
            message.addExtra(msg);
            player.sendMessage(message);
        });
    }
    
    public boolean isTeammember(ProxiedPlayer p) {
        return p.hasPermission("BroEmpire.team");
    }
    
    public boolean isSupporter(ProxiedPlayer player) {
        return player.hasPermission("BungeeEmpire.support");
    }

    public PartyManager getPartyManager() {
        return partyManager;
    }

    public Bro getBro() {
        return bro;
    }

    public MySQL getMySQL() {
        return mySQL;
    }

    public BanManager getBanManager() {
        return banManager;
    }

    public BannedPlayers getBannedPlayers() {
        return bannedPlayers;
    }

    public Permissions getPermissions() {
        return permissions;
    }

    public TeamspeakManager getTeamspeakManager() {
        return teamspeakManager;
    }

    public PlayerData getPlayerData() {
        return playerData;
    }
    
    public void sendUsage(String usage, String example, ProxiedPlayer p) {
        p.sendMessage(new TextComponent("§8§m--------§r§7»§6Benutzung§7«§8§m--------"));
        p.sendMessage(new TextComponent(" "));
        p.sendMessage(new TextComponent("§4" + usage));
        if (example != null) {
            p.sendMessage(new TextComponent("§2Beispiel§7: §6" + example));
        }
        p.sendMessage(new TextComponent(" "));
        p.sendMessage(new TextComponent("§8§m--------§r§7»§6Benutzung§7«§8§m--------"));
    }
    
    public void sendBroadcasts() {
        this.getProxy().getScheduler().schedule(this, () -> {
            switch (number) {
                case 0:
                    this.getProxy().broadcast(new TextComponent("§8§m------------------------------"));
                    this.getProxy().broadcast(new TextComponent(" "));
                    this.getProxy().broadcast(new TextComponent("§8» §7Das Server-Netzwerk befindet sich noch in der §cBeta-Phase§7!"));
                    this.getProxy().broadcast(new TextComponent("§8» §7Wir brauchen §aeuch§7, um die auftretenden §7Bugs zu §7beheben!"));
                    this.getProxy().broadcast(new TextComponent(" "));
                    this.getProxy().broadcast(new TextComponent("§8§m------------------------------"));
                    this.number++;
                    break;
                case 1:
                    TextComponent youtube = new TextComponent("§8» §5Youtube: §dBroEmpire");
                    youtube.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Sieh dir den Kanal an!").create()));
                    youtube.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.youtube.com/channel/UC_Pgq4Vdym7OVCOH_zos7qw"));
                    TextComponent twitter = new TextComponent("§8» §9Twitter: §3@BroEmpireMC");
                    twitter.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Erfahre Aktuelles über das Netzwerk!").create()));
                    twitter.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://twitter.com/BroEmpireMC"));
                    TextComponent shop = new TextComponent("§8» §2Shop: §ashop.broempire.de");
                    shop.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Besuche unseren Shop!").create()));
                    shop.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://shop.broempire.de"));
                    TextComponent ts = new TextComponent("§8» §6Teamspeak: §eBroEmpire.de");
                    TextComponent forum = new TextComponent("§8» §7Forum: §8forum.broempire.de");
                    forum.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Besuche unser Forum!").create()));
                    forum.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://forum.broempire.de"));
                    this.getProxy().broadcast(new TextComponent("§8§m------------------------------"));
                    this.getProxy().broadcast(new TextComponent(" "));
                    this.getProxy().broadcast(youtube);
                    this.getProxy().broadcast(twitter);
                    this.getProxy().broadcast(shop);
                    this.getProxy().broadcast(ts);
                    this.getProxy().broadcast(forum);
                    this.getProxy().broadcast(new TextComponent(" "));
                    this.getProxy().broadcast(new TextComponent("§8§m------------------------------"));
                    this.number++;
                    break;
                case 2:
                    TextComponent text = new TextComponent("§8» §7Weitere Infos findest du hier: ");
                    TextComponent link = new TextComponent("§2shop.broempire.de§7!");
                    link.setBold(true);
                    link.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Verbinde mich mit dieser Seite!").create()));
                    link.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://shop.broempire.de"));
                    text.addExtra(link);
                    this.getProxy().broadcast(new TextComponent("§8§m------------------------------"));
                    this.getProxy().broadcast(new TextComponent(" "));
                    this.getProxy().broadcast(new TextComponent("§8» §7Dieses Server-Netzwerk lebt von §2Spenden§7!"));
                    this.getProxy().broadcast(new TextComponent("§8» §7Nutze §6coole Vorteile §7und §3unterstütze §7uns damit §7sogar noch!"));
                    this.getProxy().broadcast(text);
                    this.getProxy().broadcast(new TextComponent(" "));
                    this.getProxy().broadcast(new TextComponent("§8§m------------------------------"));
                    this.number++;
                    break;
                case 3:
                    TextComponent hoster = new TextComponent("§8» §7Dann gucke doch mal bei MyPrepaidHost.de vorbei!");
                    hoster.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Kaufe günstige Root-Server!").create()));
                    hoster.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://myprepaidhost.de/"));
                    this.getProxy().broadcast(new TextComponent("§8§m------------------------------"));
                    this.getProxy().broadcast(new TextComponent(" "));
                    this.getProxy().broadcast(new TextComponent("§8» §7Suchst du gute und günstige Server?"));
                    this.getProxy().broadcast(hoster);
                    this.getProxy().broadcast(new TextComponent(" "));
                    this.getProxy().broadcast(new TextComponent("§8§m------------------------------"));
                    this.number = 0;
                    break;
                default:
                    break;
            }
        }, 2L, 2L, TimeUnit.MINUTES);
    }
}
