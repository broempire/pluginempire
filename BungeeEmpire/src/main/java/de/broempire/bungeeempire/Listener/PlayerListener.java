/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Listener;

import de.broempire.bungeeempire.BungeeEmpire;
import de.broempire.bungeeempire.party.Party;
import java.util.HashMap;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.ServerPing.Protocol;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.event.ProxyReloadEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 *
 * @author Matti
 */
public class PlayerListener implements Listener{

    private BungeeEmpire plugin;
    
    private HashMap<ProxiedPlayer, Long> online = new HashMap<>();
    
    public PlayerListener(BungeeEmpire plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onPlayerJoin (ServerConnectEvent e) {
        ProxiedPlayer player = e.getPlayer();
            
        boolean isPremium = plugin.getBro().isPremium(player.getUniqueId().toString());
        if (player.getServer() == null) {
            plugin.getBro().create(player.getUniqueId().toString());
        
            if (plugin.getBannedPlayers().isBanned(player.getUniqueId().toString())) {
                e.setCancelled(true);
                player.disconnect(new TextComponent(plugin.getBanManager().getBanMessage(player.getUniqueId().toString())));
            }
            
            for (String permission : plugin.getPermissions().getPermissions(plugin.getBro().getGroup(player.getUniqueId().toString()))) {
                player.setPermission(permission, true);
            }
            for (String prohibition : plugin.getPermissions().getProhibitions(plugin.getBro().getGroup(player.getUniqueId().toString()))) {
                player.setPermission(prohibition, false);
            }   
            if (plugin.isMaintaince()) {
                if (!player.hasPermission("BungeeEmpire.maintaince.bypass")) {
                    e.setCancelled(true);
                    player.disconnect(new TextComponent("§8§m------------------------------\n§r§6BroEmpire.de §cist §loffline!\n§7Grund: §cWir befinden uns in Wartungsarbeiten!\n§7Mehr Informationen gibt es auf unserem Twitter-Account: §b@BroEmpireMC\n§8§m------------------------------"));
                }
            }
            
            online.put(player, System.currentTimeMillis());
            
            Title title = plugin.getProxy().createTitle();
            title.title(new TextComponent("§6BroEmpire"));
            title.subTitle(new TextComponent("§bHerzlich willkommen!"));
            title.fadeIn(10);
            title.stay(60);
            title.fadeOut(20);
            title.send(player);
            
            player.sendMessage(new TextComponent(plugin.prefix + "Willkommen auf §6§lBroEmpire.de§7!"));
            player.sendMessage(new TextComponent(" "));
            player.sendMessage(new TextComponent(plugin.prefix + "Das Netzwerk befindet sich in der §aBeta§7!"));
            player.sendMessage(new TextComponent(plugin.prefix + "§cBugs §7bitten wir zu entschuldigen!"));
            player.sendMessage(new TextComponent(" "));
            player.sendMessage(new TextComponent(plugin.prefix + "Solltest du einen §cBug §7finden,"));
            player.sendMessage(new TextComponent(plugin.prefix + "dann melde ihn bitte einem Teammitglied!"));
            player.sendMessage(new TextComponent(" "));
            player.sendMessage(new TextComponent(plugin.prefix + "§aDanke für dein Verständnis"));
            player.sendMessage(new TextComponent(plugin.prefix + "§aund viel Spaß auf §6BroEmpire§a!"));
            
            switch(plugin.getBro().getGroup(player.getUniqueId().toString())) {
                case "PLAYER":
                    player.setDisplayName("§e" + player.getName());
                    break;
                case "PREMIUM":
                    player.setDisplayName("§6" + player.getName());
                    break;
                case "BUILDER":
                    player.setDisplayName("§9" + player.getName());
                    break;
                case "LEADBUILDER":
                    player.setDisplayName("§9" + player.getName());
                    break;
                case "VIP":
                    player.setDisplayName("§5" + player.getName());
                    break;
                case "MODERATOR":
                    player.setDisplayName("§c" + player.getName());
                    break;
                case "JRMODERATOR":
                    player.setDisplayName("§c" + player.getName());
                    break;
                case "LEADMODERATOR":
                    player.setDisplayName("§c" + player.getName());
                    break;
                case "ADMIN":
                    player.setDisplayName("§4" + player.getName());
                    break;
                case "DEVELOPER":
                    player.setDisplayName("§b" + player.getName());
                    break;
                case "LEADDEVELOPER":
                    player.setDisplayName("§b" + player.getName());
                    break;
                case "FORENMOD":
                    player.setDisplayName("§2" + player.getName());
                    break;
                case "LEADFORENMOD":
                    player.setDisplayName("§2" + player.getName());
                    break;
            }
            
            if (!e.isCancelled()) {
                if (player.hasPermission("BungeeEmpire.team")) {
                    plugin.sendMessageToTeam(new TextComponent(player.getDisplayName() + " §7ist nun §aonline"));
                }
            }
        }
        
        player.setTabHeader(new TextComponent("§2Herzlich willkommen, " + player.getDisplayName() + "§a!\n§r§7Viel Spaß auf §6§lBroEmpire.de§7!"), new TextComponent("§7Du befindest dich momentan auf dem Server: §a" + e.getTarget().getName().toUpperCase() + "\n§r§b/help §7gibt dir eine Übersicht über wichtige Befehle!"));
        
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerDisconnectEvent e) {
        ProxiedPlayer player = e.getPlayer();
        if (player.getServer() != null) {
            if (player.hasPermission("BungeeEmpire.team")) {
                plugin.sendMessageToTeam(new TextComponent(player.getDisplayName() + " §7ist nun §coffline"));
            }
        }
        if (plugin.getPartyManager().hasParty(player)) {
            Party party = plugin.getPartyManager().getParty(player);
            party.sendMessage(player.getDisplayName() + " §7hat die Allianz §cverlassen!");
            if (party.getOwner() == player) {
                if (party.getMembers().size() > 1) {
                    plugin.getPartyManager().removeFromParty(player, party);
                    party.setOwner(party.getMembers().get(0));
                    party.sendMessage("Der neue Bündnisleiter ist: " + party.getMembers().get(0).getDisplayName());
                    party.getMembers().get(0).sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§aDu bist nun der Bündnisleiter"));
                }
            } else {
                plugin.getPartyManager().removeFromParty(player, party);
            }
        }
        Long onlineTime = plugin.getBro().getOnlineTime(player.getUniqueId().toString());
        Long difference = System.currentTimeMillis() - online.get(player);
        plugin.getBro().setOnlineTime(onlineTime + difference, player.getUniqueId().toString());
        online.remove(player);
    }
    
    @EventHandler
    public void onPing (ProxyPingEvent e) {
        if (plugin.isMaintaince()) {
            e.getResponse().setVersion(new Protocol("§c§lWartungsmodus", 2));
            e.getResponse().setDescriptionComponent(new TextComponent("§a§l>§2§l>§a§l> §6§lBroEmpire §a§l>§2§l>§a§l> §r§7von §lBros, für §lBros! §8[§61.10§8]\n§r§6↠ §7Der Server befindet sich in §c§lWartung§7§r!"));
        } else {
            e.getResponse().setPlayers(new ServerPing.Players(30, plugin.getProxy().getOnlineCount(), null));
            e.getResponse().setDescriptionComponent(new TextComponent("§a§l>§2§l>§a§l> §6§lBroEmpire §a§l>§2§l>§a§l> §r§7von §lBros, für §lBros! §8[§61.10§8]\n§r§6↠ §c§lVirus Beta §7& §a§lLobby Beta"));
        }
    }
    
    @EventHandler
    public void onSwitch(ServerSwitchEvent e) {
        ProxiedPlayer p = e.getPlayer();
        Long onlineTime = plugin.getBro().getOnlineTime(p.getUniqueId().toString());
        Long difference = System.currentTimeMillis() - online.get(p);
        plugin.getBro().setOnlineTime(onlineTime + difference, p.getUniqueId().toString());
        online.replace(p, System.currentTimeMillis());
        
        if(plugin.getPartyManager().hasParty(p)){
            if(plugin.getPartyManager().getParty(p).getOwner() == p){
                plugin.getPartyManager().getParty(p).getMembers().stream().forEach((t) -> {
                    if (t != p) {
                        t.connect(p.getServer().getInfo());
                    }
                });
                plugin.getPartyManager().getParty(p).sendMessage("§aDie Allianz wechselt zu §6" + p.getServer().getInfo().getName().toUpperCase() + "§a!");
            }
        }
    }
    
    @EventHandler
    public void onReload(ProxyReloadEvent e) {
        for (ProxiedPlayer player : plugin.getProxy().getPlayers()) {
            for (String permission : plugin.getPermissions().getPermissions(plugin.getBro().getGroup(player.getUniqueId().toString()))) {
                player.setPermission(permission, true);
            }
            for (String prohibition : plugin.getPermissions().getProhibitions(plugin.getBro().getGroup(player.getUniqueId().toString()))) {
                player.setPermission(prohibition, false);
            }  
        }
    }
}
