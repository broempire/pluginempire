/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.MySQL;

import de.broempire.bungeeempire.BungeeEmpire;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.md_5.bungee.api.chat.TextComponent;

/**
 *
 * @author Matti
 */
public class BannedPlayers {
    
    private MySQL mySQL;
    
    private BungeeEmpire plugin;

    public BannedPlayers(MySQL mySQL, BungeeEmpire plugin) {
        this.mySQL = mySQL;
        this.plugin = plugin;
    }
    
    public boolean isBanned(String uuid) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM BannedPlayers WHERE UUID = '" + uuid + "'");
            
            while (resultSet.next()) {
                if (resultSet.getLong("End") < System.currentTimeMillis() && resultSet.getLong("End") != -1) {
                    unban(uuid);
                    return false;
                } else {
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public void ban (String uuid, String reason, long time) {
        if (!isBanned(uuid)) {
            long end;
            if (time == -1) {
                end = -1;
            } else {
                end = System.currentTimeMillis() + (time * 1000);
            }
            mySQL.update("INSERT INTO BannedPlayers (UUID, PlayerName, End, Reason) VALUES ('" + uuid + "', '" + plugin.getBro().getName(uuid) + "', '" + end + "', '" + reason + "');");
            if (plugin.getProxy().getPlayer(UUID.fromString(uuid)) != null) {
                plugin.getProxy().getPlayer(UUID.fromString(uuid)).disconnect(new TextComponent(plugin.getBanManager().getBanMessage(uuid)));
            }
        }
    }
    
    public String getReason(String uuid) {
        ResultSet resultSet = mySQL.query("SELECT * FROM BannedPlayers WHERE UUID = '" + uuid + "'");
        try {
            while (resultSet.next()) {
                return resultSet.getString("Reason");
            }
            return "";
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }
    
    public Long getEnd(String uuid) {
        ResultSet resultSet = mySQL.query("SELECT * FROM BannedPlayers WHERE UUID = '" + uuid + "'");
        try {
            while (resultSet.next()) {
                return resultSet.getLong("End");
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public List<String> getBannedPlayers() {
        List<String> players = new ArrayList<>();
        ResultSet resultSet = mySQL.query("SELECT * FROM BannedPlayers");
        try {
            while (resultSet.next()) {
                players.add(resultSet.getString("PlayerName"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return players;
    }
    
    public String getRemainingTime (String uuid) {
        return "";
    }
    
    public void unban(String uuid) {
        mySQL.update("DELETE FROM BannedPlayers WHERE UUID = '" + uuid + "'");
    }
    
}
