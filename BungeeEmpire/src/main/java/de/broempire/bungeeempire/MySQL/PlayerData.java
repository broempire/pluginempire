/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.MySQL;

import de.broempire.bungeeempire.BungeeEmpire;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matti
 */
public class PlayerData {
    
    private MySQL mySQL;
    private BungeeEmpire bungeeEmpire;

    public PlayerData(MySQL mySQL, BungeeEmpire bungeeEmpire) {
        this.mySQL = mySQL;
        this.bungeeEmpire = bungeeEmpire;
    }
    
    public boolean UUIDExists (String uuid) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM playerdata WHERE uuid = '" + uuid + "'");
            return resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean hasEntry(String uuid, String key) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM playerdata WHERE uuid = '" + uuid + "' AND dataKey = '" + key + "';");
            return resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public Object getValue(String uuid, String key)  {
        if (hasEntry(uuid, key)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM playerdata WHERE uuid = '" + uuid + "' AND dataKey = '" + key + "';");
                while (resultSet.next()) {
                    return resultSet.getObject("dataValue");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }
    
    public Map<String, Object> getEntries(String uuid)  {
        Map<String, Object> entries = new HashMap<>();
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM playerdata WHERE uuid = '" + uuid + "';");
            while (resultSet.next()) {
                entries.put(resultSet.getString("dataKey"), resultSet.getObject("dataValue"));
            }
            return entries;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public void setValue(String uuid, String key, Object value) {
        if (hasEntry(uuid, key)) {
            mySQL.update("UPDATE playerdata SET dataValue = '" + value + "' WHERE uuid = '" + uuid + "' AND dataKey = '" + key + "';");
        } else {
            mySQL.update("INSERT INTO playerdata (uuid, dataKey, dataValue) VALUES ('" + uuid + "', '" + key + "', '" + value + "');");
        }
    }
    
    public void deleteEntry(String uuid, String key) {
        if (hasEntry(uuid, key)) {
            mySQL.update("DELETE FROM playerdata WHERE uuid = '" + uuid + "' AND dataKey = '" + key + "'");
        }
    }
    
    public String getPlayer(String key, String value) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM playerdata WHERE dataKey = '" + key + "' AND dataValue = '" + value + "'");
            while (resultSet.next()) {
                return resultSet.getString("uuid");
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
