/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.MySQL;

import de.broempire.bungeeempire.BungeeEmpire;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author Matti
 */
public class Bro {
    
    private MySQL mySQL;
    
    private BungeeEmpire plugin;
    
    public Bro(BungeeEmpire plugin, MySQL mySQL) {
        this.plugin = plugin;
        this.mySQL = mySQL;
    }
    
    public boolean UUIDexists (String uuid) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
                
            if (resultSet == null) {
                return false;
            }
            
            return resultSet.next();
        } catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean NameExists (String name) {
        try{
            ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE PlayerName = '" + name + "'");
            
            return resultSet.next();
            
        }catch(SQLException e){
            e.printStackTrace();
	}
        return false;
    }
    
    public void create(String uuid) {
        Long firstonline = System.currentTimeMillis();
        Long lastonline = System.currentTimeMillis();
        
        if (!UUIDexists(uuid)) {
            mySQL.update("INSERT INTO Players (UUID, PlayerName, PlayerGroup, Coins, FirstOnline, LastLogin, BanPoints, OnlineTime) VALUES ('" + uuid + "', '" + plugin.getProxy().getPlayer(UUID.fromString(uuid)).getName() + "', 'PLAYER', '100', '" + firstonline + "', '" + lastonline + "', '0', '0');");
        }
        
        setName(plugin.getProxy().getPlayer(UUID.fromString(uuid)).getName(), uuid);
        setLastOnline(lastonline, uuid);
    }
    
    public String getUUID(String name) {
        if (NameExists(name)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE PlayerName = '" + name +"'");
                while (resultSet.next()) {
                    return resultSet.getString("UUID");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public String getGroup(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
                while (resultSet.next()) {
                    return resultSet.getString("PlayerGroup");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            create(uuid);
            getGroup(uuid);
        }
        return null;
    }
    public void setGroup(String group, String uuid){
        if (UUIDexists(uuid)) {
                mySQL.update("UPDATE Players SET PlayerGroup = '" + group + "' WHERE UUID='" + uuid + "';");
        } else {
            create(uuid);
            setGroup(group, uuid);
        }
            
    }
    
    public void setPremium(String uuid, Long end) {
        setGroup("PREMIUM", uuid);
        plugin.getPlayerData().setValue(uuid, "premium", end);
    }
    
    public boolean isPremium(String uuid) {
        if (plugin.getPlayerData().hasEntry(uuid, "premium") && getGroup(uuid).equals("PREMIUM")) {
            Date end = new Date(Long.valueOf((String) plugin.getPlayerData().getValue(uuid, "premium")));
            Date current = new Date(System.currentTimeMillis());
            if (current.before(end)) {
                return true;
            } else {
                plugin.getPlayerData().deleteEntry(uuid, "premium");
                setGroup("PLAYER", uuid);
                return false;
            }
        } else if (plugin.getPlayerData().hasEntry(uuid, "premium")){
            plugin.getPlayerData().deleteEntry(uuid, "premium");
            return false;
        } else if (getGroup(uuid).equals("PREMIUM")) {
            return true;
        }
        return false;
    }
    
    public Integer getCoins(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
                while (resultSet.next()) {
                    return resultSet.getInt("Coins");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            create(uuid);
            getCoins(uuid);
        }
        return null;
    }
    public void setCoins(int coins, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET Coins = '" + coins + "' WHERE UUID = '" + uuid + "';");
        } else {
            create(uuid);
            setCoins(coins, uuid);
        }
    }
    
    public void setName(String name, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET PlayerName = '" + name + "' WHERE UUID = '" + uuid + "';");
        } else {
            create(uuid);
            setName(name, uuid);
        }
    }
    
    public String getName(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid +"'");
                while (resultSet.next()) {
                    return resultSet.getString("PlayerName");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public Integer getBanPoints(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID= '" + uuid + "'");
                
                while(resultSet.next()) {
                    return resultSet.getInt("BanPoints");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            create(uuid);
            getBanPoints(uuid);
        }
        return null;
    }
    public void setBanPoints(Integer banpoints, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET BanPoints = '" + banpoints + "' WHERE UUID = '" + uuid + "';");
        } else {
            create(uuid);
            setBanPoints(banpoints, uuid);
        }
    }
    
    public void addBanPoints(Integer banpoints, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET BanPoints = '" + getBanPoints(uuid) + banpoints + "' WHERE UUID = '" + uuid + "';");
        } else {
            create(uuid);
            addBanPoints(banpoints, uuid);
        }
    }
    
    public Long getLastOnline(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
                while (resultSet.next()) {
                    return resultSet.getLong("LastLogin");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            create(uuid);
            getLastOnline(uuid);
        }
        return null;
    }
    public void setLastOnline(long time, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET LastLogin = '" + time + "' WHERE UUID = '" + uuid + "';");
        } else {
            create(uuid);
            setLastOnline(time, uuid);
        }
    }
    public Long getFirstOnline(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
                while (resultSet.next()) {
                    return resultSet.getLong("FirstOnline");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            create(uuid);
            getFirstOnline(uuid);
        }
        return null;
    }
    public void setOnlineTime(long time, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET OnlineTime = '" + time + "' WHERE UUID = '" + uuid + "';");
        } else {
            create(uuid);
            setOnlineTime(time, uuid);
        }
    }
    
    public Long getOnlineTime(String uuid) {
        if (UUIDexists(uuid)) {
            ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
            try {
                while (resultSet.next()) {
                    return resultSet.getLong("OnlineTime");
                }
            } catch (SQLException e) {
                
            }
        } else {
            create(uuid);
            getOnlineTime(uuid);
        }
        return null;
    }
    
}
