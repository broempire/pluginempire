/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.MySQL;

import java.io.IOException;
import java.util.UUID;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class UUIDTypeAdapter extends TypeAdapter<UUID>
{
  @Override
public void write(JsonWriter out, UUID value)
    throws IOException
  {
    out.value(fromUUID(value));
  }

  @Override
public UUID read(JsonReader in) throws IOException
  {
    return fromString(in.nextString());
  }

  public static String fromUUID(UUID value) {
    return value.toString().replace("-", "");
  }

  public static UUID fromString(String input) {
    return UUID.fromString(input.replaceFirst("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5"));
  }
}
