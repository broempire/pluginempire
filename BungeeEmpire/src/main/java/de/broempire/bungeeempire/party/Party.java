/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.party;

import java.util.ArrayList;
import java.util.List;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 *
 * @author Matti
 */
public class Party {
	
    private ProxiedPlayer owner;
    private final List<ProxiedPlayer> members;

    public Party(ProxiedPlayer owner) {
        this.owner = owner;
        this.members = new ArrayList<>();

        members.add(owner);
    }

    public void sendMessage(String message) {
        members.stream().forEach((member) -> {
            member.sendMessage(new TextComponent(PartyManager.prefix + message));
        });
    }

    public List<ProxiedPlayer> getMembers() {
        return members;
    }

public ProxiedPlayer getOwner() {
        return owner;
    }

    public void addMember(ProxiedPlayer player) {
        members.add(player);
    }

    public void removeMember(ProxiedPlayer player) {
        members.remove(player);
    }

    public void setOwner(ProxiedPlayer newOwner) {
        this.owner = newOwner;
    }

}
