/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.party;

import java.util.HashMap;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 *
 * @author Matti
 */
public class PartyManager {
    
    	//Strings
	public static String prefix = "§8▎ §eAlliance§8 ❘ §7";
	
	//HashMaps
	private HashMap<ProxiedPlayer, Party> party = new HashMap<>();
	private HashMap<ProxiedPlayer, ProxiedPlayer> invitations = new HashMap<>();
        
	public Party getParty(ProxiedPlayer player) {
            return party.get(player);
	}
        
        public boolean hasParty(ProxiedPlayer player) {
            return party.containsKey(player);
	}
	
	public void inviteToParty(ProxiedPlayer player, Party party) {
            if (!hasInvitation(player)) {
                invitations.put(player, party.getOwner());
                party.getOwner().sendMessage(new TextComponent(prefix + "§aDu hast " + player.getDisplayName() + " §azu einem Bündnis eingeladen!"));
                player.sendMessage(new TextComponent(prefix + "Der Spieler " + party.getOwner().getDisplayName() + " §7hat dich in sein Bündnis eingeladen!"));
                TextComponent click = new TextComponent(prefix + "§b/alliance accept " + party.getOwner().getName());
                click.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/alliance accept " + party.getOwner().getName()));
                player.sendMessage(click);
            } else {
                party.getOwner().sendMessage(new TextComponent(prefix + "§cDer Spieler hat bereits eine Einladung zu einem anderem Bündnis bekommen!"));
            }
            
	}
        
        public ProxiedPlayer getPartyInvitation(ProxiedPlayer player) {
            return invitations.get(player);
        }
        
        public boolean hasInvitation(ProxiedPlayer player) {
            return invitations.containsKey(player);
        }
        
        public void removeInvitation(ProxiedPlayer p) {
            invitations.remove(p);
        }
	
	public void removeFromParty(ProxiedPlayer player, Party party) {
            party.removeMember(player);
            this.party.remove(player);
	}
	
	public void addToParty(ProxiedPlayer player, Party party) {
            this.party.put(player, party);
            if (party.getOwner() != player) {
                party.addMember(player);
                player.sendMessage(new TextComponent(prefix + "Du hast die Bündnisanfrage §aangenommen§7!"));
                party.sendMessage(player.getDisplayName() + " §7ist dem Bündnis beigetreten!");
                removeInvitation(player);
            }
	}
    
}
