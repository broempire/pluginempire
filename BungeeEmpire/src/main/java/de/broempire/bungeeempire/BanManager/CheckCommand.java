/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.BanManager;

import de.broempire.bungeeempire.BungeeEmpire;
import java.util.List;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class CheckCommand extends Command {

    private BungeeEmpire plugin;

    public CheckCommand(BungeeEmpire plugin) {
        super("check", "BungeeEmpire.support");
        this.plugin = plugin;
    }
    
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 1) {
            String string = args[0];
            if (plugin.getBro().NameExists(string)) {
                String uuid = plugin.getBro().getUUID(string);
                sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§8§m-        -§r §cBan §8§m-        -"));
                sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§7Name: §4" + string));
                sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§7Status: " + (plugin.getBannedPlayers().isBanned(uuid) ? "§cGebannt" : "§2Nicht gebannt")));
                sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§7Bannpunkte: §4" + plugin.getBro().getBanPoints(uuid)));
                if (plugin.getBannedPlayers().isBanned(uuid)) {
                    sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§7Grund: §3" + plugin.getBannedPlayers().getReason(uuid)));
                    sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§7Dauer: §c" + plugin.getBanManager().getRemainingTime(uuid)));
                }
                sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§8§m-        -§r §cBan §8§m-        -"));
            } else if (string.equalsIgnoreCase("list")) {
                List<String> bannedPlayers = plugin.getBannedPlayers().getBannedPlayers();
                if (bannedPlayers.isEmpty()) {
                    sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§2Momentan sind keine Spieler gebannt!"));
                } else {
                    sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "Alle aktiven Bans:"));
                    for (String playername : bannedPlayers) {
                        sender.sendMessage(new TextComponent("§8● §7" + playername + " §8» §e" + plugin.getBannedPlayers().getReason(plugin.getBro().getUUID(playername))));
                    }
                }
            }
        } else {
            sendUsage(sender);
        }
    }
    
    private void sendUsage (CommandSender sender) {
        if (sender instanceof ProxiedPlayer) {
            plugin.sendUsage("/check <Spielername>", "/check Schmidtchen", (ProxiedPlayer) sender);
        } else {
            sender.sendMessage(new TextComponent("[BroBans] /check <Spielername>"));
        }
    }
    
}
