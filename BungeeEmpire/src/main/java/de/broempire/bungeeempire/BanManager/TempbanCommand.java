/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.BanManager;

import de.broempire.bungeeempire.BungeeEmpire;
import java.util.List;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti, Juli
 */
public class TempbanCommand extends Command {

    private BungeeEmpire plugin;

    public TempbanCommand(BungeeEmpire plugin) {
        super("tempban", "BungeeEmpire.support");
        this.plugin = plugin;
    }
    
    /*
    *
    * /tempban <Spielername> <Zeit> <Einheit> <Grund>
    * 
    */
    
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length >= 4) {
            String playername = args[0];
            if (plugin.getBro().NameExists(playername)) {
                if(!plugin.isTeammember(plugin.getProxy().getPlayer(args[0])) || !(sender instanceof ProxiedPlayer) || plugin.getBro().getGroup(((ProxiedPlayer) sender).getUniqueId().toString()).equalsIgnoreCase("ADMIN")) {
                    if (!plugin.getBannedPlayers().isBanned(plugin.getBro().getUUID(playername))) {
                        long value;
                        try {
                            value = Long.valueOf(args[1]);
                        } catch (NumberFormatException e){
                            sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§cDu musst eine Zahl angeben!"));
                            return;
                        }
                        String shortcut = args[2];
                        StringBuilder reason = new StringBuilder();
                        for (int i = 3; i < args.length; i++) {
                            reason.append(args[i]).append(" ");
                        }
                        List<String> unitList = TimeUnit.getUnitsAsString();
                        if (unitList.contains(shortcut.toLowerCase())) {
                            TimeUnit unit = TimeUnit.getUnit(shortcut);
                            long seconds = value * unit.getToSecond();
                            plugin.getBannedPlayers().ban(plugin.getBro().getUUID(playername), reason.toString(), seconds);
                            plugin.getBanManager().sendBanToSupporters(playername, reason.toString(), sender.getName(), value + " " + unit.getName());
                        } else {
                            sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§cDiese Einheit existiert nicht!"));
                        }
                    }
                }else{
                    sender.sendMessage(new TextComponent(plugin.getBanManager().isTeam));
                }
            } else {
                sender.sendMessage(new TextComponent(plugin.getBanManager().notInDatabase));
            }
        } else {
            sendUsage(sender);
        }
    }
    
    private void sendUsage(CommandSender sender) {
        if (sender instanceof ProxiedPlayer) {
            plugin.sendUsage("/tempban <Spielername> <Zahlenwert> <Einheit> <Grund>", "/tempban Schmidtchen 20 day Zu schlau!", (ProxiedPlayer) sender);
        } else {
            sender.sendMessage(new TextComponent("[BroBans] /tempban <Spielername> <Zahlenwert> <Einheit> <Grund>"));
        }
    }
    
}
