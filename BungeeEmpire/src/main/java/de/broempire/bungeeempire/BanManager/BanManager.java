/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.BanManager;

import de.broempire.bungeeempire.BungeeEmpire;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.chat.TextComponent;

/**
 *
 * @author Matti
 */
public class BanManager {
    
    private final BungeeEmpire plugin;
    
    public String prefix = "§8▎ §4BroBans§8 ❘ §7";
    public String isTeam = "§8▎ §4BroBans§8 ❘ §cDieser Spieler ist ein Teammitglied!";
    public String notInDatabase = "§8▎ §4BroBans§8 ❘ §cDer Spieler existiert nicht in der Datenbank!";
    public String isBanned = "§8▎ §4BroBans§8 ❘ §cDer Spieler ist bereits gebannt!";
    public String isNotBanned = "§8▎ §4BroBans§8 ❘ §cDer Spieler ist nicht gebannt!";

    public BanManager(BungeeEmpire plugin) {
        this.plugin = plugin;
    }
    
    public String getBanMessage(String uuid) {
        return "§8§m------------------------------\n"
                + "§r§cDu wurdest gebannt!\n"
                + "§7Grund: §e" + plugin.getBannedPlayers().getReason(uuid) + "\n"
                + "§7Verbleibende Zeit: §4" + getRemainingTime(uuid) + "\n"
                + "§cHier kannst du einen Entbannungsantrag stellen: §ehttps://forum.broempire.de\n"
                + "§8§m------------------------------";
    }
    
    public String getKickMessage(String reason) {
        return "§8§m------------------------------\n"
                + "§r§cDu wurdest gekickt!\n"
                + " \n"
                + "§7Grund: §e" + reason + "\n"
                + " \n"
                + "§8§m------------------------------";
    }
    
    public void sendBanToSupporters (String playername, String reason, String mod, String time) {
        plugin.sendMessageToSupporters(new TextComponent(prefix + "§8§m-        -§r §cBan §8§m-        -"));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "Name: §4" + playername));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "Grund: §3" + reason));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "Dauer: §c" + time));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "Verantwortlicher: §2" + mod));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "§8§m-        -§r §cBan §8§m-        -"));
    }
    
    public void sendKickToSupporters (String playername, String reason, String mod) {
        plugin.sendMessageToSupporters(new TextComponent(prefix + "§8§m-        -§r §cKick §8§m-        -"));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "Name: §4" + playername));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "Grund: §3" + reason));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "Verantwortlicher: §2" + mod));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "§8§m-        -§r §cKick §8§m-        -"));
    }
    
    public void sendUnbanToSupporters (String playername, String mod) {
        plugin.sendMessageToSupporters(new TextComponent(prefix + "§8§m-        -§r §2Unban §8§m-        -"));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "Name: §4" + playername));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "Verantwortlicher: §2" + mod));
        plugin.sendMessageToSupporters(new TextComponent(prefix + "§8§m-        -§r §2Unban §8§m-        -"));
    }
    
    public String getRemainingTime(String uuid) {
        long current = System.currentTimeMillis();
        long end = plugin.getBannedPlayers().getEnd(uuid);
        if (end == -1) {
            return "PERMANENT";
        }
        long difference = end-current;
        long weeks = 0;
        long days = TimeUnit.MILLISECONDS.toDays(difference);
        long hours = TimeUnit.MILLISECONDS.toHours(difference);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(difference);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(difference);
        while (days >= 7) {
            days-=7;
            weeks++;
        }
        while (hours >= 24) {
            hours-=24;
        }
        while (minutes >= 60) {
            minutes-=60;
        }
        while (seconds >= 60) {
            seconds-=60;
        }
        return weeks + " Woche(n) " + days + " Tag(e) " +  hours + " Stunde(n) " + minutes + " Minute(n) " + seconds + " Sekunde(n)";
    }
    
}
