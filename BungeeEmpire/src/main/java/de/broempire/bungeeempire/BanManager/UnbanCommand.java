/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.BanManager;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class UnbanCommand extends Command{

    private BungeeEmpire plugin;

    public UnbanCommand(BungeeEmpire plugin) {
        super("unban", "BungeeEmpire.support");
        this.plugin = plugin;
    }
    
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 1) {
            String playername = args[0];
            if (plugin.getBro().NameExists(playername)) {
                String uuid = plugin.getBro().getUUID(playername);
                if (plugin.getBannedPlayers().isBanned(uuid)) {
                    plugin.getBannedPlayers().unban(uuid);
                    plugin.getBanManager().sendUnbanToSupporters(playername, sender.getName());
                } else {
                    sender.sendMessage(new TextComponent(plugin.getBanManager().isNotBanned));
                }
            }
        } else {
            sendUsage(sender);
        }
    }
    
    private void sendUsage (CommandSender sender) {
        if (sender instanceof ProxiedPlayer) {
            plugin.sendUsage("/unban <Spielername>", "/unban Schmidtchen", (ProxiedPlayer) sender);
        } else {
            sender.sendMessage(new TextComponent("[BroBans] /unban <Spielername>"));
        }
    }
    
}
