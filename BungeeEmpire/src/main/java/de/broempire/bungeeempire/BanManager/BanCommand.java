/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.BanManager;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti, Juli
 */
public class BanCommand extends Command {

    private final BungeeEmpire plugin;

    public BanCommand(BungeeEmpire plugin) {
        super("ban", "BungeeEmpire.support");
        this.plugin = plugin;
    }
    
    /*
    *
    * /ban <Spielername> <Grund> [Text]
    *
    */
    
    @Override
    public void execute(CommandSender sender, String[] args) {
        switch (args.length) {
            case 2:
                switch (args[1].toUpperCase()) {
                    case "HACKING":
                        if (plugin.getBro().NameExists(args[0])) {
                            if(!plugin.isTeammember(plugin.getProxy().getPlayer(args[0])) || !(sender instanceof ProxiedPlayer) || plugin.getBro().getGroup(((ProxiedPlayer) sender).getUniqueId().toString()).equalsIgnoreCase("ADMIN")) {
                                if(!plugin.getBannedPlayers().isBanned(plugin.getBro().getUUID(args[0]))) {
                                    plugin.getBro().addBanPoints(100, plugin.getBro().getUUID(args[0]));
                                    plugin.getBannedPlayers().ban(plugin.getBro().getUUID(args[0]), "HACKING", -1);
                                    plugin.getBanManager().sendBanToSupporters(args[0], "HACKING", sender.getName(), plugin.getBanManager().getRemainingTime(plugin.getBro().getUUID(args[0])));
                                } else {
                                    sender.sendMessage(new TextComponent(plugin.getBanManager().isBanned));
                                }
                            }else{
                                sender.sendMessage(new TextComponent(plugin.getBanManager().isTeam));
                            }
                        } else {
                            sender.sendMessage(new TextComponent(plugin.getBanManager().notInDatabase));
                        }
                        break;
                    case "BELEIDIGUNG":
                        if (plugin.getBro().NameExists(args[0])) {
                            if(!plugin.isTeammember(plugin.getProxy().getPlayer(args[0])) || !(sender instanceof ProxiedPlayer) || plugin.getBro().getGroup(((ProxiedPlayer) sender).getUniqueId().toString()).equalsIgnoreCase("ADMIN")) {
                                if(!plugin.getBannedPlayers().isBanned(plugin.getBro().getUUID(args[0]))) {
                                    plugin.getBro().addBanPoints(20, plugin.getBro().getUUID(args[0]));
                                    if (plugin.getBro().getBanPoints(plugin.getBro().getUUID(args[0])) >= 100) {
                                        plugin.getBannedPlayers().ban(plugin.getBro().getUUID(args[0]), "BELEIDIGUNG", -1);
                                        plugin.getBanManager().sendBanToSupporters(args[0], "BELEIDIGUNG", sender.getName(), plugin.getBanManager().getRemainingTime(plugin.getBro().getUUID(args[0])));
                                    } else {
                                        plugin.getBannedPlayers().ban(plugin.getBro().getUUID(args[0]), "BELEIDIGUNG", 24*(plugin.getBro().getBanPoints(plugin.getBro().getUUID(args[0]))/10)*60*60);
                                        plugin.getBanManager().sendBanToSupporters(args[0], "BELEIDIGUNG", sender.getName(), (plugin.getBro().getBanPoints(plugin.getBro().getUUID(args[0]))/10) + " Tage");
                                    }
                                } else {
                                    sender.sendMessage(new TextComponent(plugin.getBanManager().isBanned));
                                }
                            }else{
                                sender.sendMessage(new TextComponent(plugin.getBanManager().isTeam));
                            }
                        } else {
                            sender.sendMessage(new TextComponent(plugin.getBanManager().notInDatabase));
                        }
                        break;
                    case "CHATVERGEHEN":
                        if(plugin.getBro().NameExists(args[0])) {
                            if(!plugin.isTeammember(plugin.getProxy().getPlayer(args[0])) || !(sender instanceof ProxiedPlayer) || plugin.getBro().getGroup(((ProxiedPlayer) sender).getUniqueId().toString()).equalsIgnoreCase("ADMIN")) {
                                if(!plugin.getBannedPlayers().isBanned(plugin.getBro().getUUID(args[0]))) {
                                    plugin.getBro().addBanPoints(20, plugin.getBro().getUUID(args[0]));
                                    if (plugin.getBro().getBanPoints(plugin.getBro().getUUID(args[0])) >= 100) {
                                        plugin.getBannedPlayers().ban(plugin.getBro().getUUID(args[0]), "CHATVERGEHEN", -1);
                                        plugin.getBanManager().sendBanToSupporters(args[0], "CHATVERGEHEN", sender.getName(), plugin.getBanManager().getRemainingTime(plugin.getBro().getUUID(args[0])));
                                    } else {
                                        plugin.getBannedPlayers().ban(plugin.getBro().getUUID(args[0]), "CHATVERGEHEN", 24*(plugin.getBro().getBanPoints(plugin.getBro().getUUID(args[0]))/10)*60*60);
                                        plugin.getBanManager().sendBanToSupporters(args[0], "CHATVERGEHEN", sender.getName(), (plugin.getBro().getBanPoints(plugin.getBro().getUUID(args[0]))/10) + " Tage");
                                    }
                                } else {
                                    sender.sendMessage(new TextComponent(plugin.getBanManager().isBanned));
                                }
                            }else{
                                sender.sendMessage(new TextComponent(plugin.getBanManager().isTeam));
                            }
                        } else {
                            sender.sendMessage(new TextComponent(plugin.getBanManager().notInDatabase));
                        }
                        break;
                    case "TEAMING":
                        if(plugin.getBro().NameExists(args[0])) {
                            if(!plugin.isTeammember(plugin.getProxy().getPlayer(args[0])) || !(sender instanceof ProxiedPlayer) || plugin.getBro().getGroup(((ProxiedPlayer) sender).getUniqueId().toString()).equalsIgnoreCase("ADMIN")) {
                                if(!plugin.getBannedPlayers().isBanned(plugin.getBro().getUUID(args[0]))) {
                                    plugin.getBro().addBanPoints(20, plugin.getBro().getUUID(args[0]));
                                    if (plugin.getBro().getBanPoints(plugin.getBro().getUUID(args[0])) >= 100) {
                                        plugin.getBannedPlayers().ban(plugin.getBro().getUUID(args[0]), "TEAMING", -1);
                                        plugin.getBanManager().sendBanToSupporters(args[0], "TEAMING", sender.getName(), plugin.getBanManager().getRemainingTime(plugin.getBro().getUUID(args[0])));
                                    } else {
                                        plugin.getBannedPlayers().ban(plugin.getBro().getUUID(args[0]), "TEAMING", 24*(plugin.getBro().getBanPoints(plugin.getBro().getUUID(args[0]))/10)*60*60);
                                        plugin.getBanManager().sendBanToSupporters(args[0], "TEAMING", sender.getName(), (plugin.getBro().getBanPoints(plugin.getBro().getUUID(args[0]))/10) + " Tage");
                                    }
                                } else {
                                    sender.sendMessage(new TextComponent(plugin.getBanManager().isBanned));
                                }
                            }else{
                                sender.sendMessage(new TextComponent(plugin.getBanManager().isTeam));
                            }
                        } else {
                            sender.sendMessage(new TextComponent(plugin.getBanManager().notInDatabase));
                        }
                        break;
                    default:
                        sendUsage(sender);
                        break;
                }
                break;
            default:
                if (args.length > 2) {
                    if (args[1].equalsIgnoreCase("OTHER")) {
                        String playername = args[0];
                        StringBuilder reason = new StringBuilder();
                        for (int i = 2; i < args.length; i++) {
                            reason.append(args[i]).append(" ");
                        }
                        if(plugin.getBro().NameExists(playername)) {
                            if(!plugin.isTeammember(plugin.getProxy().getPlayer(args[0])) || !(sender instanceof ProxiedPlayer) || plugin.getBro().getGroup(((ProxiedPlayer) sender).getUniqueId().toString()).equalsIgnoreCase("ADMIN")) {
                                if(!plugin.getBannedPlayers().isBanned(plugin.getBro().getUUID(playername))) {
                                    plugin.getBro().addBanPoints(100, plugin.getBro().getUUID(playername));
                                    plugin.getBannedPlayers().ban(plugin.getBro().getUUID(playername), reason.toString(), -1);
                                    plugin.getBanManager().sendBanToSupporters(playername, reason.toString(), sender.getName(), plugin.getBanManager().getRemainingTime(plugin.getBro().getUUID(args[0])));
                                } else {
                                    sender.sendMessage(new TextComponent(plugin.getBanManager().isBanned));
                                }
                            }else{
                                sender.sendMessage(new TextComponent(plugin.getBanManager().isTeam));
                            }
                        } else {
                            sender.sendMessage(new TextComponent(plugin.getBanManager().notInDatabase));
                        }
                    } else {
                        sendUsage(sender);
                    }
                } else {
                    sendUsage(sender);
                }
                break;
        }
    }
    
    private void sendUsage(CommandSender sender) {
        if (sender instanceof ProxiedPlayer) {
            plugin.sendUsage("/ban <Spielername> <HACKING/BELEIDIGUNG/CHATVERGEHEN/TEAMING/OTHER> [Nachricht]", "/ban Schmidtchen HACKING", (ProxiedPlayer) sender);
        } else {
            sender.sendMessage(new TextComponent("[BroBans] /ban <Spielername> <HACKING/BELEIDIGUNG/CHATVERGEHEN/TEAMING/OTHER> [Nachricht]"));
        }
    }
    
    
    
}
