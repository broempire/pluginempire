/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.BanManager;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class KickCommand extends Command {

    private BungeeEmpire plugin;

    public KickCommand(BungeeEmpire plugin) {
        super("kick", "BungeeEmpire.support");
        this.plugin = plugin;
    }
    
    /*
     * 
     * /kick <Name> <Reason> | /kick Schmidtchen Das ist ein Test! 5
     * 
     */
    
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length >= 2) {
            if (plugin.getProxy().getPlayer(args[0]) != null) {
                if (!plugin.isTeammember(plugin.getProxy().getPlayer(args[0])) || !(sender instanceof ProxiedPlayer) || plugin.getBro().getGroup(((ProxiedPlayer) sender).getUniqueId().toString()).equalsIgnoreCase("ADMIN")) {
                    StringBuilder reason = new StringBuilder();
                    for (int i = 1; i < args.length; i++) {
                        reason.append(args[i]);
                        if (i < args.length - 1) {
                            reason.append(" ");
                        }
                    }
                    plugin.getProxy().getPlayer(args[0]).disconnect(new TextComponent(plugin.getBanManager().getKickMessage(reason.toString())));
                    plugin.getBanManager().sendKickToSupporters(args[0], reason.toString(), sender.getName());
                } else {
                    sender.sendMessage(new TextComponent(plugin.getBanManager().isTeam));
                }
            } else {
                sender.sendMessage(new TextComponent(plugin.getBanManager().prefix + "§cDer Spieler ist nicht online!"));
            }
        } else {
            if (sender instanceof ProxiedPlayer) {
                plugin.sendUsage("/kick <Name> <Grund>", "/kick McPvPLPHD Unangebrachtes Verhalten", (ProxiedPlayer) sender);
            } else {
                sender.sendMessage(new TextComponent("[BroBans] /kick <Name> <Grund>"));
            }
        }
    }
    
}
