/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.BanManager;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Matti
 */
public enum TimeUnit {
    
    SECOND ("Sekunde(n)", 1, "sec"),
    MINUTE ("Minute(n)", 60, "min"),
    HOUR ("Stunde(n)", 60*60, "hour"),
    DAY ("Tag(e)", 24*60*60, "day"),
    WEEK ("Woche(n)", 7*24*60*60, "week"),
    MONTH ("Monat(e)", 31*7*24*60*60, "mon");
    
    private final String name;
    private final long toSecond;
    private final String shortcut;

    private TimeUnit(String name, long toSecond, String shortcut) {
        this.name = name;
        this.toSecond = toSecond;
        this.shortcut = shortcut;
    }

    public String getName() {
        return name;
    }

    public long getToSecond() {
        return toSecond;
    }

    public String getShortcut() {
        return shortcut;
    }
    
    public static List<String> getUnitsAsString() {
        List<String> list = new ArrayList<>();
        for (TimeUnit unit : TimeUnit.values()) {
            list.add(unit.getShortcut().toLowerCase());
        }
        return list;
    }
    
    public static TimeUnit getUnit (String shortcut) {
        for (TimeUnit unit : TimeUnit.values()) {
            if (unit.getShortcut().toLowerCase().equals(shortcut.toLowerCase())) {
                return unit;
            }
        }
        return null;
    }
}
