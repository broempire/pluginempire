/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.TeamspeakManager;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.ChannelCreateEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDeletedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDescriptionEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelPasswordChangedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientLeaveEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.PrivilegeKeyUsedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ServerEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3Listener;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import de.broempire.bungeeempire.BungeeEmpire;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 *
 * @author Matti
 */
public class TeamspeakManager {
    
    private BungeeEmpire plugin;
    
    private TS3Config config;
    private TS3Query query;
    private TS3Api api;

    private List<String> badNames;
    private boolean started;
    private final Map<String, String> players;
    private final List<Integer> teammember;
    private final List<Integer> supporter;
    private final List<Integer> verifying;
    public String prefix = "§8▎ §3Teamspeak§8 ❘ §7";
    
    public TeamspeakManager(BungeeEmpire plugin) {
        this.started = false;
        this.players = new HashMap<>();
        this.badNames = new ArrayList<>();
        this.teammember = new ArrayList<>();
        this.supporter = new ArrayList<>();
        this.verifying = new ArrayList<>();
        this.plugin = plugin;
        
        badNames.add("");
        
        teammember.add(6);
        teammember.add(12);
        teammember.add(40);
        teammember.add(54);
        teammember.add(13);
        teammember.add(29);
        teammember.add(55);
        teammember.add(56);
        teammember.add(14);
        teammember.add(30);
        teammember.add(59);
        teammember.add(60);
        teammember.add(45);
        teammember.add(52);
        
        supporter.add(6);
        supporter.add(12);
        supporter.add(40);
        supporter.add(13);
        supporter.add(29);
        supporter.add(55);
        supporter.add(56);
        
        connect();
    }
    
    private void connect() {
        config = new TS3Config();
        config.setHost("localhost");
        config.setDebugLevel(Level.WARNING);
        
        query = new TS3Query(config);
        query.connect();
        
        api = query.getApi();
        api.login("serveradmin", "!BroEmpireTS");
        api.selectVirtualServerById(1);
        api.setNickname("BroBot");
        
        long current = System.currentTimeMillis();
        for (Client client : api.getClients()) {
            ClientInfo clientInfo = api.getClientInfo(client.getId());
            System.out.println(clientInfo.getNickname());
        }
        System.out.println(System.currentTimeMillis()-current);
        
        api.registerAllEvents();
        api.addTS3Listeners(new TS3Listener() {
            
            @Override
            public void onTextMessage(TextMessageEvent e) {
                String[] msgWords = e.getMessage().split(" ");
                int invokerID = e.getInvokerId();
                if (e.getTargetMode().equals(TextMessageTargetMode.CLIENT)) {
                    if (verifying.contains(invokerID)) {
                        if (msgWords.length == 1) {
                            if (plugin.getProxy().getPlayer(msgWords[0]) != null) {
                                if (!plugin.getPlayerData().hasEntry(plugin.getProxy().getPlayer(msgWords[0]).getUniqueId().toString(), "Teamspeak-ID")) {
                                    ProxiedPlayer player = plugin.getProxy().getPlayer(msgWords[0]);
                                    players.put(player.getName(), e.getInvokerUniqueId());
                                    player.sendMessage(new TextComponent(prefix + "§9" + api.getClientInfo(invokerID).getNickname() + " §7möchte sich mit dem Teamspeak verbinden!"));
                                    TextComponent text = new TextComponent(prefix + "Verifiziere dich mit folgendem Command: §6/verify§7!");
                                    text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Verifizieren!").create()));
                                    text.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/verify"));
                                    player.sendMessage(text);
                                    api.sendPrivateMessage(invokerID, "Wechsle nun auf deinen Ingame-Account und schließe die Verifizierung ab!");
                                    
                                } else {
                                    api.sendPrivateMessage(invokerID, "Dieser Spieler ist bereits verifiziert!");
                                }
                            } else {
                                api.sendPrivateMessage(invokerID, "Du musst auf dem Minecraft-Netzwerk online sein!");
                            }
                        } else {
                            api.sendPrivateMessage(invokerID, "Du musst mir nur deinen Spielernamen senden!");
                        }
                    }
                }
            }

            @Override
            public void onClientJoin(ClientJoinEvent e) {
                if (api.getClientInfo(e.getClientId()).isInServerGroup(8)) {
                    if (plugin.getBro().getGroup(plugin.getPlayerData().getPlayer("Teamspeak-ID", api.getClientInfo(e.getClientId()).getUniqueIdentifier())).equals("PREMIUM")) {
                        api.addClientToServerGroup(19, e.getClientDatabaseId());
                    } else {
                        if (api.getClientInfo(e.getClientId()).isInServerGroup(19)) {
                            api.removeClientFromServerGroup(19, e.getClientDatabaseId());
                        }
                    }
                }
                moveAFK();
            }

            @Override
            public void onClientLeave(ClientLeaveEvent e) {
                
            }

            @Override
            public void onServerEdit(ServerEditedEvent e) {
                
            }

            @Override
            public void onChannelEdit(ChannelEditedEvent e) {
                
            }

            @Override
            public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent e) {
                
            }

            @Override
            public void onClientMoved(ClientMovedEvent e) {
                System.out.println("[TeamspeakManager] Ein Client hat seinen Channel gewechselt! ChannelID: " + e.getTargetChannelId());
                if (e.getTargetChannelId() == 31 || e.getTargetChannelId() == 32) {
                    System.out.println("[TeamspeakManager] Der Support-Channel wurde betreten!");
                    if (!isTeammember(e.getClientId())) {
                        api.sendPrivateMessage(e.getClientId(), "Wir kümmern uns schnellstmöglich um dich!");
                        for (Client c : api.getClients()) {
                            if (isSupporter(c.getId()) && c.getChannelId() != 117) {
                                System.out.println("[Teamspeak-Manager] Es wird eine Nachricht an " + c.getNickname()+ " gesendet!");
                                api.sendPrivateMessage(c.getId(), "Ein Support-Channel wurde von einem Client betreten!");
                            }
                        }
                    }
                } else if (e.getTargetChannelId() == 3) {
                    verifying.add(e.getClientId());
                    api.sendPrivateMessage(e.getClientId(), "Herzlich willkommen!");
                    api.sendPrivateMessage(e.getClientId(), "Für die Verifizierung musst du sowohl auf dem Teamspeak, als auch auf dem Minecraft-Netzwerk online sein!");
                    api.sendPrivateMessage(e.getClientId(), "Wie ist dein Ingame-Name?");
                }
            }

            @Override
            public void onChannelCreate(ChannelCreateEvent e) {
                
            }

            @Override
            public void onChannelDeleted(ChannelDeletedEvent e) {
                
            }

            @Override
            public void onChannelMoved(ChannelMovedEvent e) {
                
            }

            @Override
            public void onChannelPasswordChanged(ChannelPasswordChangedEvent e) {
                
            }

            @Override
            public void onPrivilegeKeyUsed(PrivilegeKeyUsedEvent e) {
                
            }
        }); 
    }
    
    private void moveAFK() {
        if (!started) {
            started = true;
            List<Client> clientlist = api.getClients();
            for (Client c : api.getClients()) {
                if ((api.getChannelInfo(c.getChannelId()).isPermanent() || api.getChannelInfo(c.getChannelId()).isSemiPermanent()) && c.getChannelId() != 22 && c.getChannelId() != 117 && c.getChannelId() != 49 && c.getChannelGroupId() != 35) {
                    System.out.println("canBeMoved");
                    if (c.getIdleTime() >= 300000 || c.isAway()) {
                        if (isTeammember(c.getId())) {
                            api.moveClient(c.getId(), 22);
                        } else {
                            api.moveClient(c.getId(), 49);
                        }
                        api.pokeClient(c.getId(), "Du wurdest in die AFK-Lobby verschoben, da du " + (c.isAway() ? "auf AFK geschaltet bist" : "jetzt seit 5 Minuten AFK bist!"));
                        clientlist.remove(c);
                    }
                }
            }
            long max = Long.MIN_VALUE;
            for (Client c : clientlist) {
                if (c.getChannelId() == 3) {
                    if (c.getIdleTime() > max) {
                        max = c.getIdleTime();
                    }
                }
            }
            try {
                Thread.sleep(300000-max);
            } catch (InterruptedException ex) {
                Logger.getLogger(TeamspeakManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            moveAFK();
        }
    }
    
    public Map<String, String> getVerifyRequests() {
        for (Entry<String, String> entry : players.entrySet()) {
            System.out.println(entry.getKey() + " » " + entry.getValue());
        }
        return players;
    } 
    
    public void completeVerification(ProxiedPlayer player, String id) {
        players.remove(player.getName());
        api.addClientToServerGroup(8, api.getDatabaseClientByUId(id).getDatabaseId());
        if (plugin.getBro().getGroup(player.getUniqueId().toString()).equals("PREMIUM")) {
            api.addClientToServerGroup(19, api.getDatabaseClientByUId(id).getDatabaseId());
        }
        player.sendMessage(new TextComponent(prefix + "Du wurdest erfolgreich verifiziert!"));
        if (api.getClientByUId(id) != null) {
            api.sendPrivateMessage(api.getClientByUId(id).getId(), "Du wurdest erfolgreich verifiziert!");
        }
        verifying.remove(api.getClientByUId(id).getId());
    }
    
    public void unverify(String id) {
        api.removeClientFromServerGroup(8, api.getDatabaseClientByUId(id).getDatabaseId());
    }
    
    private boolean isTeammember(int clientID) {
        ClientInfo clientInfo = api.getClientInfo(clientID);
        for (int member : teammember) {
            if (clientInfo.isInServerGroup(member)) {
                System.out.println("isTeammember = true");
                return true;
            }
        }
        System.out.println("isTeammember = false");
        return false;
    }
    
    private boolean isSupporter(int clientID) {
        ClientInfo clientInfo = api.getClientInfo(clientID);
        for (int group : supporter) {
            if (clientInfo.isInServerGroup(group)) {
                System.out.println("isSupporter = true");
                System.out.println(clientInfo.getNickname());
                return true;
            }
        }
        System.out.println("isSupporter = false");
        return false;
    }
    
}
