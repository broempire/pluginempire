/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.TeamspeakManager;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class VerifyCommand extends Command {

    BungeeEmpire plugin;

    public VerifyCommand(BungeeEmpire plugin) {
        super("verify");
        this.plugin = plugin;
    }
    
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            switch (args.length) {
                case 0:
                    if (plugin.getTeamspeakManager().getVerifyRequests().containsKey(player.getName())) {
                        plugin.getPlayerData().setValue(player.getUniqueId().toString(), "Teamspeak-ID", plugin.getTeamspeakManager().getVerifyRequests().get(player.getName()));
                        plugin.getTeamspeakManager().completeVerification(player, plugin.getTeamspeakManager().getVerifyRequests().get(player.getName()));
                    } else {
                        player.sendMessage(new TextComponent(plugin.getTeamspeakManager().prefix + "§cBitte starte die Verifizierung im Teamspeak!"));
                    }   
                    break;
                case 1:
                    if (args[0].equalsIgnoreCase("remove")) {
                        if (plugin.getPlayerData().hasEntry(player.getUniqueId().toString(), "Teamspeak-ID")) {
                            plugin.getPlayerData().deleteEntry(player.getUniqueId().toString(), "Teamspeak-ID");
                            System.out.println(plugin.getPlayerData().getValue(player.getUniqueId().toString(), "Teamspeak-ID"));
                            plugin.getTeamspeakManager().unverify((String) plugin.getPlayerData().getValue(player.getUniqueId().toString(), "Teamspeak-ID"));
                            player.sendMessage(new TextComponent(plugin.getTeamspeakManager().prefix + "Du bist nun nicht mehr verifiziert!"));
                        }
                    } else {
                        plugin.sendUsage("/verify [remove]", null, player);
                    }   
                    break;
                default:
                    plugin.sendUsage("/verify [remove]", null, player);
                    break;
            }
        }
    }
    
}
