/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class BrollarCommand extends Command{

    private BungeeEmpire plugin;
    
    public BrollarCommand(BungeeEmpire plugin) {
        super("brollar");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        switch (args.length) {
            case 0:
                if (sender instanceof ProxiedPlayer) {
                    ProxiedPlayer player = (ProxiedPlayer) sender;
                    int brollar = plugin.getBro().getCoins(player.getUniqueId().toString());
                    player.sendMessage(new TextComponent(plugin.prefix + "Du besitzt §6" + brollar + " §7Brollar!"));
                } else {
                    System.out.println("[BungeeEmpire] Diesen Befehl kann nur ein Spieler ausführen!");
                }
                break;
            case 1:
                plugin.sendUsage("/brollar", null, (ProxiedPlayer) sender);
                break;
            case 2:
                if (sender instanceof ProxiedPlayer) {
                    ProxiedPlayer player = (ProxiedPlayer) sender;
                    if (player.hasPermission("BungeeEmpire.team")) {
                        plugin.sendUsage("/brollar <Spielername> <set/add/remove> <Zahl>", "/brollar Schmidtchen remove 10", player);
                    } else {
                        player.sendMessage(new TextComponent(plugin.noPerms));
                    }
                }
                break;
            case 3:
                if (sender instanceof ProxiedPlayer) {
                    ProxiedPlayer player = (ProxiedPlayer) sender;
                    if (player.hasPermission("BungeeEmpire.team")) {
                        try {
                            switch (args[1]) {
                                case "set":
                                    if (plugin.getBro().getUUID(args[0]) != null) {
                                        plugin.getBro().setCoins(Integer.parseInt(args[2]), plugin.getBro().getUUID(args[0]));
                                        player.sendMessage(new TextComponent(plugin.prefix + "§aBrollar wurden gesetzt!"));
                                    } else {
                                        player.sendMessage(new TextComponent(plugin.prefix + "§cDieser Spieler existiert nicht in der Datenbank!"));
                                    }
                                    break;
                                case "add":
                                    if (plugin.getBro().getUUID(args[0]) != null) {
                                        plugin.getBro().setCoins(plugin.getBro().getCoins(plugin.getBro().getUUID(args[0])) + Integer.parseInt(args[2]), plugin.getBro().getUUID(args[0]));
                                        player.sendMessage(new TextComponent(plugin.prefix + "§aBrollar wurden addiert!"));
                                    } else {
                                        player.sendMessage(new TextComponent(plugin.prefix + "§cDieser Spieler existiert nicht in der Datenbank!"));
                                    }
                                    break;
                                case "remove":
                                    if (plugin.getBro().getUUID(args[0]) != null) {
                                        plugin.getBro().setCoins(plugin.getBro().getCoins(plugin.getBro().getUUID(args[0])) - Integer.parseInt(args[2]), plugin.getBro().getUUID(args[0]));
                                        player.sendMessage(new TextComponent(plugin.prefix + "§aBrollar wurden abgezogen!"));
                                    } else {
                                        player.sendMessage(new TextComponent(plugin.prefix + "§cDieser Spieler existiert nicht in der Datenbank!"));
                                    }
                                    break;
                                default:
                                    plugin.sendUsage("/brollar <Spielername> <set/add/remove> <Zahl>", "/brollar Schmidtchen remove 10", player);
                                    break;
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            player.sendMessage(new TextComponent(plugin.prefix + "§cDu musst eine Zahl angeben!"));
                        }
                    } else {
                        player.sendMessage(new TextComponent(plugin.noPerms));
                    }
                } else {
                    System.out.println("[BungeeEmpire] Diesen Befehl kann nur ein Spieler ausführen!");
                }
                break;
            default:
                if (sender instanceof ProxiedPlayer) {
                    plugin.sendUsage("/brollar", null, (ProxiedPlayer) sender);
                } else {
                    System.out.println("[BungeeEmpire] Diesen Befehl kann nur ein Spieler ausführen!");
                }
                break;
        }
    }
    
    
    
}
