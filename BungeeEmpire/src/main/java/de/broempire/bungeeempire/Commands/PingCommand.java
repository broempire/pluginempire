/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class PingCommand extends Command{
    
    private BungeeEmpire plugin;
    
    public PingCommand(BungeeEmpire plugin) {
        super("ping");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        player.sendMessage(new TextComponent(plugin.prefix + "§7Dein Ping beträgt: §a" + player.getPing() + "ms§7!"));
    }
    
}
