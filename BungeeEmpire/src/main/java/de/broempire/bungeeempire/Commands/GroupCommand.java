/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BanManager.TimeUnit;
import de.broempire.bungeeempire.BungeeEmpire;
import de.broempire.bungeeempire.MySQL.UUIDFetcher;
import java.util.List;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class GroupCommand extends Command{

    private BungeeEmpire plugin;
    
    public GroupCommand(BungeeEmpire plugin) {
        super("group", "BungeeEmpire.team");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        switch (args.length) {
            case 0:
                if (sender instanceof ProxiedPlayer) {
                    plugin.sendUsage("/group <Spielername> <Gruppe>", "/group Schmidtchen ADMIN", (ProxiedPlayer) sender);
                } else {
                    System.out.println("[BungeeEmpire] Falsche Benutzung!");
                }
                break;
            case 1:
                if (sender instanceof ProxiedPlayer) {
                    ProxiedPlayer player = (ProxiedPlayer) sender;
                    if (plugin.getBro().getUUID(args[0]) != null) {
                        String uuid = plugin.getBro().getUUID(args[0]);
                        String group = plugin.getBro().getGroup(uuid);
                        player.sendMessage(new TextComponent(plugin.prefix + "Der Spieler ist in der Gruppe §6" + group + "§7!"));
                    } else {
                        player.sendMessage(new TextComponent(plugin.prefix + "§cDieser Spieler existiert nicht in der Datenbank!"));
                    }
                } else {
                    if (plugin.getBro().getUUID(args[0]) != null) {
                        String uuid = plugin.getBro().getUUID(args[0]);
                        String group = plugin.getBro().getGroup(uuid);
                        sender.sendMessage(new TextComponent("[BungeeEmpire] Gruppe von " + args[0] + ": " + group));
                    } else {
                        sender.sendMessage(new TextComponent("[BungeeEmpire] Dieser Spieler existiert nicht in der Datenbank!"));
                    }
                }
                break;
            case 2:
                if (sender instanceof ProxiedPlayer) {
                    ProxiedPlayer player = (ProxiedPlayer) sender;
                    String uuid;
                    if (plugin.getBro().getUUID(args[0]) != null) {
                        uuid = plugin.getBro().getUUID(args[0]);
                    } else {
                        uuid = UUIDFetcher.getUUID(args[0]).toString();
                        plugin.getBro().create(uuid);
                    }
                    if (player.hasPermission("BungeeEmpire.groups.set")) {
                        if (args[1].equalsIgnoreCase("PREMIUM") || args[1].equalsIgnoreCase("PLAYER") || args[1].equalsIgnoreCase("VIP") || args[1].equalsIgnoreCase("BUILDER") || args[1].equalsIgnoreCase("FORENMOD") || args[1].equalsIgnoreCase("JRMODERATOR") || args[1].equalsIgnoreCase("MODERATOR")) {
                            plugin.getBro().setGroup(args[1].toUpperCase(), uuid);
                            player.sendMessage(new TextComponent(plugin.prefix + "§aGruppe gesetzt!"));
                        } else if (args[1].equalsIgnoreCase("LEADMODERATOR") || args[1].equalsIgnoreCase("ADMIN") || args[1].equalsIgnoreCase("DEVELOPER") || args[1].equalsIgnoreCase("LEADDEVELOPER") || args[1].equalsIgnoreCase("LEADFORENMOD") || args[1].equalsIgnoreCase("LEADBUILDER")) {
                            if (plugin.getBro().getGroup(player.getUniqueId().toString()).equalsIgnoreCase("ADMIN")) {
                                plugin.getBro().setGroup(args[1].toUpperCase(), uuid);
                                player.sendMessage(new TextComponent(plugin.prefix + "§aGruppe gesetzt!"));
                            } else {
                                player.sendMessage(new TextComponent(plugin.noPerms));
                            }
                        } else {
                            player.sendMessage(new TextComponent(plugin.prefix + "§cBitte wähle einen möglichen Rang aus!"));
                        }
                    } else {
                        player.sendMessage(new TextComponent(plugin.noPerms));
                    }
                } else {
                    String uuid;
                    if (plugin.getBro().getUUID(args[0]) != null) {
                        uuid = plugin.getBro().getUUID(args[0]);
                    } else {
                        uuid = UUIDFetcher.getUUID(args[0]).toString();
                        plugin.getBro().create(uuid);
                    }
                    plugin.getBro().setGroup(args[1].toUpperCase(), uuid);
                    sender.sendMessage(new TextComponent("[BungeeEmpire] Gruppe gesetzt!"));
                }
                break;
            case 4:
                if (sender instanceof ProxiedPlayer) {
                    ProxiedPlayer player = (ProxiedPlayer) sender;
                    String uuid;
                    if (plugin.getBro().getUUID(args[0]) != null) {
                        uuid = plugin.getBro().getUUID(args[0]);
                    } else {
                        uuid = UUIDFetcher.getUUID(args[0]).toString();
                        plugin.getBro().create(uuid);
                    }
                    if (player.hasPermission("BungeeEmpire.groups.set")) {
                        if (args[1].equalsIgnoreCase("PREMIUM")) {
                            try {
                                String shortcut = args[3];
                                List<String> unitList = TimeUnit.getUnitsAsString();
                                if (unitList.contains(shortcut.toLowerCase())) {
                                    Long value = Long.valueOf(args[2]);
                                    TimeUnit unit = TimeUnit.getUnit(shortcut);
                                    plugin.getBro().setPremium(uuid, System.currentTimeMillis() + (value*unit.getToSecond()*1000));
                                    player.sendMessage(new TextComponent(plugin.prefix + "§aDu hast dem Spieler für §e" + value + " " + unit.getName() + " §aPremium gegeben!"));
                                } else {
                                    player.sendMessage(new TextComponent(plugin.prefix + "&cDiese Einheit existiert nicht!"));
                                }
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        } else {
                            player.sendMessage(new TextComponent(plugin.prefix + "§cBitte wähle einen möglichen Rang aus!"));
                        }
                    } else {
                        player.sendMessage(new TextComponent(plugin.noPerms));
                    }
                } else {
                    String uuid;
                    if (plugin.getBro().getUUID(args[0]) != null) {
                        uuid = plugin.getBro().getUUID(args[0]);
                    } else {
                        uuid = UUIDFetcher.getUUID(args[0]).toString();
                        plugin.getBro().create(uuid);
                    }
                    try {
                        String shortcut = args[3];
                        List<String> unitList = TimeUnit.getUnitsAsString();
                        if (unitList.contains(shortcut.toLowerCase())) {
                            Long value = Long.valueOf(args[2]);
                            TimeUnit unit = TimeUnit.getUnit(shortcut);
                            plugin.getBro().setPremium(uuid, System.currentTimeMillis() + (value*unit.getToSecond()*1000));
                            sender.sendMessage(new TextComponent("[BungeeEmpire] Du hast dem Spieler für " + value + " " + unit.getName() + " Premium gegeben!"));
                        } else {
                            sender.sendMessage(new TextComponent("[BungeeEmpire] Diese Einheit existiert nicht!"));
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
                break;
            default:
                if (sender instanceof ProxiedPlayer) {
                    plugin.sendUsage("/group <Spielername> [Gruppe]", "/group Schmidtchen ADMIN", (ProxiedPlayer) sender);
                } else {
                    sender.sendMessage(new TextComponent("[BungeeEmpire] Falsche Benutzung!"));
                }
                break;
        }
    }
    
}
