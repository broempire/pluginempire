/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class HelpCommand extends Command {

    private BungeeEmpire plugin;

    public HelpCommand(BungeeEmpire plugin) {
        super("help");
        this.plugin = plugin;
    }
    
    @Override
    public void execute(CommandSender sender, String[] args) {
        sender.sendMessage(new TextComponent(plugin.prefix + "§8§m-                       -"));
        sender.sendMessage(new TextComponent(" "));
        sender.sendMessage(new TextComponent("§2Du befindest dich auf dem"));
        sender.sendMessage(new TextComponent("§2Servernetzwerk §6BroEmpire.de§2!"));
        sender.sendMessage(new TextComponent(" "));
        sender.sendMessage(new TextComponent("§bSchreibe bei Problemen einfach"));
        sender.sendMessage(new TextComponent("§bein Teammitglied an!"));
        sender.sendMessage(new TextComponent(" "));
        sender.sendMessage(new TextComponent("§3/alliance zum Gründen eines Bündnisses"));
        sender.sendMessage(new TextComponent("§3/brollar zum Abfragen deiner Brollar"));
        sender.sendMessage(new TextComponent("§3/lobby zum Verlassen eines Gameservers"));
        sender.sendMessage(new TextComponent("§3/support zum Kontaktieren des Supports"));
        sender.sendMessage(new TextComponent(" "));
        sender.sendMessage(new TextComponent(plugin.prefix + "§8§m-                       -"));
    }
}
