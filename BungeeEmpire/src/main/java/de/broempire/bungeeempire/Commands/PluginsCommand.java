/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class PluginsCommand extends Command{

    private BungeeEmpire plugin;

    public PluginsCommand(BungeeEmpire plugin) {
        super("plugins", null, "pl");
        this.plugin = plugin;
    }
    
    @Override
    public void execute(CommandSender sender, String[] args) {
        sender.sendMessage(new TextComponent(plugin.prefix + "Unsere Plugins sind hauptsächlich selbst \n§7programmiert worden!"));
    }
    
}
