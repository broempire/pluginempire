/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class BroadcastCommand extends Command{
    
    private BungeeEmpire plugin;
    
    public BroadcastCommand(BungeeEmpire plugin) {
        super("broadcast", "BungeeEmpire.broadcast", "bc", "alert");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        
        if (args.length > 0) {
            StringBuilder msg = new StringBuilder("§8▎ §cBroadcast§8 ❘ §7");
            for (int i = 0; i < args.length; i++) {
                msg.append(args[i]).append(" ");
            }
            plugin.getProxy().broadcast(new TextComponent(ChatColor.translateAlternateColorCodes('&', msg.toString())));
        } else {
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) sender;
                player.sendMessage(new TextComponent("§cBenutzung: §6/broadcast [Nachricht]"));
            } else {
                sender.sendMessage(new TextComponent("§cBenutzung: §6/broadcast [Nachricht]"));
            }
            
        }
    }
    
}
