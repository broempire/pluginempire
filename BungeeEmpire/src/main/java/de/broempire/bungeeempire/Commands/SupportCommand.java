/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class SupportCommand extends Command {
    
    private final BungeeEmpire plugin;
    
    private final Map<ProxiedPlayer, List<String>> tickets = new HashMap<>();

    public SupportCommand(BungeeEmpire plugin) {
        super("support");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            if (!plugin.isSupporter(player)) {
                switch (args.length) {
                    case 0:
                        plugin.sendUsage("/support <Nachricht>", "/support Ich stecke fest!", player);
                        break;
                    default:
                        StringBuilder msg = new StringBuilder();
                        for (int i = 0; i < args.length; i++) {
                            msg.append(args[i]).append(" ");
                        }
                        List<String> ticket = tickets.containsKey(player) ? tickets.get(player) : new ArrayList<>();
                        ticket.add(msg.toString());
                        tickets.put(player, ticket);
                        plugin.sendMessageToSupporters(new TextComponent(player.getDisplayName() + "§8» §f" + msg.toString()));
                        player.sendMessage(new TextComponent("§8▎ §6Support§8 ❘ §7Dein Ticket wurde erfolgreich §aerstellt§7!"));
                        break;
                }
            } else {
                switch (args.length) {
                    case 0:
                        plugin.sendUsage("/support <Spielername/list> [Nachricht]", "/support Schmidtchen ND!", player);
                        break;
                    case 1:
                        if (args[0].equalsIgnoreCase("list")) {
                            if (tickets.isEmpty()) {
                                player.sendMessage(new TextComponent("§8▎ §6Support§8 ❘ §aKeine offenen Tickets!"));
                            } else {
                                player.sendMessage(new TextComponent("§8▎ §6Support§8 ❘ §7Alle offenen Tickets:"));
                            }
                            for (Entry<ProxiedPlayer, List<String>> entry : tickets.entrySet()) {
                                for (String msg : entry.getValue()) {
                                    TextComponent ticket = new TextComponent("§8● ");
                                    ticket.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/support " + entry.getKey().getName()));
                                    ticket.addExtra(new TextComponent(entry.getKey().getDisplayName() + " §8» §7" + msg));
                                    player.sendMessage(ticket);
                                }
                            }
                        } else {
                            plugin.sendUsage("/support <Spielername/list> [Nachricht]", "/support Schmidtchen ND!", player);
                        }
                        break;
                    default:
                        if (plugin.getProxy().getPlayer(args[0]) != null) {
                            if (tickets.containsKey(plugin.getProxy().getPlayer(args[0]))) {
                                tickets.remove(plugin.getProxy().getPlayer(args[0]));
                            }
                            StringBuilder msg = new StringBuilder();
                            for (int i = 1; i < args.length; i++) {
                                msg.append(args[i]).append(" ");
                            }
                            plugin.sendMessageToSupporters(new TextComponent("§aDas Ticket von " + plugin.getProxy().getPlayer(args[0]).getDisplayName() + " §awurde bearbeitet!"));
                            plugin.getProxy().getPlayer(args[0]).sendMessage(new TextComponent("§8▎ §6Support§8 ❘ §7" + msg.toString()));
                        } else {
                            player.sendMessage(new TextComponent("§8▎ §6Support§8 ❘ §cDer Spieler existiert nicht!"));
                        }
                        break;
                }
            }
        }
    }
    
}
