/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class BuildCommand extends Command{

    private BungeeEmpire plugin;
    
    public BuildCommand(BungeeEmpire plugin) {
        super("build", "BungeeEmpire.team");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            player.connect(plugin.getProxy().getServerInfo("bauserver"));
            player.sendMessage(new TextComponent(plugin.prefix + "Verbinde mit §6BAUSERVER§7..."));
        }
    }
    
}
