/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class TeamchatCommand extends Command{

    private BungeeEmpire plugin;
    
    public TeamchatCommand(BungeeEmpire plugin) {
        super("teamchat", "BungeeEmpire.team", "tc");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if (args.length > 0) {
            TextComponent msg = new TextComponent(player.getDisplayName() + " §7>> §r");
            for (int i = 0; i < args.length; i++) {
                msg.addExtra(ChatColor.translateAlternateColorCodes('&', args[i]));
                msg.addExtra(" ");
            }
            plugin.sendMessageToTeam(msg);
        } else {
            player.sendMessage(new TextComponent("§cBenutzung: §6/teamchat [Nachricht]"));
        }
    }
    
}
