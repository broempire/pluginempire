/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import de.broempire.bungeeempire.party.Party;
import static de.broempire.bungeeempire.party.PartyManager.prefix;
import java.util.List;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */

public class AllianceCommand extends Command {
	
    private BungeeEmpire plugin;
    
	public AllianceCommand(BungeeEmpire plugin) {
            super("alliance", null, "a");
            this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxiedPlayer) {
                    ProxiedPlayer p = (ProxiedPlayer) sender;
                    if (args.length > 1) {
                        if (args[0].equalsIgnoreCase("c")) {
                                if (plugin.getPartyManager().hasParty(p)) {
                                    Party playerparty = plugin.getPartyManager().getParty(p);
                                    StringBuilder msg = new StringBuilder(p.getDisplayName() + " §8>> §f");
                                    for (int i = 1; i < args.length; i++) {
                                        msg.append(args[i]).append(" ");
                                    }
                                    playerparty.sendMessage(msg.toString());
                                } else {
                                    p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist in keiner Allianz!"));
                                }
                                return;
                        }
                    }
                    switch (args.length) {
                        case 0:
                            sendHelp(p);
                            break;
                        case 1:
                            switch(args[0]) {
                                case "list":
                                    if (plugin.getPartyManager().hasParty(p)) {
                                        Party party = plugin.getPartyManager().getParty(p);
                                        p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§5Leiter: §7" + party.getOwner().getDisplayName()));
                                        StringBuilder members = new StringBuilder();
                                        members.append(plugin.getPartyManager().prefix + "§6Mitglieder: §7");
                                        List<ProxiedPlayer> players = party.getMembers();
                                        System.out.println(players.size());
                                        if (players.size() < 2) {
                                            members.append("§cKEINE");
                                        } else {
                                            int i = players.size();
                                            for (ProxiedPlayer player : players) {
                                               System.out.println(player.getName());
                                               members.append(player.getDisplayName());
                                               if (i > 1) {
                                                   members.append("§7, ");
                                               }
                                               i--;
                                            }
                                        }
                                        p.sendMessage(new TextComponent(members.toString()));
                                    } else {
                                        p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist in keiner Allianz!"));
                                    }
                                    break;
                                case "leave":
                                    if (plugin.getPartyManager().hasParty(p)) {
                                        Party party = plugin.getPartyManager().getParty(p);
                                        if (party.getOwner() == p) {
                                            if (party.getMembers().size() > 1) {
                                                plugin.getPartyManager().removeFromParty(p, party);
                                                p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist aus der Allianz ausgetreten!"));
                                                party.sendMessage(p.getDisplayName() + " §7ist aus der Allianz ausgetreten!");
                                                party.setOwner(party.getMembers().get(0));
                                                party.sendMessage("Der neue Leiter ist: " + party.getOwner().getDisplayName());
                                            } else {
                                                plugin.getPartyManager().removeFromParty(p, party);
                                                p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDie Allianz wurde aufgelöst!"));
                                            }
                                        } else {
                                            plugin.getPartyManager().removeFromParty(p, party);
                                            p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist aus der Allianz ausgetreten!"));
                                            party.sendMessage(p.getDisplayName() + " §7ist aus der Allianz ausgetreten!");
                                        }
                                    } else {
                                        p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist in keiner Allianz!"));
                                    }
                                    break;
                                default:
                                    sendHelp(p);
                                    break;    
                            }
                            break;
                        case 2:
                            switch (args[0]) {
                                case "invite":
                                    Party party;
                                    if (!plugin.getPartyManager().hasParty(p)) {
                                        party = new Party(p);
                                        plugin.getPartyManager().addToParty(p, party);
                                    } else {
                                        party = plugin.getPartyManager().getParty(p);
                                    }
                                    if (party.getOwner() == p) {
                                        if (plugin.getProxy().getPlayer(args[1]) != null) {
                                            ProxiedPlayer t = plugin.getProxy().getPlayer(args[1]);
                                            if (!plugin.getPartyManager().hasParty(t)) {
                                                plugin.getPartyManager().inviteToParty(t, party);
                                            } else {
                                                p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDer Spieler ist bereits in einer Allianz!"));
                                            }
                                        } else {
                                            p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDer Spieler ist nicht online!"));
                                        }
                                    } else {
                                        p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist nicht der Bündnisleiter!"));
                                    }
                                    break;
                                case "accept":
                                    if (!plugin.getPartyManager().hasParty(p)) {
                                        if (plugin.getPartyManager().hasInvitation(p)) {
                                            if (plugin.getProxy().getPlayer(args[1]) != null) {
                                                ProxiedPlayer t = plugin.getProxy().getPlayer(args[1]);
                                                if (plugin.getPartyManager().getPartyInvitation(p) == t) {
                                                    if (plugin.getPartyManager().getParty(t) != null) {
                                                        if (plugin.getPartyManager().getParty(t).getMembers().size() <= 10) {
                                                            plugin.getPartyManager().addToParty(p, plugin.getPartyManager().getParty(t));
                                                        } else {
                                                            p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDie Allianz ist voll!"));
                                                        }
                                                    } else {
                                                        p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDer Spieler ist in keiner Allianz!"));
                                                    }
                                                } else {
                                                    p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu wurdest nicht eingeladen von diesem Spieler!"));
                                                }
                                            } else {
                                                p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDer Spieler ist nicht online!"));
                                            }
                                        } else {
                                            p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu hast keine Einladung!"));
                                        }
                                    } else {
                                        p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist bereits in einer Allianz!")); 
                                    }
                                    break;
                                case "deny":
                                    if (plugin.getProxy().getPlayer(args[1]) != null) {
                                        ProxiedPlayer t = plugin.getProxy().getPlayer(args[1]);
                                        if (plugin.getPartyManager().getPartyInvitation(p) == t) {
                                            p.sendMessage(new TextComponent(prefix + "Du hast die Bündnisanfrage §cabgelehnt§7!"));
                                            t.sendMessage(new TextComponent(prefix + p.getDisplayName() + " §7hat die Bündnisanfrage §cabgelehnt§7!"));
                                            plugin.getPartyManager().removeInvitation(p);
                                        } else {
                                            p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu wurdest nicht eingeladen von diesem Spieler!"));
                                        }
                                    } else {
                                        p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDer Spieler ist nicht online!"));
                                    }
                                    break;
                                case "kick":
                                    if (plugin.getPartyManager().hasParty(p)) {
                                        Party playerparty = plugin.getPartyManager().getParty(p);
                                        if (plugin.getPartyManager().getParty(p).getOwner() == p) {
                                            if (plugin.getProxy().getPlayer(args[1]) != null) {
                                                ProxiedPlayer t = plugin.getProxy().getPlayer(args[1]);
                                                if (playerparty.getMembers().contains(t)) {
                                                   plugin.getPartyManager().removeFromParty(t, playerparty);
                                                   playerparty.sendMessage(t.getDisplayName() + " §7wurde §crausgeworfen§7!");
                                                   t.sendMessage(new TextComponent(prefix + "§cDu wurdest aus der Allianz gekickt!"));
                                                } else {
                                                    p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDer Spieler ist nicht in deiner Allianz!"));
                                                }
                                            } else {
                                                p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDer Spieler ist nicht online!"));
                                            }
                                        } else {
                                            p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist nicht der Bündnisleiter!"));
                                        }
                                    } else {
                                        p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist in keiner Allianz!"));
                                    }
                                    break;
                                case "promote":
                                    if (plugin.getPartyManager().hasParty(p)) {
                                        Party playerparty = plugin.getPartyManager().getParty(p);
                                        if (playerparty.getOwner() == p) {
                                            if (plugin.getProxy().getPlayer(args[1]) != null) {
                                                ProxiedPlayer t = plugin.getProxy().getPlayer(args[1]);
                                                if (plugin.getPartyManager().getParty(t) == playerparty) {
                                                    playerparty.setOwner(t);
                                                    playerparty.sendMessage("Der neue Bündnisleiter ist: " + t.getDisplayName());
                                                    t.sendMessage(new TextComponent(prefix + "§aDu bist nun der Bündnisleiter!"));
                                                } else {
                                                    p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDer Spieler ist nicht in deiner Allianz!"));
                                                }
                                            } else {
                                                p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDer Spieler ist nicht online!"));
                                            }
                                        } else {
                                            p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist nicht der Bündnisleiter!"));
                                        }
                                    } else {
                                        p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§cDu bist in keiner Allianz!"));
                                    }
                                    break;
                                default:
                                    sendHelp(p);
                                    break;
                            }
                            break;
                        default:
                            if (!args[0].equalsIgnoreCase("c")) {
                                sendHelp(p);
                            }
                            break;
                        }
                    }
                }
        
        private void sendHelp(ProxiedPlayer p) {
            p.sendMessage(new TextComponent(plugin.getPartyManager().prefix + "§bBefehle:"));
            p.sendMessage(new TextComponent("§8● §b/alliance invite <Spieler> §7Spieler in deine Allianz einladen"));
            p.sendMessage(new TextComponent("§8● §b/alliance accept <Spieler §7Bündnisanfrage annehmen"));
            p.sendMessage(new TextComponent("§8● §b/alliance deny <Spieler> §7Bündnisanfrage ablehnen"));
            p.sendMessage(new TextComponent("§8● §b/alliance list §7Alle Allianzmitglieder auflisten"));
            p.sendMessage(new TextComponent("§8● §b/alliance leave §7Allianz verlassen"));
            p.sendMessage(new TextComponent("§8● §b/alliance kick <Spieler> §7Spieler aus der Allianz werfen"));
            p.sendMessage(new TextComponent("§8● §b/alliance promote <Spieler> §7Neuen Bündnisleiter bestimmen"));
            p.sendMessage(new TextComponent("§8● §b/alliance c <Nachricht> §7Schreibe deiner Allianz"));
        }
}
