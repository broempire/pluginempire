/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class LobbyCommand extends Command{

    private BungeeEmpire plugin;
    
    public LobbyCommand(BungeeEmpire plugin) {
        super("lobby", null, "hub", "l", "leave");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if (player.getServer().getInfo().getName().equalsIgnoreCase("lobby01")) {
            player.sendMessage(new TextComponent(plugin.prefix + "§7Du befindest dich bereits in der §aLobby§7!"));
        } else {
            player.connect(plugin.getProxy().getServerInfo("lobby01"));
            player.sendMessage(new TextComponent(plugin.prefix + "§7Du befindest dich nun in der §aLobby§7!"));
        }
    }

    
    
}
