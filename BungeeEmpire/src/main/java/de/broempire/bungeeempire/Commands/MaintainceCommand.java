/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class MaintainceCommand extends Command{
    
    private final BungeeEmpire plugin;
    
    public MaintainceCommand(BungeeEmpire plugin) {
        super("wartung", "BungeeEmpire.maintaince.use", "maintaince");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if (args.length == 0) {
            if (!plugin.isMaintaince()) {
                plugin.setMaintaince(true);
                player.sendMessage(new TextComponent(plugin.prefix + "§7Wartung wurde §aaktiviert!"));
                plugin.getProxy().getPlayers().stream().filter((online) -> (!online.hasPermission("BungeeEmpire.maintaince.bypass"))).forEach((online) -> {
                    online.disconnect(new TextComponent("§8§m------------------------------\n§r§6BroEmpire.de §cist §loffline!\n§7Grund: §cWir befinden uns in Wartungsarbeiten!\n§7Mehr Informationen gibt es auf unserem Twitter-Account: §b@BroEmpireMC\n§8§m------------------------------"));
                });
                plugin.getProxy().broadcast(new TextComponent(plugin.prefix + "Der Server befindet sich nun in §cWartung§7!"));
            } else {
                plugin.setMaintaince(false);
                player.sendMessage(new TextComponent(plugin.prefix + "§7Wartung wurde §cdeaktiviert!"));
                plugin.getProxy().broadcast(new TextComponent(plugin.prefix + "Der Server befindet sich nun nicht mehr in §aWartung§7!"));
            }
        } else if (args.length == 1 && args[0].equalsIgnoreCase("status")) {
            if (plugin.isMaintaince()) {
                player.sendMessage(new TextComponent(plugin.prefix + "§7Wartung ist §aaktiviert§7!"));
            } else {
                player.sendMessage(new TextComponent(plugin.prefix + "§7Wartung ist §cdeaktiviert§7!"));
            }
        } else {
            player.sendMessage("§8--------§7»§6Benutzung§7«§8--------");
            player.sendMessage(" ");
            player.sendMessage("§4/wartung [status]");
            player.sendMessage(" ");
            player.sendMessage("§8--------§7»§6Benutzung§7«§8--------");
        }
    }
}
