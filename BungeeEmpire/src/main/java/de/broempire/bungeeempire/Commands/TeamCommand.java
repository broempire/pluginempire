/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class TeamCommand extends Command{

    private BungeeEmpire plugin;
    
    public TeamCommand(BungeeEmpire plugin) {
        super("team", "BungeeEmpire.team");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer p = (ProxiedPlayer) sender;
        p.sendMessage(new TextComponent(plugin.prefix + "Folgende Teammitglieder sind momentan online:"));
        plugin.getProxy().getPlayers().stream().filter((pp) -> (pp.hasPermission("BungeeEmpire.team"))).forEach((pp) -> {
            TextComponent tc = new TextComponent("§8● " + pp.getDisplayName() + " §8» §6" + pp.getServer().getInfo().getName());
            tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/server " + pp.getServer().getInfo().getName()));
            p.sendMessage(tc);
        });
    }
    
}
