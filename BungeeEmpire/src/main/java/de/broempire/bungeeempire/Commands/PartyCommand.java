/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.bungeeempire.Commands;

import de.broempire.bungeeempire.BungeeEmpire;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Matti
 */
public class PartyCommand extends Command{

    private BungeeEmpire plugin;
    
    public PartyCommand(BungeeEmpire plugin) {
        super("party");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer p = (ProxiedPlayer) sender;
        p.sendMessage(new TextComponent(" "));
        TextComponent tc = new TextComponent(plugin.prefix + "§aDu möchtest eine Allianz mit deinen Freunden erstellen?");
        TextComponent click = new TextComponent(plugin.prefix + "§7Nutze dazu den Befehl §6§l/alliance §7oder §6§l/a§r§7!");
        TextComponent space = new TextComponent(" ");
        p.sendMessage(tc);
        click.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§aSchreib' das in den Chat!").create()));
        click.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/alliance"));
        p.sendMessage(click);
        p.sendMessage(space);
    }
    
}
