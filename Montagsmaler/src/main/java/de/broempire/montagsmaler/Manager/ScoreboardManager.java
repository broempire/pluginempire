/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.montagsmaler.Manager;

import de.broempire.montagsmaler.Montagsmaler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 *
 * @author Juli
 */
public class ScoreboardManager {
    
    private Montagsmaler plugin;
    
    public ScoreboardManager (Montagsmaler plugin) {
		this.plugin = plugin;
	}
    
    public void sendScoreboardLobby (Player p) {
        
        Scoreboard board = p.getScoreboard();
		
	Objective obj = board.getObjective("lobby") != null ? board.getObjective("lobby") : board.registerNewObjective("lobby", "dummy");
	obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	obj.setDisplayName("§8» §6§lMontagsmaler §8«");
        
        Team online = board.getTeam("online") != null ? board.getTeam("online") : board.registerNewTeam("online");
        online.setPrefix("§8» ");
        online.addEntry(ChatColor.YELLOW.toString());
        online.setSuffix(Bukkit.getOnlinePlayers().size() + "§7/§e" + plugin.getServer().getMaxPlayers() + " §7[4]");
        
        obj.getScore("   ").setScore(10);
        obj.getScore("§6Online:").setScore(9);
        obj.getScore(ChatColor.YELLOW.toString()).setScore(8);
        obj.getScore(" ").setScore(7);
        obj.getScore("§6Team:").setScore(6);
        obj.getScore(ChatColor.BLACK.toString()).setScore(5);
        obj.getScore("  ").setScore(4);
        obj.getScore("§6Map:").setScore(3);
        obj.getScore(ChatColor.BLUE.toString()).setScore(2);
        obj.getScore("§7-------------").setScore(1);
        obj.getScore("§7++ §6§lBroEmpire.de §7++").setScore(0);
        
        p.setScoreboard(board);
    }
    
    public void sendScoreboardIngame (Player p) {
        Scoreboard board = p.getScoreboard();
        
        if (board.getObjective("lobby") != null) {
            board.getObjective("lobby").unregister();
        }
        
        Objective obj = board.getObjective("ingame") != null ? board.getObjective("ingame") : board.registerNewObjective("ingame", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	obj.setDisplayName("§8» §c§lMontagsmaler §8«");
        
        for(Player all : Bukkit.getOnlinePlayers()) {
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            obj.getScore("" + all.getDisplayName()).setScore(0);
            
            p.setScoreboard(board);
        }
        
    }
    
    public void updateScoreboard (Player p) {
        Scoreboard board = p.getScoreboard();
        
        Objective obj = board.getObjective("ingame") != null ? board.getObjective("ingame") : board.registerNewObjective("ingame", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	obj.setDisplayName("§8» §c§lMontagsmaler §8«");
        
        for(Player all : Bukkit.getOnlinePlayers()) {
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            obj.getScore("" + all.getDisplayName()).getScore();
            
        }
    }
    
}
