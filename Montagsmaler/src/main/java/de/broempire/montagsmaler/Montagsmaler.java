/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.montagsmaler;

import de.broempire.montagsmaler.APIs.ActionAPI;
import de.broempire.montagsmaler.Commands.Lists;
import de.broempire.montagsmaler.Listener.BlockListener;
import de.broempire.montagsmaler.Listener.NatureListener;
import de.broempire.montagsmaler.Listener.PlayerListener;
import de.broempire.montagsmaler.Utils.GameStatus;
import de.broempire.montagsmaler.Listener.Listeners;
import de.broempire.montagsmaler.Commands.SetLobby;
import de.broempire.montagsmaler.Commands.SetMalerSpawn;
import de.broempire.montagsmaler.Commands.SetSpawn;
import de.broempire.montagsmaler.Commands.Start;
import de.broempire.montagsmaler.Listener.ChatListener;
import de.broempire.montagsmaler.Manager.ScoreboardManager;
import de.broempire.montagsmaler.MySQL.MySQL;
import de.broempire.montagsmaler.Utils.Mapreset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Instrument;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * 
 * @author Juligum
 */
public class Montagsmaler extends JavaPlugin {
    
    public static String prefix = "§8▎ §6Montagsmaler §8» ";
    public static String perm = prefix + "§8▎ §6BroEmpire§8 ❘ §cDu besitzt nicht die nötigen Rechte für diesen Befehl!";
    public static String nospieler = prefix + "§8▎ §6BroEmpire§8 ❘ §cDu bist kein Spieler!";
    
    public ArrayList<Player> painter = new ArrayList<>();
    public ArrayList<Player> errater = new ArrayList<>();
    public ArrayList<Player> player = new ArrayList<>();
    public HashMap<Player, Integer> points = new HashMap<>();
    
    public ArrayList<Player> guess = new ArrayList<>();
    
    //words = alle Wörter //word = aktuelles Wort
    public ArrayList<String> words = new ArrayList<>();
    public ArrayList<String> word = new ArrayList<>();
    
    //Wie oft war der Spieler Maler.
    public ArrayList<Player> notonce = new ArrayList<>();
    public ArrayList<Player> once = new ArrayList<>();
    public ArrayList<Player> twice = new ArrayList<>();
    
    public ArrayList<Location> spawnlocation = new ArrayList<>();
    public ArrayList<Location> malerlocation = new ArrayList<>();
    
    public Montagsmaler plugin;
    
    public GameStatus status;
    
    public MySQL mysql;
    //private MySQLStats mySQLStats;
    
    private ScoreboardManager scoreboardManager;
    
    public static int time = 91;
    
    @Override
    public void onEnable() {
        plugin = this;
        
        this.scoreboardManager = new ScoreboardManager(this);
        
        for(World w : Bukkit.getWorlds()) {
	    w.setThundering(false);
	    w.setStorm(false);
	    w.setTime(1000);
	}
        StartCountdown();
        setWords();
        init();
        registerCommands();
	registerEvents();
	status = GameStatus.LOBBY;
        System.out.println("[Montagsmaler] Plugin aktiviert!");
    }
    
    private void registerEvents() {
        PluginManager pm = getServer().getPluginManager();
        
        pm.registerEvents(new BlockListener(), this);
        pm.registerEvents(new PlayerListener(this), this);
        pm.registerEvents(new Listeners(), this);
        pm.registerEvents(new NatureListener(), this);
        pm.registerEvents(new ChatListener(this), this);
    }
    
    private void registerCommands() {
        this.getCommand("start").setExecutor(new Start());
        this.getCommand("lists").setExecutor(new Lists(this));
	this.getCommand("setlobby").setExecutor(new SetLobby(this));
	this.getCommand("setspawn").setExecutor(new SetSpawn(this));
	this.getCommand("setmalerspawn").setExecutor(new SetMalerSpawn(this));
    }

    public void init() {
        
    }
    
    @Override
    public void onDisable() {
        System.out.println("[Montagsmaler] Plugin deaktiviert!");
    }
    
    public void setWords() {
        
        words.add("Auto");
	words.add("Bügeleisen");
	words.add("Straße");
	words.add("Schach");
	words.add("Gemälde");
	words.add("Strand");
	words.add("Kelle");
	words.add("Tee");
	words.add("Lava");
	words.add("Wasser");
	words.add("Eimer");
	words.add("Welle");
	words.add("Kirche");
	words.add("Meer");
	words.add("Aqarium");
	words.add("Suppe");
	words.add("Topf");
	words.add("Sonne");
	words.add("Schneeball");
	words.add("Stern");
	words.add("Mond");
	words.add("Fleisch");
	words.add("Schwein");
	words.add("Huhn");
	words.add("Pommes");
	words.add("Hamburger");
	words.add("Kuh");
	words.add("Baum");
	words.add("Apfel");
	words.add("Tomate");
	words.add("Haus");
	words.add("Berg");
	words.add("Puffi");
	words.add("Kessel");
	words.add("Kissen");
	words.add("Decke");
	words.add("Bett");
	words.add("Spritze");
	words.add("Regenwolke");
	words.add("Blut");
	words.add("Maus");
	words.add("Pfeil");
	words.add("Bogen");
    }
    
    public Location getLocation(String path) {
        String world = this.getConfig().getString("Locations." + path + ".World");
        double x = this.getConfig().getDouble("Locations." + path + ".X");
        double y = this.getConfig().getDouble("Locations." + path + ".Y");
        double z = this.getConfig().getDouble("Locations." + path + ".Z");
        float yaw = this.getConfig().getInt("Locations." + path + ".Yaw");
        float pitch = this.getConfig().getInt("Locations." + path + ".Pitch");
        
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }
    
    public ScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }
    
    public GameStatus getStatus() {
        return status;
    }
    
    public ArrayList<Player> getPainter() {
        return painter;
    }
    
    public ArrayList<Player> getPlayer() {
        return player;
    }
    
    public ArrayList<Player> getErrater() {
        return errater;
    }
    
    public ArrayList<Player> getGuess() {
        return guess;
    }
    
    public ArrayList<Player> getNotOnce() {
        return notonce;
    }
    
    public ArrayList<Player> getOnce() {
        return once;
    }
    
    public ArrayList<Player> getTwice() {
        return twice;
    }
    
    public ArrayList<String> getWords() {
        return words;
    }
    
    public ArrayList<String> getWord() {
        return word;
    }
    
    public HashMap<Player, Integer> getPoints() {
        return points;
    }
    
    public void StartCountdown() {
	new BukkitRunnable() {
	    @Override
	    public void run() {
                time--;
                
                if (Bukkit.getOnlinePlayers().size() >= 2 && time > 60) {
                    time = 61;
                    Bukkit.broadcastMessage(prefix + "§aDie Mindestanzahl an Spielern ist erreicht!");
                }else if (Bukkit.getOnlinePlayers().size() < 2 && time <= 60) {
                    time = 91;
                    Bukkit.broadcastMessage(prefix + "§cEs sind nicht genug Spieler online!");
                }
                for(Player all : Bukkit.getOnlinePlayers()) {
		    all.setLevel(time);
                    
                    if(time > 5) {
                        ActionAPI.sendActionBar(all, "§7Spieler: §6" + Bukkit.getOnlinePlayers().size() + "§7/§612  §7Mindestens: §64");
                    }
                }
                    switch(time) {
                    case 60:
                    case 50:
                    case 40:
                    case 30:
                    case 20:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                        all.sendMessage(prefix + "§7Noch §b" + time + " §7Sekunden bis zum Start!");
                        }
                        break;
                    case 10:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.playSound(all.getLocation(), Sound.BLOCK_ANVIL_USE, 3F, 3F);
                        all.sendMessage(prefix + "§7Noch §b" + time + " §7Sekunden bis zum Start!");;
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                        all.sendMessage(prefix + "§7Noch §b" + time + " §7Sekunden bis zum Start!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                        all.sendMessage(prefix + "§7Noch §b" + time + " §7Sekunde bis zum Start!");
                        }
                        break;
                    case 0:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                        all.getInventory().clear();
                        all.teleport(plugin.getLocation("Spawn"));
                        //mySQLStats.addPlayed(p.getUniqueId().toString(), 1);
                        all.setGameMode(GameMode.SURVIVAL);
                        scoreboardManager.sendScoreboardIngame(all);
                        }
                        status = GameStatus.INGAME;
                        FirstMalerCountdown();
                        cancel();
                        break;
                    }
                    
                }
		}.runTaskTimer(this, 0, 20);
	}
    
    public void FirstMalerCountdown() {
        new BukkitRunnable() {
            
            int firstpaint = 131;
            
            final Random rd = new Random();
            String w = words.get(rd.nextInt(words.size()));
            Player p = errater.get(rd.nextInt(errater.size()));
            
            @Override
            public void run() {
                firstpaint--;
                
                if(firstpaint > 5) {
                    if(player.size() == guess.size()) {
                        
                        p.sendMessage(Montagsmaler.prefix + "§7Alle Mitspieler haben dein Wort erraten, deswegen bekommst du §b1 §7Punkt!");
    			firstpaint = 6;
                    }
                }
                switch(firstpaint) {
                    case 125:
                        p.teleport(getLocation("MalerSpawn"));
                        p.setGameMode(GameMode.CREATIVE);
                        p.sendMessage(Montagsmaler.prefix + "§7Dein Wort lautet: §b" + w);
                                
                        errater.remove(p);
        	        player.remove(p);
        		painter.add(p);
        		word.add(w);
                        break;
                    case 120:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Ihr habt noch §b2 §7Minuten Zeit!");
                        }
                        break;
                    case 60:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Ihr habt noch §b1 §7Minute Zeit!");
                        }
                        break;
                    case 30:
                    case 20:
                        for(Player p : Bukkit.getOnlinePlayers()) {
                            p.playNote(p.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                        }
                        break;
                    case 10:
                        for(Player p : Bukkit.getOnlinePlayers()) {
                            p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 3F, 3F);
                            p.sendMessage(prefix + "§7Noch §b10 §7Sekunden bis zum Start!");
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(Montagsmaler.prefix + "§7Ihr habt noch §b" + firstpaint + " §7Sekunden Zeit!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(Montagsmaler.prefix + "§7Ihr habt noch §b1 §7Sekunde Zeit!");
                        }
                        break;
                    case 0:
                        cancel();
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            for(String w : plugin.getWord()) {
                                all.sendMessage(Montagsmaler.prefix + "§7Das Wort war: §b" + w);
                                word.remove(w);
                            }
                            errater.add(all);
                            guess.remove(all);
                            painter.remove(p);
                            notonce.remove(p);
                            once.add(p);
        		    player.add(p);
                            
                            all.setGameMode(GameMode.SURVIVAL);
                            all.getInventory().clear();
                            all.teleport(plugin.getLocation("Spawn"));
                        }
                            Mapreset.ResetMap();
                            status = GameStatus.INGAME;
                            //getScoreboardManager().sendScoreboardGame(p);
                            MalerCountdown();
                        
                        break;
                }
            }
        }.runTaskTimer(this, 0L, 20L); 
    }
    
    public void firstMalerCountdown() {
        new BukkitRunnable() {
                
            int firstmalen = 121;
            
                final Random rd = new Random();
            	String w = words.get(rd.nextInt(words.size()));
            	Player p = errater.get(rd.nextInt(Bukkit.getOnlinePlayers().size()));
            	
		@Override
                public void run() {
                        	firstmalen--;
                        if(firstmalen == 130) {
        				}
                        if(firstmalen == 125) {
                        	
                        	p.setGameMode(GameMode.CREATIVE);
                        	p.sendMessage(Montagsmaler.prefix + "§7Dein Wort lautet: §b" + w);
        						
        				errater.remove(p);
        				player.remove(p);
        				painter.add(p);
        				word.add(w);
                        }
                        if(firstmalen > 5) {
                        if(player.size() == guess.size()) {
                        	for(Player p : painter) {
    				p.sendMessage(Montagsmaler.prefix + "§7Weil alle es erraten haben bekommst du §b1 §7Punkt!");
    				firstmalen = 5;
                        	}
                        }
                        }
                        if(firstmalen == 120) {
                        	for(Player all : Bukkit.getOnlinePlayers()) {
                                all.sendMessage(Montagsmaler.prefix + "§7Ihr habt noch §b2 §7Minuten Zeit!");
                        }
                        }
                        if(firstmalen == 60) {
                        	for(Player all : Bukkit.getOnlinePlayers()) {
                                all.sendMessage(Montagsmaler.prefix + "§7Ihr habt noch §b1 §7Minute Zeit!");
                        }
                        }
                        if(firstmalen == 30 || firstmalen == 20 || firstmalen == 10 || firstmalen <= 5 && firstmalen > 1) {
                        for(Player all : Bukkit.getOnlinePlayers()) {
                                all.sendMessage(Montagsmaler.prefix + "§7Ihr habt noch §b" + firstmalen + " §7Sekunden Zeit!");
                        }
                        }
                        if(firstmalen == 1) {
                        for(Player all : Bukkit.getOnlinePlayers()) {
                                all.sendMessage(Montagsmaler.prefix + "§7Ihr habt noch §b1 §7Sekunde Zeit!");
                        }
                        }
                        if(firstmalen == 0) {
                        	for(Player all : Bukkit.getOnlinePlayers()) {
                        		for(String w : word) {
                        		 all.sendMessage(Montagsmaler.prefix + "§7Das Wort war: §b" + w);
                        		}
                        	}   
                                    
                                p.setGameMode(GameMode.SURVIVAL);
                                p.getInventory().clear();
                                    
                                once.add(p);
                                player.add(p);
                                painter.remove(p);
                                word.remove(w);
                        	status = GameStatus.INGAME;
                            cancel();
                            }
                }
        }.runTaskTimer(this, 0, 20);
}
    
    public void MalerCountdown() {
        new BukkitRunnable() {
            
            int paint = 131;
            
            final Random rd = new Random();
            String w = words.get(rd.nextInt(words.size()));
            Player p = notonce.get(rd.nextInt(notonce.size()));
            
            @Override
            public void run() {
                paint--;
                
                if(paint > 5) {
                    if(player.size() == guess.size()) {
                        
                        p.sendMessage(Montagsmaler.prefix + "§7Alle Mitspieler haben dein Wort erraten, deswegen bekommst du §b1 §7Punkt!");
    			paint = 6;
                    }
                }
                switch(paint) {
                    case 125:
                        p.teleport(getLocation("MalerSpawn"));
                        p.setGameMode(GameMode.CREATIVE);
                        p.sendMessage(Montagsmaler.prefix + "§7Dein Wort lautet: §b" + w);
                                
                        errater.remove(p);
        	        player.remove(p);
        		painter.add(p);
        		word.add(w);
                        break;
                    case 120:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Ihr habt noch §b2 §7Minuten Zeit!");
                        }
                        break;
                    case 60:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Ihr habt noch §b1 §7Minute Zeit!");
                        }
                        break;
                    case 30:
                    case 20:
                        for(Player p : Bukkit.getOnlinePlayers()) {
                            p.playNote(p.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                        }
                        break;
                    case 10:
                        for(Player p : Bukkit.getOnlinePlayers()) {
                            p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 3F, 3F);
                            p.sendMessage(prefix + "§7Noch §b" + time + " §7Sekunden bis zum Start!");
                            p.getInventory().setItem(4, new ItemStack(Material.AIR));
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(Montagsmaler.prefix + "§7Ihr habt noch §b" + paint + " §7Sekunden Zeit!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(Montagsmaler.prefix + "§7Ihr habt noch §b1 §7Sekunde Zeit!");
                        }
                        break;
                    case 0:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.sendMessage(Montagsmaler.prefix + "§7Das Wort war: §b" + word);
                                
                            errater.add(all);
                            guess.remove(all);
                            painter.remove(p);
                            notonce.remove(p);
                            once.add(p);
        		    player.add(p);
                                
                            all.getInventory().clear();
                            all.teleport(plugin.getLocation("Spawn"));
                            Mapreset.ResetMap();
                            //mySQLStats.addPlayed(p.getUniqueId().toString(), 1);
                            status = GameStatus.INGAME;
                            //getScoreboardManager().sendScoreboardGame(p);
                            all.setGameMode(GameMode.SURVIVAL);
                        }
                            
                            if(notonce.isEmpty()) {
                                Maler2Countdown();
                            }else{
                                MalerCountdown();
                            }
                            cancel();
                        break;
                }
            }
        }.runTaskTimer(this, 0L, 20L); 
    }
    
    public void Maler2Countdown() {
        new BukkitRunnable() {
            
            int paint = 131;
            
            final Random rd = new Random();
            String w = words.get(rd.nextInt(words.size()));
            Player p = once.get(rd.nextInt(once.size()));
            
            @Override
            public void run() {
                paint--;
                
                if(paint > 5) {
                    if(player.size() == guess.size()) {
                        
                        p.sendMessage(Montagsmaler.prefix + "§7Alle Mitspieler haben dein Wort erraten, deswegen bekommst du §b1 §7Punkt!");
    			paint = 6;
                    }
                }
                switch(paint) {
                    case 125:
                        p.teleport(getLocation("MalerSpawn"));
                        p.setGameMode(GameMode.CREATIVE);
                        p.sendMessage(Montagsmaler.prefix + "§7Dein Wort lautet: §b" + w);
                                
                        errater.remove(p);
        	        player.remove(p);
        		painter.add(p);
        		word.add(w);
                        break;
                    case 120:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Ihr habt noch §b2 §7Minuten Zeit!");
                        }
                        break;
                    case 60:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playNote(all.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            all.sendMessage(prefix + "§7Ihr habt noch §b1 §7Minute Zeit!");
                        }
                        break;
                    case 30:
                    case 20:
                        for(Player p : Bukkit.getOnlinePlayers()) {
                            p.playNote(p.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                        }
                        break;
                    case 10:
                        for(Player p : Bukkit.getOnlinePlayers()) {
                            p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 3F, 3F);
                            p.sendMessage(prefix + "§7Noch §b" + time+ " §7Sekunden bis zum Start!");
                            p.getInventory().setItem(4, new ItemStack(Material.AIR));
                        }
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(Montagsmaler.prefix + "§7Ihr habt noch §6" + paint + " §7Sekunden Zeit!");
                        }
                        break;
                    case 1:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            all.sendMessage(Montagsmaler.prefix + "§7Ihr habt noch §61 §7Sekunde Zeit!");
                        }
                        break;
                    case 0:
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.sendMessage(Montagsmaler.prefix + "§7Das Wort war: §b" + word);
                                
                            errater.add(all);
                            guess.remove(all);
                            painter.remove(p);
                            once.remove(p);
                            twice.add(p);
                            player.add(p);
                                
                            all.getInventory().clear();
                            all.teleport(plugin.getLocation("Spawn"));
                            Mapreset.ResetMap();
                            //mySQLStats.addPlayed(p.getUniqueId().toString(), 1);
                            status = GameStatus.INGAME;
                            //getScoreboardManager().sendScoreboardGame(p);
                            all.setGameMode(GameMode.SURVIVAL);
                        }
                            if(once.isEmpty()) {
                                
                            }else{
                                Maler2Countdown();
                            }
                            cancel();
                        break;
                }
            }
        }.runTaskTimer(this, 0L, 20L); 
    }
    
}
