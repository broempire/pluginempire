/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.montagsmaler.Listener;

import de.broempire.montagsmaler.Montagsmaler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

/**
 *
 * @author Juli
 */
public class ChatListener implements Listener {
    
    private Montagsmaler plugin;
    
    public ChatListener(Montagsmaler plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onChat (AsyncPlayerChatEvent e) {
        
        Player p = e.getPlayer();
        Scoreboard board = p.getScoreboard();
        
        Objective obj = board.getObjective("ingame") != null ? board.getObjective("ingame") : board.registerNewObjective("ingame", "dummy");
        
        if(plugin.getPainter().contains(p)) {
            p.sendMessage(Montagsmaler.prefix + "§cDu bist der Maler und darfst nicht schreiben!");
            e.setFormat("");
            
        }else if(plugin.getErrater().contains(p)) {
            e.setFormat(p.getDisplayName() + "§7>> §r" + e.getMessage());
            
            for(String w : plugin.getWord()) {
                if(e.getMessage().equalsIgnoreCase(w)) {
                    e.setFormat("");
                    for(Player all : Bukkit.getOnlinePlayers()) {
                    
                    if(plugin.getGuess().isEmpty()) {
                        all.sendMessage(Montagsmaler.prefix + p.getDisplayName() + " §7hat das Wort erraten! Und erhält §b3 §7Punkte!");
                        
                        obj.getScore("" + p.getDisplayName()).setScore(obj.getScore("" + all.getDisplayName()).getScore() +3);
                        Delay(p);
                    }else if(plugin.getGuess().size() == 1) {
                        all.sendMessage(Montagsmaler.prefix + p.getDisplayName() + " §7hat das Wort erraten! Und erhält §b2 §7Punkte!");
                        
                        obj.getScore("" + p.getDisplayName()).setScore(obj.getScore("" + all.getDisplayName()).getScore() +2);
                        Delay(p);
                    }else{
                        all.sendMessage(Montagsmaler.prefix + p.getDisplayName() + " §7hat das Wort erraten! Und erhält §b1 §7Punkt!");
                        
                        obj.getScore("" + p.getDisplayName()).setScore(obj.getScore("" + all.getDisplayName()).getScore() +1);
                        Delay(p);
                    }
                    }
                   }
                }
        }else if(plugin.getGuess().contains(p)) {
            p.sendMessage(Montagsmaler.prefix + "§cDu hast das Wort bereits erraten und darfst nicht mehr schreiben!");
	    e.setFormat("");
        }
            
    }
    
    public void Delay(Player p) {
        new BukkitRunnable() {
            
            int delay = 2;
            
            @Override
            public void run() {
                delay--;
                
                if(delay == 0) {
                    plugin.getErrater().remove(p);
                    plugin.getGuess().add(p);
                    
                    delay = 2;
                    cancel();
                }
            }
        }.runTaskTimer(plugin, 0, 20);
    }
    
    
}
