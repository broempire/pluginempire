/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.montagsmaler.Listener;

import de.broempire.montagsmaler.Utils.Mapreset;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

/**
 *
 * @author Juli
 */
public class BlockListener implements Listener {
	
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
            
		Player p = e.getPlayer();
                
		if(p.getGameMode() == GameMode.CREATIVE) {
		if(Mapreset.locations.contains(e.getBlock().getLocation())) {
		e.setCancelled(false);
		}else{
			e.setCancelled(true);
	}
		}else{
			e.setCancelled(true);
	}

}
	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
            
	    Mapreset.locations.add(e.getBlock().getLocation());
	}
    
}
