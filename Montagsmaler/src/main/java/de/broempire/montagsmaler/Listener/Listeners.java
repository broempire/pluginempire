package de.broempire.montagsmaler.Listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class Listeners implements Listener {
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
			e.setCancelled(true);
	}
	
	@EventHandler
	public void onDamage(EntityDamageByBlockEvent e) {
			e.setCancelled(true);
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
			e.setCancelled(true);
	}
	
	@EventHandler
	public void onFood(FoodLevelChangeEvent e) {
			e.setCancelled(true);
	}

}
