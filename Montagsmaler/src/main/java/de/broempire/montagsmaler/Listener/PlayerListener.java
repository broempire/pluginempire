/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.montagsmaler.Listener;

import de.broempire.montagsmaler.APIs.ActionAPI;
import de.broempire.montagsmaler.Montagsmaler;
import de.broempire.montagsmaler.Utils.GameStatus;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;

/**
 *
 * @author Juli
 */
public class PlayerListener implements Listener {
    
    private Montagsmaler plugin;
    
    public PlayerListener (Montagsmaler plugin) {
		this.plugin = plugin;
	}
    
    @EventHandler
	public void JoinEvent (PlayerJoinEvent e) {
		
		if(plugin.getStatus() == GameStatus.LOBBY) {
                    
		
		    Player p = e.getPlayer();
			
		    readyPlayer(p);
			
                    plugin.getErrater().add(p);
		    plugin.getPlayer().add(p);
                    plugin.getNotOnce().add(p);
		    p.teleport(plugin.getLocation("Lobby"));
		    e.setJoinMessage(Montagsmaler.prefix + "§8[§2+§8] §7" + p.getDisplayName());
                    
                    plugin.getScoreboardManager().sendScoreboardLobby(p);
			
		}
	}
        
        @EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
                
                ClearFromArray(p);
                
		e.setQuitMessage(Montagsmaler.prefix + "§8[§4-§8] §7" + p.getDisplayName());
		
                /*for(Player all : Bukkit.getOnlinePlayers()) {
                //Überdenken -> plugin.getScoreboardManager().updateScoreboard(all);
                
                if(Bukkit.getOnlinePlayers().isEmpty()) {
                    Scoreboard board = p.getScoreboard();
                    Bukkit.broadcastMessage("Test");
                    
                    if(board.getObjective("ingame") != null) {
                        board.getObjective("ingame").unregister();
                        Bukkit.broadcastMessage("remove");
                    }
                }
                }*/
	}
    
    @EventHandler
	public void onLogin (PlayerLoginEvent e) {
		
		if(plugin.getStatus() != GameStatus.LOBBY) {
		    e.disallow(PlayerLoginEvent.Result.KICK_FULL, "§cDas Spiel hat schon gestartet!");
		}
		if(e.getResult().equals(PlayerLoginEvent.Result.KICK_FULL)) {
                    
		    e.setKickMessage(Montagsmaler.prefix + "Der Server ist voll!");
		}
		
	}
    
    public void readyPlayer(Player p) {
		
		p.setHealth(20D);
		p.setFoodLevel(20);
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		p.setFireTicks(0);
		p.setAllowFlight(false);
		p.setFlying(false);
		p.setGameMode(GameMode.SURVIVAL);
		p.setExp(0F);
		p.setLevel(0);
		for(PotionEffect effect : p.getActivePotionEffects()) {
			p.removePotionEffect(effect.getType());
		}
		
	}
    
    public void ClearFromArray(Player p) {
		
		plugin.getErrater().remove(p);
		plugin.getPlayer().remove(p);
		plugin.getPainter().remove(p);
		plugin.getGuess().remove(p);
                plugin.getNotOnce().remove(p);
                plugin.getOnce().remove(p);
                plugin.getTwice().remove(p);
                
		
	}
    
    private boolean b = false; 
	
    int r = 16;
    
    /*public void setEnd() {
		if (b == false) {
                    if(plugin.getStatus() == GameStatus.RESTART) {
			b = true;
			plugin.getLocation("LOBBY").getWorld().playEffect(plugin.getLocation("LOBBY"), Effect.FLYING_GLYPH, 10);
			new BukkitRunnable() {

				@Override
				public void run() {
                                    r--;
                                    for (Player p : plugin.getServer().getOnlinePlayers()) {
                                        ActionAPI.sendActionBar(p, "§7Der Server startet in §6" + r + "§7 Sekunden neu!");
                                    }
                                    switch(r) {
					case 1:
                                            for (Player p : plugin.getServer().getOnlinePlayers()) {
                                                ActionAPI.sendActionBar(p, "§7Der Server startet in §6" + r + "§7 Sekunde neu!");
                                            }
                                            break;
					case 0:
						Bukkit.broadcastMessage(Montagsmaler.prefix + "§cDer Server startet jetzt neu");
						for (Player p : Bukkit.getOnlinePlayers()) {
							ByteArrayOutputStream b = new ByteArrayOutputStream();
							DataOutputStream out = new DataOutputStream(b);
							
							try {
								out.writeUTF("Connect");
								out.writeUTF("lobby01");
							} catch (IOException e) {
								e.printStackTrace();
							}
							
							p.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
							
							Bukkit.shutdown();
							
						}
                                                break;
					}
				}
				
                        }.runTaskTimer(plugin, 0L, 20L);
                    }
		}
		
		
		
	}*/
    
}
