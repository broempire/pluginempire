/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.montagsmaler.Commands;

import de.broempire.montagsmaler.Montagsmaler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class Lists implements CommandExecutor {
    
    private Montagsmaler plugin;
    
    public Lists (Montagsmaler plugin) {
		this.plugin = plugin;
	}
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Montagsmaler.nospieler);
			return true;
		}
		
		Player p = (Player)sender;
		if(!p.hasPermission("montagsmaler.start")) {
			p.sendMessage(Montagsmaler.perm);
			return true;
		}
		
		if(args.length != 0) {
			p.sendMessage(Montagsmaler.prefix + "§cFehler! §9Benutze: /start");
			return true;
		}
                p.sendMessage(Montagsmaler.prefix + "§6Player: §6" + plugin.player + " §7Painter: §6" + plugin.painter + " §7Errater: §6" + plugin.errater + " §7Guess: §6" + plugin.guess + " §7NotOnce: §6" + plugin.notonce + " §7Once: §6" + plugin.once + " §7Twice: §6" + plugin.twice);
		return true;
	}
    
}
