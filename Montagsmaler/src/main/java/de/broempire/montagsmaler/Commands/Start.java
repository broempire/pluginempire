/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.montagsmaler.Commands;

import de.broempire.montagsmaler.Montagsmaler;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class Start implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Montagsmaler.nospieler);
			return true;
		}
		
		Player p = (Player)sender;
		if(!p.hasPermission("montagsmaler.start")) {
			p.sendMessage(Montagsmaler.perm);
			return true;
		}
		
		if(args.length != 0) {
			p.sendMessage(Montagsmaler.prefix + "§cFehler! §9Benutze: /start");
			return true;
		}
                
		if(Bukkit.getOnlinePlayers().size() == 2) {
		if(Montagsmaler.time > 11) {
                    
		    for(Player all : Bukkit.getOnlinePlayers()) {
			all.sendMessage(Montagsmaler.prefix + "§6Die Wartezeit wurde verkürzt!");
		    }
		    Montagsmaler.time = 11;
		}
                }
		return true;
	}
    
}
