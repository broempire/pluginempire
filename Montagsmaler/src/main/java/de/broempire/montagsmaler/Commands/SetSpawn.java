/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.montagsmaler.Commands;

import de.broempire.montagsmaler.Montagsmaler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class SetSpawn implements CommandExecutor {
    
    private Montagsmaler plugin;
    
    public SetSpawn (Montagsmaler plugin) {
		this.plugin = plugin;
	}
    
    @Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Montagsmaler.nospieler);
			return true;
		}
		Player p = (Player)sender;
		if(!p.hasPermission("montagsmaler.setspawn")) {
			p.sendMessage(Montagsmaler.perm);
			return true;
		}
		
		if(args.length != 0) {
			p.sendMessage(Montagsmaler.prefix + "§cFehler! §7Benutze: /setspawn");
			return true;
		}
		
			plugin.getConfig().set("Locations.Spawn.World", p.getWorld().getName());
			plugin.getConfig().set("Locations.Spawn.X", p.getLocation().getX());
			plugin.getConfig().set("Locations.Spawn.Y", p.getLocation().getY());
			plugin.getConfig().set("Locations.Spawn.Z", p.getLocation().getZ());
                        plugin.getConfig().set("Locations.Spawn.Yaw", p.getLocation().getYaw());
                        plugin.getConfig().set("Locations.Spawn.Pitch", p.getLocation().getPitch());
			plugin.saveConfig();
			if(plugin.getConfig().getString("Locations.Spawn.World") != null)
				p.sendMessage(Montagsmaler.prefix + "§7Du hast den §3Spawn §7erfolgreich gesetzt");
		
		return true;
  }
    
}
