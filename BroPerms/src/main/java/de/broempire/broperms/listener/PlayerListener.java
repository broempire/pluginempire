/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.broperms.listener;

import de.broempire.broperms.BroPerms;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;

/**
 *
 * @author Matti
 */
public class PlayerListener implements Listener {

    private BroPerms plugin;
    
    public PlayerListener(BroPerms plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        
        PermissionAttachment permissionAttachment = p.addAttachment(plugin);
        
        for (String permission : plugin.getPermissions().getPermissions(plugin.getBro().getGroup(p.getUniqueId().toString()))) {
            permissionAttachment.setPermission(permission, true);
        }
        for (String permission : plugin.getPermissions().getProhibitions(plugin.getBro().getGroup(p.getUniqueId().toString()))) {
            permissionAttachment.unsetPermission(permission);
        }
        
        plugin.addAttachment(p.getName(), permissionAttachment);
        
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        plugin.removeAttachment(p.getName());
    }
    
}
