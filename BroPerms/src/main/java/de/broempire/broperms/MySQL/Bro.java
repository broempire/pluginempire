/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.broperms.MySQL;

import de.broempire.broperms.BroPerms;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 *
 * @author Matti
 */
public class Bro {
    
    private MySQL mySQL;
    
    private BroPerms plugin;
    
    public Bro(BroPerms plugin, MySQL mySQL) {
        this.plugin = plugin;
        this.mySQL = mySQL;
    }
    
    public boolean UUIDexists (String uuid) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
                
            if (resultSet.next()) {
                return true;
            }
            
            return false;
                    
        } catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }
    public boolean NameExists (String name) {
        try{
            ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE PlayerName = '" + name + "'");
            
            if (resultSet.next()) {
                return true;
            }
            
            return false;
            
        }catch(SQLException e){
            e.printStackTrace();
	}
        return false;
    }
    
    public String getUUID(String name) {
        if (NameExists(name)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE PlayerName = '" + name +"'");
                while (resultSet.next()) {
                    return resultSet.getString("UUID");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public String getGroup(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
                while (resultSet.next()) {
                    return resultSet.getString("PlayerGroup");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            getGroup(uuid);
        }
        return null;
    }
    public void setGroup(String Group, String uuid){
        if (UUIDexists(uuid)) {
                mySQL.update("UPDATE Players SET PlayerGroup = '" + Group + "' WHERE UUID='" + uuid + "';");
        } else {
            setGroup(Group, uuid);
        }
            
    }
    public Integer getCoins(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
                while (resultSet.next()) {
                    return resultSet.getInt("Coins");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            getCoins(uuid);
        }
        return null;
    }
    public void setCoins(int coins, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET Coins = '" + coins + "' WHERE UUID = '" + uuid + "';");
        } else {
            setCoins(coins, uuid);
        }
    }
    
    public void setName(String name, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET PlayerName = '" + name + "' WHERE UUID = '" + uuid + "';");
        } else {
            setName(name, uuid);
        }
    }
    
    public Integer getBanPoints(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID= '" + uuid + "'");
                
                while(resultSet.next()) {
                    return resultSet.getInt("BanPoints");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            getBanPoints(uuid);
        }
        return null;
    }
    public void setBanPoints(Integer banpoints, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET BanPoints = '" + banpoints + "' WHERE UUID = '" + uuid + "';");
        } else {
            setBanPoints(banpoints, uuid);
        }
    }
    public Long getLastOnline(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
                while (resultSet.next()) {
                    return resultSet.getLong("LastLogin");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            getLastOnline(uuid);
        }
        return null;
    }
    public void setLastOnline(long time, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET LastLogin = '" + time + "' WHERE UUID = '" + uuid + "';");
        } else {
            setLastOnline(time, uuid);
        }
    }
    public Long getFirstOnline(String uuid) {
        if (UUIDexists(uuid)) {
            try {
                ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
                while (resultSet.next()) {
                    return resultSet.getLong("FirstOnline");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            getFirstOnline(uuid);
        }
        return null;
    }
    public void setOnlineTime(long time, String uuid) {
        if (UUIDexists(uuid)) {
            mySQL.update("UPDATE Players SET OnlineTime = '" + time + "' WHERE UUID = '" + uuid + "';");
        } else {
            setOnlineTime(time, uuid);
        }
    }
    
    public Long getOnlineTime(String uuid) {
        if (UUIDexists(uuid)) {
            ResultSet resultSet = mySQL.query("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
            try {
                while (resultSet.next()) {
                    return resultSet.getLong("OnlineTime");
                }
            } catch (SQLException e) {
                
            }
        } else {
            getOnlineTime(uuid);
        }
        return null;
    }
    
}
