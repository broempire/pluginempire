/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.broperms.MySQL;

import de.broempire.broperms.BroPerms;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Matti
 */
public class Permissions {

    private BroPerms plugin;
    private MySQL mySQL;
    
    public Permissions(BroPerms plugin, MySQL mySQL) {
        this.plugin = plugin;
        this.mySQL = mySQL;
    }
    
    public boolean entryExists(String group, String permission) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM Permissions WHERE PlayerGroup = '" + group + "' AND Permission = '" + permission + "'");
            return resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean groupExists(String group) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM Permissions WHERE PlayerGroup = '" + group + "'");
            return resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public void addPermission(String group, String permission) {
        if (!entryExists(group, permission)) {
            mySQL.update("INSERT INTO Permissions (PlayerGroup, Permission, Type) VALUES ('" + group + "', '" + permission + "', '1');");
        } else {
            mySQL.update("UPDATE Permissions SET Type = '1' WHERE PlayerGroup = '" + group + "' AND Permission = '" + permission + "';");
        }
    }
    
    public void removePermission(String group, String permission) {
        if (!entryExists(group, permission)) {
            mySQL.update("INSERT INTO Permissions (PlayerGroup, Permission, Type) VALUES ('" + group + "', '" + permission + "', '0');");
        } else {
            mySQL.update("UPDATE Permissions SET Type = '0' WHERE PlayerGroup = '" + group + "' AND Permission = '" + permission + "';");
        }
    }
    
    public List<String> getPermissions(String group) {
        List perms = new ArrayList<String>();
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM Permissions WHERE PlayerGroup = '" + group + "' AND type = '1'");
            while (resultSet.next()) {
                perms.add(resultSet.getString("permission"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return perms;
    }
    
    public List<String> getProhibitions(String group) {
        List prohibitions = new ArrayList<String>();
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM Permissions WHERE PlayerGroup = '" + group + "' AND type = '0'");
            while (resultSet.next()) {
                prohibitions.add(resultSet.getString("permission"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return prohibitions;
    }
    
}
