/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.broperms.MySQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Matti
 */
public class MySQL {
    
    private String host;
    private String username;
    private String database;
    private String password;
    
    private Connection con;
    
    public MySQL(String host, String database, String username, String password) {
        this.host = host;
        this.database = database;
        this.username = username;
        this.password = password;
        
        connect();
    }
    
    private void connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database + "?autoReconnect=true", username, password);
            System.out.println("[SQLManager] Verbindung zu MySQL hergestellt!");
        } catch (ClassNotFoundException e) {
            System.out.println("[SQLManager] Treiber konnte nicht gefunden werden!");
        } catch (SQLException e) {
            System.out.println("[SQLManager] Verbindung zu MySQL konnte nicht hergestellt werden! Fehler: " + e.getMessage());
        }
    }
    private void close() {
        try {
            if(isConnected()) {
                con.close();
                System.out.println("[MySQLManager] Verbindung zu MySQL beendet!");
            }
        } catch (SQLException e) {
            System.out.println("[MySQLManager] Fehler beim Beenden der Verbindung zur MySQL! Fehler: " + e.getMessage());
        }
    }
    public boolean isConnected() {
        System.out.println("[MySQL] Die Verbindung wird getestet...");
        return con != null;
    }
    public void update(String qry) {
        try {
            Statement statement = con.createStatement();
            statement.executeUpdate(qry);
            statement.close();
        } catch (SQLException e) {
            connect();
            System.err.println(e);
        }
    }
    public ResultSet query(String qry) {
        ResultSet rs = null;
        try {
            Statement statement = con.createStatement();
            rs = statement.executeQuery(qry);
        } catch (SQLException e) {
            connect();
            System.err.println(e);
        }
        return rs;
               
    }
    
}
