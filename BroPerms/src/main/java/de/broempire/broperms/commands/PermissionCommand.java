/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.broperms.commands;

import de.broempire.broperms.BroPerms;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Matti
 */
public class PermissionCommand implements CommandExecutor{

    private BroPerms plugin;
    
    public PermissionCommand(BroPerms plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("BroEmpire.perms")) {
                switch (args.length) {
                    case 0:
                        plugin.sendUsage("/broperms <Gruppe> [add/remove/check] [Permission]", "/broperms PLAYER remove LobbyEmpire.spawn", p);
                        break;
                    case 1:
                        if (plugin.getPermissions().groupExists(args[0])) {
                            p.sendMessage(plugin.prefix + "§8§m----------§r§7»§6Gruppe: " + args[0] + "§7«§8§m----------");
                            p.sendMessage("§aPermissions:");
                            for (String permission : plugin.getPermissions().getPermissions(args[0])) {
                                p.sendMessage("§8● §7" + permission);
                            }
                            p.sendMessage("§cProhibitions:");
                            for (String prohibition : plugin.getPermissions().getProhibitions(args[0])) {
                                p.sendMessage("§8● §7" + prohibition);
                            }
                            p.sendMessage(plugin.prefix + "§8§m----------§r§7»§6Gruppe: " + args[0] + "§7«§8§m----------");
                        } else {
                            p.sendMessage(plugin.prefix + "§cDiese Gruppe existiert nicht in der Datenbank!");
                        }
                        break;
                    case 2:
                        plugin.sendUsage("/broperms <Gruppe> [add/remove/check] [Permission]", "/broperms PLAYER remove LobbyEmpire.spawn", p);
                        break;
                    case 3:
                        switch (args[1]) {
                            case "add":
                                if (!plugin.getPermissions().getPermissions(args[0]).contains(args[2])) {
                                    plugin.getPermissions().addPermission(args[0], args[2]);
                                    for (Player player : plugin.getServer().getOnlinePlayers()) {
                                        if (plugin.getBro().getGroup(player.getUniqueId().toString()).equals(args[0])) {
                                            plugin.getAttachment(player.getName()).setPermission(args[2], true);
                                        }
                                    }
                                    p.sendMessage(plugin.prefix + "§aDie Gruppe " + args[0] + " hat nun die Berechtigung §b" + args[2] + "§a!");
                                } else {
                                    p.sendMessage(plugin.prefix + "§cDie Gruppe hat schon diese Berechtigung!");
                                }
                                break;
                            case "remove":
                                if (!plugin.getPermissions().getProhibitions(args[0]).contains(args[2])) {
                                    plugin.getPermissions().removePermission(args[0], args[2]);
                                    for (Player player : plugin.getServer().getOnlinePlayers()) {
                                        if (plugin.getBro().getGroup(player.getUniqueId().toString()).equals(args[0])) {
                                            plugin.getAttachment(player.getName()).unsetPermission(args[2]);
                                        }
                                    }
                                    p.sendMessage(plugin.prefix + "§aDie Gruppe " + args[0] + " hat nun nicht mehr die Berechtigung §b" + args[2] + "§a!");
                                } else {
                                    p.sendMessage(plugin.prefix + "§cDie Gruppe hat diese Berechtigung schon entzogen bekommen!");
                                }
                                break;
                            case "check":
                                if (plugin.getPermissions().getPermissions(args[0]).contains(args[2])) {
                                    p.sendMessage(plugin.prefix + "§aDie Gruppe hat die nötigen Rechte!");
                                } else {
                                    p.sendMessage(plugin.prefix + "§cDie Gruppe hat nicht die nötigen Rechte!");
                                }
                                break;
                            default:
                                plugin.sendUsage("/broperms <Gruppe> [add/remove/check] [Permission]", "/broperms PLAYER remove LobbyEmpire.spawn", p);
                                break;
                        }
                        break;
                    default:
                        plugin.sendUsage("/broperms <Gruppe> [add/remove/check] [Permission]", "/broperms PLAYER remove LobbyEmpire.spawn", p);
                        break;
                }
            } else {
                p.sendMessage(plugin.noPerms);
            }
        }
        return true;
    }
    
}
