/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.broperms;

import de.broempire.broperms.MySQL.Bro;
import de.broempire.broperms.MySQL.MySQL;
import de.broempire.broperms.MySQL.Permissions;
import de.broempire.broperms.commands.PermissionCommand;
import de.broempire.broperms.listener.PlayerListener;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Matti
 */
public class BroPerms extends JavaPlugin {
    
    public String prefix = "§8▎ §6BroEmpire§8 ❘ §7";
    public String noPerms = "§8▎ §6BroEmpire§8 ❘ §cDu besitzt nicht die nötigen Rechte für diesen Befehl!";
    
    private HashMap<String, PermissionAttachment> attachments = new HashMap<>();

    File file;
    FileConfiguration cfg;
    
    private Bro bro;
    private MySQL mySQL;
    private Permissions permissions;
    
    @Override
    public void onEnable() {
        
        init();
        
        System.out.println("[BroPerms] Plugin aktiviert!");
    }
    
    private void init() {
        attachments = new HashMap<>();
        loadConfig();
        prepareDatabase();
        
        this.getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
        this.getCommand("broperms").setExecutor(new PermissionCommand(this));
        
        loadAttachments();
    }
    
    public FileConfiguration getFileConfiguration() {
        return cfg;
    }
    
    private void prepareDatabase() {
        FileConfiguration cfg = getFileConfiguration();
        mySQL = new MySQL(cfg.getString("MySQL.Hostname"), cfg.getString("MySQL.Database"), cfg.getString("MySQL.Username"), cfg.getString("MySQL.Password"));
        mySQL.update("CREATE TABLE IF NOT EXISTS Permissions (PlayerGroup VARCHAR(100), Permission VARCHAR(100), Type VARCHAR(100))");
        
        bro = new Bro(this, mySQL);
        permissions = new Permissions(this, mySQL);
    }
    
    private void loadConfig() {
        file = new File(getDataFolder(), "MySQL.yml");
        cfg = YamlConfiguration.loadConfiguration(file);
        
        cfg.options().copyDefaults(true);
        cfg.addDefault("MySQL.Hostname", "localhost");
        cfg.addDefault("MySQL.Database", "sql23");
        cfg.addDefault("MySQL.Username", "sql23");
        cfg.addDefault("MySQL.Password", "s1UNRmQGyW");
        try {
            cfg.save(file);
        } catch (IOException ex) {
            Logger.getLogger(BroPerms.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Bro getBro() {
        return bro;
    }

    public MySQL getMySQL() {
        return mySQL;
    }

    public Permissions getPermissions() {
        return permissions;
    }
    
    public void addAttachment (String p, PermissionAttachment pa) {
        attachments.put(p, pa);
    }
    
    public void removeAttachment (String p) {
        if (attachments.containsKey(p)) {
            attachments.remove(p);
        }
    }
    
    public PermissionAttachment getAttachment (String p) {
        return attachments.get(p);
    }
    
    public void sendUsage(String usage, String example, Player p) {
        p.sendMessage("§8§m          §r§7»§6Benutzung§7«§8§m         -");
        p.sendMessage(" ");
        p.sendMessage("§4" + usage);
        if (example != null) {
            p.sendMessage("§2Beispiel§7: §6" + example);
        }
        p.sendMessage(" ");
        p.sendMessage("§8§m          §r§7»§6Benutzung§7«§8§m         -");
    } 
    
    public void loadAttachments() {
        for (Player p : this.getServer().getOnlinePlayers()) {
            PermissionAttachment permissionAttachment = p.addAttachment(this);
            for (String permission : this.getPermissions().getPermissions(this.getBro().getGroup(p.getUniqueId().toString()))) {
                permissionAttachment.setPermission(permission, true);
            }
            for (String permission : this.getPermissions().getProhibitions(this.getBro().getGroup(p.getUniqueId().toString()))) {
                permissionAttachment.unsetPermission(permission);
            }
            addAttachment(p.getName(), permissionAttachment);
        }
    }

    @Override
    public void onDisable() {
        System.out.println("[BroPerms] Plugin deaktiviert!");
    }
    
}
