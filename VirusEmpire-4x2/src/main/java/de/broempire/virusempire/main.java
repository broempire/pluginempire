package de.broempire.virusempire;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import de.broempire.virusempire.APIs.ActionAPI;
import de.broempire.virusempire.APIs.TitleAPI;
import de.broempire.virusempire.Commands.BuildmodeCMD;
import de.broempire.virusempire.Commands.StartCommand;
import de.broempire.virusempire.Commands.StatsCMD;
import de.broempire.virusempire.Commands.VirusCMD;
import de.broempire.virusempire.Events.BlockHandler;
import de.broempire.virusempire.Events.BuyItemListener;
import de.broempire.virusempire.Events.Chat;
import de.broempire.virusempire.Events.DamageHandler;
import de.broempire.virusempire.Events.DeathHandler;
import de.broempire.virusempire.Events.FoodHandler;
import de.broempire.virusempire.Events.InteractEvent;
import de.broempire.virusempire.Events.InventoryHandler;
import de.broempire.virusempire.Events.MoveHandler;
import de.broempire.virusempire.Events.NatureHandler;
import de.broempire.virusempire.Events.ServerEvent;
import de.broempire.virusempire.Manager.CommandInteractManager;
import de.broempire.virusempire.Manager.MapvotingManager;
import de.broempire.virusempire.Manager.UpgradeManager;
import de.broempire.virusempire.MySQL.Bro;
import de.broempire.virusempire.MySQL.MySQL;
import de.broempire.virusempire.MySQL.MySQLStats;
import de.broempire.virusempire.MySQL.Ranking;
import de.broempire.virusempire.Shop.ShopManager;
import de.broempire.virusempire.Shop.VillagerInteract;
import de.broempire.virusempire.Teams.ScoreboardTeams;
import de.broempire.virusempire.Teams.Teams;
import de.broempire.virusempire.Utils.GameManager;
import de.broempire.virusempire.Utils.MapFile;
import de.broempire.virusempire.Utils.Mapreseter;
import de.broempire.virusempire.Utils.Methods;
import de.broempire.virusempire.Utils.ScoreboardManager;
import de.broempire.virusempire.Utils.Spawner;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Instrument;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_10_R1.CraftServer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class main extends JavaPlugin{
	
    public main plugin;

    private List<Player> setupmode = new ArrayList<>();

    public List <String> rot = new ArrayList<>();
    public List <String> grün = new ArrayList<>();
    public List <String> gelb = new ArrayList<>();
    public List <String> blau = new ArrayList<>();
    public List <String> spectator = new ArrayList<>();
    
    public static String teamplayerprefix;
    public static String teampremiumprefix;
    public static String teamvipprefix;
    public static String teambuilderprefix;
    public static String teamleadbuilderprefix;
    public static String teammoderatorprefix;
    public static String teamjrmoderatorprefix;
    public static String teamleadmoderatorprefix;
    public static String teamforenmodprefix;
    public static String teamleadforenmodprefix;
    public static String teamdeveloperprefix;
    public static String teamleaddeveloperprefix;
    public static String teamadminprefix;
    
    public Location lobbySpawn;

    public boolean canRespawnRot = true;
    public boolean canRespawnBlau = true;
    public boolean canRespawnGelb = true;
    public boolean canRespawnGrün = true;
    
    private MapFile mapFile;

    public static String pr = "§8▎ §cVirus §8» §7";
    public static String noperm = "§8▎ §6BroEmpire§8 ❘ §7 §cDu besitzt nicht die nötigen Rechte für diesen Befehl!";

    public static Plugin pl;

    public MySQL mysql;
    private MySQLStats mySQLStats;
    private Bro bro;

    private ScoreboardManager scoreboardManager;
    private ScoreboardTeams scoreboardTeams;
    private Methods methods;
    private ShopManager shopManager;
    private Spawner spawner;
    private MapvotingManager mapvotingManager;
    private CommandInteractManager commandInteractManager;
    private UpgradeManager upgradeManager;
    private Mapreseter mapreseter;
    private DeathHandler deathHandler;

    private boolean countdown = false;
    private boolean setup = false;
    public int time = 91;
/**
 *
 */
@Override
    public void onEnable() {

        pl = this;
        this.scoreboardManager = new ScoreboardManager(this);
        this.scoreboardTeams = new ScoreboardTeams(this);
        this.methods = new Methods(this);
        this.spawner = new Spawner(this);
        this.shopManager = new ShopManager(this);
        this.upgradeManager = new UpgradeManager(this);
        this.mapreseter = new Mapreseter(this);
        
        loadConfig();
        loadConfigData();
        
        scoreboardTeams.scoreboardteamsinit();

        this.mapvotingManager = new MapvotingManager(this);
        this.commandInteractManager = new CommandInteractManager(this);
        
        for (Hologram holo : HologramsAPI.getHolograms(this)) {
            holo.delete();
        }

        ConnectToMySQL();
        
        this.deathHandler = new DeathHandler(this);
        
        GameManager.setState(GameManager.LOBBY);
        registerEvents();
        registerCommands();
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        ((CraftServer)this.getServer()).getServer().setMotd("§2Mapvoting");
        
        new Ranking(this).set();
        
        for (MapFile file : this.getMaps()) {
            getServer().createWorld(new WorldCreator(file.getWorld()));
        }
        
        for (World w : getServer().getWorlds()) {
            w.setThundering(false);
            w.setStorm(false);
            w.setTime(6000L);
            for (Entity entity : w.getEntities()) {
                if (entity.getType().equals(EntityType.DROPPED_ITEM) || entity.getType().equals(EntityType.VILLAGER)) {
                    entity.remove();
                }
            }  
        }

        System.out.println("[Virus] Plugin gestartet!");

    }

    public MySQL getMysql() {
        return mysql;
    }
    
    @Override
    public void onDisable() {
        
        this.getMapreseter().resetMap();

        pl = null;

        System.out.println("[Virus] Plugin gestoppt");

    }
    private void registerEvents() {

        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new InteractEvent(this), this);
        pm.registerEvents(new ServerEvent(this), this);
        pm.registerEvents(new NatureHandler(this), this);
        pm.registerEvents(new FoodHandler(this), this);
        pm.registerEvents(deathHandler, this);
        pm.registerEvents(new DamageHandler(this), this);
        pm.registerEvents(new Chat(this), this);
        pm.registerEvents(new BlockHandler(this), this);
        pm.registerEvents(new VillagerInteract(this), this);
        pm.registerEvents(shopManager, this);
        pm.registerEvents(new InventoryHandler(), this);
        pm.registerEvents(new MoveHandler(this), this);
        pm.registerEvents(commandInteractManager, this);
        pm.registerEvents(mapvotingManager, this);
        pm.registerEvents(new BuyItemListener(this), this);


    }
    private void registerCommands() {

        this.getCommand("virus").setExecutor(new VirusCMD(this));
        this.getCommand("buildmode").setExecutor(new BuildmodeCMD());
        this.getCommand("stats").setExecutor(new StatsCMD(this));
        this.getCommand("start").setExecutor(new StartCommand(this));
    }

    public CommandInteractManager getCommandInteractManager() {
        return commandInteractManager;
    }

    public Mapreseter getMapreseter() {
        return mapreseter;
    }

    public DeathHandler getDeathHandler() {
        return deathHandler;
    }
    
    public static Plugin getPlugin() {
            return pl;
    }
    public void loadConfig() {

        FileConfiguration cfg = getConfig();

        cfg.addDefault("MySQL.Hostname", "localhost");
        cfg.addDefault("MySQL.Username", "BroEmpireMC");
        cfg.addDefault("MySQL.PW", "!BroEmpireSQL");
        cfg.addDefault("MySQL.Database", "BroEmpire");
        
        cfg.addDefault("Prefix.Player", "§eSpieler §7| ");
        cfg.addDefault("Prefix.Premium", "§6Premium §7| ");
        cfg.addDefault("Prefix.VIP", "§dVIP §7| ");
        cfg.addDefault("Prefix.Builder", "§9Build §7| ");
        cfg.addDefault("Prefix.LeadBuilder", "§9LeadBuild §7| ");
        cfg.addDefault("Prefix.Moderator", "§cMod §7| ");
        cfg.addDefault("Prefix.JrModerator", "§cJrMod §7| ");
        cfg.addDefault("Prefix.LeadModerator", "§cLeadMod §7| ");
        cfg.addDefault("Prefix.Forenmod", "§2F-Mod §7| ");
        cfg.addDefault("Prefix.LeadForenmod", "§2LeadF-Mod §7| ");
        cfg.addDefault("Prefix.Developer", "§bDev §7| ");
        cfg.addDefault("Prefix.LeadDeveloper", "§bLeadDev §7| ");
        cfg.addDefault("Prefix.Admin", "§4Admin §7| ");
        
        cfg.options().copyDefaults(true);

        saveConfig();

    }
    
    public void loadConfigData() {
        teamplayerprefix = this.getConfig().getString("Prefix.Player");
        teampremiumprefix = this.getConfig().getString("Prefix.Premium");
        teamvipprefix = this.getConfig().getString("Prefix.VIP");
        teambuilderprefix = this.getConfig().getString("Prefix.Builder");
        teamleadbuilderprefix = this.getConfig().getString("Prefix.LeadBuilder");
        teammoderatorprefix = this.getConfig().getString("Prefix.Moderator");
        teamjrmoderatorprefix = this.getConfig().getString("Prefix.JrModerator");
        teamleadmoderatorprefix = this.getConfig().getString("Prefix.LeadModerator");
        teamforenmodprefix = this.getConfig().getString("Prefix.Forenmod");
        teamleadforenmodprefix = this.getConfig().getString("Prefix.LeadForenmod");
        teamdeveloperprefix = this.getConfig().getString("Prefix.Developer");
        teamleaddeveloperprefix = this.getConfig().getString("Prefix.LeadDeveloper");
        teamadminprefix = this.getConfig().getString("Prefix.Admin");
        
        if (this.getLobbyLocation("LOBBY") != null) {
            lobbySpawn = this.getLobbyLocation("LOBBY");
        }
    }

    public ScoreboardTeams getScoreboardTeams() {
        return scoreboardTeams;
    }
    
    public void addMap(String map, String name, String builder) {
        FileConfiguration cfg = this.getConfig();
        
        int count = !cfg.contains("MAPS.COUNTER") ? 0 : cfg.getInt("MAPS.COUNTER");
        
        if (cfg.contains("MAPS.LIST")) {
            List<String> maps = cfg.getStringList("MAPS.LIST");
            if (!maps.contains(name)) {
                maps.add(name);
                MapFile file = new MapFile(getDataFolder().getPath(), name);
                file.setWorld(Bukkit.getWorld(map));
                file.setCreator(builder);
                cfg.set("MAPS.LIST", maps);
                
                count++;
                cfg.set("MAPS.COUNTER", count);
            }
        } else {
            List<String> maps = new ArrayList<>();
            maps.add(name);
            MapFile file = new MapFile(getDataFolder().getPath(), name);
            file.setWorld(Bukkit.getWorld(map));
            file.setCreator(builder);
            cfg.set("MAPS.LIST", maps);
            
            count++;
            cfg.set("MAPS.COUNTER", count);
        }
        
        this.saveConfig();
    }
    
    public List<MapFile> getMaps() {
        List<MapFile> maps = new ArrayList<>();
        if (getConfig().contains("MAPS.LIST")) {
            for (String map : getConfig().getStringList("MAPS.LIST")) {
                maps.add(new MapFile(getDataFolder().getPath(), map));
            }
        }
        return maps;
    }
    
    public String getNameByWorld(String world) {
        String map;
        for (MapFile file : getMaps()) {
            if (file.getWorld().equals(world)) {
                map = file.getName();
                return map;
            }
        }
        return null;
    }
    
    public MapFile getWinnerMapFile() {
        return mapFile;
    }
    
    public void ConnectToMySQL() {
        mysql = new MySQL (this.getConfig().getString("MySQL.Hostname"), this.getConfig().getString("MySQL.Database"), this.getConfig().getString("MySQL.Username"), this.getConfig().getString("MySQL.PW"));
        mysql.update("CREATE TABLE IF NOT EXISTS VirusStats (uuid varchar (64) PRIMARY KEY, played VARCHAR(100), wins VARCHAR(100), points VARCHAR(100), kills VARCHAR(100), deaths VARCHAR(100), disabledLevers VARCHAR(100));");

        mySQLStats = new MySQLStats(mysql);
        bro = new Bro(this, mysql);
        
    }
        
    public void setLobbyLocation(Location loc, String path) {
        this.getConfig().set("Locations." + path + ".world", loc.getWorld().getName());
        this.getConfig().set("Locations." + path + ".x", loc.getX());
        this.getConfig().set("Locations." + path + ".y", loc.getY());
        this.getConfig().set("Locations." + path + ".z", loc.getZ());
        this.getConfig().set("Locations." + path + ".yaw", loc.getYaw());
        this.getConfig().set("Locations." + path + ".pitch", loc.getPitch());
        
        this.saveConfig();
    }
    public Location getLobbyLocation(String path) {
        String world = this.getConfig().getString("Locations." + path + ".world");
        double x = this.getConfig().getDouble("Locations." + path + ".x");
        double y = this.getConfig().getDouble("Locations." + path + ".y");
        double z = this.getConfig().getDouble("Locations." + path + ".z");
        float yaw = this.getConfig().getInt("Locations." + path + ".yaw");
        float pitch = this.getConfig().getInt("Locations." + path + ".pitch");
        
        return new Location(getServer().getWorld(world), x, y, z, yaw, pitch);
    }

    public ScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }

    public List<String> getBlue() {
        return blau;
    }

    public List<String> getRed() {
        return rot;
    }

    public List<String> getGreen() {
        return grün;
    }

    public List<String> getYellow() {
        return gelb;
    }

    public Bro getBro() {
        return bro;
    }
    
    public boolean isSpectator(Player p) {
        return spectator.contains(p.getName());
    }

    public Methods getMethods() {
        return methods;
    }

    public ShopManager getShopManager() {
        return shopManager;
    }

    public MapvotingManager getMapvotingManager() {
        return mapvotingManager;
    }
    
    public boolean isSetupMode(Player p) {
        return setupmode.contains(p);
    }
    public void changeSetupMode(Player p) {
        if (isSetupMode(p)) {
            setupmode.remove(p);
            p.sendMessage(pr + "Du bist nun nicht mehr im §cSetupMode§7!");
            setup = false;
            startCountdown();
        } else {
            setupmode.add(p);
            p.sendMessage(pr + "Du bist nun im §aSetupMode§7!");
            setup = true;
        }
    }

    public Spawner getSpawner() {
        return spawner;
    }

    public MySQLStats getMySQLStats() {
        return mySQLStats;
    }

    public UpgradeManager getUpgradeManager() {
        return upgradeManager;
    }
    
    public boolean isReady(MapFile file) {
        if (!file.getBrickSpawner().isEmpty() && !file.getIronSpawner().isEmpty() && !file.getGoldSpawner().isEmpty()) {
            System.out.println("[Virus] Spawner --- GEFUNDEN");
            if (file.getSpawn("BLAU") != null && file.getSpawn("GRÜN") != null && file.getSpawn("GELB") != null && file.getSpawn("ROT") != null) {
                System.out.println("[Virus] Spawns --- GEFUNDEN");
                if (file.getVillager() != null && file.getVillager().size() >= 4) {
                    System.out.println("[Virus] Villager --- GEFUNDEN");
                    if (file.getLever("BLAU") != null && file.getLever("GRÜN") != null && file.getLever("GELB") != null && file.getLever("ROT") != null) {
                        System.out.println("[Virus] Lever --- GEFUNDEN");
                        System.out.println("[Virus] ------------------");
                        System.out.println("[Virus] SUCCESS");
                        System.out.println("[Virus] ------------------");
                        System.out.println("[Virus] Die Map " + file.getName() + " kann eingesetzt werden!");
                        return true;
                    } else {
                        System.out.println("[Virus] Lever --- NICHT GEFUNDEN");
                    }
                } else {
                    System.out.println("[Virus] Villager --- NICHT GEFUNDEN");
                }
            } else {
                System.out.println("[Virus] Spawns --- NICHT GEFUNDEN");
            }
        } else {
            System.out.println("[Virus] Spawner --- NICHT GEFUNDEN");
        }
        return false;
    }
    
    public boolean isCountdown() {
        return countdown;
    }
    
    public static void sendUsage(String usage, String example, Player p) {
        p.sendMessage("§8§m--------§r§7»§6Benutzung§7«§8§m--------");
        p.sendMessage(" ");
        p.sendMessage("§4" + usage);
        if (example != null) {
            p.sendMessage("§2Beispiel§7: §6" + example);
        }
        p.sendMessage(" ");
        p.sendMessage("§8§m--------§r§7»§6Benutzung§7«§8§m--------");
    }
    
    public void startCountdown() {
        countdown = true;
        for (Player p : getServer().getOnlinePlayers()) {
            p.setExp(0.99F);
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                if (getServer().getOnlinePlayers().isEmpty() || setup) {
                    countdown = false;
                    cancel();
                }
                time--;
                if (getServer().getOnlinePlayers().size() >= 4 && time > 60) {
                    time = 60;
                    getServer().broadcastMessage(pr + "§aDie Mindestanzahl an Spielern ist erreicht!");
                } else if (Bukkit.getOnlinePlayers().size() < 4 && time <= 60 && time > 10) {
                    time = 90;
                    getServer().broadcastMessage(pr + "§cEs sind nicht genug Spieler online!");
                }
                float exp = 0.99F/90*time;
                for (Player p : getServer().getOnlinePlayers()) {
                    p.setExp(exp);
                    p.setLevel(time);
                }
                if (time > 5) {
                    for (Player p : getServer().getOnlinePlayers()) {
                        ActionAPI.sendActionBar(p, "§7Die Runde startet in §6" + time + " §7Sekunden");
                    }
                }
                switch (time) {
                    case 30:
                    case 60:
                        for (Player p : getServer().getOnlinePlayers()) {
                            p.playNote(p.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                            p.sendMessage(pr + "§7Noch §b" + time+ " §7Sekunden bis zum Start!");
                        }
                        break;
                    case 50:
                    case 40:
                    case 20:
                        for (Player p : getServer().getOnlinePlayers()) {
                            p.playNote(p.getLocation(), Instrument.PIANO, new Note (0, Note.Tone.E, true));
                        }
                        break;
                    case 10:
                        mapFile = new MapFile(getDataFolder().getPath(), mapvotingManager.getWinner());
                        for (Player p : getServer().getOnlinePlayers()) {
                            p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 10F, 10F);
                            p.sendMessage(pr + "Es wird die Map §6" + mapFile.getName() + " §7gespielt!");
                            TitleAPI.sendTitle(p, 10, 40, 20, "§6" + mapFile.getName(), "§7von: §c" + mapFile.getCreator());
                            p.getInventory().setItem(4, new ItemStack(Material.AIR));
                            getScoreboardManager().updateScoreboard(p);
                            getServer().createWorld(new WorldCreator(mapFile.getWorld()));
                        }
                        getMethods().setupGame();
                        ((CraftServer)getServer()).getServer().setMotd("§2" + mapFile.getName());
                        break;
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                    case 1:
                        for (Player p : getServer().getOnlinePlayers()) {
                            p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 3F);
                            TitleAPI.sendTitle(p, 5, 20, 0, "§6" + time, null);
                        }
                        break;
                    case 0:
                        getServer().broadcastMessage(pr + "§8§m----------§r §6Infos §8§m----------");
                        getServer().broadcastMessage(pr + "§bMap§7: §6" + getWinnerMapFile().getName() + " §7von: §c" + getWinnerMapFile().getCreator());
                        getServer().broadcastMessage(pr + "§7Nutze §3@a §7oder §3@all §7für globale Chatnachrichten!");
                        getServer().broadcastMessage(pr + "§8§m----------§r §6Infos §8§m----------");
                        getSpawner().startSpawning(mapFile.getName());
                        commandInteractManager.setMapFile(mapFile);
                        for (Player p : getServer().getOnlinePlayers()) {
                            if (!getMethods().hasTeam(p)) {
                                if (blau.size() <= rot.size() && blau.size() <= gelb.size() && blau.size() <= grün.size()) {
                                    getScoreboardTeams().addTeam(p, Teams.BLUE);
                                } else if (rot.size() <= blau.size() && rot.size() <= gelb.size() && rot.size() <= grün.size()){
                                    getScoreboardTeams().addTeam(p, Teams.RED);
                                } else if (gelb.size() <= rot.size() && gelb.size() <= blau.size() && gelb.size() <= grün.size()) {
                                    getScoreboardTeams().addTeam(p, Teams.YELLOW);
                                } else if (grün.size() <= rot.size() && grün.size() <= gelb.size() && grün.size() <= blau.size()) {
                                    getScoreboardTeams().addTeam(p, Teams.GREEN);
                                }
                            }
                            String team = getMethods().getExactTeam(p).toUpperCase();
                            p.getInventory().clear();
                            p.setExp(0F);
                            p.setLevel(0);
                            TitleAPI.sendTitle(p, 10, 40, 20, "§2Los geht's!", "§6Viel Spaß!");
                            p.teleport(getWinnerMapFile().getSpawn(team));
                            p.setGameMode(GameMode.SURVIVAL);
                            p.setHealth(20D);
                            p.setFoodLevel(20);
                            getScoreboardManager().sendScoreboardGame(p);
                            mySQLStats.addPlayed(p.getUniqueId().toString(), 1);
                            GameManager.setState(GameManager.GAME);
                        }
                        cancel();
                        break;
                }
            }
        }.runTaskTimer(this, 0L, 20L);
        
    }
		
}
