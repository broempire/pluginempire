/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Teams;

import com.nametagedit.plugin.NametagEdit;
import de.broempire.virusempire.APIs.ScoreboardTeam;
import de.broempire.virusempire.Utils.GameManager;
import de.broempire.virusempire.main;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author Fabian
 */
public class ScoreboardTeams {
    
    private main plugin;
    
    private Map<String, ScoreboardTeam> teams = new HashMap<>();
    
    public ScoreboardTeams(main plugin) {
        this.plugin = plugin;
    }
    
    public void scoreboardteamsinit()
    {
        
        ScoreboardTeam admin = new ScoreboardTeam("00ADMIN");
        admin.setPrefix(main.teamadminprefix);
        teams.put("ADMIN", admin);
        ScoreboardTeam developer = new ScoreboardTeam("01DEVELOPER");
        developer.setPrefix(main.teamdeveloperprefix);
        teams.put("DEVELOPER", developer);
        ScoreboardTeam leaddeveloper = new ScoreboardTeam("02LEADDEVELOPER");
        leaddeveloper.setPrefix(main.teamleaddeveloperprefix);
        teams.put("LEADDEVELOPER", leaddeveloper);
        ScoreboardTeam moderator = new ScoreboardTeam("03MODERATOR");
        moderator.setPrefix(main.teammoderatorprefix);
        teams.put("MODERATOR", moderator);
        ScoreboardTeam jrmoderator = new ScoreboardTeam("04JRMODERATOR");
        jrmoderator.setPrefix(main.teamjrmoderatorprefix);
        teams.put("JRMODERATOR", jrmoderator);
        ScoreboardTeam leadmoderator = new ScoreboardTeam("05LEADMODERATOR");
        leadmoderator.setPrefix(main.teamleadmoderatorprefix);
        teams.put("LEADMODERATOR", leadmoderator);
        ScoreboardTeam forenmod = new ScoreboardTeam("06FORENMOD");
        forenmod.setPrefix(main.teamforenmodprefix);
        teams.put("FORENMOD", forenmod);
        ScoreboardTeam leadforenmod = new ScoreboardTeam("07LEADFORENMOD");
        leadforenmod.setPrefix(main.teamleadforenmodprefix);
        teams.put("LEADFORENMOD", leadforenmod);
        ScoreboardTeam builder = new ScoreboardTeam("08BUILDER");
        builder.setPrefix(main.teambuilderprefix);
        teams.put("BUILDER", builder);
        ScoreboardTeam leadbuilder = new ScoreboardTeam("09LEADBUILDER");
        leadbuilder.setPrefix(main.teamleadbuilderprefix);
        teams.put("LEADBUILDER", leadbuilder);
        ScoreboardTeam vip = new ScoreboardTeam("10VIP");
        vip.setPrefix(main.teamvipprefix);
        teams.put("VIP", vip);
        ScoreboardTeam premium = new ScoreboardTeam("11PREMIUM");
        premium.setPrefix(main.teampremiumprefix);
        teams.put("PREMIUM", premium);
        ScoreboardTeam player = new ScoreboardTeam("12PLAYER");
        player.setPrefix(main.teamplayerprefix);
        teams.put("PLAYER", player);
        
        ScoreboardTeam blue = new ScoreboardTeam("tblue");
        blue.setPrefix("§9Blau §7| ");
        teams.put("Blau", blue);
        ScoreboardTeam red = new ScoreboardTeam("tred");
        red.setPrefix("§cRot §7| ");
        teams.put("Rot", red);
        ScoreboardTeam yellow = new ScoreboardTeam("tyellow");
        yellow.setPrefix("§eGelb §7| ");
        teams.put("Gelb", yellow);
        ScoreboardTeam green = new ScoreboardTeam("tgreen");
        green.setPrefix("§2Grün §7| ");
        teams.put("Grün", green);
        ScoreboardTeam spectator = new ScoreboardTeam("tspectator");
        spectator.setPrefix("§7");
        teams.put("Kein Team", spectator);
        
    }
    public void updatePlayer(Player p) {
        ScoreboardTeam team;
        if (GameManager.isState(GameManager.GAME)) {
            if (plugin.getMethods().hasTeam(p)) {
                team = teams.get(plugin.getMethods().getExactTeam(p));

                p.setPlayerListName(team.getPrefix() + p.getName());
                p.setDisplayName(team.getPrefix()+p.getName());
                p.setCustomName(plugin.getMethods().getColor(p) + p.getName());
            } else {
                team = teams.get("Kein Team");

                p.setPlayerListName(team.getPrefix() + p.getName());
                p.setDisplayName(team.getPrefix() + p.getName());
                p.setCustomName(team.getPrefix() + p.getName());
            }
        } else {
            if (plugin.getMethods().hasTeam(p)) {
                team = teams.get(plugin.getMethods().getExactTeam(p));

                p.setPlayerListName(team.getPrefix() + p.getName());
                p.setDisplayName(team.getPrefix()+p.getName());
                p.setCustomName(plugin.getMethods().getColor(p) + p.getName());
            } else {
                team = teams.get(plugin.getBro().getGroup(p.getUniqueId().toString()));

                p.setPlayerListName(team.getPrefix() + p.getName());
                p.setDisplayName(team.getPrefix()+p.getName());
                p.setCustomName(team.getPrefix() + p.getName());
            }
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                NametagEdit.getApi().setPrefix(p, team.getPrefix());
                for (Player online : plugin.getServer().getOnlinePlayers()) {
                    plugin.getScoreboardManager().updateScoreboard(online);
                }
            }
        }.runTaskAsynchronously(plugin);
    }
    
    public void addTeam(Player p, Teams team) {
        plugin.getMethods().clearFromArray(p);
        switch (team) {
            case BLUE:
                if (plugin.getBlue().size() >= 2) {
                    p.sendMessage(main.pr + "§cDieses Team ist bereits voll!");
                } else {
                    plugin.blau.add(p.getName());
                    p.sendMessage(main.pr + "Du bist nun in Team §9Blau§7!");
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
                    updatePlayer(p);
                }
                break;
            case RED:
                 if (plugin.getRed().size() >= 2) {
                    p.sendMessage(main.pr + "§cDieses Team ist bereits voll!");
                } else {
                    plugin.rot.add(p.getName());
                    p.sendMessage(main.pr + "Du bist nun in Team §cRot§7!");
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
                    updatePlayer(p);
                }
                break;
            case YELLOW:
                 if (plugin.getYellow().size() >= 2) {
                    p.sendMessage(main.pr + "§cDieses Team ist bereits voll!");
                } else {
                    plugin.gelb.add(p.getName());
                    p.sendMessage(main.pr + "Du bist nun in Team §eGelb§7!");
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
                    updatePlayer(p);
                }
                break;
            case GREEN:
                 if (plugin.getGreen().size() >= 2) {
                    p.sendMessage(main.pr + "§cDieses Team ist bereits voll!");
                } else {
                    plugin.grün.add(p.getName());
                    p.sendMessage(main.pr + "Du bist nun in Team §2Grün§7!");
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
                    updatePlayer(p);
                }
                break;
            default:
                plugin.spectator.add(p.getName());
                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3, 3);
                updatePlayer(p);
                break;
        }
    }
}
	
