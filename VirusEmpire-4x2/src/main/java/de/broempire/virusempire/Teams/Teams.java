package de.broempire.virusempire.Teams;

import de.broempire.virusempire.APIs.ActionAPI;
import de.broempire.virusempire.APIs.TitleAPI;
import de.broempire.virusempire.Utils.ResetBlock;
import de.broempire.virusempire.main;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public enum Teams {
	
    RED ("Rot", "§c"),
    BLUE ("Blau", "§9"),
    YELLOW ("Gelb", "§e"),
    GREEN ("Grün", "§a"),
    SPECTATOR ("Spectator", "§8");

    private final String name;
    private final String color;
    
    private HashMap<Teams, Boolean> starting = new HashMap<>();

    private Teams (String name, String color) {
            this.name = name;
            this.color = color;
    }

    public String getName() {
            return name;
    }
    
    public String getColor() {
            return color;
    }

    public void sendMessage(String message, main plugin) {
        switch(this) {
            case BLUE:
                for (String s : plugin.getBlue()) {
                    Player p = Bukkit.getPlayer(s);
                    p.sendMessage(main.pr + message);
                }
                break;
            case RED:
                for (String s : plugin.getRed()) {
                    Player p = Bukkit.getPlayer(s);
                    p.sendMessage(main.pr + message);
                }
                break;
            case YELLOW:
                for (String s : plugin.getYellow()) {
                    Player p = Bukkit.getPlayer(s);
                    p.sendMessage(main.pr + message);
                }
                break;
            case GREEN:
                for (String s : plugin.getGreen()) {
                    Player p = Bukkit.getPlayer(s);
                    p.sendMessage(main.pr + message);
                }
                break;
            case SPECTATOR:
                for (String s : plugin.spectator) {
                    Player p = Bukkit.getPlayer(s);
                    p.sendMessage(main.pr + message);
                }
                break;
        }
    }
    
    public void giveExp(float exp, main plugin) {
        switch(this) {
            case BLUE:
                for (String s : plugin.getBlue()) {
                    Player p = Bukkit.getPlayer(s);
                    p.setExp(exp);
                }
                break;
            case RED:
                for (String s : plugin.getRed()) {
                    Player p = Bukkit.getPlayer(s);
                    p.setExp(exp);
                }
                break;
            case YELLOW:
                for (String s : plugin.getYellow()) {
                    Player p = Bukkit.getPlayer(s);
                    p.setExp(exp);
                }
                break;
            case GREEN:
                for (String s : plugin.getGreen()) {
                    Player p = Bukkit.getPlayer(s);
                    p.setExp(exp);
                }
                break;
        }
    }

    public void sendActionBar(String message, main plugin) {
        switch(this) {
            case BLUE:
                for (String s : plugin.getBlue()) {
                    Player p = Bukkit.getPlayer(s);
                    ActionAPI.sendActionBar(p, message);
                }
                break;
            case RED:
                for (String s : plugin.getRed()) {
                    Player p = Bukkit.getPlayer(s);
                    ActionAPI.sendActionBar(p, message);
                }
                break;
            case YELLOW:
                for (String s : plugin.getYellow()) {
                    Player p = Bukkit.getPlayer(s);
                    ActionAPI.sendActionBar(p, message);
                }
                break;
            case GREEN:
                for (String s : plugin.getGreen()) {
                    Player p = Bukkit.getPlayer(s);
                    ActionAPI.sendActionBar(p, message);
                }
                break;
        }
    }

    public void sendTitle(String title, String subtitle, int time,main plugin) {
        switch(this) {
            case BLUE:
                for (String s : plugin.getBlue()) {
                    Player p = Bukkit.getPlayer(s);
                    TitleAPI.sendTitle(p, 20, time, 20, title, subtitle);
                }
                break;
            case RED:
                for (String s : plugin.getRed()) {
                    Player p = Bukkit.getPlayer(s);
                    TitleAPI.sendTitle(p, 20, time, 20, title, subtitle);
                }
                break;
            case YELLOW:
                for (String s : plugin.getYellow()) {
                    Player p = Bukkit.getPlayer(s);
                    TitleAPI.sendTitle(p, 20, time, 20, title, subtitle);
                }
                break;
            case GREEN:
                for (String s : plugin.getGreen()) {
                    Player p = Bukkit.getPlayer(s);
                    TitleAPI.sendTitle(p, 20, time, 20, title, subtitle);
                }
                break;
        }
    }

    public void playSound(Sound sound, float f, main plugin) {
        switch(this) {
            case BLUE:
                for (String s : plugin.getBlue()) {
                    Player p = Bukkit.getPlayer(s);
                    p.playSound(p.getLocation(), sound, f, f);
                }
                break;
            case RED:
                for (String s : plugin.getRed()) {
                    Player p = Bukkit.getPlayer(s);
                    p.playSound(p.getLocation(), sound, f, f);
                }
                break;
            case YELLOW:
                for (String s : plugin.getYellow()) {
                    Player p = Bukkit.getPlayer(s);
                    p.playSound(p.getLocation(), sound, f, f);
                }
                break;
            case GREEN:
                for (String s : plugin.getGreen()) {
                    Player p = Bukkit.getPlayer(s);
                    p.playSound(p.getLocation(), sound, f, f);
                }
                break;
        }
    }
        
    public void shutDown(Player p, Teams team, main plugin) {
        if (starting.containsKey(team)) {
            starting.replace(team, Boolean.FALSE);
        } else {
            starting.put(team, Boolean.FALSE);
        }
        team.sendTitle("§4Achtung!", null, 40, plugin);
        team.playSound(Sound.ENTITY_WITHER_SPAWN, 5F, plugin);
        team.sendMessage("§cIn deinem Nucleus wurde ein Virus entdeckt!", plugin);
        team.sendMessage("§4Der Virus verbreitet sich und wird deinen Nucleus zerstören!", plugin);
        team.sendActionBar("§6Entferne den Virus, indem du deinen Sicherheitshebel benutzt!", plugin);
        new BukkitRunnable() {
            int percent = 0;
            @Override
            public void run() {
                if (starting.get(team).equals(Boolean.FALSE)) {
                    percent++;
                    if (percent % 10 == 0) {
                        team.playSound(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1F, plugin);
                    }
                    StringBuilder bar = new StringBuilder();
                    bar.append("§e" + percent + "% ");
                    int times = percent/5;
                    for (int i = 0; i < times; i++) {
                        bar.append("§c§l▶");
                    }
                    for (int i = 0; i < 20-times; i++) {
                        bar.append("§0§l▶");
                    }
                    bar.append(" §c§lInkubation §4§l...");
                    team.sendActionBar(bar.toString(), plugin);
                    if (percent==100) {
                        percent = 0;
                        plugin.getUpgradeManager().addUpgradeLevel(p, 5);
                        p.sendMessage(main.pr + "§aVirus erfolgreich installiert!");
                        plugin.getBro().setCoins(plugin.getBro().getCoins(p.getUniqueId().toString()) + 10, p.getUniqueId().toString());
                        p.sendMessage(main.pr + "Du hast §610 Coins §7erhalten!");
                        plugin.getMySQLStats().addPoints(p.getUniqueId().toString(), 10);
                        team.sendActionBar("§8» §cWichtige Fähigkeiten wurden beeinflusst! §8«", plugin);
                        team.sendMessage("§4Dein Nucleus wurde irreversibel infiziert!", plugin);
                        team.giveExp((float) 0.99, plugin);
                        team.playSound(Sound.ENTITY_ENDERDRAGON_DEATH, 10F, plugin);
                        switch (team) {
                            case BLUE:
                                plugin.canRespawnBlau = false;
                                plugin.getWinnerMapFile().getLever("BLAU").getBlock().setType(Material.AIR);
                                plugin.getWinnerMapFile().getLever("BLAU").getWorld().playEffect(plugin.getWinnerMapFile().getLever("BLAU"), Effect.EXPLOSION, 5);
                                plugin.getWinnerMapFile().getLever("BLAU").getWorld().playSound(plugin.getWinnerMapFile().getLever("BLAU"), Sound.ENTITY_GENERIC_EXPLODE, 5F, 5F);
                                plugin.getMapreseter().addBlock(new ResetBlock(Material.LEVER, plugin.getWinnerMapFile().getLever("BLAU").getBlock()));
                                break;
                            case RED:
                                plugin.canRespawnRot = false;
                                plugin.getWinnerMapFile().getLever("ROT").getBlock().setType(Material.AIR);
                                plugin.getWinnerMapFile().getLever("ROT").getWorld().playEffect(plugin.getWinnerMapFile().getLever("ROT"), Effect.EXPLOSION, 5);
                                plugin.getWinnerMapFile().getLever("ROT").getWorld().playSound(plugin.getWinnerMapFile().getLever("ROT"), Sound.ENTITY_GENERIC_EXPLODE, 5F, 5F);
                                plugin.getMapreseter().addBlock(new ResetBlock(Material.LEVER, plugin.getWinnerMapFile().getLever("ROT").getBlock()));
                                break;
                            case GREEN:
                                plugin.canRespawnGrün = false;
                                plugin.getWinnerMapFile().getLever("GRÜN").getBlock().setType(Material.AIR);
                                plugin.getWinnerMapFile().getLever("GRÜN").getWorld().playEffect(plugin.getWinnerMapFile().getLever("GRÜN"), Effect.EXPLOSION, 5);
                                plugin.getWinnerMapFile().getLever("GRÜN").getWorld().playSound(plugin.getWinnerMapFile().getLever("GRÜN"), Sound.ENTITY_GENERIC_EXPLODE, 5F, 5F);
                                plugin.getMapreseter().addBlock(new ResetBlock(Material.LEVER, plugin.getWinnerMapFile().getLever("GRÜN").getBlock()));
                                break;
                            case YELLOW:
                                plugin.canRespawnGelb = false;
                                plugin.getWinnerMapFile().getLever("GELB").getBlock().setType(Material.AIR);
                                plugin.getWinnerMapFile().getLever("GELB").getWorld().playEffect(plugin.getWinnerMapFile().getLever("GELB"), Effect.EXPLOSION, 5);
                                plugin.getWinnerMapFile().getLever("GELB").getWorld().playSound(plugin.getWinnerMapFile().getLever("GELB"), Sound.ENTITY_GENERIC_EXPLODE, 5F, 5F);
                                plugin.getMapreseter().addBlock(new ResetBlock(Material.LEVER, plugin.getWinnerMapFile().getLever("GELB").getBlock()));
                                break;
                        }
                        for (Player p : plugin.getServer().getOnlinePlayers()) {
                            plugin.getScoreboardManager().updateScoreboard(p);
                        }
                        plugin.getServer().broadcastMessage(main.pr + "§7Der Nucleus von Team "+ team.getColor()+ team.getName() + "§7 wurde von " + p.getCustomName() + " §cirreversibel infiziert§7!");
                        cancel();
                    }
                } else {
                    cancel();
                }
            }
        }.runTaskTimer(plugin, 0L, plugin.getUpgradeManager().getTime(team)*20/100);
    }
        
    public void restart(Player p, Teams team, main plugin) {
        if (starting.containsKey(team)) {
            if (starting.get(team).equals(Boolean.TRUE)) {
                p.sendMessage(main.pr + "§aDer Nucleus wird bereits wieder hochgefahren!");
            } else {
                starting.replace(team, Boolean.TRUE);
            }
        } else {
            starting.put(team, Boolean.TRUE);
        }
        team.sendMessage("§aVirus wurde entfernt!", plugin);
        team.sendTitle("§cSysteme", "§4werden neu gestartet...", 40, plugin);
        p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3F, 3F);
        new BukkitRunnable() {
            int percent = 0;
            @Override
            public void run() {
                if (starting.get(team).equals(Boolean.TRUE)) {
                    percent += 2;
                    StringBuilder bar = new StringBuilder();
                    bar.append("§e" + percent + "% ");
                    int times = percent/5;
                    for (int i = 0; i < times; i++) {
                        bar.append("§a§l▶");
                    }
                    for (int i = 0; i < 20-times; i++) {
                        bar.append("§0§l▶");
                    }
                    bar.append(" §2§lhochfahren §a§l...");
                    team.sendActionBar(bar.toString(), plugin);
                    if (percent >= 100) {
                        percent = 0;
                        team.sendActionBar("§8» §2Systeme neu gestartet! §8«", plugin);
                        team.playSound(Sound.ENTITY_WITHER_SPAWN, 6F, plugin);
                        cancel();
                    }
                } else {
                    cancel();
                }
            }
        }.runTaskTimer(plugin, 0, 1L);
    }
}
