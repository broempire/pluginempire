package de.broempire.virusempire.MySQL;

import de.broempire.virusempire.main;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;

public class Ranking {

    private main plugin;
    
    private MySQL mySQL;
    private MySQLStats mySQLStats;
    
    HashMap<Integer, String> rang = new HashMap<>();
    
    public Ranking(main plugin) {
        this.plugin = plugin;
        this.mySQLStats = plugin.getMySQLStats();
        this.mySQL = plugin.getMysql();
    }
    public void set(){
        ResultSet rs = mySQL.query("SELECT * FROM VirusStats ORDER BY Points DESC LIMIT 3");

        int in = 0;
            try {
                while(rs.next()){
                    in++;
                    rang.put(in, rs.getString("uuid"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        Location loc = plugin.getLobbyLocation("RANKING.1");
        Location loc2 = plugin.getLobbyLocation("RANKING.2");
        Location loc3 = plugin.getLobbyLocation("RANKING.3");

        List<Location> locs = new ArrayList<>();
        locs.add(loc);
        locs.add(loc2);
        locs.add(loc3);

        for(int i = 0; i < locs.size(); i++){
            int id = i+1;
            if(rang.get(id) == null){
                if(locs.get(i).getBlock().getType() != Material.SKULL){
                    return;
                }
                Skull s = (Skull) locs.get(i).getBlock().getState();
                s.setSkullType(SkullType.ZOMBIE);
                s.update();

                Location newloc = new Location(locs.get(i).getWorld(), locs.get(i).getX(), locs.get(i).getY() -1, locs.get(i).getZ());
                if(!(newloc.getBlock().getState() instanceof Sign)){
                    newloc.getBlock().setType(Material.WALL_SIGN);
                }
                if(newloc.getBlock().getType() == Material.WALL_SIGN){
                    BlockState b = newloc.getBlock().getState();
                    Sign S = (Sign) b;
                    S.setLine(0, "§4§l" + id + "." + " Platz");
                    S.setLine(1, "§c-/-");
                    S.setLine(2, "§8» §9Wins: §0-/-");
                    S.setLine(3, "§8» §9Punkte: §0-/-");
                    S.update();
                }
            } else {
                if(locs.get(i).getBlock().getType() != Material.SKULL){
                    return;
                }
                Skull s = (Skull) locs.get(i).getBlock().getState();
                s.setSkullType(SkullType.PLAYER);
                String name = plugin.getBro().getName(rang.get(id));
                s.setOwner(name);
                s.update();

                Location newloc = new Location(locs.get(i).getWorld(), locs.get(i).getX(), locs.get(i).getY() -1, locs.get(i).getZ());
                if(!(newloc.getBlock().getState() instanceof Sign)){
                    newloc.getBlock().setType(Material.WALL_SIGN);
                }
                if(newloc.getBlock().getType() == Material.WALL_SIGN){
                    BlockState b = newloc.getBlock().getState();
                    Sign S = (Sign) b;

                    S.setLine(0, "§4§l" + id + "." + " Platz");
                    S.setLine(1, name);
                    S.setLine(2, "§8» §9Wins: §0§l " + mySQLStats.getWins(rang.get(id)));
                    S.setLine(3, "§8» §9Punkte: §0§l " + mySQLStats.getPoints(rang.get(id)));
                    S.update();
                }
            }
        }
    }

}