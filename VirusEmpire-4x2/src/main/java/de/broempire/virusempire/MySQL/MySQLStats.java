/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.MySQL;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Matti
 */
public class MySQLStats {
    
    private MySQL mySQL;

    public MySQLStats(MySQL mySQL) {
        this.mySQL = mySQL;
    }
    
    public boolean playerExists(String uuid){	
	try{
            ResultSet rs = mySQL.query("SELECT * FROM VirusStats WHERE uuid= '" + uuid + "'");
		
            if (rs.next()) {
                return true;
            }
            rs.close();
            return false;
	}catch(SQLException e){
            e.printStackTrace();
	}
        return false;
    }
	
    public void createPlayer(String uuid){
        if(!(playerExists(uuid))){
            mySQL.update("INSERT INTO VirusStats (uuid, played, wins, points, kills, deaths, disabledLevers) VALUES ('" + uuid + "', '0', '0', '0', '0', '0', '0');");
        }
    }

    public Integer getWins(String uuid){
        if(playerExists(uuid)){
            try{
                ResultSet rs = mySQL.query("SELECT * FROM VirusStats WHERE uuid= '" + uuid + "'");
                while (rs.next()) {
                    return rs.getInt("wins");
                }	
            }catch(SQLException e){
                e.printStackTrace();
            }		
        }else{
            createPlayer(uuid);
            getWins(uuid);
        }
        return null;
    }

    public Integer getDisabled(String uuid){
        if(playerExists(uuid)){
            try{
                ResultSet rs = mySQL.query("SELECT * FROM VirusStats WHERE uuid = '" + uuid + "'");
                while (rs.next()) {
                    return rs.getInt("disabledLevers");
                }	
            }catch(SQLException e){
                e.printStackTrace();
            }	
        }else{
            createPlayer(uuid);
            getDisabled(uuid);
        }
        return null;
    }

    public Integer getKills(String uuid){
        if(playerExists(uuid)){
            try{
                ResultSet rs = mySQL.query("SELECT * FROM VirusStats WHERE uuid= '" + uuid + "'");
                while (rs.next()) {
                    return rs.getInt("kills");
                }	
            }catch(SQLException e){
                e.printStackTrace();
            }

        }else{
            createPlayer(uuid);
            getKills(uuid);
        }
        return null;
    }

    public Integer getDeaths(String uuid){
        if(playerExists(uuid)){
            try{
                ResultSet rs = mySQL.query("SELECT * FROM VirusStats WHERE uuid= '" + uuid + "'");
                while (rs.next()) {
                    return rs.getInt("deaths");
                }
            }catch(SQLException e){
                e.printStackTrace();
            }		
        }else{
            createPlayer(uuid);
            getWins(uuid);
        }
        return null;
    }

    public void setWins(String uuid, Integer wins){
        if(playerExists(uuid)){
            mySQL.update("UPDATE VirusStats SET wins = '" + wins + "' WHERE uuid = '" + uuid + "';");
        }else{
            createPlayer(uuid);
            setWins(uuid, wins);
        }
    }


    public void addWins(String uuid, Integer wins){
        if(playerExists(uuid)){
            setWins(uuid, getWins(uuid) + wins);
        }else{
            createPlayer(uuid);
            addWins(uuid, wins);
        }
    }

    public Integer getPlayed(String uuid){
        if(playerExists(uuid)){
            try{
                ResultSet rs = mySQL.query("SELECT * FROM VirusStats WHERE uuid = '" + uuid + "'");
                while (rs.next()) {
                    return rs.getInt("played");
                }
            }catch(SQLException e){
                e.printStackTrace();
            }
        }else{
            createPlayer(uuid);
            getWins(uuid);
        }
        return null;
    }

    public void setPlayed(String uuid, Integer played){
        if(playerExists(uuid)){
            mySQL.update("UPDATE VirusStats SET played = '" + played + "' WHERE uuid = '" + uuid + "';");
        }else{
            createPlayer(uuid);
            setPlayed(uuid, played);
        }
    }


    public void addPlayed(String uuid, Integer played){
        if(playerExists(uuid)){
            setPlayed(uuid, getPlayed(uuid) + played);
        }else{
            createPlayer(uuid);
            addPlayed(uuid, played);
        }
    }

    public Integer getPoints(String uuid){
        if(playerExists(uuid)){
            try{
                ResultSet rs = mySQL.query("SELECT * FROM VirusStats WHERE uuid= '" + uuid + "'");
                while (rs.next()) {
                    return rs.getInt("points");
                }
            }catch(SQLException e){
                e.printStackTrace();
            }		
        }else{
            createPlayer(uuid);
            getPoints(uuid);
        }
        return null;
    }

    public void setPoints(String uuid, Integer points){
        if(playerExists(uuid)){
            mySQL.update("UPDATE VirusStats SET points = '" + points + "' WHERE uuid = '" + uuid + "';");
        }else{
            createPlayer(uuid);
            setPoints(uuid, points);
        }
    }


    public void addPoints(String uuid, Integer points){
        if(playerExists(uuid)){
            setPoints(uuid, getPoints(uuid) + points);
        }else{
            createPlayer(uuid);
            addPoints(uuid, points);
        }
    }



    public void setKills(String uuid, Integer kills){
        if(playerExists(uuid)){
            mySQL.update("UPDATE VirusStats SET kills = '" + kills + "' WHERE uuid= '" + uuid + "';");
        }else{
            createPlayer(uuid);
            setKills(uuid, kills);
        }
    }


    public void addKills(String uuid, Integer kills){
        if(playerExists(uuid)){
            setKills(uuid, getKills(uuid) + kills);
        }else{
            createPlayer(uuid);
            addKills(uuid, kills);
        }
    }

    public void setDeaths(String uuid, Integer deaths){
        if(playerExists(uuid)){
            mySQL.update("UPDATE VirusStats SET deaths = '" + deaths + "' WHERE uuid = '" + uuid + "';");
        }else{
            createPlayer(uuid);
            setDeaths(uuid, deaths);
        }
    }


    public void addDeaths(String uuid, Integer deaths){
        if(playerExists(uuid)){
            setDeaths(uuid, getDeaths(uuid) + deaths);
        }else{
            createPlayer(uuid);
            addDeaths(uuid, deaths);
        }
    }

    public void setDisabled(String uuid, Integer levers){
        if(playerExists(uuid)){
            mySQL.update("UPDATE VirusStats SET disabledLevers = '" + levers + "' WHERE uuid = '" + uuid + "';");
        }else{
            createPlayer(uuid);
            setDisabled(uuid, levers);
        }
    }


    public void addDisabled(String uuid, Integer levers){
        if(playerExists(uuid)){
            setDisabled(uuid, getDisabled(uuid) + levers);
        }else{
            createPlayer(uuid);
            addDisabled(uuid, levers);
        }
    }
    
}
