/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Events;

import de.broempire.virusempire.APIs.ItemBuilder;
import de.broempire.virusempire.main;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/**
 *
 * @author Matti
 */
public class BuyItemListener implements Listener {
    
    private main plugin;

    public BuyItemListener(main plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onBuyItem(BuyItemEvent e) {
        ItemStack stack = e.getItemStack();
        switch (stack.getType()) {
            case LEATHER_HELMET:
                LeatherArmorMeta helmetMeta = (LeatherArmorMeta) stack.getItemMeta();
                switch (plugin.getMethods().getExactTeam(e.getPlayer())) {
                    case "Blau":
                        helmetMeta.setColor(Color.BLUE);
                        stack.setItemMeta(helmetMeta);
                        e.setItemStack(stack);
                        break;
                    case "Rot":
                        helmetMeta.setColor(Color.RED);
                        stack.setItemMeta(helmetMeta);
                        e.setItemStack(stack);
                        break;
                    case "Grün":
                        helmetMeta.setColor(Color.GREEN);
                        stack.setItemMeta(helmetMeta);
                        e.setItemStack(stack);
                        break;
                    case "Gelb":
                        helmetMeta.setColor(Color.YELLOW);
                        stack.setItemMeta(helmetMeta);
                        e.setItemStack(stack);
                        break;
                }
                break;
            case LEATHER_LEGGINGS:
                LeatherArmorMeta legginsMeta = (LeatherArmorMeta) stack.getItemMeta();
                switch (plugin.getMethods().getExactTeam(e.getPlayer())) {
                    case "Blau":
                        legginsMeta.setColor(Color.BLUE);
                        stack.setItemMeta(legginsMeta);
                        e.setItemStack(stack);
                        break;
                    case "Rot":
                        legginsMeta.setColor(Color.RED);
                        stack.setItemMeta(legginsMeta);
                        e.setItemStack(stack);
                        break;
                    case "Grün":
                        legginsMeta.setColor(Color.GREEN);
                        stack.setItemMeta(legginsMeta);
                        e.setItemStack(stack);
                        break;
                    case "Gelb":
                        legginsMeta.setColor(Color.YELLOW);
                        stack.setItemMeta(legginsMeta);
                        e.setItemStack(stack);
                        break;
                }
                break;
            case LEATHER_BOOTS:
                LeatherArmorMeta bootsMeta = (LeatherArmorMeta) stack.getItemMeta();
                switch (plugin.getMethods().getExactTeam(e.getPlayer())) {
                    case "Blau":
                        bootsMeta.setColor(Color.BLUE);
                        stack.setItemMeta(bootsMeta);
                        e.setItemStack(stack);
                        break;
                    case "Rot":
                        bootsMeta.setColor(Color.RED);
                        stack.setItemMeta(bootsMeta);
                        e.setItemStack(stack);
                        break;
                    case "Grün":
                        bootsMeta.setColor(Color.GREEN);
                        stack.setItemMeta(bootsMeta);
                        e.setItemStack(stack);
                        break;
                    case "Gelb":
                        bootsMeta.setColor(Color.YELLOW);
                        stack.setItemMeta(bootsMeta);
                        e.setItemStack(stack);
                        break;
                }
                break;
            case HARD_CLAY:
                switch (plugin.getMethods().getExactTeam(e.getPlayer())) {
                    case "Blau":
                        stack = new ItemBuilder(Material.STAINED_CLAY, stack.getAmount(), (short) 11).setDisplayName("§7> §3Farbiger Ton").build();
                        break;
                    case "Rot":
                        stack = new ItemBuilder(Material.STAINED_CLAY, stack.getAmount(), (short) 14).setDisplayName("§7> §3Farbiger Ton").build();
                        break;
                    case "Grün":
                        stack = new ItemBuilder(Material.STAINED_CLAY, stack.getAmount(), (short) 5).setDisplayName("§7> §3Farbiger Ton").build();
                        break;
                    case "Gelb":
                        stack = new ItemBuilder(Material.STAINED_CLAY, stack.getAmount(), (short) 4).setDisplayName("§7> §3Farbiger Ton").build();
                        break;
                }
                e.setItemStack(stack);
                break;
            default:
                e.setItemStack(stack);
                break;
        }
    }
    
}
