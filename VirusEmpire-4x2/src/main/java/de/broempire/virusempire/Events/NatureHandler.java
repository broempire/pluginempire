package de.broempire.virusempire.Events;

import de.broempire.virusempire.Utils.ResetBlock;
import de.broempire.virusempire.main;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class NatureHandler implements Listener{
    
    private main plugin;

    public NatureHandler(main plugin) {
        this.plugin = plugin;
    }
    
	
    @EventHandler
    public void onCreaturesSpawn (CreatureSpawnEvent e) {
        if (e.getEntityType().equals(EntityType.VILLAGER)) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onWeatherChange (WeatherChangeEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onEntityExplode (EntityExplodeEvent e) {
        for (Block block : e.blockList()) {
            plugin.getMapreseter().addBlock(new ResetBlock(block.getType(), block.getWorld().getBlockAt(block.getLocation())));
        }
    }

}
