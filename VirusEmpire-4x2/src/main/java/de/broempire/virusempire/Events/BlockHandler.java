package de.broempire.virusempire.Events;

import de.broempire.virusempire.APIs.ItemBuilder;
import de.broempire.virusempire.Utils.Buildmode;
import de.broempire.virusempire.Utils.GameManager;
import de.broempire.virusempire.Utils.ResetBlock;
import de.broempire.virusempire.main;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class BlockHandler implements Listener{

    private main plugin;
    
    public BlockHandler(main plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        if (GameManager.isState(GameManager.GAME)) {
            e.setCancelled(false);
            if (!Buildmode.isBuildmode(p)) {
                plugin.getMapreseter().addBlock(new ResetBlock(e.getBlockReplacedState().getType(), e.getBlockPlaced()));
            }
        } else if (GameManager.isState(GameManager.Restart)) {
            e.setCancelled(!Buildmode.isBuildmode(p));
        } else if (GameManager.isState(GameManager.LOBBY)) {
            e.setCancelled(!Buildmode.isBuildmode(p));
        }
    }
    
    @EventHandler
    public void onBreak (BlockBreakEvent e) {
        Player p = e.getPlayer();
        Block b = e.getBlock();
        if (GameManager.isState(GameManager.GAME)) {
            e.setExpToDrop(0);
            if (plugin.getMapreseter().containsBlock(b)) {
                switch (b.getType()) {
                    case STAINED_CLAY:
                        e.setCancelled(false);
                        b.setType(Material.HARD_CLAY);
                        break;
                    case ENDER_CHEST:
                        e.setCancelled(true);
                        b.getWorld().dropItemNaturally(b.getLocation(), new ItemBuilder(Material.ENDER_CHEST, 1).setDisplayName("§7> §3Teamkiste").build());
                        b.setType(Material.AIR);
                        break;
                    default:
                        e.setCancelled(false);
                        break;
                }
            } else {
                e.setCancelled(!Buildmode.isBuildmode(p));
            }
        } else if (GameManager.isState(GameManager.Restart)) {
            e.setCancelled(!Buildmode.isBuildmode(p));
        } else if (GameManager.isState(GameManager.LOBBY)) {
            e.setCancelled(!Buildmode.isBuildmode(p));
        }
    }
    
    @EventHandler
    public void onItemPickup(PlayerPickupItemEvent e){
        Player p = e.getPlayer();
        if (GameManager.isState(GameManager.GAME)) {
            if (plugin.getMethods().hasTeam(p)) {
                if (e.getItem().getItemStack().getType().equals(Material.HARD_CLAY)) {
                    System.out.println("Es wurde HARD_CLAY aufgesammelt!");
                    switch (plugin.getMethods().getExactTeam(p)) {
                        case "Blau":
                            e.getItem().getItemStack().setType(Material.STAINED_CLAY);
                            e.getItem().getItemStack().setDurability((short) 11);
                            ItemMeta blueMeta = e.getItem().getItemStack().getItemMeta();
                            blueMeta.setDisplayName("§7> §3Farbiger Ton");
                            e.getItem().getItemStack().setItemMeta(blueMeta);
                            break;
                        case "Rot":
                            e.getItem().getItemStack().setType(Material.STAINED_CLAY);
                            e.getItem().getItemStack().setDurability((short) 14);
                            ItemMeta redMeta = e.getItem().getItemStack().getItemMeta();
                            redMeta.setDisplayName("§7> §3Farbiger Ton");
                            e.getItem().getItemStack().setItemMeta(redMeta);
                            break;
                        case "Grün":
                            e.getItem().getItemStack().setType(Material.STAINED_CLAY);
                            e.getItem().getItemStack().setDurability((short) 5);
                            ItemMeta greenMeta = e.getItem().getItemStack().getItemMeta();
                            greenMeta.setDisplayName("§7> §3Farbiger Ton");
                            e.getItem().getItemStack().setItemMeta(greenMeta);
                            break;
                        case "Gelb":
                            e.getItem().getItemStack().setType(Material.STAINED_CLAY);
                            e.getItem().getItemStack().setDurability((short) 4);
                            ItemMeta yellowMeta = e.getItem().getItemStack().getItemMeta();
                            yellowMeta.setDisplayName("§7> §3Farbiger Ton");
                            e.getItem().getItemStack().setItemMeta(yellowMeta);
                            break;
                    }
                } else if (e.getItem().getItemStack().getType().equals(Material.ARROW)) {
                    ItemMeta meta = e.getItem().getItemStack().getItemMeta();
                    meta.setDisplayName("§7> §3Pfeil");
                    e.getItem().getItemStack().setItemMeta(meta);
                }
            } else {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        ItemStack stack = e.getItemDrop().getItemStack();
        if (stack.getType().equals(Material.STAINED_CLAY)) {
            e.getItemDrop().getItemStack().setType(Material.HARD_CLAY);
        }
    }
}
