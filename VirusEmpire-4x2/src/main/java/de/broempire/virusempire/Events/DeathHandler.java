package de.broempire.virusempire.Events;

import de.broempire.virusempire.APIs.ActionAPI;
import de.broempire.virusempire.APIs.TitleAPI;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import de.broempire.virusempire.main;
import de.broempire.virusempire.MySQL.MySQLStats;
import de.broempire.virusempire.Utils.GameManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class DeathHandler implements Listener {
    
    private final main plugin;
    private final ServerEvent serverEvent;
    private final MySQLStats mySQLStats;
    
    private final Map<String, BukkitTask> respawn;
    private final List<String> dead;
    
    public Map<String, String> lastPlayer = new HashMap<>();
    
    public DeathHandler(main plugin) {
        this.plugin = plugin;
        this.serverEvent = new ServerEvent(plugin);
        this.mySQLStats = plugin.getMySQLStats();
        respawn = new HashMap<>();
        dead = new ArrayList<>();
    }
	
    @EventHandler
    public void onDeath (PlayerDeathEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            
            e.getDrops().clear();
            e.setDroppedExp(0);
            
            p.setHealth(20D);
            p.setFoodLevel(20);
            p.setExp(0.00F);

            if (p.getKiller() instanceof Player) {
                Player k = (Player) p.getKiller();

                k.playSound(k.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 5F, 5F);
                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_DEATH, 5F, 5F);
                e.setDeathMessage(main.pr + p.getCustomName() + " §7wurde von " + k.getCustomName() + "§7 getötet!");
                ActionAPI.sendActionBar(p, "§cDu wurdest von " + k.getCustomName() + " §cgetötet!");

                int upgradeLevel = 0;

                if (p.getInventory().getChestplate() != null && k.getInventory().getItemInMainHand().getType() != Material.IRON_SWORD) {
                    upgradeLevel++;
                }
                if (p.getInventory().contains(Material.IRON_SWORD)) {
                    upgradeLevel++;
                }
                plugin.getUpgradeManager().addUpgradeLevel(k, upgradeLevel);
                tryKickOut (p, k);
            } else if (e.getEntity().getLastDamageCause().getEntity() instanceof Player) {

                Player k = (Player) e.getEntity().getLastDamageCause().getEntity();
                EntityDamageEvent.DamageCause cause = e.getEntity().getLastDamageCause().getCause();

                if (p != k) {
                    k.playSound(k.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 5F, 5F);
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_DEATH, 5F, 5F);
                    e.setDeathMessage(main.pr + p.getCustomName() + " §7wurde von " + k.getCustomName() + "§7 getötet!");
                    ActionAPI.sendActionBar(p, "§cDu wurdest von " + k.getCustomName() + " §cgetötet!");

                    int upgradeLevel = 0;

                    if (cause.equals(EntityDamageEvent.DamageCause.PROJECTILE) && k.getLocation().distance(p.getLocation()) >= 50.0) {
                        upgradeLevel++;
                    }
                    if (p.getInventory().getChestplate() != null && k.getInventory().getItemInMainHand().getType() != Material.IRON_SWORD) {
                        upgradeLevel++;
                    }
                    if (p.getInventory().contains(Material.IRON_SWORD)) {
                        upgradeLevel++;
                    }
                    plugin.getUpgradeManager().addUpgradeLevel(k, upgradeLevel);
                    tryKickOut (p, k);
                } else {
                    e.setDeathMessage(main.pr + p.getCustomName() + " §7ist gestorben!");
                    ActionAPI.sendActionBar(p, "§cDu bist gestorben!");
                    tryKickOut (p, null);
                }	
            } else if (p.getLastDamageCause().getCause().equals(DamageCause.VOID)) {
                if (lastPlayer.containsKey(p.getName()) && Bukkit.getPlayer(lastPlayer.get(p.getName())) != null) {
                    Player k = Bukkit.getPlayer(lastPlayer.get(p.getName()));
                    
                    k.playSound(k.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 5F, 5F);
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_DEATH, 5F, 5F);
                    e.setDeathMessage(main.pr + p.getCustomName() + " §7wurde von " + k.getCustomName() + "§7 getötet!");
                    ActionAPI.sendActionBar(p, "§cDu wurdest von " + k.getCustomName() + " §cgetötet!");

                    int upgradeLevel = 0;

                    if (p.getInventory().getChestplate() != null && k.getInventory().getItemInMainHand().getType() != Material.IRON_SWORD) {
                        upgradeLevel++;
                    }
                    if (p.getInventory().contains(Material.IRON_SWORD)) {
                        upgradeLevel++;
                    }
                    
                    plugin.getUpgradeManager().addUpgradeLevel(k, upgradeLevel);
                    tryKickOut (p, k);
                }
            } else {
                e.setDeathMessage(main.pr + p.getCustomName() + " §7ist gestorben!");
                ActionAPI.sendActionBar(p, "§cDu bist gestorben!");
                tryKickOut (p, null);
            }
            
            if (plugin.getMethods().canRespawn(p)) {
                dead.add(p.getName());
                p.setGameMode(GameMode.SPECTATOR);
                p.teleport(plugin.getWinnerMapFile().getSpawn(plugin.getMethods().getExactTeam(p).toUpperCase()));
                respawn.put(p.getName(), new BukkitRunnable() {
                    int time = 3;
                    @Override
                    public void run() {
                        if (time == 0) {
                            p.setGameMode(GameMode.SURVIVAL);
                            TitleAPI.sendTitle(p, 0, 20, 20, "§cDu bist §4gestorben§c!", "§2Respawn§a...");
                            p.teleport(plugin.getWinnerMapFile().getSpawn(plugin.getMethods().getExactTeam(p).toUpperCase()));
                            p.sendMessage(main.pr + "Dein Nucleus hat dich wiederbelebt!");
                            cancel();
                        } else {
                            TitleAPI.sendTitle(p, 5, 20, 5, "§cDu bist §4gestorben§c!", "§7Respawn in §6" + time + " §7Sekunde(n)");
                            time--;
                        }
                    }
                }.runTaskTimer(plugin, 0L, 20L));
                dead.remove(p.getName());
                respawn.remove(p.getName());
            }
        }
    }
    
    public boolean isDead(Player p) {
        String name = p.getName();
        return dead.contains(name);
    }
	
    public void trySettingWinner() {

        if (plugin.blau.isEmpty() && plugin.rot.isEmpty() && plugin.gelb.isEmpty() && plugin.grün.size() >= 1) {

            GameManager.setState(GameManager.Restart);
            for (Player all : Bukkit.getOnlinePlayers()) {
                serverEvent.readyPlayer(all);
                all.teleport(plugin.lobbySpawn);
                TitleAPI.sendTitle(all, 20, 40, 20, "§2Team Grün", "§7hat §cVirus §7gewonnen!");
            }
            plugin.getServer().broadcastMessage(main.pr + "§7Team §2Grün §7hat gewonnen!");
            for(String winnerName : plugin.grün){
                Player winner = Bukkit.getPlayer(winnerName);
                mySQLStats.addWins(winner.getUniqueId().toString(), 1);
                mySQLStats.addPoints(winner.getUniqueId().toString(), 30);
                plugin.getBro().setCoins(plugin.getBro().getCoins(winner.getUniqueId().toString()) + 20, winner.getUniqueId().toString());
                winner.sendMessage(main.pr + "Du hast §620 Coins §7erhalten!");
                plugin.getScoreboardTeams().updatePlayer(winner);
            }
            plugin.getMethods().setEnd(Color.GREEN);

        } else if (plugin.blau.isEmpty() && plugin.rot.isEmpty() && plugin.grün.isEmpty() && plugin.gelb.size() >= 1) {

            GameManager.setState(GameManager.Restart);
            for (Player all : Bukkit.getOnlinePlayers()) {
                serverEvent.readyPlayer(all);
                all.teleport(plugin.lobbySpawn);
                TitleAPI.sendTitle(all, 20, 40, 20, "§eTeam Gelb", "§7hat §cVirus §7gewonnen!");
            }
            plugin.getServer().broadcastMessage(main.pr + "§7Team §eGelb §7hat gewonnen!");
            for(String winnerName : plugin.gelb){
                Player winner = Bukkit.getPlayer(winnerName);
                mySQLStats.addWins(winner.getUniqueId().toString(), 1);
                mySQLStats.addPoints(winner.getUniqueId().toString(), 30);
                plugin.getBro().setCoins(plugin.getBro().getCoins(winner.getUniqueId().toString()) + 20, winner.getUniqueId().toString());
                winner.sendMessage(main.pr + "Du hast §620 Coins §7erhalten!");
                plugin.getScoreboardTeams().updatePlayer(winner);
            }
            plugin.getMethods().setEnd(Color.YELLOW);

        } else if (plugin.blau.isEmpty() && plugin.gelb.isEmpty() && plugin.grün.isEmpty() && plugin.rot.size() >= 1) {
            
            GameManager.setState(GameManager.Restart);
            for (Player all : Bukkit.getOnlinePlayers()) {
                serverEvent.readyPlayer(all);
                all.teleport(plugin.lobbySpawn);
                TitleAPI.sendTitle(all, 20, 40, 20, "§cTeam Rot", "§7hat §cVirus §7gewonnen!");
            }
            plugin.getServer().broadcastMessage(main.pr + "§7Team §cRot §7hat gewonnen!");
            for(String winnerName : plugin.rot){
                Player winner = Bukkit.getPlayer(winnerName);
                mySQLStats.addWins(winner.getUniqueId().toString(), 1);
                mySQLStats.addPoints(winner.getUniqueId().toString(), 30);
                plugin.getBro().setCoins(plugin.getBro().getCoins(winner.getUniqueId().toString()) + 20, winner.getUniqueId().toString());
                winner.sendMessage(main.pr + "Du hast §620 Coins §7erhalten!");
                plugin.getScoreboardTeams().updatePlayer(winner);
            }
            plugin.getMethods().setEnd(Color.RED);

        } else if (plugin.rot.isEmpty() && plugin.gelb.isEmpty() && plugin.grün.isEmpty() && plugin.blau.size() >= 1) {

            GameManager.setState(GameManager.Restart);
            for (Player all : Bukkit.getOnlinePlayers()) {
                serverEvent.readyPlayer(all);
                all.teleport(plugin.lobbySpawn);
                TitleAPI.sendTitle(all, 20, 40, 20, "§9Team Blau", "§7hat §cVirus §7gewonnen!");
            }
            plugin.getServer().broadcastMessage(main.pr + "§7Team §9Blau §7hat gewonnen!");
            for(String winnerName : plugin.blau){
                Player winner = Bukkit.getPlayer(winnerName);
                mySQLStats.addWins(winner.getUniqueId().toString(), 1);
                mySQLStats.addPoints(winner.getUniqueId().toString(), 30);
                plugin.getBro().setCoins(plugin.getBro().getCoins(winner.getUniqueId().toString()) + 20, winner.getUniqueId().toString());
                winner.sendMessage(main.pr + "Du hast §620 Coins §7erhalten!");
                plugin.getScoreboardTeams().updatePlayer(winner);
            }
            plugin.getMethods().setEnd(Color.BLUE);
            
        }
    }
    public void tryKickOut(Player p, Player k) {
        if (!plugin.getMethods().canRespawn(p)) {
            plugin.getMethods().clearFromArray(p);
            mySQLStats.addDeaths(p.getUniqueId().toString(), 1);
            serverEvent.readySpectator(p);
            if (k != null) {
                k.playSound(k.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5F, 5F);
                mySQLStats.addKills(k.getUniqueId().toString(), 1);
                mySQLStats.addPoints(k.getUniqueId().toString(), 5);
                plugin.getBro().setCoins(plugin.getBro().getCoins(k.getUniqueId().toString()) + 5, k.getUniqueId().toString());
                k.sendMessage(main.pr + "Du hast §65 Coins §7erhalten!");
            }
            if (plugin.getMethods().getTeamlist(p) != null && plugin.getMethods().getTeamlist(p).isEmpty()) {
                plugin.getServer().broadcastMessage(main.pr + "Team " + plugin.getMethods().getTeam(p) + " §7ist nun ausgeschieden!");
            }
            for (Player all : plugin.getServer().getOnlinePlayers()) {
                plugin.getScoreboardManager().updateScoreboard(all);
            }
            trySettingWinner();
       }
    }
}
