/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Events;

import de.broempire.virusempire.Shop.PayType;
import java.util.function.Consumer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Matti
 */
public class BuyItemEvent extends Event {

    private static HandlerList handlerList = new HandlerList();
    
    private ItemStack itemStack;
    private final PayType payType;
    private final int price;
    private final Player player;
    private final Consumer<ItemStack> callback;

    public BuyItemEvent(ItemStack itemStack, PayType payType, int price, Player player, Consumer<ItemStack> callback) {
        this.itemStack = itemStack;
        this.payType = payType;
        this.price = price;
        this.player = player;
        this.callback = callback;
    }
    
    public static HandlerList getHandlerList() {
        return handlerList;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }
    
    public void setItemStack(ItemStack newStack) {
        this.itemStack = newStack;
        callback.accept(newStack);
    }

    public PayType getPayType() {
        return payType;
    }

    public Player getPlayer() {
        return player;
    }

    public int getPrice() {
        return price;
    }
    
}
