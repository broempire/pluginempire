package de.broempire.virusempire.Events;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import de.broempire.virusempire.Utils.GameManager;
import de.broempire.virusempire.main;

public class DamageHandler implements Listener {
    
    private main plugin;
    
    public DamageHandler(main plugin) {
        this.plugin = plugin;
    }
	
    @EventHandler
    public void onDamageByEntity (EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            if (GameManager.isState(GameManager.LOBBY) || GameManager.isState(GameManager.Restart)) {
                e.setCancelled(true);
            } else if (GameManager.isState(GameManager.GAME)) {
                if (plugin.isSpectator(p)) {
                    e.setCancelled(true);
                } else if (e.getDamager() instanceof Player) {
                    Player d = (Player) e.getDamager();
                    String teamD = plugin.getMethods().getExactTeam(d);
                    String teamP = plugin.getMethods().getExactTeam(p);
                    if (teamD.equalsIgnoreCase(teamP)) {
                        e.setCancelled(true);
                    } else {
                        e.setCancelled(false);
                        plugin.getDeathHandler().lastPlayer.put(p.getName(), d.getName());
                    }
                } else if (e.getDamager() instanceof Arrow) {
                    Arrow a = (Arrow) e.getDamager();
                    Player d = (Player) a.getShooter();
                    String TeamD = plugin.getMethods().getExactTeam(d);
                    String TeamP = plugin.getMethods().getExactTeam(p);
                    if (TeamD.equalsIgnoreCase(TeamP)) {
                        e.setCancelled(true);
                    } else {
                        e.setCancelled(false);
                        plugin.getDeathHandler().lastPlayer.put(p.getName(), d.getName());
                        if (d.getLocation().distance(p.getLocation()) >= 50.0) {
                            plugin.getUpgradeManager().addUpgradeLevel(d, 1);
                        }
                    }
                }

            }
        }
    }

    @EventHandler
    public void onDamage (EntityDamageEvent e) {
        if (GameManager.isState(GameManager.LOBBY) || GameManager.isState(GameManager.Restart)) {
            e.setCancelled(true);
        } else if (GameManager.isState(GameManager.GAME)) {
            if (e.getEntity() instanceof Villager) {
                e.setCancelled(true);
            } else if (e.getEntity() instanceof Player) {
                Player p = (Player) e.getEntity();
                if (!plugin.getMethods().hasTeam(p) || plugin.getDeathHandler().isDead(p)) {
                    e.setCancelled(true);
                }
            }
        }
    }
}
