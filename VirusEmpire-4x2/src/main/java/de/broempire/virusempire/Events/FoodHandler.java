package de.broempire.virusempire.Events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import de.broempire.virusempire.Utils.GameManager;
import de.broempire.virusempire.main;
import org.bukkit.entity.Player;

public class FoodHandler implements Listener{

    private main plugin;
    
    public FoodHandler(main plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onFood (FoodLevelChangeEvent e) {
        if (GameManager.isState(GameManager.LOBBY) || GameManager.isState(GameManager.Restart)) {
            e.setCancelled(true);
        } else if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            if (plugin.getMethods().getExactTeam(p).equalsIgnoreCase("Kein Team")) {
                e.setCancelled(true);
            }
        }
    }

}
