package de.broempire.virusempire.Events;

import de.broempire.virusempire.Utils.GameManager;
import de.broempire.virusempire.main;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;

public class InventoryHandler implements Listener{
	
    @EventHandler
    public void onInventoryClose (InventoryCloseEvent e) {
        if (e.getInventory().getType() == InventoryType.ENDER_CHEST) {
            Player p = (Player) e.getPlayer();
            p.playSound(p.getEyeLocation(), Sound.BLOCK_CHEST_CLOSE, 1F, 1F);
        }
    }

    @EventHandler
    public void onInventoryClick (InventoryClickEvent e) {
        if (e.getInventory().getName().equalsIgnoreCase("§7> §5Teleporter")) {
            e.setCancelled(true);
            Player p = (Player) e.getWhoClicked();
            if (Bukkit.getPlayer(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())) != null) {
                p.teleport(Bukkit.getPlayer(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())));
            } else {
                p.sendMessage(main.pr + "§cDieser Spieler ist nicht mehr online!");
            }
        }
    }

}
