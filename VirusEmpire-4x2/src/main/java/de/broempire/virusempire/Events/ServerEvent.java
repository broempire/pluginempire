package de.broempire.virusempire.Events;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.VisibilityManager;
import de.broempire.virusempire.APIs.ItemBuilder;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.broempire.virusempire.Teams.Teams;
import de.broempire.virusempire.main;
import de.broempire.virusempire.Utils.GameManager;
import de.broempire.virusempire.Utils.MapFile;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class ServerEvent implements Listener{
	
    private final main plugin;
    private boolean ingame;

    public ServerEvent (main plugin) {
        this.plugin = plugin;
    }

    @EventHandler (priority = EventPriority.HIGHEST)
    public void JoinEvent (PlayerJoinEvent e) {
        if (!ingame) {
            Player p = e.getPlayer();
            
            if (!plugin.isCountdown()) {
                plugin.startCountdown();
            }
            
            p.teleport(plugin.lobbySpawn);
            
            readyPlayer(p);

            p.getInventory().setItem(0, new ItemBuilder(Material.INK_SACK, 1, (short) 8).setDisplayName("§7↠ §6Teamwahl").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
            p.getInventory().setItem(4, new ItemBuilder(Material.PAPER).setDisplayName("§7↠ §bMaps").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
            p.getInventory().setItem(8, new ItemBuilder(Material.REDSTONE).setDisplayName("§7↠ §cSpiel verlassen").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
            
            plugin.getScoreboardManager().sendScoreboardLobby(p);
            
            for (Player player : Bukkit.getOnlinePlayers()) {
                plugin.getScoreboardManager().updateScoreboard(player);
            }
            
            plugin.getScoreboardTeams().updatePlayer(p);
            
            e.setJoinMessage("§8[§2+§8] §7" + p.getDisplayName());
            
            plugin.getMySQLStats().createPlayer(p.getUniqueId().toString());
            
            if (plugin.getLobbyLocation("HOLOGRAM") != null) {
                Hologram hologram = HologramsAPI.createHologram(plugin, plugin.getLobbyLocation("HOLOGRAM"));
                VisibilityManager visibilityManager = hologram.getVisibilityManager();
                
                visibilityManager.showTo(p);
                visibilityManager.setVisibleByDefault(false);
                
                ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
                SkullMeta sm = (SkullMeta) is.getItemMeta();
                sm.setOwner(p.getName());
                is.setItemMeta(sm);
                
                hologram.appendItemLine(is);
                hologram.appendTextLine("§7» §6§lStats §7«");
                hologram.appendTextLine("§7↠ §9gespielte Spiele: §c" + plugin.getMySQLStats().getPlayed(p.getUniqueId().toString()));
                hologram.appendTextLine("§7↠ §9gewonnene Spiele: §c" + plugin.getMySQLStats().getWins(p.getUniqueId().toString()));
                hologram.appendTextLine("§7↠ §9deaktivierte Energiekerne: §c" + plugin.getMySQLStats().getDisabled(p.getUniqueId().toString()));
                hologram.appendTextLine("§7↠ §9Kills: §c" + plugin.getMySQLStats().getKills(p.getUniqueId().toString()));
                hologram.appendTextLine("§7↠ §9Tode: §c" + plugin.getMySQLStats().getDeaths(p.getUniqueId().toString()));
                hologram.appendTextLine("§7↠ §9Punkte: §c" + plugin.getMySQLStats().getPoints(p.getUniqueId().toString()));
            }
            
            for (Player online : plugin.getServer().getOnlinePlayers()) {
                if (online != p) {
                    online.hidePlayer(p);
                    p.hidePlayer(online);
                }
            }
            new BukkitRunnable() {
                @Override
                public void run() {
                    for (Player online : plugin.getServer().getOnlinePlayers()) {
                        online.showPlayer(p);
                        p.showPlayer(online);
                    }
                }
            }.runTaskLater(plugin, 5L);
        } else {
            e.setJoinMessage(null);
            readySpectator(e.getPlayer());
        }
    }

    @EventHandler
    public void onQuit (PlayerQuitEvent e) {
        Player p = e.getPlayer();

        plugin.getMethods().clearFromArray(p);

        if (!GameManager.isState(GameManager.GAME)) {
            e.setQuitMessage("§8[§4-§8] §7" + p.getDisplayName());
        } else {
            plugin.getDeathHandler().trySettingWinner();
            if (plugin.getMethods().hasTeam(p)) {
                plugin.getMySQLStats().addDeaths(p.getUniqueId().toString(), 1);
                e.setQuitMessage("§8[§4-§8] §7" + p.getCustomName());
            } else {
                e.setQuitMessage(null);
            }
        }
        
        for (Hologram hologram : HologramsAPI.getHolograms(plugin)) {
            if (hologram.getVisibilityManager().isVisibleTo(p)) {
                hologram.delete();
            }
        }
        
        new BukkitRunnable() {
            @Override
            public void run() {
                Bukkit.getOnlinePlayers().stream().forEach((all) -> {
                    plugin.getScoreboardManager().updateScoreboard(all);
                });
            }
        }.runTaskAsynchronously(plugin);
        
        
    }
    @EventHandler
    public void onLogin (PlayerLoginEvent e) {
        if (GameManager.isState(GameManager.GAME)) {
            e.allow();
            ingame = true;
        } else {
            ingame = false;
            if (e.getResult().equals(Result.KICK_FULL)) {
                e.setKickMessage(main.pr + "§cDer Server ist voll!");
            }
        }
        
    }
    
    public void readyPlayer(Player p) {
        plugin.getMethods().clearFromArray(p);
        p.setHealth(20D);
        p.setFoodLevel(20);
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        p.setFireTicks(0);
        p.setAllowFlight(false);
        p.setFlying(false);
        p.setGameMode(GameMode.ADVENTURE);
        p.setExp(0F);
        p.setLevel(0);
        for (PotionEffect effect : p.getActivePotionEffects()) {
            p.removePotionEffect(effect.getType());
        }
        p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(8D);
    }
    
    public void readySpectator(Player p) {
        plugin.getScoreboardTeams().addTeam(p, Teams.SPECTATOR);
        p.sendMessage(main.pr + "§cDein Energiekern ist deaktiviert!");
        p.sendMessage(main.pr + "§cDaher konnte er dich nicht respawnen lassen!");
        p.teleport(Bukkit.getWorld(plugin.getWinnerMapFile().getWorld()).getSpawnLocation());
        p.setHealth(20D);
        p.setFoodLevel(20);
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        p.setFireTicks(0);
        p.setGameMode(GameMode.ADVENTURE);
        p.setAllowFlight(true);
        p.setFlying(true);
        p.setExp(0F);
        p.setLevel(0);
        for (PotionEffect effect : p.getActivePotionEffects()) {
            p.removePotionEffect(effect.getType());
        }
        p.addPotionEffect(new PotionEffect (PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, true, false));
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            if (!plugin.isSpectator(player)) {
                player.hidePlayer(p);
            }
        }
        
        new BukkitRunnable() {
            @Override
            public void run() { 
                p.getInventory().setItem(0, new ItemBuilder(Material.COMPASS).setDisplayName("§7↠ §5Teleporter").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
                plugin.getScoreboardManager().sendScoreboardGame(p);
                plugin.getScoreboardManager().updateScoreboard(p);
            }
        }.runTaskAsynchronously(plugin);
    }

    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        Player p = e.getPlayer();
        if (plugin.isSetupMode(p)) {
            if (e.getLine(0).equalsIgnoreCase("[Virus]")) {
                MapFile file;
                switch (e.getLine(1)) {
                    case "Spawner":
                        if (plugin.getNameByWorld(e.getBlock().getWorld().getName()) == null) {
                            p.sendMessage(main.pr + "§cDu musst erst einmal die Welt registrieren!");
                        } else {
                            if (e.getLine(2).equalsIgnoreCase("BRONZE") || e.getLine(2).equalsIgnoreCase("SILBER") || e.getLine(2).equalsIgnoreCase("GOLD")) {
                                plugin.getSpawner().addSpawner(plugin.getNameByWorld(e.getBlock().getWorld().getName()), e.getLine(2), e.getBlock().getLocation());
                                p.sendMessage(main.pr + "Du hast einen Spawner mit dem Typen " + e.getLine(2) + " gesetzt!");
                                e.getBlock().setType(Material.AIR);
                            }
                        }
                        break;
                    case "Spawn":
                        if (plugin.getNameByWorld(e.getBlock().getWorld().getName()) == null) {
                            p.sendMessage(main.pr + "§cDu musst erst einmal die Welt registrieren!");
                        } else {
                            if (e.getLine(2).equalsIgnoreCase("BLAU") || e.getLine(2).equalsIgnoreCase("GRÜN") || e.getLine(2).equalsIgnoreCase("GELB") || e.getLine(2).equalsIgnoreCase("ROT")) {
                                file = new MapFile(plugin.getDataFolder().getPath(), plugin.getNameByWorld(e.getBlock().getWorld().getName()));
                                file.setSpawn(e.getLine(2).toUpperCase(), e.getBlock().getLocation());
                                p.sendMessage(main.pr + "Du hast den Spawn von Team " + e.getLine(2).toUpperCase() + " auf der Map " + file.getName() + " gesetzt!");
                                e.getBlock().setType(Material.AIR);
                            }
                        }
                        break;
                    case "Lobby":
                        plugin.setLobbyLocation(e.getBlock().getLocation(), "LOBBY");
                        p.sendMessage(main.pr + "Du hast den Lobbyspawn gesetzt!");
                        e.getBlock().setType(Material.AIR);
                        break;
                    case "Ranking":
                        if (e.getLine(2) != null) {
                            if ("1".equals(e.getLine(2)) || "2".equals(e.getLine(2)) || "3".equals(e.getLine(2))) {
                                plugin.setLobbyLocation(e.getBlock().getLocation(), "RANKING." + e.getLine(2));
                                p.sendMessage(main.pr + "Du hast den Platz §e" + e.getLine(2) + " §7platziert!");
                                e.getBlock().setType(Material.SKULL);
                            }
                        }
                        break;
                    case "Holo":
                        plugin.setLobbyLocation(e.getBlock().getLocation().add(0, 2.00, 0), "HOLOGRAM");
                        p.sendMessage(main.pr + "Du hast die Location für das Hologramm gesetzt!");
                        e.getBlock().setType(Material.AIR);
                        break;
                    case "Villager":
                        if (plugin.getNameByWorld(e.getBlock().getWorld().getName()) == null) {
                            p.sendMessage(main.pr + "§cDu musst erst einmal die Welt registrieren!");
                        } else {
                            file = new MapFile(plugin.getDataFolder().getPath(), plugin.getNameByWorld(e.getBlock().getWorld().getName()));
                            file.setVillager(e.getBlock().getLocation());
                            p.sendMessage(main.pr + "Du hast einen Villager gesetzt!");
                            e.getBlock().setType(Material.AIR);
                        }
                        break;
                    default:
                        p.sendMessage(main.pr + "§cGebe in der zweiten Zeile Spawn/Spawner/Lobby/Ranking/Villager an!");
                        break;
                }
            }
        }
    }
    
    @EventHandler
    public void onEntityCombust(EntityCombustEvent e) {
        if (GameManager.isState(GameManager.GAME)) {
            if (e.getEntity() instanceof Player) {
                if (plugin.isSpectator((Player) e.getEntity())) {
                    e.setCancelled(true);
                }
            } else if (e.getEntity() instanceof Villager) {
                e.setCancelled(true);
            }
        }
    }
}

