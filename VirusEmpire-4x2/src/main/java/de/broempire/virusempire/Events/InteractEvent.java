package de.broempire.virusempire.Events;

import de.broempire.virusempire.APIs.GUIBuilder;
import de.broempire.virusempire.APIs.ItemBuilder;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.FireworkMeta;

import de.broempire.virusempire.Teams.Teams;
import de.broempire.virusempire.Utils.GameManager;
import de.broempire.virusempire.main;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class InteractEvent implements Listener{
	
	private main plugin;
        
        private GUIBuilder teams;
        
        private Consumer<Player> blueCallback;
        private Consumer<Player> redCallback;
        private Consumer<Player> yellowCallback;
        private Consumer<Player> greenCallback;
        
        private final ItemBuilder blueIB;
        private final ItemBuilder redIB;
        private final ItemBuilder greenIB;
        private final ItemBuilder yellowIB;
        
        private HashMap <Teams, Inventory> content = new HashMap<>();
        
	public InteractEvent (main plugin) {
            initCallbacks();
            this.plugin = plugin;
            this.teams = new GUIBuilder(plugin, 9, "§7> §3Teamwahl");
            this.blueIB = new ItemBuilder(Material.WOOL, 1, (short)11, blueCallback).setDisplayName("§9Blau");
            this.redIB = new ItemBuilder(Material.WOOL, 1, (short) 14, redCallback).setDisplayName("§cRot");
            this.greenIB = new ItemBuilder(Material.WOOL, 1, (short)5, greenCallback).setDisplayName("§2Grün");
            this.yellowIB = new ItemBuilder(Material.WOOL, 1, (short) 4, yellowCallback).setDisplayName("§eGelb");
	}
	
	@EventHandler
	public void PlayerInteract (PlayerInteractEvent e) {
		
		Player p = e.getPlayer();
		
		if (e.getAction() == Action.PHYSICAL) {
			
			if (e.getClickedBlock().getType() == Material.STONE_PLATE) {
				
                            if (e.getClickedBlock().getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() == Material.REDSTONE_BLOCK) {
                                if (plugin.getMethods().hasTeam(p)) {
                                        Bukkit.getServer().broadcastMessage(p.getCustomName() + " §6hat den Parkour geschafft!");
                                } else {
                                        Bukkit.getServer().broadcastMessage(p.getDisplayName() + " §6hat den Parkour geschafft!");
                                }

                                Firework firework = p.getWorld().spawn(p.getLocation(), Firework.class);
                                FireworkEffect effect = FireworkEffect.builder()
                                                .withColor(Color.RED)
                                                .withFade(Color.BLUE)
                                                .flicker(true)
                                                .trail(true)
                                                .with(FireworkEffect.Type.STAR)
                                                .build();

                                FireworkMeta meta = firework.getFireworkMeta();
                                meta.addEffect(effect);
                                meta.setPower(1);

                                firework.setFireworkMeta(meta);

                                p.teleport(plugin.getLobbyLocation("LOBBY"));
                            }
			}
			
		}
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    if (e.getItem() != null && e.getItem().getItemMeta() != null && e.getItem().getItemMeta().getDisplayName() != null) {
			if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7↠ §6Teamwahl")) {
                            ArrayList<String> blue = new ArrayList<>();
                            ArrayList<String> red = new ArrayList<>();
                            ArrayList<String> yellow = new ArrayList<>();
                            ArrayList<String> green = new ArrayList<>();
                            
                            yellow.clear();
                            red.clear();
                            blue.clear();
                            green.clear();
                            
                            for (String s : plugin.getBlue()) {
                                blue.add("§8» §9" + s);
                            }
                            for (String s : plugin.getRed()) {
                                red.add("§8» §c" + s);
                            }
                            for (String s : plugin.getYellow()) {
                                yellow.add("§8» §e" + s);
                            }
                            for (String s : plugin.getGreen()) {
                                green.add("§8» §2" + s);
                            }
                            
                            this.teams.addGUIItem(1, blueIB.setLore(blue))
                                    .addGUIItem(3, redIB.setLore(red))
                                    .addGUIItem(5, greenIB.setLore(green))
                                    .addGUIItem(7, yellowIB.setLore(yellow));
                            
                            this.teams.cancelOthers(true);
                            
                            this.teams.open(p);
                        
                        } else if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7↠ §5Teleporter")) {
                            Inventory compassinv = Bukkit.createInventory(null, 9, "§7> §5Teleporter");
                            for (Player player : plugin.getServer().getOnlinePlayers()) {
                                if (plugin.getMethods().hasTeam(player)) {
                                    ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                                    SkullMeta sm = (SkullMeta) is.getItemMeta();
                                    sm.setOwner(player.getName());
                                    sm.setDisplayName(player.getCustomName());
                                    is.setItemMeta(sm);
                                    compassinv.addItem(is);
                                }
                            }
                            p.openInventory(compassinv);
                        } else if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7↠ §bMaps")) {
                            plugin.getMapvotingManager().openMapvoting(p);
                        } else if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7↠ §cSpiel verlassen")) {
                            try {
                                ByteArrayOutputStream b = new ByteArrayOutputStream();
                                DataOutputStream out = new DataOutputStream(b);
                                out.writeUTF("Connect");
                                out.writeUTF("lobby01");
                                p.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
                            } catch (IOException ex) {
                                Logger.getLogger(InteractEvent.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
		}
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    if (!plugin.getMethods().hasTeam(p) && GameManager.isState(GameManager.GAME)) {
                        e.setCancelled(true);
                    } else if (e.getClickedBlock().equals(Material.ENDER_CHEST)) {
                            e.setCancelled(true);
                            if (plugin.getMethods().getExactTeam(p).equalsIgnoreCase("Blau")) {
                                if (content.containsKey(Teams.BLUE)) {
                                        p.openInventory(content.get(Teams.BLUE));
                                        p.playSound(e.getClickedBlock().getLocation().add(0.5D, 0.5D, 0.5D), Sound.BLOCK_CHEST_OPEN, 1F, 1F);
                                } else {
                                        Inventory enderchest = Bukkit.createInventory(null, InventoryType.ENDER_CHEST, "§e§lTeamkiste");
                                        p.openInventory(enderchest);
                                        p.playSound(e.getClickedBlock().getLocation().add(0.5D, 0.5D, 0.5D), Sound.BLOCK_CHEST_OPEN, 1F, 1F);
                                        content.put(Teams.BLUE, enderchest);
                                }
                            }
                            if (plugin.getMethods().getExactTeam(p).equalsIgnoreCase("Rot")) {
                                if (content.containsKey(Teams.RED)) {
                                        p.openInventory(content.get(Teams.RED));
                                        p.playSound(e.getClickedBlock().getLocation().add(0.5D, 0.5D, 0.5D), Sound.BLOCK_CHEST_OPEN, 1F, 1F);
                                } else {
                                        Inventory enderchest = Bukkit.createInventory(null, InventoryType.ENDER_CHEST, "§e§lTeamkiste");
                                        e.getPlayer().openInventory(enderchest);
                                        p.playSound(e.getClickedBlock().getLocation().add(0.5D, 0.5D, 0.5D), Sound.BLOCK_CHEST_OPEN, 1F, 1F);
                                        content.put(Teams.RED, enderchest);
                                }
                            }
                            if (plugin.getMethods().getExactTeam(p).equalsIgnoreCase("Gelb")) {
                                if (content.containsKey(Teams.YELLOW)) {
                                        p.openInventory(content.get(Teams.YELLOW));
                                        p.playSound(e.getClickedBlock().getLocation().add(0.5D, 0.5D, 0.5D), Sound.BLOCK_CHEST_OPEN, 1F, 1F);
                                } else {
                                        Inventory enderchest = Bukkit.createInventory(null, InventoryType.ENDER_CHEST, "§e§lTeamkiste");
                                        e.getPlayer().openInventory(enderchest);
                                        p.playSound(e.getClickedBlock().getLocation().add(0.5D, 0.5D, 0.5D), Sound.BLOCK_CHEST_OPEN, 1F, 1F);
                                        content.put(Teams.YELLOW, enderchest);
                                }
                            }
                            if (plugin.getMethods().getExactTeam(p).equalsIgnoreCase("Grün")) {
                                if (content.containsKey(Teams.GREEN)) {
                                        p.openInventory(content.get(Teams.GREEN));
                                        p.playSound(e.getClickedBlock().getLocation().add(0.5D, 0.5D, 0.5D), Sound.BLOCK_CHEST_OPEN, 1F, 1F);
                                } else {
                                        Inventory enderchest = Bukkit.createInventory(null, InventoryType.ENDER_CHEST, "§e§lTeamkiste");
                                        e.getPlayer().openInventory(enderchest);
                                        p.playSound(e.getClickedBlock().getLocation().add(0.5D, 0.5D, 0.5D), Sound.BLOCK_CHEST_OPEN, 1F, 1F);
                                        content.put(Teams.GREEN, enderchest);
                                }
                            }
			}
		}
	}
        
        private void initCallbacks() {
            blueCallback = (Player t) -> {
                plugin.getScoreboardTeams().addTeam(t, Teams.BLUE);
                plugin.getScoreboardManager().updateScoreboard(t);
                t.closeInventory();
            };
            redCallback = (Player t) -> {
                plugin.getScoreboardTeams().addTeam(t, Teams.RED);
                plugin.getScoreboardManager().updateScoreboard(t);
                t.closeInventory();
            };
            greenCallback = (Player t) -> {
                plugin.getScoreboardTeams().addTeam(t, Teams.GREEN);
                plugin.getScoreboardManager().updateScoreboard(t);
                t.closeInventory();
            };
            yellowCallback = (Player t) -> {
                plugin.getScoreboardTeams().addTeam(t, Teams.YELLOW);
                plugin.getScoreboardManager().updateScoreboard(t);
                t.closeInventory();
            };
        }

}
