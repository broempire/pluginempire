package de.broempire.virusempire.Events;

import de.broempire.virusempire.Teams.Teams;
import de.broempire.virusempire.Utils.GameManager;
import de.broempire.virusempire.main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Chat implements Listener {
    
    private main plugin;
    
    public Chat(main plugin) {
        this.plugin = plugin;
    }
	
    @EventHandler
    public void onChat (AsyncPlayerChatEvent e) {

        Player p = e.getPlayer();

        if (GameManager.isState(GameManager.LOBBY) || GameManager.isState(GameManager.Restart)) {
            if (plugin.getMethods().hasTeam(p)) {
                e.setFormat(p.getCustomName() + " §8>> §f"+ e.getMessage());			
            } else {
                e.setFormat(p.getDisplayName() + " §8>> §f"+ e.getMessage());
            }
        } else if (GameManager.isState(GameManager.GAME)){
            if (e.getMessage().startsWith("@a") && plugin.getMethods().hasTeam(p)) {
                e.setFormat("§8[§bGlobal§8] " + p.getCustomName() + " §8>> §f" + e.getMessage().replaceFirst((e.getMessage().startsWith("@all") ? "@all" : "@a"), ""));
            } else {
                e.setCancelled(true);
                if (plugin.blau.contains(p.getName())) {
                    Teams.BLUE.sendMessage(p.getCustomName() + " §8>> §r" + e.getMessage(), plugin);
                }else if (plugin.rot.contains(p.getName())) {
                    Teams.RED.sendMessage(p.getCustomName() + " §8>> §r" + e.getMessage(), plugin);
                } else if (plugin.gelb.contains(p.getName())) {
                    Teams.YELLOW.sendMessage(p.getCustomName() + " §8>> §r" + e.getMessage(), plugin);
                } else if (plugin.grün.contains(p.getName())) {
                    Teams.GREEN.sendMessage(p.getCustomName() + " §8>> §r" + e.getMessage(), plugin);
                } else if (plugin.isSpectator(p)) {
                    Teams.SPECTATOR.sendMessage(p.getCustomName() + " §8>> §r" + e.getMessage(), plugin);
                }   
            }
        }
    }
}
