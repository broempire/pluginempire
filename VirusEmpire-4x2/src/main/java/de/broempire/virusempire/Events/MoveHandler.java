package de.broempire.virusempire.Events;

import de.broempire.virusempire.Utils.GameManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import de.broempire.virusempire.Utils.Teleporter;
import de.broempire.virusempire.main;
import org.bukkit.util.Vector;

public class MoveHandler implements Listener{

    private main plugin;
    
    public MoveHandler(main plugin) {
        this.plugin = plugin;
    }
	
    @EventHandler
    public void onMove (PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if (GameManager.isState(GameManager.GAME)) {
            if (e.getFrom() != e.getTo()) {
                for (Player online : plugin.getServer().getOnlinePlayers()) {
                    if (plugin.isSpectator(online)) {
                        if (Teleporter.getNearest(online) != null) {
                            online.setCompassTarget(Teleporter.getNearest(online).getLocation());
                        }
                    }
                }
                
                if (plugin.isSpectator(p)) {
                    for (Player online : plugin.getServer().getOnlinePlayers()) {
                        if (p != online) {
                            if (p.getLocation().distance(online.getLocation()) <= 5.00) {
                                double pX = p.getLocation().getX();
                                double pY = p.getLocation().getY();
                                double pZ = p.getLocation().getZ();
                                
                                double oX = online.getLocation().getX();
                                double oY = online.getLocation().getY();
                                double oZ = online.getLocation().getZ();
                                
                                double x = pX-oX;
                                double y = pY-oY;
                                double z = pZ-oZ;
                                
                                Vector v = new Vector(x, y, z).normalize().multiply(1.0).setY(0.5);
                                p.setVelocity(v);
                            }
                        }
                    }
                } else {
                    for (Player online : plugin.getServer().getOnlinePlayers()) {
                        if (plugin.isSpectator(online)) {
                            if (p.getLocation().distance(online.getLocation()) <= 5.00) {
                                double pX = p.getLocation().getX();
                                double pY = p.getLocation().getY();
                                double pZ = p.getLocation().getZ();
                                
                                double oX = online.getLocation().getX();
                                double oY = online.getLocation().getY();
                                double oZ = online.getLocation().getZ();
                                
                                double x = oX-pX;
                                double y = oY-pY;
                                double z = oZ-pZ;
                                
                                Vector v = new Vector(x, y, z).normalize().multiply(1.0).setY(0.5);
                                online.setVelocity(v);
                            }
                        }
                    }
                }
            }
        }
    }

}
