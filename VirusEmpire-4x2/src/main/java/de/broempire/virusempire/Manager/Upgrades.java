/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Manager;

/**
 *
 * @author Matti
 */
public enum Upgrades {
    
    TIME("§6Antivirenprogramm"),
    SENSOR("§3Bewegungssensor"),
    DAMAGE("§cElektroschocker");
    
    private String name;
    
    private Upgrades(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
}
