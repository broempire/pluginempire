/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Manager;

import de.broempire.virusempire.APIs.ItemBuilder;
import de.broempire.virusempire.Utils.MapFile;
import de.broempire.virusempire.main;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;

/**
 *
 * @author Matti
 */
public class MapvotingManager implements Listener{
 
    private final main plugin;
    
    private HashMap<String, Integer> votes = new HashMap<>();
    private HashMap<Player, String> playermaps = new HashMap<>();
    private HashMap<Integer, MapFile> selection;
    private boolean voting = true;
    private Inventory inv;

    public MapvotingManager(main plugin) {
        this.plugin = plugin;
        if (!plugin.getMaps().isEmpty()) {
            for (MapFile file : plugin.getMaps()) {
                votes.put(file.getName(), 0);
            }
        }
        inv = Bukkit.createInventory(null, 9, "§7> §bMaps");
        
        List<MapFile> maps = plugin.getMaps();
        selection = new HashMap<>();
        
        for (int i = 0; i < 4; i++) {
            if (!maps.isEmpty()) {
                Random r = new Random();
                int random = r.nextInt(maps.size());
                selection.put(i, maps.get(random));
                maps.remove(random);
            }
        }
        
        if (selection.size() >= 4) {
            inv.setItem(1, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(0).getName()).setAmount(votes.get(selection.get(0).getName())).setLore("§7von: §c" + selection.get(0).getCreator()).build());
            inv.setItem(3, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(1).getName()).setAmount(votes.get(selection.get(1).getName())).setLore("§7von: §c" + selection.get(1).getCreator()).build());
            inv.setItem(5, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(2).getName()).setAmount(votes.get(selection.get(2).getName())).setLore("§7von: §c" + selection.get(2).getCreator()).build());
            inv.setItem(7, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(3).getName()).setAmount(votes.get(selection.get(3).getName())).setLore("§7von: §c" + selection.get(3).getCreator()).build());
        }
    }

    public Integer getVotes(String map) {
        return votes.get(map);
    }
    
    public void addVote(String map) {
        int current = votes.get(map);
        
        votes.replace(map, current + 1);
    }
    
    public void removeVote(String map) {
        int current = votes.get(map);
        
        setVotes(map, (current - 1));
    }
    
    public void setVotes(String map, Integer value) {
        votes.replace(map, value);
    }
    
    public String getWinner() {
        int currentMax = -1;
        String currentMap = null;
        for (MapFile map : selection.values()) {
            if (votes.get(map.getName()) > currentMax) {
                currentMax = votes.get(map.getName());
                currentMap = map.getName();
            }
        }
        endVoting();
        return currentMap;
    }
    
    public void openMapvoting(Player p) {
        p.openInventory(inv);
    }
    
    public void endVoting() {
        voting = false;
    }
    
    public boolean isVoting() {
        return voting;
    }
    
    @EventHandler
    public void onMapClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory() != null && e.getClickedInventory().getName() != null && e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getItemMeta().getDisplayName() != null) {
            if (e.getClickedInventory().getName().equalsIgnoreCase("§7> §bMaps")) {
                e.setCancelled(true);
                if (voting) {
                    addVote(e.getCurrentItem().getItemMeta().getDisplayName());
                    if (playermaps.containsKey(p)) {
                        if (!playermaps.get(p).equalsIgnoreCase(e.getCurrentItem().getItemMeta().getDisplayName())) {
                            removeVote(playermaps.get(p));
                            p.sendMessage(main.pr + "Du hast deinen Vote zu der Map §b" + e.getCurrentItem().getItemMeta().getDisplayName() + " §7geändert!");
                            p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FLAP, 3, 3);
                            playermaps.replace(p, e.getCurrentItem().getItemMeta().getDisplayName());
                            inv.setItem(1, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(0).getName()).setAmount(votes.get(selection.get(0).getName())).setLore("§7von §c" + selection.get(0).getCreator()).build());
                            inv.setItem(3, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(1).getName()).setAmount(votes.get(selection.get(1).getName())).setLore("§7von §c" + selection.get(1).getCreator()).build());
                            inv.setItem(5, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(2).getName()).setAmount(votes.get(selection.get(2).getName())).setLore("§7von §c" + selection.get(2).getCreator()).build());
                            inv.setItem(7, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(3).getName()).setAmount(votes.get(selection.get(3).getName())).setLore("§7von §c" + selection.get(3).getCreator()).build());
                        } else {
                            p.sendMessage(main.pr + "§cDu hast schon für diese Map gestimmt!");
                            p.playSound(p.getLocation(), Sound.ENTITY_VILLAGER_NO, 3, 3);
                        }
                    } else {
                        p.sendMessage(main.pr + "Du hast für die Map §b" + e.getCurrentItem().getItemMeta().getDisplayName() + " §7gevotet!");
                        p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FLAP, 3, 3);
                        playermaps.put(p, e.getCurrentItem().getItemMeta().getDisplayName());
                        inv.setItem(1, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(0).getName()).setAmount(votes.get(selection.get(0).getName())).setLore("§7von §c" + selection.get(0).getCreator()).build());
                        inv.setItem(3, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(1).getName()).setAmount(votes.get(selection.get(1).getName())).setLore("§7von §c" + selection.get(1).getCreator()).build());
                        inv.setItem(5, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(2).getName()).setAmount(votes.get(selection.get(2).getName())).setLore("§7von §c" + selection.get(2).getCreator()).build());
                        inv.setItem(7, new ItemBuilder(Material.PAPER).setDisplayName(selection.get(3).getName()).setAmount(votes.get(selection.get(3).getName())).setLore("§7von §c" + selection.get(3).getCreator()).build());
                    }
                    p.closeInventory();
                } else {
                    p.sendMessage(main.pr + "§cDie Votingphase ist bereits beendet!");
                }
            }
        }
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (playermaps.containsKey(p)) {
            removeVote(playermaps.get(p));
        }
    }
    
}
