/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Manager;

import com.sun.javafx.geom.Crossings;
import de.broempire.virusempire.APIs.ActionAPI;
import de.broempire.virusempire.APIs.GUIBuilder;
import de.broempire.virusempire.APIs.ItemBuilder;
import de.broempire.virusempire.Teams.Teams;
import de.broempire.virusempire.Utils.GameManager;
import de.broempire.virusempire.Utils.MapFile;
import de.broempire.virusempire.main;
import java.util.HashMap;
import java.util.function.Consumer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSprintEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author Matti
 */

public class CommandInteractManager implements Listener {
    
    private Consumer<Player> setupgreen;
    private Consumer<Player> setupblue;
    private Consumer<Player> setupyellow;
    private Consumer<Player> setupred;
    private Consumer<Player> setupcancel;
    
    private Location setupblock;
    
    private final HashMap<Teams, Boolean> isShuttingDown;
    
    private MapFile mapFile;
    
    private final GUIBuilder setupgui;

    private final main plugin;
    
    public CommandInteractManager(main plugin) {
        initCallbacks();
        this.isShuttingDown = new HashMap<>();
        this.plugin = plugin;
        this.setupgui = new GUIBuilder(plugin, 9, "§7> §eWähle das Team")
                .addGUIItem(0, new ItemBuilder(Material.WOOL, 1, (short) 11, setupblue).setDisplayName("§7Team §9Blau"))
                .addGUIItem(1, new ItemBuilder(Material.WOOL, 1, (short) 14, setupred).setDisplayName("§7Team §cRot"))
                .addGUIItem(2, new ItemBuilder(Material.WOOL, 1, (short) 5, setupgreen).setDisplayName("§7Team §2Grün"))
                .addGUIItem(3, new ItemBuilder(Material.WOOL, 1, (short) 4, setupyellow).setDisplayName("§7Team §eGelb"))
                .addGUIItem(8, new ItemBuilder(Material.BARRIER, setupcancel).setDisplayName("§4Zurück"));
        
        this.isShuttingDown.put(Teams.BLUE, Boolean.FALSE);
        this.isShuttingDown.put(Teams.RED, Boolean.FALSE);
        this.isShuttingDown.put(Teams.GREEN, Boolean.FALSE);
        this.isShuttingDown.put(Teams.YELLOW, Boolean.FALSE);
    }
    
    public void setMapFile(MapFile mapFile) {
        this.mapFile = mapFile;
    }
    
    @EventHandler
    public void onCommandblockInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            Block block = e.getClickedBlock();
            if (GameManager.isState(GameManager.LOBBY)) {
                if (plugin.isSetupMode(p)) {
                    if (block.getType().equals(Material.LEVER)) {
                        e.setCancelled(true);
                        setupblock = block.getLocation();
                        setupgui.open(p);
                    } else if (block.getRelative(BlockFace.EAST).getType().equals(Material.LEVER)) {
                        e.setCancelled(true);
                        setupblock = block.getRelative(BlockFace.EAST).getLocation();
                        setupgui.open(p);
                    } else if (block.getRelative(BlockFace.WEST).getType().equals(Material.LEVER)) {
                        e.setCancelled(true);
                        setupblock = block.getRelative(BlockFace.WEST).getLocation();
                        setupgui.open(p);
                    } else if (block.getRelative(BlockFace.NORTH).getType().equals(Material.LEVER)) {
                        e.setCancelled(true);
                        setupblock = block.getRelative(BlockFace.NORTH).getLocation();
                        setupgui.open(p);
                    } else if (block.getRelative(BlockFace.SOUTH).getType().equals(Material.LEVER)) {
                        e.setCancelled(true);
                        setupblock = block.getRelative(BlockFace.SOUTH).getLocation();
                        setupgui.open(p);
                    } else if (block.getRelative(BlockFace.UP).getType().equals(Material.LEVER)) {
                        e.setCancelled(true);
                        setupblock = block.getRelative(BlockFace.UP).getLocation();
                        setupgui.open(p);
                    } else if (block.getRelative(BlockFace.DOWN).getType().equals(Material.LEVER)) {
                        e.setCancelled(true);
                        setupblock = block.getRelative(BlockFace.DOWN).getLocation();
                        setupgui.open(p);
                    }
                }
            } else if (GameManager.isState(GameManager.GAME)) {
                if (plugin.getMethods().hasTeam(p)) {
                    if (block.getType().equals(Material.LEVER) || block.getType().equals(Material.COMMAND)) {
                        if (block.getLocation().distance(mapFile.getLever("BLAU")) <= 1) {
                            if (!plugin.getMethods().getExactTeam(p).equalsIgnoreCase("Blau")) {
                                if (isShuttingDown.get(Teams.BLUE).equals(Boolean.FALSE)) {
                                    isShuttingDown.replace(Teams.BLUE, Boolean.TRUE);
                                    Teams.BLUE.shutDown(p, Teams.BLUE, plugin);
                                    p.damage(plugin.getUpgradeManager().getDamage(Teams.BLUE));
                                    e.setUseInteractedBlock(Event.Result.ALLOW);
                                    e.setCancelled(true);
                                } else { 
                                   e.setCancelled(true);
                                    ActionAPI.sendActionBar(p, "§8» §cDieser Nucleus wurde bereits infiziert! §8«");
                                }
                            } else {
                                if (isShuttingDown.get(Teams.BLUE).equals(Boolean.FALSE)) {
                                    plugin.getUpgradeManager().openUpgradeInventory(p);
                                } else if (plugin.getMethods().canRespawn(p)) {
                                    isShuttingDown.replace(Teams.BLUE, Boolean.FALSE);
                                    Teams.BLUE.restart(p, Teams.BLUE, plugin);
                                } else {
                                    e.setCancelled(true);
                                }
                            }
                        } else if (block.getLocation().distance(mapFile.getLever("ROT")) <= 1) {
                            if (!plugin.getMethods().getExactTeam(p).equalsIgnoreCase("Rot")) {
                                if (isShuttingDown.get(Teams.RED).equals(Boolean.FALSE)) {
                                    isShuttingDown.replace(Teams.RED, Boolean.TRUE);
                                    Teams.RED.shutDown(p, Teams.RED, plugin);
                                    p.damage(plugin.getUpgradeManager().getDamage(Teams.RED));
                                    e.setUseInteractedBlock(Event.Result.ALLOW);
                                    e.setCancelled(true);
                                } else {
                                    e.setCancelled(true);
                                    ActionAPI.sendActionBar(p, "§8» §cDieser Nucleus wurde bereits infiziert! §8«");
                                }
                            } else {
                                if (isShuttingDown.get(Teams.RED).equals(Boolean.FALSE)) {
                                    plugin.getUpgradeManager().openUpgradeInventory(p);
                                } else if (plugin.getMethods().canRespawn(p)){
                                    isShuttingDown.replace(Teams.RED, Boolean.FALSE);
                                    Teams.RED.restart(p, Teams.RED, plugin);
                                } else {
                                    e.setCancelled(true);
                                }
                            }
                        } else if (block.getLocation().distance(mapFile.getLever("GRÜN")) <= 1) {
                            if (!plugin.getMethods().getExactTeam(p).equalsIgnoreCase("Grün")) {
                                if (isShuttingDown.get(Teams.GREEN).equals(Boolean.FALSE)) {
                                    isShuttingDown.replace(Teams.GREEN, Boolean.TRUE);
                                    Teams.GREEN.shutDown(p, Teams.GREEN, plugin);
                                    p.damage(plugin.getUpgradeManager().getDamage(Teams.GREEN));
                                    e.setUseInteractedBlock(Event.Result.ALLOW);
                                    e.setCancelled(true);
                                } else {
                                    e.setCancelled(true);
                                    ActionAPI.sendActionBar(p, "§8» §cDieser Nucleus wurde bereits infiziert! §8«");
                                }
                            } else {
                                if (isShuttingDown.get(Teams.GREEN).equals(Boolean.FALSE)) {
                                    plugin.getUpgradeManager().openUpgradeInventory(p);
                                } else if (plugin.getMethods().canRespawn(p)) {
                                    isShuttingDown.replace(Teams.GREEN, Boolean.FALSE);
                                    Teams.GREEN.restart(p, Teams.GREEN, plugin);
                                } else {
                                    e.setCancelled(true);
                                }
                            }
                        } else if (block.getLocation().distance(mapFile.getLever("GELB")) <= 1) {
                            if (!plugin.getMethods().getExactTeam(p).equalsIgnoreCase("Gelb")) {
                                if (isShuttingDown.get(Teams.YELLOW).equals(Boolean.FALSE)) {
                                    isShuttingDown.replace(Teams.YELLOW, Boolean.TRUE);
                                    Teams.YELLOW.shutDown(p, Teams.YELLOW, plugin);
                                    p.damage(plugin.getUpgradeManager().getDamage(Teams.YELLOW));
                                    e.setUseInteractedBlock(Event.Result.ALLOW);
                                    e.setCancelled(true);
                                } else {
                                    e.setCancelled(true);
                                    ActionAPI.sendActionBar(p, "§8» §cDieser Nucleus wurde bereits infiziert! §8«");
                                }
                            } else {
                                if (isShuttingDown.get(Teams.YELLOW).equals(Boolean.FALSE)) {
                                    plugin.getUpgradeManager().openUpgradeInventory(p);
                                } else if (plugin.getMethods().canRespawn(p)) {
                                    isShuttingDown.replace(Teams.YELLOW, Boolean.FALSE);
                                    Teams.YELLOW.restart(p, Teams.YELLOW, plugin);
                                } else {
                                    e.setCancelled(true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @EventHandler
    public void onSprint(PlayerToggleSprintEvent e) {
        Player p = e.getPlayer();
        if (GameManager.isState(GameManager.GAME)) {
            if (e.isSprinting()) {
                if (!plugin.getMethods().canRespawn(p) && plugin.getMethods().hasTeam(p)) {
                    if (p.getExp() <= (float) 0.00) {
                        ActionAPI.sendActionBar(p, "§7» §cDeine Ausdauer muss sich regenerieren! §7«");
                        e.setCancelled(true);
                    }
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            if (p.isSprinting()) {
                                p.setExp(p.getExp() - (float) 0.2);
                            } else {
                                p.setExp(p.getExp() + (float) 0.2);
                            }
                            if (p.getExp() <= (float) 0.00) {
                                ActionAPI.sendActionBar(p, "§7» §cDeine Ausdauer muss sich regenerieren! §7«");
                                p.setSprinting(false);
                            } else if (p.getExp() >= (float) 0.99) {
                                p.setExp(0.99F);
                                cancel();
                            }
                        }
                    }.runTaskTimer(plugin, 20L, 50L);
                }
            }
        }
    }
    
    @EventHandler
    public void onPlayerMove (PlayerMoveEvent e) {
        if (GameManager.isState(GameManager.GAME)) {
            Player p = e.getPlayer();
            Teams team;
            switch (plugin.getMethods().getExactTeam(p)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = null;
                    break;
            }
            if (e.getFrom() != e.getTo() && team != null) {
                if (p.getLocation().distance(mapFile.getLever("BLAU")) <= plugin.getUpgradeManager().getDistance(Teams.BLUE)) {
                    if (team != Teams.BLUE) {
                        if (!isShuttingDown.get(Teams.BLUE)) {
                            if (!p.isSneaking()) {
                                Teams.BLUE.sendActionBar("§4ACHTUNG! §cEin Spieler ist " + Math.round(p.getLocation().distance(mapFile.getLever("BLAU"))) + " Blöcke von deinem Nucleus entfernt!", plugin);
                                if (plugin.getUpgradeManager().isLevel(Teams.BLUE, Upgrades.SENSOR, 5)) {
                                    if (p.isSprinting()) {
                                        p.setSprinting(false);
                                    }
                                }
                            }
                        }
                    }
                } else if (p.getLocation().distance(mapFile.getLever("ROT")) <= plugin.getUpgradeManager().getDistance(Teams.RED)) {
                    if (team != Teams.RED) {
                        if (!isShuttingDown.get(Teams.RED)) {
                            if (!p.isSneaking()) {
                                Teams.RED.sendActionBar("§4ACHTUNG! §cEin Spieler ist " + Math.round(p.getLocation().distance(mapFile.getLever("ROT"))) + " Blöcke von deinem Nucleus entfernt!", plugin);
                                if (plugin.getUpgradeManager().isLevel(Teams.RED, Upgrades.SENSOR, 5)) {
                                    if (p.isSprinting()) {
                                        p.setSprinting(false);
                                    }
                                }
                            }
                        }
                    }
                } else if (p.getLocation().distance(mapFile.getLever("GRÜN")) <= plugin.getUpgradeManager().getDistance(Teams.GREEN)) {
                    if (team != Teams.GREEN) {
                        if (!isShuttingDown.get(Teams.GREEN)) {
                            if (!p.isSneaking()) {
                                Teams.GREEN.sendActionBar("§4ACHTUNG! §cEin Spieler ist " + Math.round(p.getLocation().distance(mapFile.getLever("GRÜN"))) + " Blöcke von deinem Nucleus entfernt!", plugin);
                                if (plugin.getUpgradeManager().isLevel(Teams.GREEN, Upgrades.SENSOR, 5)) {
                                    if (p.isSprinting()) {
                                        p.setSprinting(false);
                                    }
                                }
                            }
                        }
                    }
                } else if (p.getLocation().distance(mapFile.getLever("GELB")) <= plugin.getUpgradeManager().getDistance(Teams.YELLOW)) {
                    if (team != Teams.YELLOW) {
                        if (!isShuttingDown.get(Teams.YELLOW)) {
                            if (!p.isSneaking()) {
                                Teams.YELLOW.sendActionBar("§4ACHTUNG! §cEin Spieler ist " + Math.round(p.getLocation().distance(mapFile.getLever("GELB"))) + " Blöcke von deinem Nucleus entfernt!", plugin);
                                if (plugin.getUpgradeManager().isLevel(Teams.YELLOW, Upgrades.SENSOR, 5)) {
                                    if (p.isSprinting()) {
                                        p.setSprinting(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void initCallbacks() {
        setupblue = (Player t) -> {
            for (MapFile file : plugin.getMaps()) {
                if (file.getWorld().equals(t.getWorld().getName())) {
                    mapFile = new MapFile(plugin.getDataFolder().getPath(), file.getName());
                    mapFile.setLever("BLAU", setupblock);
                    t.sendMessage(main.pr + "Du hast den Nucleus von Team §9Blau §7gesetzt!");
                    t.closeInventory();
                    return;
                }
            }
            t.sendMessage(main.pr + "§cDu musst zuerst die Welt registrieren!");
            t.closeInventory();
        };
        setupred = (Player t) -> {
            for (MapFile file : plugin.getMaps()) {
                if (file.getWorld().equals(t.getWorld().getName())) {
                    mapFile = new MapFile(plugin.getDataFolder().getPath(), file.getName());
                    mapFile.setLever("ROT", setupblock);
                    t.sendMessage(main.pr + "Du hast den Nucleus von Team §cRot §7gesetzt!");
                    t.closeInventory();
                    return;
                }
            }
            t.sendMessage(main.pr + "§cDu musst zuerst die Welt registrieren!");
            t.closeInventory();
        };
        setupgreen = (Player t) -> {
            for (MapFile file : plugin.getMaps()) {
                if (file.getWorld().equals(t.getWorld().getName())) {
                    mapFile = new MapFile(plugin.getDataFolder().getPath(), file.getName());
                    mapFile.setLever("GRÜN", setupblock);
                    t.sendMessage(main.pr + "Du hast den Nucleus von Team §2Grün §7gesetzt!");
                    t.closeInventory();
                    return;
                }
            }
            t.sendMessage(main.pr + "§cDu musst zuerst die Welt registrieren!");
            t.closeInventory();
        };
        setupyellow = (Player t) -> {
            for (MapFile file : plugin.getMaps()) {
                if (file.getWorld().equals(t.getWorld().getName())) {
                    mapFile = new MapFile(plugin.getDataFolder().getPath(), file.getName());
                    mapFile.setLever("GELB", setupblock);
                    t.sendMessage(main.pr + "Du hast den Nucleus von Team §eGelb §7gesetzt!");
                    t.closeInventory();
                    return;
                }
            }
            t.sendMessage(main.pr + "§cDu musst zuerst die Welt registrieren!");
            t.closeInventory();
        };
        setupcancel = (Player t) -> {
            t.closeInventory();
        };
    }
    
}
