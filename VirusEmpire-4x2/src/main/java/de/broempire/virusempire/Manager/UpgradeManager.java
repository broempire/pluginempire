/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Manager;

import de.broempire.virusempire.APIs.ActionAPI;
import de.broempire.virusempire.APIs.GUIBuilder;
import de.broempire.virusempire.APIs.ItemBuilder;
import de.broempire.virusempire.Teams.Teams;
import de.broempire.virusempire.main;
import java.util.HashMap;
import java.util.function.Consumer;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 *
 * @author Matti
 */
public class UpgradeManager {

    private final main plugin;
    
    private final HashMap<Teams, Integer> timelevel = new HashMap<>();
    private final HashMap<Teams, Integer> sensorlevel = new HashMap<>();
    private final HashMap<Teams, Integer> damagelevel = new HashMap<>();
    
    private Consumer<Player> timelvl2;
    private Consumer<Player> sensorlvl2;
    private Consumer<Player> damagelvl2;
    
    private Consumer<Player> timelvl3;
    private Consumer<Player> sensorlvl3;
    private Consumer<Player> damagelvl3;
    
    private Consumer<Player> timelvl4;
    private Consumer<Player> sensorlvl4;
    private Consumer<Player> damagelvl4;
    
    private Consumer<Player> timelvl5;
    private Consumer<Player> sensorlvl5;
    private Consumer<Player> damagelvl5;
    
    private HashMap<Teams, GUIBuilder> upgradeinv = new HashMap<>();
    
    public UpgradeManager(main plugin) {
        this.plugin = plugin;
        initCallbacks();
        
        timelevel.put(Teams.BLUE, 1);
        timelevel.put(Teams.RED, 1);
        timelevel.put(Teams.GREEN, 1);
        timelevel.put(Teams.YELLOW, 1);
        
        damagelevel.put(Teams.BLUE, 1);
        damagelevel.put(Teams.RED, 1);
        damagelevel.put(Teams.GREEN, 1);
        damagelevel.put(Teams.YELLOW, 1);
        
        sensorlevel.put(Teams.BLUE, 1);
        sensorlevel.put(Teams.RED, 1);
        sensorlevel.put(Teams.GREEN, 1);
        sensorlevel.put(Teams.YELLOW, 1);
        
        upgradeinv.put(Teams.BLUE, new GUIBuilder(plugin, 45, "§7> §6Nucleus"));
        upgradeinv.put(Teams.RED, new GUIBuilder(plugin, 45, "§7> §6Nucleus"));
        upgradeinv.put(Teams.YELLOW, new GUIBuilder(plugin, 45, "§7> §6Nucleus"));
        upgradeinv.put(Teams.GREEN, new GUIBuilder(plugin, 45, "§7> §6Nucleus"));
        
        for (int i = 0; i < 45; i++) {
            upgradeinv.get(Teams.BLUE).addItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 0).setDisplayName(" ").build());
            upgradeinv.get(Teams.GREEN).addItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 0).setDisplayName(" ").build());
            upgradeinv.get(Teams.YELLOW).addItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 0).setDisplayName(" ").build());
            upgradeinv.get(Teams.RED).addItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 0).setDisplayName(" ").build());
        }
        
    }
    
    public void levelUp(Player p, Teams team, Upgrades upgrade) {
        switch (upgrade) {
            case TIME:
                setLevel(team, upgrade, timelevel.get(team) + 1);
            case SENSOR:
                setLevel(team, upgrade, sensorlevel.get(team) + 1);
            case DAMAGE:
                setLevel(team, upgrade, damagelevel.get(team) + 1);
        }
    }
    
    public Integer getLevel(Teams team, Upgrades upgrade) {
        switch (upgrade) {
            case TIME:
                return timelevel.get(team);
            case SENSOR:
                return sensorlevel.get(team);
            case DAMAGE:
                return damagelevel.get(team);
            default:
                return null;
        }
    }
    
    public void setLevel(Teams team, Upgrades upgrade, int level) {
        switch(upgrade) {
            case TIME:
                if (timelevel.containsKey(team)) {
                    timelevel.remove(team);
                }
                timelevel.put(team, level);
                break;
            case SENSOR:
                if (sensorlevel.containsKey(team)) {
                    sensorlevel.remove(team);
                }
                sensorlevel.put(team, level);
                break;
            case DAMAGE:
                if (damagelevel.containsKey(team)) {
                    damagelevel.remove(team);
                }
                damagelevel.put(team, level);
                break;
            default:
                break;
        }
    }
    
    public boolean isLevel(Teams team, Upgrades upgrade, int level) {
        switch(upgrade) {
            case TIME:
                return timelevel.get(team) >= level;
            case SENSOR:
                return sensorlevel.get(team) >= level;
            case DAMAGE:
                return damagelevel.get(team) >= level;
            default:
                return false;
        }
    }
    
    public Integer getTime(Teams team) {
        if (timelevel.containsKey(team)) {
            switch (timelevel.get(team)) {
                case 1:
                    return 3;
                case 2:
                    return 5;
                case 3:
                    return 7;
                case 4:
                    return 10;
                case 5:
                    return 15;
            }
        }
        return null;
    }
    
    public Integer getTime(Integer level) {
        switch (level) {
            case 1:
                return 3;
            case 2:
                return 5;
            case 3:
                return 7;
            case 4:
                return 10;
            case 5:
                return 15;
        }
        return null;
    }
    
    public Integer getDistance(Teams team) {
        if (sensorlevel.containsKey(team)) {
            switch (sensorlevel.get(team)) {
                case 1:
                    return 0;
                case 2:
                    return 5;
                case 3:
                    return 10;
                case 4:
                    return 15;
                case 5:
                    return 25;
            }
        }
        return null;
    }
    
    public Integer getDistance(Integer level) {
        switch (level) {
            case 1:
                return 0;
            case 2:
                return 5;
            case 3:
                return 10;
            case 4:
                return 15;
            case 5:
                return 25;
        }
        return null;
    }
    
    public Double getDamage(Teams team) {
        if (damagelevel.containsKey(team)) {
            switch (damagelevel.get(team)) {
                case 1:
                    return 0.0;
                case 2:
                    return 1.0;
                case 3:
                    return 3.0;
                case 4:
                    return 7.0;
                case 5:
                    return 12.0;
            }
        }
        return null;
    }
    
    public Double getDamage(Integer level) {
        switch (level) {
            case 1:
                return 0.0;
            case 2:
                return 1.0;
            case 3:
                return 3.0;
            case 4:
                return 7.0;
            case 5:
                return 12.0;
        }
        return null;
    }
    
    public boolean hasLevel(Player p, Upgrades upgrade, int level) {
        return p.getLevel() >= getNeededLevel(upgrade, level);
    }
    
    public int getNeededLevel(Upgrades upgrade, int level) {
        switch (upgrade) {
            case TIME:
                switch (level) {
                    case 2:
                        return 4;
                    case 3:
                        return 7;
                    case 4:
                        return 10;
                    case 5:
                        return 15;
                }
                break;
            case SENSOR:
                switch (level) {
                    case 2:
                        return 5;
                    case 3:
                        return 8;
                    case 4:
                        return 12;
                    case 5:
                        return 18;
                }
                break;
            case DAMAGE:
                switch (level) {
                    case 2:
                        return 5;
                    case 3:
                        return 9;
                    case 4:
                        return 15;
                    case 5:
                        return 20;
                }
                break;
        }
        return 0;
    }
    
    public void addUpgradeLevel(Player p, int points) {
        p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5F, 5F);
        p.giveExpLevels(points);
        if (points != 0) {
            ActionAPI.sendActionBar(p, "§a+ §7Du hast §6" + points + " §bUpgrade-Level §7erhalten §a+");
        }
    }
    
    public void openUpgradeInventory(Player p) {
        Teams team = null;
        switch(plugin.getMethods().getExactTeam(p)) {
            case "Blau":
                team = Teams.BLUE;
                break;
            case "Rot":
                team = Teams.RED;
                break;
            case "Grün":
                team = Teams.GREEN;
                break;
            case "Gelb":
                team = Teams.YELLOW;
                break;
            default:
                team = Teams.SPECTATOR;
                break;
        }
        GUIBuilder inv = upgradeinv.get(team);
        
        inv.getItems().clear();
        
        inv.addItem(0, new ItemBuilder(Material.WATCH).setDisplayName(Upgrades.TIME.getName() + "§8» §aLevel " + getLevel(team, Upgrades.TIME)).setLore("§7Du brauchst etwas mehr", "§7Zeit, um das Herunterfahren des", "§7Energiekerns zu stoppen?", "§7Dann kannst du diese", "§7hiermit erhöhen!").build());
        inv.addItem(18, new ItemBuilder(Material.STRING).setDisplayName(Upgrades.SENSOR.getName() + "§8» §aLevel " + getLevel(team, Upgrades.SENSOR)).setLore("§7Willst du deinen Gegner", "§7schon vor deinem Energiekern", "§7stoppen?", "§7Hiermir gelingt dir das", "§7vielleicht!").build());
        inv.addItem(36, new ItemBuilder(Material.REDSTONE_COMPARATOR).setDisplayName(Upgrades.DAMAGE.getName() + "§8» §aLevel " + getLevel(team, Upgrades.DAMAGE)).setLore("§7Deine Gegner sollen leiden?", "§7Das ist die Aufgabe", "§7des schmerzhaften Elektroschockers!").build());
        
        inv.addItem(25, new ItemBuilder(Material.COMMAND).setDisplayName("§6Energiekern").setLore(" ", "§9Zeit: §3" + getTime(team) + "s", "§9Entfernung: §3" + getDistance(team) + " Blöcke", "§9Schaden: §3" + getDamage(team)/2 + " Herzen").build());
        
        inv.addItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short)5).setDisplayName("§2§l✔ §6Level 1").setLore(" ", "§9Zeit: §3" + getTime(1) + "s").build());
        inv.addItem(19, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short)5).setDisplayName("§2§l✔ §6Level 1").setLore(" ", "§9Entfernung: §3" + getDistance(1) + " Blöcke").build());
        inv.addItem(37, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short)5).setDisplayName("§2§l✔ §6Level 1").setLore(" ", "§9Schaden: §3" + getDamage(1)/2 + " Herzen").build());
        
        inv.addGUIItem(2, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.TIME, 2) ? (short)5 : (short)7, timelvl2).setDisplayName(isLevel(team, Upgrades.TIME, 2) ? "§2§l✔ §6Level 2" : "§c§l✘ §4Level 2").setLore(" ", "§9Zeit: §3" + getTime(2) + "s", " ", isLevel(team, Upgrades.TIME, 2) ? "§2Preis: §a" + getNeededLevel(Upgrades.TIME, 2) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.TIME, 2) + " Level"));
        inv.addGUIItem(20, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.SENSOR, 2) ? (short)5 : (short)7, sensorlvl2).setDisplayName(isLevel(team, Upgrades.SENSOR, 2) ? "§2§l✔ §6Level 2" : "§c§l✘ §4Level 2").setLore(" ", "§9Entfernung: §3" + getDistance(2) + " Blöcke", " ", isLevel(team, Upgrades.SENSOR, 2) ? "§2Preis: §a" + getNeededLevel(Upgrades.SENSOR, 2) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.SENSOR, 2) + " Level"));
        inv.addGUIItem(38, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.DAMAGE, 2) ? (short)5 : (short)7, damagelvl2).setDisplayName(isLevel(team, Upgrades.DAMAGE, 2) ? "§2§l✔ §6Level 2" : "§c§l✘ §4Level 2").setLore(" ", "§9Schaden: §3" + getDamage(2)/2 + " Herzen", " ", isLevel(team, Upgrades.DAMAGE, 2) ? "§2Preis: §a" + getNeededLevel(Upgrades.DAMAGE, 2) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.DAMAGE, 2) + " Level"));
        
        inv.addGUIItem(3, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.TIME, 3) ? (short)5 : (short)7, timelvl3).setDisplayName(isLevel(team, Upgrades.TIME, 3) ? "§2§l✔ §6Level 3" : "§c§l✘ §4Level 3").setLore(" ", "§9Zeit: §3" + getTime(3) + "s", " ", isLevel(team, Upgrades.TIME, 3) ? "§2Preis: §a" + getNeededLevel(Upgrades.TIME, 3) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.TIME, 3) + " Level"));
        inv.addGUIItem(21, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.SENSOR, 3) ? (short)5 : (short)7, sensorlvl3).setDisplayName(isLevel(team, Upgrades.SENSOR, 3) ? "§2§l✔ §6Level 3" : "§c§l✘ §4Level 3").setLore(" ", "§9Entfernung: §3" + getDistance(3) + " Blöcke", " ", isLevel(team, Upgrades.SENSOR, 3) ? "§2Preis: §a" + getNeededLevel(Upgrades.SENSOR, 3) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.SENSOR, 3) + " Level"));
        inv.addGUIItem(39, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.DAMAGE, 3) ? (short)5 : (short)7, damagelvl3).setDisplayName(isLevel(team, Upgrades.DAMAGE, 3) ? "§2§l✔ §6Level 3" : "§c§l✘ §4Level 3").setLore(" ", "§9Schaden: §3" + getDamage(3)/2 + " Herzen", " ", isLevel(team, Upgrades.DAMAGE, 3) ? "§2Preis: §a" + getNeededLevel(Upgrades.DAMAGE, 3) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.DAMAGE, 3) + " Level"));
        
        inv.addGUIItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.TIME, 4) ? (short)5 : (short)7, timelvl4).setDisplayName(isLevel(team, Upgrades.TIME, 4) ? "§2§l✔ §6Level 4" : "§c§l✘ §4Level 4").setLore(" ", "§9Zeit: §3" + getTime(4) + "s", " ", isLevel(team, Upgrades.TIME, 4) ? "§2Preis: §a" + getNeededLevel(Upgrades.TIME, 4) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.TIME, 4) + " Level"));
        inv.addGUIItem(22, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.SENSOR, 4) ? (short)5 : (short)7, sensorlvl4).setDisplayName(isLevel(team, Upgrades.SENSOR, 4) ? "§2§l✔ §6Level 4" : "§c§l✘ §4Level 4").setLore(" ", "§9Entfernung: §3" + getDistance(4) + " Blöcke", " ", isLevel(team, Upgrades.SENSOR, 4) ? "§2Preis: §a" + getNeededLevel(Upgrades.SENSOR, 4) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.SENSOR, 4) + " Level"));
        inv.addGUIItem(40, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.DAMAGE, 4) ? (short)5 : (short)7, damagelvl4).setDisplayName(isLevel(team, Upgrades.DAMAGE, 4) ? "§2§l✔ §6Level 4" : "§c§l✘ §4Level 4").setLore(" ", "§9Schaden: §3" + getDamage(4)/2 + " Herzen", " ", isLevel(team, Upgrades.DAMAGE, 4) ? "§2Preis: §a" + getNeededLevel(Upgrades.DAMAGE, 4) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.DAMAGE, 4) + " Level"));
    
        inv.addGUIItem(5, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.TIME, 5) ? (short)5 : (short)7, timelvl5).setDisplayName(isLevel(team, Upgrades.TIME, 5) ? "§2§l✔ §6Level 5" : "§c§l✘ §4Level 5").setLore(" ", "§9Zeit: §3" + getTime(5) + "s", " ", isLevel(team, Upgrades.TIME, 5) ? "§2Preis: §a" + getNeededLevel(Upgrades.TIME, 5) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.TIME, 5) + " Level"));
        inv.addGUIItem(23, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.SENSOR, 5) ? (short)5 : (short)7, sensorlvl5).setDisplayName(isLevel(team, Upgrades.SENSOR, 5) ? "§2§l✔ §6Level 5" : "§c§l✘ §4Level 5").setLore(" ", "§9Entfernung: §3" + getDistance(5) + " Blöcke", " ", isLevel(team, Upgrades.SENSOR, 5) ? "§2Preis: §a" + getNeededLevel(Upgrades.SENSOR, 5) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.SENSOR, 5) + " Level"));
        inv.addGUIItem(41, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, isLevel(team, Upgrades.DAMAGE, 5) ? (short)5 : (short)7, damagelvl5).setDisplayName(isLevel(team, Upgrades.DAMAGE, 5) ? "§2§l✔ §6Level 5" : "§c§l✘ §4Level 5").setLore(" ", "§9Schaden: §3" + getDamage(5)/2 + " Herzen", " ", isLevel(team, Upgrades.DAMAGE, 5) ? "§2Preis: §a" + getNeededLevel(Upgrades.DAMAGE, 5) + " Level" : "§4Preis: §c" + getNeededLevel(Upgrades.DAMAGE, 5) + " Level"));
        
        inv.open(p);
    }

    private void initCallbacks() {
        timelvl2 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.TIME, 2)) {
                if (hasLevel(t, Upgrades.TIME, 2)) {
                    setLevel(team, Upgrades.TIME, 2);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.TIME, 2));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.TIME.getName() + " §8» §6Level " + getLevel(team, Upgrades.TIME) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.TIME.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.TIME) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        sensorlvl2 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.SENSOR, 2)) {
                if (hasLevel(t, Upgrades.SENSOR, 2)) {
                    setLevel(team, Upgrades.SENSOR, 2);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.SENSOR, 2));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.SENSOR.getName() + " §8» §6Level " + getLevel(team, Upgrades.SENSOR) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.SENSOR.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.SENSOR) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        damagelvl2 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.DAMAGE, 2)) {
                if (hasLevel(t, Upgrades.DAMAGE, 2)) {
                    setLevel(team, Upgrades.DAMAGE, 2);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.DAMAGE, 2));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.DAMAGE.getName() + " §8» §6Level " + getLevel(team, Upgrades.DAMAGE) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.DAMAGE.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.DAMAGE) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        
        timelvl3 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.TIME, 3)) {
                if (hasLevel(t, Upgrades.TIME, 3)) {
                    setLevel(team, Upgrades.TIME, 3);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.TIME, 3));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.TIME.getName() + " §8» §6Level " + getLevel(team, Upgrades.TIME) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.TIME.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.TIME) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        sensorlvl3 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.SENSOR, 3)) {
                if (hasLevel(t, Upgrades.SENSOR, 3)) {
                    setLevel(team, Upgrades.SENSOR, 3);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.SENSOR, 3));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.SENSOR.getName() + " §8» §6Level " + getLevel(team, Upgrades.SENSOR) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.SENSOR.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.SENSOR) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        damagelvl3 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.DAMAGE, 3)) {
                if (hasLevel(t, Upgrades.DAMAGE, 3)) {
                    setLevel(team, Upgrades.DAMAGE, 3);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.DAMAGE, 3));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.DAMAGE.getName() + " §8» §6Level " + getLevel(team, Upgrades.DAMAGE) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.DAMAGE.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.DAMAGE) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        
        timelvl4 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.TIME, 4)) {
                if (hasLevel(t, Upgrades.TIME, 4)) {
                    setLevel(team, Upgrades.TIME, 4);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.TIME, 4));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.TIME.getName() + " §8» §6Level " + getLevel(team, Upgrades.TIME) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.TIME.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.TIME) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        sensorlvl4 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.SENSOR, 4)) {
                if (hasLevel(t, Upgrades.SENSOR, 4)) {
                    setLevel(team, Upgrades.SENSOR, 4);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.SENSOR, 4));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.SENSOR.getName() + " §8» §6Level " + getLevel(team, Upgrades.SENSOR) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.SENSOR.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.SENSOR) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        damagelvl4 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.DAMAGE, 4)) {
                if (hasLevel(t, Upgrades.DAMAGE, 4)) {
                    setLevel(team, Upgrades.DAMAGE, 4);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.DAMAGE, 4));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.DAMAGE.getName() + " §8» §6Level " + getLevel(team, Upgrades.DAMAGE) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.DAMAGE.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.DAMAGE) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        
        timelvl5 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.TIME, 5)) {
                if (hasLevel(t, Upgrades.TIME, 5)) {
                    setLevel(team, Upgrades.TIME, 5);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.TIME, 5));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.TIME.getName() + " §8» §6Level " + getLevel(team, Upgrades.TIME) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.TIME.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.TIME) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        sensorlvl5 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.SENSOR, 5)) {
                if (hasLevel(t, Upgrades.SENSOR, 5)) {
                    setLevel(team, Upgrades.SENSOR, 5);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.SENSOR, 5));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.SENSOR.getName() + " §8» §6Level " + getLevel(team, Upgrades.SENSOR) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.SENSOR.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.SENSOR) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
        damagelvl5 = (Player t) -> {
            Teams team;
            switch(plugin.getMethods().getExactTeam(t)) {
                case "Blau":
                    team = Teams.BLUE;
                    break;
                case "Rot":
                    team = Teams.RED;
                    break;
                case "Grün":
                    team = Teams.GREEN;
                    break;
                case "Gelb":
                    team = Teams.YELLOW;
                    break;
                default:
                    team = Teams.SPECTATOR;
                    break;
            }
            if (!isLevel(team, Upgrades.DAMAGE, 5)) {
                if (hasLevel(t, Upgrades.DAMAGE, 5)) {
                    setLevel(team, Upgrades.DAMAGE, 5);
                    t.setLevel(t.getLevel()-getNeededLevel(Upgrades.DAMAGE, 5));
                    team.playSound(Sound.BLOCK_ANVIL_USE, 10F, plugin);
                    team.sendActionBar("§a+ " + Upgrades.DAMAGE.getName() + " §8» §6Level " + getLevel(team, Upgrades.DAMAGE) + " §a+", plugin);
                    team.sendMessage("Euer " + Upgrades.DAMAGE.getName() + " §7ist nun auf §6Level " + getLevel(team, Upgrades.DAMAGE) + "§7!", plugin);
                    openUpgradeInventory(t);
                } else {
                    t.sendMessage(main.pr + "§cDu hast nicht genug Upgrade-Level!");
                }
            }
        };
    }
    
}
