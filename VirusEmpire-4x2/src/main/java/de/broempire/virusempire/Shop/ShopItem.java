/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Shop;

import de.broempire.virusempire.APIs.ItemBuilder;
import de.broempire.virusempire.Events.BuyItemEvent;
import de.broempire.virusempire.main;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author Matti
 */
public class ShopItem {
    
    private main plugin;
    
    private PayType payType;
    private int price;
    private ItemStack item;
    private ItemStack resource;

    public ShopItem(main plugin, PayType payType, int price, ItemStack item) {
        this.plugin = plugin;
        this.payType = payType;
        this.price = price;
        this.item = item;
        
        switch (payType) {
            case BRONZE:
                resource = new ItemBuilder(Material.CLAY_BRICK).setDisplayName("§cBronze").build();
                break;
            case SILBER:
                resource = new ItemBuilder(Material.IRON_INGOT).setDisplayName("§7Silber").build();
                break;
            case GOLD:
                resource = new ItemBuilder(Material.GOLD_INGOT).setDisplayName("§6Gold").build();
                break;
        }
        
        ItemMeta im = item.getItemMeta();
        List<String> lore = im.getLore() != null ? im.getLore() : new ArrayList<>();
        lore.add(" ");
        lore.add("§8» " + payType.getColor() + price + " " + payType.getName() + " §8«");
        im.setLore(lore);
        item.setItemMeta(im);
    }

    public PayType getPayType() {
        return payType;
    }

    public int getPrice() {
        return price;
    }

    public ItemStack getItem() {
        return item;
    }
    
    public void buy(Player p, boolean shift) {
        if (p.getInventory().containsAtLeast(resource, price)) {
            ItemStack stack = item.clone();
            if (shift) {
                int invResources = 0;
                for (ItemStack is : p.getInventory().getContents()) {
                    if (is != null && is.getType() != Material.AIR) {
                        if (is.getType().equals(resource.getType())) {
                            invResources+=is.getAmount();
                        }
                    }
                }
                System.out.println("InvResources: " + invResources);
                int amount = 0;
                System.out.println("MaxStackSize von " + stack.getType().toString() + ": " + stack.getMaxStackSize());
                while (invResources >= price) {
                    if ((amount+stack.getAmount()) <= stack.getMaxStackSize()) {
                        amount+=stack.getAmount();
                        invResources-=price;
                    } else {
                        break;
                    }
                }
                int toPay = (amount/stack.getAmount())*price;
                int stackPrice = toPay;
                System.out.println("Amount: " + amount + ", toPay: " + toPay);
                stack.setAmount(amount);
                for (ItemStack is : p.getInventory().getContents()) {
                    if (is != null && is.getType() != Material.AIR) {
                        if (is.getType().equals(resource.getType())) {
                            if (is.getAmount() > toPay) {
                                is.setAmount(is.getAmount() - toPay);
                                break;
                            } else if (is.getAmount() == toPay) {
                                p.getInventory().remove(is);
                                break;
                            } else {
                                p.getInventory().remove(is);
                                toPay-=is.getAmount();
                            }
                        }
                    }
                }
                p.playSound(p.getLocation(), Sound.BLOCK_COMPARATOR_CLICK, 3F, 3F);
                BuyItemEvent event = new BuyItemEvent(stack, payType, stackPrice, p, (ItemStack t) -> {
                    p.getInventory().addItem(t);
                });
                plugin.getServer().getPluginManager().callEvent(event);
            } else {
                int toPay = price;
                for (ItemStack is : p.getInventory().getContents()) {
                    if (is != null && is.getType() != Material.AIR) {
                        if (is.getType().equals(resource.getType())) {
                            if (is.getAmount() > toPay) {
                                is.setAmount(is.getAmount() - toPay);
                                break;
                            } else if (is.getAmount() == toPay) {
                                p.getInventory().remove(is);
                                break;
                            } else {
                                p.getInventory().remove(is);
                                toPay-=is.getAmount();
                            }
                        }
                    }
                }
                p.playSound(p.getLocation(), Sound.BLOCK_COMPARATOR_CLICK, 3F, 3F);
                BuyItemEvent event = new BuyItemEvent(stack, payType, price, p, (ItemStack t) -> {
                    p.getInventory().addItem(t);
                });
                plugin.getServer().getPluginManager().callEvent(event);
            }
        } else {
            p.sendMessage(main.pr + "§cDas kannst du dir nicht leisten!");
            p.playSound(p.getLocation(), Sound.ENTITY_VILLAGER_NO, 3F, 3F);
        }
    }
}
