/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Shop;

import org.bukkit.ChatColor;

/**
 *
 * @author Matti
 */
public enum PayType {
    
    BRONZE("Bronze", ChatColor.RED),
    SILBER("Silber", ChatColor.GRAY),
    GOLD("Gold", ChatColor.GOLD);
    
    private String name;
    private ChatColor color;

    private PayType(String name, ChatColor color) {
        this.name = name;
        this.color = color;
    }

    public ChatColor getColor() {
        return color;
    }

    public String getName() {
        return name;
    }
    
}
