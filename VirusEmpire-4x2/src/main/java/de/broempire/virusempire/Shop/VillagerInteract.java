package de.broempire.virusempire.Shop;

import de.broempire.virusempire.main;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class VillagerInteract implements Listener{

    private main plugin;
    
    public VillagerInteract(main plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onInteractVillager(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();
        if (e.getRightClicked().getType() == EntityType.VILLAGER) {
            e.setCancelled(true);
            if (plugin.getMethods().hasTeam(p)) {
                plugin.getShopManager().sections.get(ShopSection.BLOCKS).open(p);
            }
        }
    }
	
}
