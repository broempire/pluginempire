/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Shop;

/**
 *
 * @author Matti
 */
public enum ShopSection {
    
    BLOCKS("§7> §6Blöcke"),
    PICKAXES("§7> §5Spitzhacken"),
    FOOD("§7> §2Essen"),
    SWORDS("§7> §3Schwerter"),
    BOWS("§7> §9Bögen"),
    SPECIAL("§7> §4Spezial"),
    POTIONS("§7> §aTränke"),
    ARMOR("§7> §8Rüstung"),
    CHESTS("§7> §0Kisten");
    
    private final String name;
    
    ShopSection(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
}
