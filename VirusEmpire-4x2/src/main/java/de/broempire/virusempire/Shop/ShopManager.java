package de.broempire.virusempire.Shop;

import de.broempire.virusempire.APIs.GUIBuilder;
import de.broempire.virusempire.APIs.ItemBuilder;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

import de.broempire.virusempire.main;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Consumer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ShopManager implements Listener {

    private final main plugin;
    
    public HashMap<ShopSection, GUIBuilder> sections;
    
    private Consumer<Player> openBlocks;
    private Consumer<Player> openPickaxes;
    private Consumer<Player> openFood;
    private Consumer<Player> openSwords;
    private Consumer<Player> openBows;
    private Consumer<Player> openPotions;
    private Consumer<Player> openArmor;
    private Consumer<Player> openChests;
    private Consumer<Player> openSpecial;
    
    private HashMap<ShopSection, List<ShopItem>> shopItems;
    
    public ShopManager(main plugin) {
        this.shopItems = new HashMap<>();
        this.plugin = plugin;
        initCallbacks();
        createSections();
    }
    
    private void createSections() {
        sections = new HashMap<>();
        
        sections.put(ShopSection.BLOCKS, new GUIBuilder(plugin, 27, ShopSection.BLOCKS.getName()));
        sections.put(ShopSection.PICKAXES, new GUIBuilder(plugin, 27, ShopSection.PICKAXES.getName()));
        sections.put(ShopSection.FOOD, new GUIBuilder(plugin, 27, ShopSection.FOOD.getName()));
        sections.put(ShopSection.SWORDS, new GUIBuilder(plugin, 27, ShopSection.SWORDS.getName()));
        sections.put(ShopSection.BOWS, new GUIBuilder(plugin, 27, ShopSection.BOWS.getName()));
        sections.put(ShopSection.POTIONS, new GUIBuilder(plugin, 27, ShopSection.POTIONS.getName()));
        sections.put(ShopSection.ARMOR, new GUIBuilder(plugin, 27, ShopSection.ARMOR.getName()));
        sections.put(ShopSection.CHESTS, new GUIBuilder(plugin, 27, ShopSection.CHESTS.getName()));
        sections.put(ShopSection.SPECIAL, new GUIBuilder(plugin, 27, ShopSection.SPECIAL.getName()));
        
        for (GUIBuilder inv : sections.values()) {
            inv.addGUIItem(0, new ItemBuilder(Material.HARD_CLAY, openBlocks).setDisplayName("§7> §6Blöcke"))
                    .addGUIItem(1, new ItemBuilder(Material.IRON_PICKAXE, openPickaxes).setDisplayName("§7> §5Spitzhacken"))
                    .addGUIItem(2, new ItemBuilder(Material.COOKED_BEEF, openFood).setDisplayName("§7> §2Essen"))
                    .addGUIItem(3, new ItemBuilder(Material.IRON_SWORD, openSwords).setDisplayName("§7> §3Schwerter"))
                    .addGUIItem(4, new ItemBuilder(Material.BOW, openBows).setDisplayName("§7> §9Bögen"))
                    .addGUIItem(5, new ItemBuilder(Material.IRON_CHESTPLATE, openArmor).setDisplayName("§7> §8Rüstung"))
                    .addGUIItem(6, new ItemBuilder(Material.POTION, 1, (short) 8201, openPotions).setDisplayName("§7> §aTränke"))
                    .addGUIItem(7, new ItemBuilder(Material.ENDER_CHEST, openChests).setDisplayName("§7> §0Kisten"))
                    .addGUIItem(8, new ItemBuilder(Material.FIREWORK, openSpecial).setDisplayName("§7> §4Spezial"));
            
            for (int i = 9; i < 18; i++) {
                inv.addItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 0).setDisplayName(" ").build());
            }
        }
        
        createItems();
    }
    
    private void addShopItem(int slot, ShopSection section, ShopItem item) {
        if (shopItems.containsKey(section)) {
            shopItems.get(section).add(item);
        } else {
            List<ShopItem> list = new ArrayList<>();
            list.add(item);
            shopItems.put(section, list);
        }
        sections.get(section).addItem(slot, item.getItem());
        
        System.out.println(section.toString() + " » " + item.getItem().getItemMeta().getDisplayName());
    }
    
    private void createItems() {
        ShopItem clay = new ShopItem(plugin, PayType.BRONZE, 1, new ItemBuilder(Material.HARD_CLAY, 2).setDisplayName("§7> §3Farbiger Ton").build());
        ShopItem endstone = new ShopItem(plugin, PayType.BRONZE, 16, new ItemBuilder(Material.ENDER_STONE, 1).setDisplayName("§7> §3Enderstein").build());
        ShopItem glass = new ShopItem(plugin, PayType.BRONZE, 16, new ItemBuilder(Material.GLASS, 1).setDisplayName("§7> §3Glas").build());
        ShopItem ladder = new ShopItem(plugin, PayType.BRONZE, 2, new ItemBuilder(Material.LADDER, 1).setDisplayName("§7> §3Leiter").build());
        ShopItem glowstone = new ShopItem(plugin, PayType.BRONZE, 32, new ItemBuilder(Material.GLOWSTONE, 1).setDisplayName("§7> §3Glowstone").build());
        
        addShopItem(20, ShopSection.BLOCKS, clay);
        addShopItem(21, ShopSection.BLOCKS, endstone);
        addShopItem(22, ShopSection.BLOCKS, glass);
        addShopItem(23, ShopSection.BLOCKS, ladder);
        addShopItem(24, ShopSection.BLOCKS, glowstone);
        
        ShopItem woodpickaxe = new ShopItem(plugin, PayType.BRONZE, 8, new ItemBuilder(Material.WOOD_PICKAXE, 1).setDisplayName("§7> §3Holzspitzhacke").addEnchantment(Enchantment.DURABILITY, 3).build());
        ShopItem stonepickaxe = new ShopItem(plugin, PayType.SILBER, 2, new ItemBuilder(Material.STONE_PICKAXE, 1).setDisplayName("§7> §3Steinspitzhacke").addEnchantment(Enchantment.DURABILITY, 3).build());
        ShopItem ironpickaxe1 = new ShopItem(plugin, PayType.GOLD, 1, new ItemBuilder(Material.IRON_PICKAXE, 1).setDisplayName("§7> §3Eisenspitzhacke §b1").addEnchantment(Enchantment.DURABILITY, 3).build());
        ShopItem ironpickaxe2 = new ShopItem(plugin, PayType.GOLD, 6, new ItemBuilder(Material.IRON_PICKAXE, 1).setDisplayName("§7> §3Eisenspitzhacke §b2").addEnchantment(Enchantment.DURABILITY, 3).addEnchantment(Enchantment.DIG_SPEED, 3).build());
        
        addShopItem(20, ShopSection.PICKAXES, woodpickaxe);
        addShopItem(21, ShopSection.PICKAXES, stonepickaxe);
        addShopItem(22, ShopSection.PICKAXES, ironpickaxe1);
        addShopItem(24, ShopSection.PICKAXES, ironpickaxe2);
        
        ShopItem knockbackstick = new ShopItem(plugin, PayType.BRONZE, 12, new ItemBuilder(Material.STICK, 1).setDisplayName("§7> §3Knockbackstick").addEnchantment(Enchantment.KNOCKBACK, 1).build());
        ShopItem woodsword = new ShopItem(plugin, PayType.SILBER, 1, new ItemBuilder(Material.WOOD_SWORD, 1).setDisplayName("§7> §3Holzschwert").build());
        ShopItem stonesword = new ShopItem(plugin, PayType.SILBER, 3, new ItemBuilder(Material.STONE_SWORD, 1).setDisplayName("§7> §3Steinschwert §b1").build());
        ShopItem stonesword2 = new ShopItem(plugin, PayType.SILBER, 5, new ItemBuilder(Material.STONE_SWORD, 1).setDisplayName("§7> §3Steinschwert §b2").addEnchantment(Enchantment.DAMAGE_ALL, 2).build());
        ShopItem ironsword = new ShopItem(plugin, PayType.GOLD, 5, new ItemBuilder(Material.IRON_SWORD, 1).setDisplayName("§7> §3Eisenschwert §b1").addEnchantment(Enchantment.KNOCKBACK, 1).addEnchantment(Enchantment.DAMAGE_ALL, 1).build());
        ShopItem ironsword2 = new ShopItem(plugin, PayType.GOLD, 12, new ItemBuilder(Material.IRON_SWORD, 1).setDisplayName("§7> §3Eisenschwert §b2").addEnchantment(Enchantment.DAMAGE_ALL, 2).addEnchantment(Enchantment.FIRE_ASPECT, 1).build());
        
        addShopItem(19, ShopSection.SWORDS, knockbackstick);
        addShopItem(21, ShopSection.SWORDS, woodsword);
        addShopItem(22, ShopSection.SWORDS, stonesword);
        addShopItem(22, ShopSection.SWORDS, stonesword2);
        addShopItem(23, ShopSection.SWORDS, ironsword);
        addShopItem(24, ShopSection.SWORDS, ironsword2);
        
        ShopItem bow1 = new ShopItem(plugin, PayType.GOLD, 3, new ItemBuilder(Material.BOW, 1).setDisplayName("§7> §3Bogen §b1").addEnchantment(Enchantment.DURABILITY, 1).build());
        ShopItem bow2 = new ShopItem(plugin, PayType.GOLD, 8, new ItemBuilder(Material.BOW, 1).setDisplayName("§7> §3Bogen §b2").addEnchantment(Enchantment.DURABILITY, 1).addEnchantment(Enchantment.ARROW_DAMAGE, 1).build());
        ShopItem bow3 = new ShopItem(plugin, PayType.GOLD, 16, new ItemBuilder(Material.BOW, 1).setDisplayName("§7> §3Bogen §b3").addEnchantment(Enchantment.ARROW_KNOCKBACK, 1).addEnchantment(Enchantment.ARROW_DAMAGE, 2).addEnchantment(Enchantment.ARROW_FIRE, 1).build());
        ShopItem arrow = new ShopItem(plugin, PayType.BRONZE, 32, new ItemBuilder(Material.ARROW, 10).setDisplayName("§7> §3Pfeil").build());
        
        addShopItem(20, ShopSection.BOWS, arrow);
        addShopItem(22, ShopSection.BOWS, bow1);
        addShopItem(23, ShopSection.BOWS, bow2);
        addShopItem(24, ShopSection.BOWS, bow3);
        
        ShopItem helmet = new ShopItem(plugin, PayType.BRONZE, 5, new ItemBuilder(Material.LEATHER_HELMET, 1).setDisplayName("§7> §3Lederhelm").addEnchantment(Enchantment.DURABILITY, 3).build());
        ShopItem leggings = new ShopItem(plugin, PayType.BRONZE, 5, new ItemBuilder(Material.LEATHER_LEGGINGS, 1).setDisplayName("§7> §3Lederhose").addEnchantment(Enchantment.DURABILITY, 3).build());
        ShopItem boots = new ShopItem(plugin, PayType.BRONZE, 5, new ItemBuilder(Material.LEATHER_BOOTS, 1).setDisplayName("§7> §3Lederschuhe").addEnchantment(Enchantment.DURABILITY, 3).build());
        ShopItem chest1 = new ShopItem(plugin, PayType.SILBER, 1, new ItemBuilder(Material.CHAINMAIL_CHESTPLATE, 1).setDisplayName("§7> §3Kettenhemd §b1").addEnchantment(Enchantment.DURABILITY, 3).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());
        ShopItem chest2 = new ShopItem(plugin, PayType.SILBER, 5, new ItemBuilder(Material.CHAINMAIL_CHESTPLATE, 1).setDisplayName("§7> §3Kettenhemd §b2").addEnchantment(Enchantment.DURABILITY, 3).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3).build());
        ShopItem chest3 = new ShopItem(plugin, PayType.GOLD, 7, new ItemBuilder(Material.IRON_CHESTPLATE, 1).setDisplayName("§7> §3Eisenbrust").addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2).addEnchantment(Enchantment.PROTECTION_FIRE, 1).addEnchantment(Enchantment.PROTECTION_PROJECTILE, 2).build());
        
        addShopItem(19, ShopSection.ARMOR, helmet);
        addShopItem(20, ShopSection.ARMOR, leggings);
        addShopItem(21, ShopSection.ARMOR, boots);
        addShopItem(23, ShopSection.ARMOR, chest1);
        addShopItem(24, ShopSection.ARMOR, chest2);
        addShopItem(25, ShopSection.ARMOR, chest3);
        
        ShopItem apple = new ShopItem(plugin, PayType.BRONZE, 1, new ItemBuilder(Material.APPLE, 2).setDisplayName("§7> §3Apfel").build());
        ShopItem steak = new ShopItem(plugin, PayType.BRONZE, 2, new ItemBuilder(Material.COOKED_BEEF, 1).setDisplayName("§7> §3Fleisch").build());
        ShopItem cake = new ShopItem(plugin, PayType.SILBER, 1, new ItemBuilder(Material.CAKE, 1).setDisplayName("§7> §3Kuchen").build());
        
        addShopItem(21, ShopSection.FOOD, apple);
        addShopItem(22, ShopSection.FOOD, steak);
        addShopItem(23, ShopSection.FOOD, cake);
        
        ShopItem chest = new ShopItem(plugin, PayType.SILBER, 1, new ItemBuilder(Material.CHEST, 1).setDisplayName("§7> §3Kiste").build());
        ShopItem enderchest = new ShopItem(plugin, PayType.GOLD, 2, new ItemBuilder(Material.ENDER_CHEST, 1).setDisplayName("§7> §3Teamkiste").build());
        
        addShopItem(21, ShopSection.CHESTS, chest);
        addShopItem(23, ShopSection.CHESTS, enderchest);
        
        createPotions();
        
        for (Entry<ShopSection, List<ShopItem>> entry : shopItems.entrySet()) {
            System.out.println("Added successfully " + entry.getValue().size() + " ShopItems in '" + entry.getKey().getName() + "'!");
        }
    }
    
    private void createPotions() {
        ItemStack potion1 = new ItemStack(Material.POTION);
        PotionMeta meta1 = (PotionMeta) potion1.getItemMeta();
        meta1.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 1, 0), true);
        meta1.setDisplayName("§7> §3Heilungstrank §b1");
        potion1.setItemMeta(meta1);
        ShopItem healing1 = new ShopItem(plugin, PayType.SILBER, 3, potion1);
        addShopItem(20, ShopSection.POTIONS, healing1);
        
        ItemStack potion2 = new ItemStack(Material.POTION);
        PotionMeta meta2 = (PotionMeta) potion2.getItemMeta();
        meta2.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 1, 1), true);
        meta2.setDisplayName("§7> §3Heilungstrank §b2");
        potion2.setItemMeta(meta2);
        ShopItem healing2 = new ShopItem(plugin, PayType.SILBER, 5, potion2);
        addShopItem(21, ShopSection.POTIONS, healing2);
        
        ItemStack potion3 = new ItemStack(Material.POTION);
        PotionMeta meta3 = (PotionMeta) potion3.getItemMeta();
        meta3.addCustomEffect(new PotionEffect(PotionEffectType.SPEED, 30*20, 1), true);
        meta3.setDisplayName("§7> §3Schnelligkeitstrank");
        potion3.setItemMeta(meta3);
        ShopItem swiftness = new ShopItem(plugin, PayType.SILBER, 7, potion3);
        addShopItem(22, ShopSection.POTIONS, swiftness);
        
        ItemStack potion4 = new ItemStack(Material.POTION);
        PotionMeta meta4 = (PotionMeta) potion4.getItemMeta();
        meta4.addCustomEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 30*20, 0), true);
        meta4.setDisplayName("§7> §3Feuerrestistenztrank");
        potion4.setItemMeta(meta4);
        ShopItem fireres = new ShopItem(plugin, PayType.GOLD, 3, potion4);
        addShopItem(23, ShopSection.POTIONS, fireres);
        
        ItemStack potion5 = new ItemStack(Material.POTION);
        PotionMeta meta5 = (PotionMeta) potion5.getItemMeta();
        meta5.addCustomEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 30*20, 0), true);
        meta5.setDisplayName("§7> §3Stärketrank");
        potion5.setItemMeta(meta5);
        ShopItem strength = new ShopItem(plugin, PayType.GOLD, 8, potion5);
        addShopItem(24, ShopSection.POTIONS, strength);
    }
    
    private ShopSection getCurrentSection(Player p) {
        if (p.getOpenInventory() != null && p.getOpenInventory().getTopInventory() != null && p.getOpenInventory().getTopInventory().getName() != null) {
            for (ShopSection section : ShopSection.values()) {
                if (section.getName().equals(p.getOpenInventory().getTopInventory().getName())) {
                    return section;
                }
            }
        }
        return null;
    }
    
    @EventHandler
    public void onBuy(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (e.getCurrentItem() != null && e.getCurrentItem().getItemMeta() != null && e.getCurrentItem().getItemMeta().getDisplayName() != null) {
            if (getCurrentSection(p) != null) {
                for (ShopItem item : shopItems.get(getCurrentSection(p))) {
                    if (e.getCurrentItem().getItemMeta().getDisplayName().equals(item.getItem().getItemMeta().getDisplayName())) {
                        if (e.isShiftClick()) {
                            item.buy(p, true);
                        } else {
                            item.buy(p, false);
                        }
                        return;
                    }
                }
            }
        }
    }
        
    private void initCallbacks() {
        openBlocks = (Player t) -> {
            sections.get(ShopSection.BLOCKS).open(t);
        };
        openPickaxes = (Player t) -> {
            sections.get(ShopSection.PICKAXES).open(t);
        };
        openFood = (Player t) -> {
            sections.get(ShopSection.FOOD).open(t);
        };
        openSwords = (Player t) -> {
            sections.get(ShopSection.SWORDS).open(t);
        };
        openBows = (Player t) -> {
            sections.get(ShopSection.BOWS).open(t);
        };
        openPotions = (Player t) -> {
            sections.get(ShopSection.POTIONS).open(t);
        };
        openArmor = (Player t) -> {
            sections.get(ShopSection.ARMOR).open(t);
        };
        openChests = (Player t) -> {
            sections.get(ShopSection.CHESTS).open(t);
        };
        openSpecial = (Player t) -> {
            t.sendMessage(main.pr + "§cDie Special-Items sind leider noch nicht verfügbar!");
        };
    }
}
