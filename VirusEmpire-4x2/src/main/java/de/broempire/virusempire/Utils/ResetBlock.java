/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Utils;

import org.bukkit.Material;
import org.bukkit.block.Block;

/**
 *
 * @author Matti
 */
public class ResetBlock {
    
    private final Material resetMaterial;
    private final Block newBlock;

    public ResetBlock(Material resetMaterial, Block newBlock) {
        this.resetMaterial = resetMaterial;
        this.newBlock = newBlock;
    }
    
    public void reset() {
        System.out.println(newBlock.getType().toString() + " » " + resetMaterial.toString());
        newBlock.setType(resetMaterial);
    }
    
    public Material getResetMaterial() {
        return resetMaterial;
    }

    public Block getNewBlock() {
        return newBlock;
    }
}
