package de.broempire.virusempire.Utils;

import de.broempire.virusempire.main;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScoreboardManager {

    private HashMap<Player, Scoreboard> boards = new HashMap<>();
    
    private final main plugin;
    
    public ScoreboardManager(main plugin) {
        this.plugin = plugin;
    }
	
    public void sendScoreboardGame (Player p) {
		
        Scoreboard s = p.getScoreboard();
        
        if (s.getObjective("lobby") != null) {
            s.getObjective("lobby").unregister();
        } else if (s.getObjective("teams") != null) {
            s.getObjective("teams").unregister();
        }
		
        Objective o = s.getObjective("teams") != null ? s.getObjective("teams") : s.registerNewObjective("teams", "player");
	o.setDisplaySlot(DisplaySlot.SIDEBAR);
        o.setDisplayName("§8» §c§lVirus §8«");
		
        Team blue = s.getTeam("blue") != null ? s.getTeam("blue") : s.registerNewTeam("blue");
        blue.setPrefix("§9▩ ");
        blue.addEntry("§9Blau ");
        blue.setSuffix("§3[" + plugin.getBlue().size() + "/2]");
        
        Team red = s.getTeam("red") != null ? s.getTeam("red") : s.registerNewTeam("red");
        red.setPrefix("§c▩ ");
        red.addEntry("§cRot ");
        red.setSuffix("§3[" + plugin.getRed().size() + "/2]");
        
        Team green = s.getTeam("green") != null ? s.getTeam("green") : s.registerNewTeam("green");
        green.setPrefix("§2▩ ");
        green.addEntry("§2Grün ");
        green.setSuffix("§3[" + plugin.getGreen().size() + "/2]");
        
        Team yellow = s.getTeam("yellow") != null ? s.getTeam("yellow") : s.registerNewTeam("yellow");
        yellow.setPrefix("§e▩ ");
        yellow.addEntry("§eGelb ");
        yellow.setSuffix("§3[" + plugin.getYellow().size() + "/2]");
        
        Team team = s.getTeam("team") != null ? s.getTeam("team") : s.registerNewTeam("team");
        team.setPrefix("§8§l↳ ");
        team.addEntry(ChatColor.AQUA.toString());
        team.setSuffix(plugin.getMethods().getTeam(p));
        
        Team respawn = s.getTeam("respawn") != null ? s.getTeam("respawn") : s.registerNewTeam("respawn");
        respawn.setPrefix("§8§l↳ ");
        respawn.addEntry(ChatColor.GRAY.toString());
        respawn.setSuffix(plugin.getMethods().canRespawn(p) ? "§2Ja" : "§cNein");
        
        o.getScore("§7§m---------------").setScore(12);
        o.getScore("§9Blau ").setScore(11);
        o.getScore("§cRot ").setScore(10);
        o.getScore("§2Grün ").setScore(9);
        o.getScore("§eGelb ").setScore(8);
        o.getScore("  ").setScore(7);
        o.getScore("§6Team:").setScore(6);
        o.getScore(ChatColor.AQUA.toString()).setScore(5);
        o.getScore("   ").setScore(4);
        o.getScore("§6Respawn:").setScore(3);
        o.getScore(ChatColor.GRAY.toString()).setScore(2);
        o.getScore("§7§m--------------- ").setScore(1);
        o.getScore("§6§lBroEmpire").setScore(0);
        
	p.setScoreboard(s);
		
    }
    
    public void sendScoreboardLobby (Player p) {
		
	Scoreboard s = Bukkit.getScoreboardManager().getNewScoreboard();
		
	Objective o = s.getObjective("lobby") != null ? s.getObjective("lobby") : s.registerNewObjective("lobby", "dummy");
	o.setDisplaySlot(DisplaySlot.SIDEBAR);
	o.setDisplayName("§8» §c§lVirus §8«");
        
        Team map = s.getTeam("map") != null ? s.getTeam("map") : s.registerNewTeam("map");
        map.setPrefix("§8§l↳ ");
        map.addEntry(ChatColor.BLUE.toString());
        map.setSuffix("§bVoting");
		
	Team team = s.getTeam("team") != null ? s.getTeam("team") : s.registerNewTeam("team");
        team.setPrefix("§8§l↳ ");
        team.addEntry(ChatColor.BLACK.toString());
        team.setSuffix(plugin.getMethods().hasTeam(p) ? plugin.getMethods().getTeam(p) : "§7Keins");
        
        Team online = s.getTeam("online") != null ? s.getTeam("online") : s.registerNewTeam("online");
        online.setPrefix("§8§l↳ ");
        online.addEntry(ChatColor.YELLOW.toString());
        online.setSuffix(plugin.getServer().getOnlinePlayers().size() + "§7/§e" + plugin.getServer().getMaxPlayers() + " §7[4]");
        
        o.getScore("§7§m---------------").setScore(10);
        o.getScore("§6Online:").setScore(9);
        o.getScore(ChatColor.YELLOW.toString()).setScore(8);
        o.getScore(" ").setScore(7);
        o.getScore("§6Team:").setScore(6);
        o.getScore(ChatColor.BLACK.toString()).setScore(5);
        o.getScore("  ").setScore(4);
        o.getScore("§6Map:").setScore(3);
        o.getScore(ChatColor.BLUE.toString()).setScore(2);
        o.getScore("§7§m--------------- ").setScore(1);
        o.getScore("§6§lBroEmpire.de").setScore(0);
        
	p.setScoreboard(s);
		
    }
	
    public void updateScoreboard (Player p) {
        Scoreboard s = p.getScoreboard();
        
        if (GameManager.isState(GameManager.GAME)) {
            s.getTeam("blue").setPrefix(plugin.canRespawnBlau ? "§9▩ " : "§8✘ ");
            s.getTeam("red").setPrefix(plugin.canRespawnRot ? "§c▩ " : "§8✘ ");
            s.getTeam("green").setPrefix(plugin.canRespawnGrün ? "§2▩ " : "§8✘ ");
            s.getTeam("yellow").setPrefix(plugin.canRespawnGelb ? "§e▩ " : "§8✘ ");

            s.getTeam("blue").setSuffix("§3[" + plugin.getBlue().size() + "/2]");
            s.getTeam("red").setSuffix("§3[" + plugin.getRed().size() + "/2]");
            s.getTeam("green").setSuffix("§3[" + plugin.getGreen().size() + "/2]");
            s.getTeam("yellow").setSuffix("§3[" + plugin.getYellow().size() + "/2]");

            s.getTeam("respawn").setSuffix(plugin.getMethods().canRespawn(p) ? "§2Ja" : "§cNein");
        } else if (GameManager.isState(GameManager.LOBBY)){
            if (!plugin.getMapvotingManager().isVoting() && ("§b" + plugin.getWinnerMapFile().getName()).length() >= 16) { 
                String suffix = "§b" + plugin.getWinnerMapFile().getName().substring(0, 14);
                System.out.println(suffix + " » " + suffix.length());
                s.getTeam("map").setSuffix(suffix);
            } else {
                s.getTeam("map").setSuffix(plugin.getMapvotingManager().isVoting() ? "§bVoting" : "§b" + plugin.getWinnerMapFile().getName());
            }
            s.getTeam("team").setSuffix(plugin.getMethods().hasTeam(p) ? plugin.getMethods().getTeam(p) : "§7Keins");
            s.getTeam("online").setSuffix(plugin.getServer().getOnlinePlayers().size() + "§7/§e" + plugin.getServer().getMaxPlayers() + " §7[4]");
        }
    }
}