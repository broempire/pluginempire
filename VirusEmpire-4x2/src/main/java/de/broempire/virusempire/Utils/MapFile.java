/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author Matti
 */
public class MapFile {

    private File file;
    
    private FileConfiguration config;
    
    public MapFile(String path, String name) {
        file = new File(path, name + ".yml");
        
        if (!file.exists()) {
            try {
                FileWriter writer = new FileWriter(file);
                writer.write("MapName: " + name.split(".yml")[0]);
                writer.write("\nCreator: Unbekannt");
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        config = new YamlConfiguration();
        
        try {
            config.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
    
    public void saveFile() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void setWorld(World world) {
        config.set("World", world.getName());
        saveFile();
    }
    
    public String getWorld() {
        return config.getString("World");
    }
    
    public void setCreator(String creator) {
        config.set("Creator", creator);
        saveFile();
    }
    
    public String getCreator() {
        return config.getString("Creator");
    }
    
    public void setName(String name) {
        config.set("MapName", name);
        saveFile();
    }
    
    public String getName() {
        return config.getString("MapName");
    }
    
    public String serializeLocation(Location loc) {
        return loc.getX() + " " + loc.getY() + " " + loc.getZ() + " " + loc.getYaw() + " " + loc.getPitch();
    }
    
    public Location deserializeLocation(String input) {
        String[] parts = input.split(" ");
        World world = Bukkit.getWorld(config.getString("World"));
        return new Location(world, Double.valueOf(parts[0]), Double.valueOf(parts[1]), Double.valueOf(parts[2]), Float.valueOf(parts[3]), Float.valueOf(parts[4]));
    }
    
    public Location getSpawn(String team) {
        team = team.toUpperCase();
        Location loc; 
        for (String key : config.getConfigurationSection("SPAWN").getKeys(false)) {
            if (key.equals(team)) {
                loc = getLocation("SPAWN." + team + ".LOCATION");
                return loc;
            }
        }
        return null;
    }
    
    public void setSpawn(String team, Location loc) {
        team = team.toUpperCase();
        setLocation(loc, "SPAWN." + team + ".LOCATION");
    }
    
    public Location getLever(String team) {
        team = team.toUpperCase();
        Location loc; 
        for (String key : config.getConfigurationSection("LEVER").getKeys(false)) {
            if (key.equals(team)) {
                loc = getLocation("LEVER." + team + ".LOCATION");
                return loc;
            }
        }
        return null;
    }
    
    public List<Location> getLevers() {
        return getLocations("LEVER");
    }
    
    public void setLever(String team, Location loc) {
        team = team.toUpperCase();
        setLocation(loc, "LEVER." + team + ".LOCATION");
    }
    
    public List<Location> getBrickSpawner() {
        return getLocations("SPAWNER.BRONZE");
    }
    
    public List<Location> getIronSpawner() {
        return getLocations("SPAWNER.SILBER");
    }
    
    public List<Location> getGoldSpawner() {
        return getLocations("SPAWNER.GOLD");
    }
    
    public void setBrickSpawner(Location loc) {
        createListLocation(loc, "SPAWNER.BRONZE");
    }
    
    public void setIronSpawner(Location loc) { 
       createListLocation(loc, "SPAWNER.SILBER");
    }
    
    public void setGoldSpawner(Location loc) {
         createListLocation(loc, "SPAWNER.GOLD");
    }
    
    public void setVillager(Location loc) {
        createListLocation(loc, "VILLAGER");
    }
    
    public List<Location> getVillager() {
        return getLocations("VILLAGER");
    }
    
    public List<Location> getLocations(String path) {
        List<Location> locs = new ArrayList<>();
        if (config.getConfigurationSection(path) != null) {
            for (String set : config.getConfigurationSection(path).getKeys(false)) {
                if (!set.equals("AMOUNT")) {
                    locs.add(getLocation(path + "." + set + ".LOCATION"));
                }
            }
        }
        return locs;
    }
    
    public Location getLocation (String path) {
        String[] parts = config.getString(path).split(" ");
        String w = config.getString("World");
        World world = Bukkit.getWorld(w);
        int x = (int) Double.parseDouble(parts[0]);
        int y = (int) Double.parseDouble(parts[1]);
        int z = (int) Double.parseDouble(parts[2]);
        float yaw = (float) Double.parseDouble(parts[3]);
        float pitch = (float) Double.parseDouble(parts[4]);
        return new Location(world, x, y, z, yaw, pitch);
    }
    
    public void setLocation (Location loc, String path) {
        config.set(path, serializeLocation(loc));
        saveFile();
    }
    
    public int createListLocation(Location location, String path) {
        int next = config.contains(path + ".AMOUNT") ? config.getInt(path + ".AMOUNT") + 1 : 1;
        config.set(path + ".AMOUNT", next);
        config.set(path + "." + next + ".LOCATION", serializeLocation(location));
        saveFile();
        return next;
    }

    public FileConfiguration getConfig() {
        return config;
    }
    
}
