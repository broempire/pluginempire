package de.broempire.virusempire.Utils;

import de.broempire.virusempire.main;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.block.Block;

public class Mapreseter {
	
    public List<ResetBlock> blocks = new ArrayList<>();

    private final main plugin;
    
    public Mapreseter(main plugin) {
        this.plugin = plugin;
    }
    
    public void resetMap() {
        System.out.println(blocks.size());
        try {
            Iterator<ResetBlock> locationlist = blocks.iterator();
            while (locationlist.hasNext()) {
                ResetBlock resetBlock = locationlist.next();
                resetBlock.reset();
            }
            blocks.clear();
        } catch (Exception e) {
            e.printStackTrace();
            plugin.getServer().broadcastMessage("§4§lFehler beim Mapreseten!");
        }

    }
    
    public void addBlock(ResetBlock rb) {
        blocks.add(rb);
    }
    
    public boolean containsBlock(Block block) {
        System.out.println(block.getX() + " " + block.getY() + " " + block.getZ() + " » " + block.getType().toString());
        for (ResetBlock resetBlock : blocks) {
            if (resetBlock.getNewBlock().getX() == block.getX() && resetBlock.getNewBlock().getY() == block.getY() && resetBlock.getNewBlock().getZ() == block.getZ()) {
                return true;
            }
        }
        return false;
    }
}
