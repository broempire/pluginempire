package de.broempire.virusempire.Utils;

import de.broempire.virusempire.APIs.ActionAPI;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

import de.broempire.virusempire.main;
import java.util.List;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.material.Lever;
import org.bukkit.scheduler.BukkitRunnable;

public class Methods {

    private main plugin;
    
    public Methods(main plugin) {
        this.plugin = plugin;
    }
    private boolean b = false; 

    int i = 21;

    public void setEnd(Color color) {
        if (b == false) {
            Firework firework = plugin.getLobbyLocation("LOBBY").getWorld().spawn(plugin.getLobbyLocation("LOBBY"), Firework.class);
            FireworkEffect fireworkEffect = FireworkEffect.builder()
                    .withColor(color)
                    .withFade(color)
                    .flicker(true)
                    .trail(true)
                    .with(FireworkEffect.Type.STAR)
                    .build();
            FireworkMeta meta = firework.getFireworkMeta();
            meta.addEffect(fireworkEffect);
            meta.setPower(1);
            firework.setFireworkMeta(meta);
            b = true;
            new BukkitRunnable() {
                @Override
                public void run() {
                    i--;
                    
                    switch(i) {
                        case 1:
                            for (Player p : plugin.getServer().getOnlinePlayers()) {
                                ActionAPI.sendActionBar(p, "§7Der Server startet in §6" + i + "§7 Sekunde neu!");
                            }
                            break;
                        case 0:
                            Bukkit.broadcastMessage(main.pr + "§cDer Server startet jetzt neu");
                            for (Player p : plugin.getServer().getOnlinePlayers()) {
                                ByteArrayOutputStream b = new ByteArrayOutputStream();
                                DataOutputStream out = new DataOutputStream(b);
                                try {
                                        out.writeUTF("Connect");
                                        out.writeUTF("lobby01");
                                } catch (IOException e) {
                                        e.printStackTrace();
                                }
                                p.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
                            }
                            plugin.getServer().shutdown();
                            break;
                        default:
                            for (Player p : plugin.getServer().getOnlinePlayers()) {
                                ActionAPI.sendActionBar(p, "§7Der Server startet in §6" + i + "§7 Sekunden neu!");
                            }
                            break;
                    }
                }
            }.runTaskTimer(plugin, 0L, 20L);
        }
    }

    public String getTeam (Player p) {

        String s;

        if (plugin.blau.contains(p.getName())) {
            s = "§9Blau";
        }
        else if (plugin.rot.contains(p.getName())) {
            s = "§cRot";
        }
        else if (plugin.grün.contains(p.getName())) {
            s = "§2Grün";
        }
        else if (plugin.gelb.contains(p.getName())) {
            s = "§eGelb";
        } else {
            s = "§cKein Team";
        }
        return s;

    }
    public List<String> getTeamlist(Player p) {
        switch(getExactTeam(p)) {
            case "Blau":
                return plugin.blau;
            case "Rot":
                return plugin.rot;
            case "Grün":
                return plugin.grün;
            case "Gelb":
                return plugin.gelb;
            default:
                return null;
        }
    }

    public String getExactTeam (Player p) {

            String s;

            if (plugin.blau.contains(p.getName())) {
                    s = "Blau";
            }
            else if (plugin.rot.contains(p.getName())) {
                    s = "Rot";
            }
            else if (plugin.grün.contains(p.getName())) {
                    s = "Grün";
            }
            else if (plugin.gelb.contains(p.getName())) {
                    s = "Gelb";
            } else {
                    s = "Kein Team";
            }
            return s;

    }
    public String getColor (Player p) {

            String s;

            if (plugin.blau.contains(p.getName())) {
                    s = "§9";
            }
            else if (plugin.rot.contains(p.getName())) {
                    s = "§c";
            }
            else if (plugin.grün.contains(p.getName())) {
                    s = "§2";
            }
            else if (plugin.gelb.contains(p.getName())) {
                    s = "§e";
            } else {
                    s = "§c";
            }
            return s;

    }

    public void clearFromArray(Player p) {
        plugin.blau.remove(p.getName());
        plugin.rot.remove(p.getName());
        plugin.gelb.remove(p.getName());
        plugin.grün.remove(p.getName());
        plugin.spectator.remove(p.getName());
    }
    public boolean hasTeam (Player p) {

        boolean b;

        if (plugin.blau.contains(p.getName())) {
            b = true;
        }
        else if (plugin.rot.contains(p.getName())) {
            b = true;
        }
        else if (plugin.grün.contains(p.getName())) {
            b = true;
        }
        else if (plugin.gelb.contains(p.getName())) {
            b = true;
        } else {
            b = false;
        }
        return b;

    }

    public boolean canRespawn(Player p) {
        switch(getExactTeam(p)) {
            case "Blau":
                return plugin.canRespawnBlau;
            case "Rot":
                return plugin.canRespawnRot;
            case "Grün":
                return plugin.canRespawnGrün;
            case "Gelb":
                return plugin.canRespawnGelb;
            default:
                return false;
        }
    }
    public void setupGame () {
        MapFile map = plugin.getWinnerMapFile();
        World w = plugin.getServer().getWorld(map.getWorld());
        for (Location loc : map.getVillager()) {
            Villager v = (Villager) w.spawnEntity(loc, EntityType.VILLAGER);
            v.setCustomName("§3§lShop");
            v.setCustomNameVisible(true);
            v.setFireTicks(0);
            LivingEntity le = (LivingEntity) v;
            le.setAI(false);
        }
        for (Location loc : map.getLevers()) {
            BlockFace face = loc.getBlock().getFace(w.getBlockAt(w.getSpawnLocation().getBlockX(), loc.getBlockY(), w.getSpawnLocation().getBlockZ()));
            System.out.println(w.getBlockAt(w.getSpawnLocation().getBlockX(), loc.getBlockY(), w.getSpawnLocation().getBlockZ()).getType().toString());
            System.out.println(face.toString());
            Lever lever = new Lever(Material.LEVER);
            lever.setPowered(false);
            if (face.equals(BlockFace.NORTH) || face.equals(BlockFace.NORTH_EAST) || face.equals(BlockFace.NORTH_WEST) || face.equals(BlockFace.NORTH_NORTH_EAST)  || face.equals(BlockFace.NORTH_NORTH_WEST)) {
                lever.setFacingDirection(BlockFace.NORTH);
            } else if (face.equals(BlockFace.EAST) || face.equals(BlockFace.EAST_NORTH_EAST) || face.equals(BlockFace.EAST_SOUTH_EAST)) {
                lever.setFacingDirection(BlockFace.EAST);
            } else if (face.equals(BlockFace.SOUTH) || face.equals(BlockFace.SOUTH_EAST) || face.equals(BlockFace.SOUTH_WEST) || face.equals(BlockFace.SOUTH_SOUTH_WEST)|| face.equals(BlockFace.SOUTH_SOUTH_EAST)){
                lever.setFacingDirection(BlockFace.SOUTH);
            } else {
                lever.setFacingDirection(BlockFace.WEST);
            }
            System.out.println("AttachedFace: " + lever.getAttachedFace().toString() + ", Facing: " + lever.getFacing().toString());
            loc.getBlock().setType(Material.LEVER);
            BlockState state = loc.getBlock().getState();
            state.setData(lever);
            state.update();
        }
    }

}
