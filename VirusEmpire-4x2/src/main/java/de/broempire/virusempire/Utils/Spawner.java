package de.broempire.virusempire.Utils;

import de.broempire.virusempire.APIs.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import de.broempire.virusempire.main;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

public class Spawner{

    private main plugin;
    
    private MapFile file;
    
    public Spawner(main plugin) {
        this.plugin = plugin;
    }
        
        public void addSpawner(String map, String type, Location loc) {
            file = new MapFile(plugin.getDataFolder().getPath(), map);
            switch (type.toUpperCase()) {
                case "BRONZE":
                    file.setBrickSpawner(loc);
                    break;
                case "SILBER":
                    file.setIronSpawner(loc);
                    break;
                case "GOLD":
                    file.setGoldSpawner(loc);
                    break;
                default:
                    break;
            }
        }
	
	public void startSpawning(String map) {
            file = new MapFile(plugin.getDataFolder().getPath(), map);
            new BukkitRunnable() {
                @Override
                public void run() {
                     spawnBricks(Bukkit.getWorld(file.getWorld()));
                }
            }.runTaskTimer(plugin, 20L, 20L);
            
            new BukkitRunnable() {
                @Override
                public void run() {
                     spawnSilver(Bukkit.getWorld(file.getWorld()));
                }
            }.runTaskTimer(plugin, 20*15L, 20*15L);
            
            new BukkitRunnable() {
                @Override
                public void run() {
                     spawnGold(Bukkit.getWorld(file.getWorld()));
                }
            }.runTaskTimer(plugin, 20*30L, 20*30L);
	}
        
        public void spawnGold(World world) {
            for (Location loc : file.getGoldSpawner()) {
                loc.getWorld().dropItemNaturally(loc, new ItemBuilder(Material.GOLD_INGOT).setDisplayName("§6Gold").build());
            }
        }
        public void spawnBricks(World world) {
            for (Location loc : file.getBrickSpawner()) {
                loc.getWorld().dropItemNaturally(loc, new ItemBuilder(Material.CLAY_BRICK).setDisplayName("§cBronze").build());
            }
        }
        public void spawnSilver(World world) {
            for (Location loc : file.getIronSpawner()) {
                loc.getWorld().dropItemNaturally(loc, new ItemBuilder(Material.IRON_INGOT).setDisplayName("§7Silber").build());
            }
            
        }

}
