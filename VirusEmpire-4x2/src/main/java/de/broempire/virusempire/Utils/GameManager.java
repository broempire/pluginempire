package de.broempire.virusempire.Utils;

public enum GameManager {
	
    LOBBY,
    GAME,
    Restart;

    private static GameManager currentstate;

    public static void setState (GameManager state) {

        currentstate = state;

    }
    public static boolean isState (GameManager state) {

        return currentstate == state;

    }
    public static GameManager getState () {

        return currentstate;

    }
    public static String getStatus() {

        String s = "";
        if (isState(GameManager.LOBBY)) {
                s = "§7«§aLobby§7»";
        } else if (isState(GameManager.GAME)) {
                s = "§7«§cIngame§7»";

        } else if (isState(GameManager.Restart)) {
                s = "§7«§bRestarting§7»";
        }
        return s;

    }

}
