package de.broempire.virusempire.Utils;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Teleporter {

    public static Player getNearest(Player p) {
        double distance = Double.MAX_VALUE;
        Player target = null;

        for (Entity entity : p.getNearbyEntities(300, 300, 300)) {
            if (entity instanceof Player) {
                double dis = p.getLocation().distance(entity.getLocation());
                if (dis < distance) {
                    distance = dis;
                    target = (Player) entity;
                }
            }
        }
        return target;
    }
	
}
