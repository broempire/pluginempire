/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Commands;

import static de.broempire.virusempire.Utils.Buildmode.isBuildmode;
import static de.broempire.virusempire.Utils.Buildmode.isbuildmodeoff;
import static de.broempire.virusempire.Utils.Buildmode.isbuildmodeon;
import static de.broempire.virusempire.Utils.Buildmode.notonline;
import static de.broempire.virusempire.Utils.Buildmode.setBuildmodeFalse;
import static de.broempire.virusempire.Utils.Buildmode.setBuildmodeTrue;
import de.broempire.virusempire.main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Fabian
 */
public class BuildmodeCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            if(args.length == 1){
                Player target = Bukkit.getPlayer(args[0]);
                if (target != null) {
                    if (isBuildmode(target) == true) {
                        setBuildmodeFalse(target);
                        String msg = isbuildmodeoff.replace("[Player]", target.getDisplayName());
                        sender.sendMessage(main.pr + msg);
                    } else {
                        setBuildmodeTrue(target);
                        String msg = isbuildmodeon.replace("[Player]", target.getDisplayName());
                        sender.sendMessage(main.pr + msg);
                    }
                } else {
                    sender.sendMessage(main.pr + notonline);
                }
            }
        } else if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("BroEmpire.build")) {
                switch (args.length) {
                    case 0:
                        if (isBuildmode(p)) {
                            setBuildmodeFalse(p);
                            p.sendMessage(main.pr + "Du bist nun nicht mehr im §cBuildmode§7!");
                        } else {
                            setBuildmodeTrue(p);
                            p.sendMessage(main.pr + "Du bist nun im §aBuildmode§7!");
                        }
                        break;
                    case 1:
                        Player target = Bukkit.getPlayer(args[0]);
                        if (target != null) {
                            if (isBuildmode(target) == true) {
                                setBuildmodeFalse(target);
                                String msg = isbuildmodeoff.replace("[Player]", target.getDisplayName());
                                sender.sendMessage(main.pr + msg);
                                msg = isbuildmodeoff.replace("Der Spieler [Player] ist", "Du bist");
                                target.sendMessage(main.pr + ((Player) sender).getDisplayName() + " §8» " + msg);
                                //Target soll vor der Message der Name des Senders angezeigt werden (brauche dafür die richtige Formatierung)
                                //Fabi
                            } else {
                                setBuildmodeTrue(target);
                                String msg = isbuildmodeon.replace("[Player]", target.getDisplayName());
                                sender.sendMessage(main.pr + msg);
                                msg = isbuildmodeon.replace("Der Spieler [Player] ist", "Du bist");
                                target.sendMessage(main.pr + ((Player) sender).getDisplayName() + " §8» "  + msg);
                                //same here
                            }
                        } else {
                            sender.sendMessage(main.pr + notonline);
                        }
                        break;
                    case 2:
                        if(args[1].equalsIgnoreCase("on") || args[1].equalsIgnoreCase("off") || args[1].equalsIgnoreCase("get")) {
                            target = Bukkit.getPlayer(args[0]);
                            if(target != null) {
                                switch (args[1]) {
                                    case "on":
                                        setBuildmodeTrue(target);
                                        String msg = isbuildmodeon.replace("[Player]", target.getDisplayName());
                                        sender.sendMessage(main.pr + msg);
                                        msg = isbuildmodeon.replace("Der Spieler [Player] ist", "Du bist");
                                        target.sendMessage(main.pr + ((Player) sender).getDisplayName() + " §8» "  + msg);
                                        //same here
                                        break;
                                    case "off":
                                        setBuildmodeFalse(target);
                                        msg = isbuildmodeoff.replace("[Player]", target.getDisplayName());
                                        sender.sendMessage(main.pr + msg);
                                        msg = isbuildmodeoff.replace("Der Spieler [Player] ist", "Du bist");
                                        target.sendMessage(main.pr + ((Player) sender).getDisplayName() + " §8» "  + msg);
                                        //same here
                                        break;
                                    case "get":
                                        if(isBuildmode(target)==true) {
                                            msg = isbuildmodeon.replace("ist nun", "befindet sich").replace("[Player]", target.getDisplayName());
                                            sender.sendMessage(main.pr + msg);
                                        } else {
                                            msg = isbuildmodeoff.replace("ist nun", "befindet sich").replace("mehr ","").replace("[Player]", target.getDisplayName());
                                            sender.sendMessage(main.pr + msg);
                                        }
                                        break;
                                }
                            } else {
                                sender.sendMessage(main.pr + notonline);
                            }
                        } else {
                            main.sendUsage("/buildmode [Spieler] [on|off|get]", "/buildmode Phottor on", p);
                        }
                        break;
                    default:
                        main.sendUsage("/buildmode [Spieler] [on|off|get]", "/buildmode Phottor", p);
                        break;
                    }
            } else {
                sender.sendMessage(main.noperm);
            }
        }
        return true;
    }

    
}
