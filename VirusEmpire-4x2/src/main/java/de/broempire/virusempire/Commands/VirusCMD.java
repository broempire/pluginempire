package de.broempire.virusempire.Commands;

import de.broempire.virusempire.Utils.MapFile;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.broempire.virusempire.main;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;

public class VirusCMD implements CommandExecutor{

    main plugin;

    public VirusCMD(main plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p= (Player) sender;
        if (p.hasPermission("Virus.setup")) {
            switch (args.length) {
                case 0:
                    sendHelp(p);
                    break;
                case 1:
                    switch (args[0]) {
                        case "setup":
                            plugin.changeSetupMode(p);
                            break;
                        case "ready":
                            for (MapFile file : plugin.getMaps()) {
                                if (plugin.isReady(file)) {
                                    p.sendMessage(main.pr + file.getName() + " §3--- §2FERTIG");
                                } else {
                                    p.sendMessage(main.pr + file.getName() + " §3--- §cNICHT FERTIG");
                                }
                            }
                            break;
                        default:
                            sendHelp(p);
                            break;
                    }
                    break;
                case 2:
                    switch (args[0]) {
                        case "register":
                            plugin.getServer().createWorld(new WorldCreator(args[1]));
                            p.sendMessage(main.pr + "Welt wurde registriert!");
                            break;
                        case "tp":
                            for (MapFile file : plugin.getMaps()) {
                                plugin.getServer().createWorld(new WorldCreator(file.getWorld()));
                                System.out.println("[Virus] " + file.getWorld() + " geladen!");
                            }
                            
                            if (Bukkit.getWorld(args[1]) != null) {
                                p.teleport(Bukkit.getWorld(args[1]).getSpawnLocation());
                            } else {
                                p.sendMessage(main.pr + "§cWelt existiert nicht!");
                            }
                            break;
                        case "ready":
                            for (MapFile file : plugin.getMaps()) {
                                plugin.getServer().createWorld(new WorldCreator(file.getWorld()));
                                System.out.println("[Virus] " + file.getWorld() + " geladen!");
                            }
                            
                            if (plugin.getServer().getWorld(args[1]) != null) {
                                p.sendMessage(main.pr + "Die Welt " + args[1] + " ist " + (plugin.isReady(new MapFile(plugin.getDataFolder().getPath(), plugin.getNameByWorld(args[1]))) ? "fertig eingerichtet!" : "noch nicht fertig eingerichtet!"));
                            } else {
                                p.sendMessage(main.pr + "§cWelt existiert nicht!");
                            }
                            break;
                        default:
                            sendHelp(p);
                            break;
                    }
                    break;
                case 3:
                    if (args[0].equalsIgnoreCase("set")) {
                        switch (args[1].toUpperCase()) {
                            case "SPAWNER":
                                if (args[2].equalsIgnoreCase("BRONZE") || args[2].equalsIgnoreCase("SILBER") || args[2].equalsIgnoreCase("GOLD")) {
                                    if (plugin.getNameByWorld(p.getLocation().getWorld().getName()) == null) {
                                        p.sendMessage(main.pr + "§cDu musst erst einmal die Welt registrieren!");
                                    } else {
                                        plugin.getSpawner().addSpawner(plugin.getNameByWorld(p.getLocation().getWorld().getName()), args[2], p.getLocation());
                                        p.sendMessage(main.pr + "Du hast einen Spawner mit dem Typen " + args[2] + " gesetzt!");
                                    }
                                }
                                break;
                            case "SPAWN":
                                if (plugin.getNameByWorld(p.getLocation().getWorld().getName()) == null) {
                                    p.sendMessage(main.pr + "§cDu musst erst einmal die Welt registrieren!");
                                } else {
                                    if (args[2].toUpperCase().equalsIgnoreCase("BLAU") || args[2].toUpperCase().equalsIgnoreCase("ROT") || args[2].toUpperCase().equalsIgnoreCase("GRÜN") || args[2].toUpperCase().equalsIgnoreCase("GELB")) {
                                        MapFile file = new MapFile(plugin.getDataFolder().getPath(), plugin.getNameByWorld(p.getLocation().getWorld().getName()));
                                        file.setSpawn(args[2].toUpperCase(), p.getLocation());
                                        if (file.getSpawn(args[2]) == null) {
                                            p.sendMessage(main.pr + "Du hast den Spawn von Team " + args[2].toUpperCase() + " auf der Map " + file.getName() + " gesetzt!");
                                        } else { 
                                            p.sendMessage(main.pr + "Du hast die Position des Spawns von Team " + args[2].toUpperCase() + " geändert!");
                                        }      
                                    }
                                }
                                break;
                            default:
                                sendHelp(p);
                                break;
                        }
                    }
                    break;
                default:
                    if (args.length >= 4) {
                        if (args[0].equalsIgnoreCase("add")) {
                            if (plugin.getServer().getWorld(args[2]) != null) {
                                StringBuilder sb = new StringBuilder();
                                for (int i = 3; i < args.length; i++) {
                                    sb.append(args[i]).append(" ");
                                }
                                plugin.addMap(args[2], args[1], sb.toString());
                                p.sendMessage(main.pr + "Welt wurde gespeichert!");
                            } else {
                                sendHelp(p);
                            }
                        } else {
                            sendHelp(p);
                        }
                    } else {
                        sendHelp(p);
                    }
                    break;
            }
        } else {
            p.sendMessage(main.noperm);
        }
        return true;
    }

    public void sendHelp(Player p){
            p.sendMessage(main.pr + "von Schmidtchen:");
            p.sendMessage("§7- /virus setup");
            p.sendMessage("§7- /virus ready");
            p.sendMessage("§7- /virus register <Weltname>");
            p.sendMessage("§7- /virus ready <Weltname>");
            p.sendMessage("§7- /virus add <Mapname> <Weltname> <Bauteam>");
            p.sendMessage("§7- /virus tp <Weltname>");
            p.sendMessage("§7- /virus set <spawner/spawn> <Typ/Team>");
    }

}
