/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Commands;

import de.broempire.virusempire.main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class StartCommand implements CommandExecutor {
    
    private main plugin;

    public StartCommand(main plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player)sender;
        if(p.hasPermission("Virus.start")) {
            if(args.length == 0) {
                if(Bukkit.getOnlinePlayers().size() >= 2) {
                    if(plugin.time > 11) {
                        plugin.getServer().broadcastMessage(main.pr + "§6Die Wartezeit wurde verkürzt!");
                        plugin.time = 11;
                    }
                } else {
                    p.sendMessage(main.pr + "§cEs sind zu wenig Spieler online!");
                }
            }else{
                main.sendUsage("/start", null, p);
            }
        }else{
            p.sendMessage(main.noperm);
        }
        return true;
    }
    
}
