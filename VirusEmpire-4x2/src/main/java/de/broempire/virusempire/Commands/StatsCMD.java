/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.Commands;

import de.broempire.virusempire.main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Matti
 */
public class StatsCMD implements CommandExecutor {

    private main plugin;

    public StatsCMD(main plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            switch (args.length) {
                case 0:
                    
                    double kd;
                    if (plugin.getMySQLStats().getKills(p.getUniqueId().toString()) != 0 && plugin.getMySQLStats().getDeaths(p.getUniqueId().toString()) != 0) {
                        kd = plugin.getMySQLStats().getKills(p.getUniqueId().toString())/plugin.getMySQLStats().getDeaths(p.getUniqueId().toString());
                        kd = kd*10;
                        Math.round(kd);
                        kd = kd/10;
                    } else if (plugin.getMySQLStats().getKills(p.getUniqueId().toString()) != 0 && plugin.getMySQLStats().getDeaths(p.getUniqueId().toString()) == 0){
                        kd = plugin.getMySQLStats().getKills(p.getUniqueId().toString());
                    } else {
                        kd = 0.00;
                    }
                    
                    p.sendMessage(main.pr + "§8§m-    §r §6§lSTATS §8§m-----");
                    p.sendMessage("§7↠ §9gespielte Spiele: §c" + plugin.getMySQLStats().getPlayed(p.getUniqueId().toString()));
                    p.sendMessage("§7↠ §9gewonnene Spiele: §c" + plugin.getMySQLStats().getWins(p.getUniqueId().toString()));
                    p.sendMessage("§7↠ §9deaktivierte Energiekerne: §c" + plugin.getMySQLStats().getDisabled(p.getUniqueId().toString()));
                    p.sendMessage("§7↠ §9Kills: §c" + plugin.getMySQLStats().getKills(p.getUniqueId().toString()));
                    p.sendMessage("§7↠ §9Tode: §c" + plugin.getMySQLStats().getDeaths(p.getUniqueId().toString()));
                    p.sendMessage("§7↠ §9Punkte: §c" + plugin.getMySQLStats().getPoints(p.getUniqueId().toString()));
                    p.sendMessage("§7↠ §9K/D: §c" + kd);
                    p.sendMessage(main.pr + "§8§m-    §r §6§lSTATS §8§m-----");
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }
        return true;
    }
    
}
