/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.virusempire.APIs;

import java.util.ArrayList;
import java.util.function.Consumer;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author Matti
 */
public class ItemBuilder {
    
    private ItemStack itemStack;
    private Consumer<Player> callback;

    public ItemBuilder(Material material) {
        itemStack = new ItemStack(material);
    }

    public ItemBuilder(Material material, int amount) {
        this.itemStack = new ItemStack(material, amount);
    }

    public ItemBuilder(Material material, int amount, short data) {
        this.itemStack = new ItemStack(material, amount, data);
    }
    public ItemBuilder(Material material, Consumer<Player> callback) {
        this.itemStack = new ItemStack(material);
        this.callback = callback;
    }
    public ItemBuilder(Material material, int amount, Consumer<Player> callback) {
        this.itemStack = new ItemStack(material, amount);
        this.callback = callback;
    }
    public ItemBuilder(Material material, int amount, short data, Consumer<Player> callback) {
        this.itemStack = new ItemStack(material, amount, data);
        this.callback = callback;
    }
    
    
    public ItemMeta getItemMeta() {
        return itemStack.getItemMeta();
    }
    
    public ItemBuilder setDisplayName(String diplayName) {
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(diplayName);
        itemStack.setItemMeta(meta);
        
        return this;
    }
    public ItemBuilder setLore(String ... lore) {
        ArrayList<String> alore= new ArrayList<>();
        for (String line : lore) {
            alore.add(line);
        }
        ItemMeta meta = itemStack.getItemMeta();
        meta.setLore(alore);
        itemStack.setItemMeta(meta);
        
        return this;
    }
    public ItemBuilder setLore(ArrayList<String> lore) {
        ItemMeta meta = itemStack.getItemMeta();
        meta.setLore(lore);
        itemStack.setItemMeta(meta);
        return this;
    }
    public ItemBuilder addEnchantment(Enchantment enchantment, int level) {
        ItemMeta meta = itemStack.getItemMeta();
        meta.addEnchant(enchantment, level, true);
        itemStack.setItemMeta(meta);
        
        return this;
    }
    public ItemBuilder setAmount(int amount) {
        itemStack.setAmount(amount);
        return this;
    }
    public ItemBuilder setData(short data) {
        ItemStack is = new ItemStack(itemStack.getType(), itemStack.getAmount(), data);
        is.setItemMeta(itemStack.getItemMeta());
        is.setDurability(itemStack.getDurability());
        itemStack = is;
        return this;
    }
    public void click(Player player) {
        if (callback != null)
            callback.accept(player);
        }
    
    public ItemStack build() {
        return itemStack;
    }
    
    
    
}
