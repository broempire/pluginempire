/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempir;

import de.broempire.buildempire.Commands.BuildCommand;
import de.broempire.buildempire.Commands.BuildModeCommand;
import de.broempire.buildempire.Commands.SetLobbyCommand;
import de.broempire.buildempire.Commands.SpawnCommand;
import de.broempire.buildempire.Commands.ViewCommand;
import de.broempire.buildempire.Listener.ChatListener;
import de.broempire.buildempire.Listener.Listeners;
import de.broempire.buildempire.Listener.PlayerListener;
import de.broempire.buildempire.Listener.WorldListener;
import de.broempire.buildempire.Manager.GroupManager;
import de.broempire.buildempire.Manager.Map;
import de.broempire.buildempire.Manager.MenüManager;
import de.broempire.buildempire.Manager.ScoreboardManager;
import de.broempire.buildempire.Manager.ScoreboardTeams;
import de.broempire.buildempire.MySQL.Bro;
import de.broempire.buildempire.MySQL.MySQL;
import de.broempire.buildempire.Utils.Build;
import de.broempire.buildempire.Utils.BuildMode;
import de.broempire.buildempire.Utils.Methods;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scoreboard.Score;

/**
 *
 * @author Matti
 */
public class BuildEmpire extends JavaPlugin {
    
    public static String prefix = "§8▎ §6Build§8 ❘ §7";
    public static String noPerms = "§8▎ §6BroEmpire§8 ❘ §cDu besitzt nicht die nötigen Rechte für diesen Befehl!";
    
    public static String teamplayerprefix;
    public static String teampremiumprefix;
    public static String teamvipprefix;
    public static String teambuilderprefix;
    public static String teamleadbuilderprefix;
    public static String teamjrmoderatorprefix;
    public static String teammoderatorprefix;
    public static String teamleadmoderatorprefix;
    public static String teamforenmodprefix;
    public static String teamleadforenmodprefix;
    public static String teamdeveloperprefix;
    public static String teamleaddeveloperprefix;
    public static String teamadminprefix;
    
    private Methods methods;
    private BuildMode buildmode;
    private Build build;
    private MySQL mySQL;
    private Bro bro;
    private GroupManager groupmanager;
    private ScoreboardManager scoreboardmanager;
    private ScoreboardTeams scoreboardteams;
    
    static BuildEmpire instance;
    
    @Override
    public void onEnable() {
        init();
        ScoreboardTeams.scoreboardteamsinit();
        
        System.out.println("[BuildEmpire] Plugin geladen!");
    }
    
    private void init() {
        BuildEmpire.instance = this;
        this.methods = new Methods(this);
        this.scoreboardmanager = new ScoreboardManager(this);
        this.scoreboardteams = new ScoreboardTeams(this);
        this.buildmode = new BuildMode();
        this.build = new Build();
        
        registerEvents();
        registerCommands();
        
        connectToMySQL();
                
        loadConfig();
        loadConfigStrings();
        
        for (World w : Bukkit.getWorlds()) {
            w.setDifficulty(Difficulty.PEACEFUL);
            w.setStorm(false);
            w.setThundering(false);
        }
        
        /*
        
        Beispiel für die Verwendung von der Klasse: Jemand will eine neue Map erstellen:
        if (!Map.exists(name, plugin)) {
            Map map = new Map(name, Generator.EMPTY, player.getUniqueID().toString(), plugin);
            player.sendMessage("Map wurde erstellt!" + map.getGenerator(), ...);
        
        Du willst in eine Map springen:
        Map map = Map.getMapByName(name, plugin);
        player.teleport(map.getSpawn());
        */
    }
    
    private void registerEvents() {
        PluginManager pm = getServer().getPluginManager();
        
        pm.registerEvents(new PlayerListener(this), this);
        pm.registerEvents(new Listeners(), this);
        pm.registerEvents(new WorldListener(this), this);
        pm.registerEvents(new MenüManager(this), this);
        pm.registerEvents(new ChatListener(this), this);
    }
    
    private void registerCommands() {
        
        this.getCommand("setlobby").setExecutor(new SetLobbyCommand(this));
        this.getCommand("buildmode").setExecutor(new BuildModeCommand(this));
        this.getCommand("world").setExecutor(new BuildCommand(this));
        this.getCommand("view").setExecutor(new ViewCommand(this));
        this.getCommand("spawn").setExecutor(new SpawnCommand(this));
    }
    
    @Override
    public void onDisable() {
        System.out.println("[BuildEmpire] Plugin deaktiviert!");
    }
    
    public void loadConfig() {
        
	FileConfiguration cfg = this.getConfig();
		
        cfg.options().copyDefaults(true);
                
	cfg.addDefault("MySQL.Hostname", "Hostname");
	cfg.addDefault("MySQL.Username", "Username");
	cfg.addDefault("MySQL.Database", "Database");
        cfg.addDefault("MySQL.Password", "Password");
                
        cfg.addDefault("Prefix.Player", "§eSpieler §7| ");
        cfg.addDefault("Prefix.Premium", "§6Premium §7| ");
        cfg.addDefault("Prefix.VIP", "§dVIP §7| ");
        cfg.addDefault("Prefix.LeadBuilder", "§9LeadBuild §7| ");
        cfg.addDefault("Prefix.Builder", "§9Build §7| ");
        cfg.addDefault("Prefix.LeadModerator", "§cLeadMod §7| ");
        cfg.addDefault("Prefix.Moderator", "§cMod §7| ");
        cfg.addDefault("Prefix.JrModerator", "§cJrMod §7| ");
        cfg.addDefault("Prefix.LeadForenmod", "§2LeadF-Mod §7| ");
        cfg.addDefault("Prefix.Forenmod", "§2F-Mod §7| ");
        cfg.addDefault("Prefix.LeadDeveloper", "§bLeadDev §7| ");
        cfg.addDefault("Prefix.Developer", "§bDev §7| ");
        cfg.addDefault("Prefix.Admin", "§4Admin §7| ");
	
        saveConfig();
    }
    
    public void loadConfigStrings() {
        teamplayerprefix = this.getConfig().getString("Prefix.Player");
        teampremiumprefix = this.getConfig().getString("Prefix.Premium");
        teamvipprefix = this.getConfig().getString("Prefix.VIP");
        teambuilderprefix = this.getConfig().getString("Prefix.Builder");
        teamleadbuilderprefix = this.getConfig().getString("Prefix.LeadBuilder");
        teamjrmoderatorprefix = this.getConfig().getString("Prefix.JrModerator");
        teammoderatorprefix = this.getConfig().getString("Prefix.Moderator");
        teamleadmoderatorprefix = this.getConfig().getString("Prefix.LeadModerator");
        teamforenmodprefix = this.getConfig().getString("Prefix.Forenmod");
        teamleadforenmodprefix = this.getConfig().getString("Prefix.LeadDeveloper");
        teamdeveloperprefix = this.getConfig().getString("Prefix.Developer");
        teamleaddeveloperprefix = this.getConfig().getString("Prefix.LeadDeveloper");
        teamadminprefix = this.getConfig().getString("Prefix.Admin");
    }
    
    public void connectToMySQL() {
        mySQL = new MySQL("164.132.113.250", "BroEmpire", "WebEmpireSQL", "!WebEmpireSQL");
        
        bro = new Bro(this, mySQL);
    }
    
    public void setLocation(Location loc, String path) {
        this.getConfig().set("Locations." + path + ".World", loc.getWorld().getName());
        this.getConfig().set("Locations." + path + ".X", loc.getX());
        this.getConfig().set("Locations." + path + ".Y", loc.getY() + 1);
        this.getConfig().set("Locations." + path + ".Z", loc.getZ());
        this.getConfig().set("Locations." + path + ".Yaw", loc.getYaw());
        this.getConfig().set("Locations." + path + ".Pitch", loc.getPitch());
        
        this.saveConfig();
    }
    
    public Location getLocation(String path) {
        String world = this.getConfig().getString("Locations." + path + ".World");
        double x = this.getConfig().getDouble("Locations." + path + ".X");
        double y = this.getConfig().getDouble("Locations." + path + ".Y");
        double z = this.getConfig().getDouble("Locations." + path + ".Z");
        float yaw = this.getConfig().getInt("Locations." + path + ".Yaw");
        float pitch = this.getConfig().getInt("Locations." + path + ".Pitch");
     
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }
    
    public void sendUsage(String usage, String example, Player p) {
        p.sendMessage("§8§m--------§r§7»§6Benutzung§7«§8§m--------");
        p.sendMessage(" ");
        p.sendMessage("§4" + usage);
        if(example != null) {
            p.sendMessage("§2Beispiel§7: §6" + example);
        }
        p.sendMessage(" ");
        p.sendMessage("§8§m--------§r§7»§6Benutzung§7«§8§m--------");
    }
    
    public boolean isTeammember(Player p) {
        return p.hasPermission("BroEmpire.team");
    }
    
    public static BuildEmpire getInstance() {
        return instance;
    }
    
    public MySQL getMySQL() {
        return mySQL;
    }
    
    public Bro getBro() {
        return bro;
    }
    
    public GroupManager getGroupManager() {
        return groupmanager;
    }
    
    public ScoreboardManager getScoreboardManager() {
        return scoreboardmanager;
    }
    
    public ScoreboardTeams getScoreboardTeams() {
        return scoreboardteams;
    }
    
    public Methods getMethods() {
        return methods;
    }
    
    public Build getBuild() {
        return build;
    }
    
    public BuildMode getBuildMode() {
        return buildmode;
    }

    public void setBuildmodeTrue(Player p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void readyPlayer(Player p) {
        	
	p.setHealth(20D);
        p.setMaxHealth(20D);
	p.getInventory().clear();
	p.getInventory().setArmorContents(null);
	p.setFireTicks(0);
	p.setAllowFlight(false);
	p.setFlying(false);
	p.setGameMode(GameMode.ADVENTURE);
	p.setExp(0F);
	p.setLevel(0);
        p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(100D);
	for(PotionEffect effect : p.getActivePotionEffects()) {
		p.removePotionEffect(effect.getType());
	}
    }
    
    public List<Map> getWorlds() {
        List<Map> maps = new ArrayList<>();
        if(this.getConfig().contains("Maps")) {
            for(String map : this.getConfig().getStringList("Maps")) {
                maps.add(Map.getMapByName(map, this));
            }
        }
        return maps;
    }
    
    public void sendHelp(Player p) {
        p.sendMessage(BuildEmpire.prefix + "§bBefehle:");
        p.sendMessage("§8● §b/view §7Listet alle Welten auf");
        p.sendMessage("§8● §b/view info <Mapname> §7Zeigt ein paar Infos über die Welt");
        p.sendMessage("§8● §b/view tp <Mapname> §7Teleportiert dich zu der Welt");
    }
}
