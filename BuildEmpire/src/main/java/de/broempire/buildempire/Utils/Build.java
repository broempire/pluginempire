/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Utils;

import de.broempire.buildempir.BuildEmpire;
import de.broempire.buildempire.APIs.ItemBuilder;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class Build {
    
    private List<Player> players = new ArrayList<>();
    private List<Player> invitations = new ArrayList<>();
    
    public boolean hasPlayer(Player p) {
            return players.contains(p);
	}
    
    public List<Player> getPlayers() {
        return players;
    }
    
    public void removePlayers() {
        players.removeAll(players);
    }
    
    public void inviteToBuild(String worldOwner, Player p, Player target) {
        if(!invitations.contains(target)) {
            if(p.getName().equals(worldOwner)) {
            invitations.add(target);
            p.sendMessage(BuildEmpire.prefix + "§aDu hast " + target.getDisplayName() + " §azum Bauen eingeladen!");
            target.sendMessage(BuildEmpire.prefix + p.getDisplayName() + " §ahat dich zum Bauen eingeladen!");
            target.sendMessage(BuildEmpire.prefix + "§b/world accept " + p.getName());
            target.sendMessage(BuildEmpire.prefix + "§b/world deny " + p.getName());
            }else{
                p.sendMessage(BuildEmpire.prefix + "§cDu bist nicht der Bauleiter!");
            }
        }else{
            p.sendMessage(BuildEmpire.prefix + "§cDer Spieler hat bereits eine Einladung zu einem anderem Bündnis bekommen!");
        }
    }
    
    public boolean hasInvitation(Player p) {
            return invitations.contains(p);
    }
    
    public void removeInvitation(Player p) {
        invitations.remove(p);
    }
    
    public void addToBuild(Player p, Player owner) {
        players.add(p);
        players.add(owner);
        p.sendMessage(BuildEmpire.prefix + "Du hast die Bauanfrage §aangenommen§7!");
        owner.sendMessage(BuildEmpire.prefix + p.getDisplayName() + " §7hat die Bauanfrage §aangenommen!");
        invitations.remove(p);
       
        p.teleport(owner.getLocation());
        p.setGameMode(GameMode.CREATIVE);
        p.getInventory().setItem(8, new ItemBuilder(Material.REDSTONE, 1).setDisplayName("§7>>§6Einstellungen§7<<").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
    }
    
    public void removeFromBuild(Player target) {
            players.remove(target);
	} 
    
}
