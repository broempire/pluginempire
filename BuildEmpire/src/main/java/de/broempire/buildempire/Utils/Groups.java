/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Utils;

/**
 *
 * @author Matti
 */
public enum Groups {
    PLAYER("Player", "§eSpieler"),
    PREMIUM("Premium", "§6Premium"),
    VIP("VIP", "§5VIP"),
    BUILDER("Builder", "§9Builder"),
    LEADBUILDER("LeadBuilder", "§9LeadBuilder"),
    MODERATOR("Moderator", "§cModerator"),
    JRMODERATOR("JrModerator", "§cJrModerator"),
    LEADMODERATOR("LeadModerator", "§cLeadModerator"),
    DEVELOPER("Developer", "§bDeveloper"),
    LEADDEVELOPER("LeadDeveloper", "§bLeadDeveloper"),
    ADMIN("Admin", "§4Admin"),
    FORENMOD("Forenmod", "§2Forenmod"),
    LEADFORENMOD("LeadForenmod", "§2LeadForenmod");
    
    String name;
    String groupname;
    
    Groups(String groupname, String name) {
        this.name = name;
        this.groupname = groupname;
    }
    public String getGroupname() {
        return groupname;
    }
    public String getName() {
        return name;
    }
}
