/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Utils;

/**
 *
 * @author Matti
 */
public enum Generator {
    
    FLAT("FLAT"),
    EMPTY("EMPTY"),
    NORMAL("NORMAL");

    private String name;

    private Generator(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
}
