/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Fabian, modified by Juli
 */
public class BuildMode {

    public static List<String> buildmodelist = new ArrayList<>();
    private static final HashMap<String, ItemStack[]> inv = new HashMap<>();

    public String notonline = "§cDieser Spieler ist momentan nicht online oder er existiert nicht!";

    public String isbuildmodeon = "§7Der Spieler [Player] §7ist nun im §aBuildmode§7!";
    public String isbuildmodeoff = "§7Der Spieler [Player] §7ist nun nicht mehr im §cBuildmode§7!";

    public void setBuildmodeTrue(Player p) {
        buildmodelist.add(p.getName());
        p.setGameMode(GameMode.CREATIVE);
        inv.put(p.getName(), p.getInventory().getContents());
        p.getInventory().clear();
        p.setFlying(true);
    }

    public void setBuildmodeFalse(Player p) {
        buildmodelist.remove(p.getName());
        p.setGameMode(GameMode.ADVENTURE);
        p.setFlying(false);
        p.setAllowFlight(false);
        p.getInventory().setContents(inv.get(p.getName()));
    }

    public boolean isBuildmode(Player p) {
        return buildmodelist.contains(p.getName());
    }
}
