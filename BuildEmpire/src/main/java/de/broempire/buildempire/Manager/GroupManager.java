/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Manager;

import de.broempire.buildempir.BuildEmpire;
import de.broempire.buildempire.Utils.Groups;
import org.bukkit.entity.Player;

/**
 *
 * @author Matti
 */
public class GroupManager {
    
    BuildEmpire plugin;
    
    public GroupManager(BuildEmpire plugin) {
        this.plugin = plugin;
    }
    
    public Groups getGroup(Player p) {
        switch(plugin.getBro().getGroup(p.getUniqueId().toString())) {
            case "PLAYER":
                return Groups.PLAYER;
            case "PREMIUM":
                return Groups.PREMIUM;
            case "VIP":
                return Groups.VIP;
            case "BUILDER":
                return Groups.BUILDER;
            case "LEADBUILDER":
                return Groups.LEADBUILDER;
            case "MODERATOR":
                return Groups.MODERATOR;
            case "JRMODERATOR":
                return Groups.JRMODERATOR;
            case "LEADMODERATOR":
                return Groups.LEADMODERATOR;
            case "DEVELOPER":
                return Groups.DEVELOPER;
            case "LEADDEVELOPER":
                return Groups.LEADDEVELOPER;
            case "ADMIN":
                return Groups.ADMIN;
            case "FORENMOD":
                return Groups.FORENMOD;
            case "LEADFORENMOD":
                return Groups.LEADFORENMOD;
            default:
                return Groups.PLAYER;
        }     
    }
}
