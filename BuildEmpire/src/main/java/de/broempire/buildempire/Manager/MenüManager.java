/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Manager;

import de.broempire.buildempire.Utils.*;
import de.broempire.buildempire.Manager.*;
import de.broempire.buildempir.BuildEmpire;
import de.broempire.buildempire.APIs.AnvilGUI;
import de.broempire.buildempire.APIs.ItemBuilder;
import de.broempire.buildempire.Utils.Generator;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.EntityEffect;
import org.bukkit.FireworkEffect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author Juli
 */
public class MenüManager implements Listener {
    
    private BuildEmpire plugin;
    private HashMap<Player, Integer> mapsInt = new HashMap<>();
    String worldName;
    
    public MenüManager(BuildEmpire plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            if(e.getItem() != null && e.getItem().getItemMeta() != null && e.getItem().getItemMeta().getDisplayName() != null) {
                if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7>>§6Menü§7<<")) {
                    Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Hauptmenü");
                        
                    inv.setItem(0, new ItemBuilder(Material.REDSTONE_BLOCK, 1).setDisplayName("§cDeine Maps").build());
                    inv.setItem(1, new ItemBuilder(Material.GLOWSTONE, 1).setDisplayName("§6Veröffentlichte Maps").build());
                    inv.setItem(7, new ItemBuilder(Material.PRISMARINE_CRYSTALS, 1).setDisplayName("§9Spawn").build());
                    inv.setItem(8, new ItemBuilder(Material.BARRIER, 1).setDisplayName("§cSchließen").build());
                    
                    p.openInventory(inv);
                }else if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7>>§6Einstellungen§7<<")) {
                    Inventory inv = Bukkit.createInventory(null, 27, "§7Menü >> Einstellungen");
                    
                    inv.setItem(0, new ItemBuilder(Material.DIAMOND, 1).setDisplayName("§aSpawnpunkt setzen").setLore("§7↳ §8Bei deiner aktuellen Position!").build());
                    inv.setItem(1, new ItemBuilder(Material.COMPASS, 1).setDisplayName("§aGamemode wechseln").setLore("§7↳ §8Wechselt den Gamemode!").build());
                    inv.setItem(2, new ItemBuilder(Material.GLASS, 1, (short)4).setDisplayName("§aWetter").setLore("§7↳ §8Wechselt das Wetter!").build());
                    inv.setItem(3, new ItemBuilder(Material.DAYLIGHT_DETECTOR, 1).setDisplayName("§aZeit").setLore("§7↳ §8Wechselt die Zeit!").build());
                    //inv.setItem(4, new ItemBuilder(Material.NAME_TAG, 1).setDisplayName("§aUmbennen").build());
                    //inv.setItem(5, new ItemBuilder(Material.BOOK, 1).setDisplayName("§aBeschreibung setzten").build());
                    inv.setItem(26, new ItemBuilder(Material.EMERALD, 1).setDisplayName("§aVeröffentlichen").setLore("").build());
                    inv.setItem(25, new ItemBuilder(Material.STICK, 1).setDisplayName("§aSpieler einladen").setLore("").build());
                    inv.setItem(6, new ItemBuilder(Material.PRISMARINE_CRYSTALS, 1).setDisplayName("§9Spawn").setLore("").build());
                    
                    p.openInventory(inv);
                }
            }
        }
    }
    
    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        
        if(e.getInventory().getName().equalsIgnoreCase("§7Menü >> Hauptmenü")) {
            e.setCancelled(true);
            if(e.getSlot() == e.getRawSlot()) {
                if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
                    if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cDeine Maps")) {
                        Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Deine Maps");
                        if(plugin.isTeammember(p)) {
                            inv.setItem(5, new ItemBuilder(Material.SLIME_BLOCK, 1).setDisplayName("§9Alle Maps").build());
                        }
                        inv.setItem(6, new ItemBuilder(Material.REDSTONE, 1).setDisplayName("§bErstellen").build());
                        inv.setItem(7, new ItemBuilder(Material.TNT, 1).setDisplayName("§cLöschen").build());
                        inv.setItem(8, new ItemBuilder(Material.BARRIER, 1).setDisplayName("§cZurück").build());
                        
                        mapsInt.put(p, 0);
                        
                        for(Map map : plugin.getWorlds()) {
                            if(map.getOwner().equals(p.getName())) {
                                if(inv.getItem(0) == null) {
                                    inv.setItem(0, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 1);
                                }else if(mapsInt.get(p) == 1) {
                                    inv.setItem(1, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 2);
                                }else if(mapsInt.get(p) == 2) {
                                    inv.setItem(2, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 3);
                                }else if(mapsInt.get(p) == 3) {
                                    inv.setItem(3, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 4);
                                }else if(mapsInt.get(p) == 4) {
                                    inv.setItem(4, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 5);
                                }
                            }
                        }
                        
                        p.openInventory(inv);
                        
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Veröffentlichte Maps")) {
                        int i = 0;
                        if(i <= 54) {
                            Inventory inv = Bukkit.createInventory(null, 54, "§7Menü >> Veröffentlichte Maps");
                            
                            for(Map map : plugin.getWorlds()) {
                                if(map.getPrivacy().equals("Offentlich")) {
                                    inv.setItem(i, new ItemBuilder(Material.GLOWSTONE, 1).setDisplayName("[" + map.getPräfix() + "] §a" + map.getName()).build());
                                    
                                    i++;
                                }
                            }
                            p.openInventory(inv);
                        }else{
                            
                        }
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§9Spawn")) {
                        p.setGameMode(GameMode.SURVIVAL);
                        p.getInventory().clear();
                        p.teleport(plugin.getLocation("LOBBY"));
                        
                        p.getInventory().setItem(8, new ItemBuilder(Material.REDSTONE_COMPARATOR, 1).setDisplayName("§7>>§6Menü§7<<").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cSchließen")) {
                        p.closeInventory();
                        
                    }
                }
            }
        }else if(e.getInventory().getName().equalsIgnoreCase("§7Menü >> Deine Maps")) {
            e.setCancelled(true);
            if(e.getSlot() == e.getRawSlot()) {
                if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
                    
                    for(Map map : plugin.getWorlds()) {
                        if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a" + map.getName())) {
                            p.setGameMode(GameMode.CREATIVE);
                            p.teleport(map.getLocation("SPAWN"));
                            
                            p.getInventory().setItem(8, new ItemBuilder(Material.REDSTONE, 1).setDisplayName("§7>>§6Einstellungen§7<<").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
                        }
                    }
                            
                    if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bErstellen")) {
                        if(mapsInt.get(p) < 3) {
                            Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Maptemplates");
                            
                            //inv.setItem(0, new ItemBuilder(Material.GLASS, 1).setDisplayName("Leere Welt").setLore("§7Eine komplett leere Welt").build());
                            inv.setItem(1, new ItemBuilder(Material.GRASS, 1).setDisplayName("Flachland").build());
                            //inv.setItem(2, new ItemBuilder(Material.STONE, 1).setDisplayName("Steinfläche").build());
                            //inv.setItem(3, new ItemBuilder(Material.NETHERRACK, 1).setDisplayName("Netherfläche").build());
                            inv.setItem(8, new ItemBuilder(Material.BARRIER, 1).setDisplayName("§cZurück").build());
                            
                            p.openInventory(inv);
                        }else if(mapsInt.get(p) < 5 && plugin.isTeammember(p)) {
                            Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Maptemplates");
                            
                            //inv.setItem(0, new ItemBuilder(Material.GLASS, 1).setDisplayName("Leere Welt").setLore("§7Eine komplett leere Welt").build());
                            inv.setItem(1, new ItemBuilder(Material.GRASS, 1).setDisplayName("Flachland").build());
                            //inv.setItem(2, new ItemBuilder(Material.STONE, 1).setDisplayName("Steinfläche").build());
                            //inv.setItem(3, new ItemBuilder(Material.NETHERRACK, 1).setDisplayName("Netherfläche").build());
                            inv.setItem(8, new ItemBuilder(Material.BARRIER, 1).setDisplayName("§cZurück").build());
                            
                            p.openInventory(inv);
                        }else{
                            if(plugin.isTeammember(p)) {
                                p.sendMessage(BuildEmpire.prefix + "§cDu hast bereits 5 Welten erstellt");
                            }else{
                                p.sendMessage(BuildEmpire.prefix + "§cDu hast bereits 3 Welten erstellt");
                            }
                        }
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cLöschen")) {
                        Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Löschen");
                        
                        inv.setItem(8, new ItemBuilder(Material.BARRIER, 1).setDisplayName("§cZurück").build());
                        
                        for(Map map : plugin.getWorlds()) {
                            if(map.getOwner().equals(p.getName())) {
                                if(inv.getItem(0) == null) {
                                    inv.setItem(0, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 1);
                                }else if(mapsInt.get(p) == 1) {
                                    inv.setItem(1, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 2);
                                }else if(mapsInt.get(p) == 2) {
                                    inv.setItem(2, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 3);
                                }else if(mapsInt.get(p) == 3) {
                                    inv.setItem(3, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 4);
                                }else if(mapsInt.get(p) == 4) {
                                    inv.setItem(4, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 5);
                                }
                            }
                        }
                        
                        p.openInventory(inv);
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cZurück")) {
                        Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Hauptmenü");
                        
                        inv.setItem(0, new ItemBuilder(Material.REDSTONE_BLOCK, 1).setDisplayName("§cDeine Maps").build());
                        inv.setItem(1, new ItemBuilder(Material.GLOWSTONE, 1).setDisplayName("§6Veröffentlichte Maps").build());
                        inv.setItem(7, new ItemBuilder(Material.PRISMARINE_CRYSTALS, 1).setDisplayName("§9Spawn").build());
                        inv.setItem(8, new ItemBuilder(Material.BARRIER, 1).setDisplayName("§cSchließen").build());
                        
                        p.openInventory(inv);
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§9Alle Maps")) {
                        plugin.sendHelp(p);
                        
                        p.closeInventory();
                    }
                }
            }
        }else if(e.getInventory().getName().equalsIgnoreCase("§7Menü >> Veröffentlichte Maps")) {
            e.setCancelled(true);
            if(e.getSlot() == e.getRawSlot()) {
                if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
                    for(Map map : plugin.getWorlds()) {
                        if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a" + map.getName())) {
                            p.setGameMode(GameMode.SPECTATOR);
                            p.teleport(map.getLocation("SPAWN"));
                        }
                    }
                }
            }
        }else if(e.getInventory().getName().equalsIgnoreCase("§7Menü >> Maptemplates")) {
            e.setCancelled(true);
            if(e.getSlot() == e.getRawSlot()) {
                if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
                    if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Leere Welt")) {
                        
                        AnvilGUI gui = new AnvilGUI(p, new AnvilGUI.AnvilClickEventHandler() {
                            @Override
                            public void onAnvilClick(AnvilGUI.AnvilClickEvent e) {
                                if(e.getSlot() == AnvilGUI.AnvilSlot.OUTPUT) {
                                    e.setWillClose(true);
                                    e.setWillDestroy(true);
                                    /*p.sendMessage(BuildEmpire.prefix + "Die Welt wird erstellt...");
                                    Map map = new Map(e.getName(), Generator.EMPTY, p.getName(), plugin);
                                    map.setLocation(getWXYZ(e.getName()), "SPAWN");
                                    
                                    World w = Bukkit.getWorld(e.getName());
                                    
                                    
                                    Firework firework = p.getWorld().spawn(p.getLocation(), Firework.class);
                                    FireworkEffect effect = FireworkEffect.builder().withColor(Color.AQUA).flicker(true).trail(true).withFade(Color.RED).with(FireworkEffect.Type.BALL_LARGE).build();
                                    FireworkMeta meta = firework.getFireworkMeta();
                                    meta.addEffect(effect);
                                    meta.setPower(1);
                                    firework.setFireworkMeta(meta);
                                    
                                    p.sendMessage(BuildEmpire.prefix + "Die Welt wurde erstellt...");
                                    */
                                }else{
                                    e.setWillClose(false);
                                    e.setWillDestroy(false);
                                }
                            }
                        });
                        ItemStack i = new ItemStack(Material.NAME_TAG);
                        ItemMeta meta = i.getItemMeta();
                        meta.setDisplayName("Weltenname");
                        i.setItemMeta(meta);
                        
                        gui.setSlot(AnvilGUI.AnvilSlot.INPUT_LEFT, i);
                        gui.open();
                        
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Flachland")) {
                        
                        AnvilGUI gui = new AnvilGUI(p, new AnvilGUI.AnvilClickEventHandler() {
                            @Override
                            public void onAnvilClick(AnvilGUI.AnvilClickEvent e) {
                                if(e.getSlot() == AnvilGUI.AnvilSlot.OUTPUT) {
                                    e.setWillClose(true);
                                    e.setWillDestroy(true);
                                    p.sendMessage(BuildEmpire.prefix + "Die Welt wird erstellt...");
                                    Map map = new Map(e.getName(), Generator.FLAT, p.getName(), plugin);
                                    map.setLocation(getWXYZ(e.getName()), "SPAWN");
                                    
                                    Firework firework = p.getWorld().spawn(p.getLocation(), Firework.class);
                                    FireworkEffect effect = FireworkEffect.builder().withColor(Color.AQUA).flicker(true).trail(true).withFade(Color.RED).with(FireworkEffect.Type.BALL_LARGE).build();
                                    FireworkMeta meta = firework.getFireworkMeta();
                                    meta.addEffect(effect);
                                    meta.setPower(1);
                                    firework.setFireworkMeta(meta);
                                    
                                    p.sendMessage(BuildEmpire.prefix + "Die Welt wurde erstellt...");
                                }else{
                                    e.setWillClose(false);
                                    e.setWillDestroy(false);
                                }
                            }
                        });
                        ItemStack i = new ItemStack(Material.NAME_TAG);
                        ItemMeta meta = i.getItemMeta();
                        meta.setDisplayName("Weltenname");
                        i.setItemMeta(meta);
                        
                        gui.setSlot(AnvilGUI.AnvilSlot.INPUT_LEFT, i);
                        gui.open();
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Steinfläche")) {
                        
                        
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Netherfläche")) {
                        
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cZurück")) {
                        Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Deine Maps");
                        
                        inv.setItem(6, new ItemBuilder(Material.REDSTONE, 1).setDisplayName("§bErstellen").build());
                        inv.setItem(7, new ItemBuilder(Material.TNT, 1).setDisplayName("§cLöschen").build());
                        inv.setItem(8, new ItemBuilder(Material.BARRIER, 1).setDisplayName("§cZurück").build());
                        
                        mapsInt.put(p, 0);
                        
                        for(Map map : plugin.getWorlds()) {
                            if(map.getOwner().equals(p.getName())) {
                                if(inv.getItem(0) == null) {
                                    inv.setItem(0, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 1);
                                }else if(mapsInt.get(p) == 1) {
                                    inv.setItem(1, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 2);
                                }else if(mapsInt.get(p) == 2) {
                                    inv.setItem(2, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 3);
                                }else if(mapsInt.get(p) == 3) {
                                    inv.setItem(3, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 4);
                                }else if(mapsInt.get(p) == 4) {
                                    inv.setItem(4, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 5);
                                }
                            }
                        }
                        
                        p.openInventory(inv);
                    }
                }
            }
        }else if(e.getInventory().getName().equalsIgnoreCase("§7Menü >> Löschen")) {
            e.setCancelled(true);
            if(e.getSlot() == e.getRawSlot()) {
                if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
                    for(Map map : plugin.getWorlds()) {
                        if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a" + map.getName())) {
                        Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Löschen");
                        
                        worldName = map.getName();
                        
                        inv.setItem(0, new ItemBuilder(Material.TNT, 1).setDisplayName("§cJa").setLore("§7Möchtest du §a" + map.getName() + " §7wirklich §clöschen?").build());
                        inv.setItem(8, new ItemBuilder(Material.EMERALD, 1).setDisplayName("§aNein").setLore("§7Möchtest du §a" + map.getName() + " §7wirklich §clöschen?").build());
                        
                        p.openInventory(inv);
                        }
                    }
                    if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cJa")) {
                        Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Hauptmenü");
                        List<String> maps = plugin.getConfig().getStringList("Maps");
                        
                        inv.setItem(0, new ItemBuilder(Material.REDSTONE_BLOCK, 1).setDisplayName("§cDeine Maps").build());
                        inv.setItem(1, new ItemBuilder(Material.GLOWSTONE, 1).setDisplayName("§6Veröffentlichte Maps").build());
                        inv.setItem(7, new ItemBuilder(Material.PRISMARINE_CRYSTALS, 1).setDisplayName("§9Spawn").build());
                        inv.setItem(8, new ItemBuilder(Material.BARRIER, 1).setDisplayName("§cSchließen").build());
                        
                        maps.remove(worldName);
                        Map world = Map.getMapByName(worldName, plugin);
                        
                        plugin.getConfig().set("Maps", maps);
                        plugin.saveConfig();
                        world.getFile().delete();
                        plugin.getServer().getWorld(worldName).getWorldFolder().delete();
                        
                        p.openInventory(inv);
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aNein")) {
                        Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Löschen");
                        
                        inv.setItem(8, new ItemBuilder(Material.BARRIER, 1).setDisplayName("§cZurück").build());
                        
                        mapsInt.put(p, 0);
                        
                        for(Map map : plugin.getWorlds()) {
                            if(map.getOwner().equals(p.getName())) {
                                if(inv.getItem(0) == null) {
                                    inv.setItem(0, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 1);
                                }else if(mapsInt.get(p) == 1) {
                                    inv.setItem(1, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 2);
                                }else if(mapsInt.get(p) == 2) {
                                    inv.setItem(2, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                    mapsInt.put(p, 3);
                                }else if(mapsInt.get(p) == 3) {
                                        inv.setItem(4, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                        mapsInt.put(p, 4);
                                }else if(mapsInt.get(p) == 4) {
                                        inv.setItem(5, new ItemBuilder(Material.CLAY_BALL, 1).setDisplayName("§a" + map.getName()).build());
                                        mapsInt.put(p, 5);
                                }
                            }
                        }
                        p.openInventory(inv);
                    }
                }    
            }
        }else if(e.getInventory().getName().equalsIgnoreCase("§7Menü >> Einstellungen")) {
            e.setCancelled(true);
            if(e.getSlot() == e.getRawSlot()) {
                if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
                    if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aSpawnpunkt setzen")) {
                        Map world = Map.getMapByName(p.getWorld().getName(), plugin);                      
                        world.setLocation(p.getLocation(), "SPAWN");
                        
                        p.sendMessage(BuildEmpire.prefix + "Der neue §bSpawnpunkt §7wurde §berfolgreich §7gesetzt!");
                        p.closeInventory();
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aGamemode wechseln")) {
                        if(p.getGameMode() == GameMode.CREATIVE) {
                            p.setGameMode(GameMode.SURVIVAL);
                        }else{
                            p.setGameMode(GameMode.CREATIVE);
                        }
                        p.closeInventory();
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aWetter")) {
                        World w = p.getWorld();
                        
                        if(w.hasStorm()) {
                            w.setStorm(false);
                        }else{
                            w.setStorm(true);
                        }
                        p.closeInventory();
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aZeit")) {
                        Inventory inv = Bukkit.createInventory(null, 9, "§7Menü >> Einstellungen >> Zeit");
                        
                        inv.setItem(0, new ItemBuilder(Material.DAYLIGHT_DETECTOR, 1).setDisplayName("Morgens").build());
                        inv.setItem(1, new ItemBuilder(Material.DAYLIGHT_DETECTOR, 1).setDisplayName("Mittags").build());
                        inv.setItem(2, new ItemBuilder(Material.DAYLIGHT_DETECTOR, 1).setDisplayName("Nachmittags").build());
                        inv.setItem(3, new ItemBuilder(Material.DAYLIGHT_DETECTOR, 1).setDisplayName("Abends").build());
                        inv.setItem(4, new ItemBuilder(Material.DAYLIGHT_DETECTOR, 1).setDisplayName("Nachts").build());
                        
                        p.openInventory(inv);
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aUmbennen")) {
                        Map world = Map.getMapByName(p.getWorld().getName(), plugin);
                        List<String> maps = plugin.getConfig().getStringList("Maps");
                        
                        AnvilGUI gui = new AnvilGUI(p, new AnvilGUI.AnvilClickEventHandler() {
                            @Override
                            public void onAnvilClick(AnvilGUI.AnvilClickEvent e) {
                                if(e.getSlot() == AnvilGUI.AnvilSlot.OUTPUT) {
                                    e.setWillClose(true);
                                    e.setWillDestroy(true);
                                    
                                    maps.remove(world.getName());
                                    
                                    world.setName(e.getName());
                                    world.setFileName(e.getName());
                                    //world.setWorldName(p.getLocation(), "SPAWN");
                                    
                                    maps.add(e.getName());
                                    plugin.getConfig().set("Maps", maps);
                                    
                                    p.sendMessage(BuildEmpire.prefix + "Der neue Name: §a" + e.getName());
                                }else{
                                    e.setWillClose(false);
                                    e.setWillDestroy(false);
                                }
                            }
                        });
                        ItemStack i = new ItemStack(Material.NAME_TAG);
                        ItemMeta meta = i.getItemMeta();
                        meta.setDisplayName("Weltenname");
                        i.setItemMeta(meta);
                        
                        gui.setSlot(AnvilGUI.AnvilSlot.INPUT_LEFT, i);
                        gui.open();
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aSpieler einladen")) {
                        Map world = Map.getMapByName(p.getWorld().getName(), plugin);
                        
                        AnvilGUI gui = new AnvilGUI(p, new AnvilGUI.AnvilClickEventHandler() {
                            @Override
                            public void onAnvilClick(AnvilGUI.AnvilClickEvent e) {
                                if(e.getSlot() == AnvilGUI.AnvilSlot.OUTPUT) {
                                    e.setWillClose(true);
                                    e.setWillDestroy(true);
                                        if(Bukkit.getPlayer(e.getName()) != null) {
                                            Player t = Bukkit.getPlayer(e.getName());
                                            plugin.getBuild().inviteToBuild(world.getOwner(), p, t);
                                        }else{
                                            p.sendMessage(BuildEmpire.prefix + "§cDer Spieler ist nicht online!");
                                        }
                                }else{
                                    e.setWillClose(false);
                                    e.setWillDestroy(false);
                                }
                            }
                        });
                        ItemStack i = new ItemStack(Material.NAME_TAG);
                        ItemMeta meta = i.getItemMeta();
                        meta.setDisplayName("Spieler");
                        i.setItemMeta(meta);
                        
                        gui.setSlot(AnvilGUI.AnvilSlot.INPUT_LEFT, i);
                        gui.open();
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aBeschreibung setzten")) {
                        p.getInventory().setItem(0, new ItemBuilder(Material.BOOK_AND_QUILL, 1).setDisplayName("§cBeschreibung").build());
                        
                        p.closeInventory();
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aVeröffentlichen")) {
                        Inventory inv = Bukkit.createInventory(p, 9, "§7Menü >> Einstellungen >> Präfix");
                        
                        inv.setItem(0, new ItemBuilder(Material.SPIDER_EYE, 1).setDisplayName("§cVirus").build());
                        inv.setItem(1, new ItemBuilder(Material.BONE, 1).setDisplayName("§aWerwolf").build());
                        inv.setItem(8, new ItemBuilder(Material.HOPPER, 1).setDisplayName("§aAnderes").build());
                        
                        p.openInventory(inv);
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§9Spawn")) {
                        Map world = Map.getMapByName(p.getWorld().getName(), plugin);
                        
                        plugin.readyPlayer(p);
                        p.setGameMode(GameMode.SURVIVAL);
                        p.teleport(plugin.getLocation("LOBBY"));
                        
                        if(world.getOwner().equalsIgnoreCase(p.getName())) {
                            plugin.getBuild().removePlayers();
                        }else if(plugin.getBuild().hasPlayer(p)) {
                            plugin.getBuild().removeFromBuild(p);
                        }
                        
                        p.getInventory().setItem(8, new ItemBuilder(Material.REDSTONE_COMPARATOR, 1).setDisplayName("§7>>§6Menü§7<<").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
                    }
               }
            }
        }else if(e.getInventory().getName().equalsIgnoreCase("§7Menü >> Einstellungen >> Zeit")) {
            e.setCancelled(true);
            World w = p.getWorld();
            if(e.getSlot() == e.getRawSlot()) {
                if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
                    if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Morgens")) {
                        w.setTime(600);
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Mittags")) {
                        w.setTime(6000);
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Nachmittags")) {
                        w.setTime(10000);
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Abends")) {
                        w.setTime(13500);
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Nachts")) {
                        w.setTime(20000);
                    }
                }
            }
        }else if(e.getInventory().getName().equalsIgnoreCase("§7Menü >> Einstellungen >> Präfix")) {
            e.setCancelled(true);
            if(e.getSlot() == e.getRawSlot()) {
                if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
                    if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cVirus")) {
                        Veröffentlichen(p, "Virus");
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§4Werwolf")) {
                        Veröffentlichen(p, "Werwolf");
                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aAnderes")) {
                        Veröffentlichen(p, "Anderes");
                    }
                }
            }
        }
    }
    
    public Location getWXYZ(String world) {
        World w = Bukkit.getWorld(world);
	double x = plugin.getConfig().getDouble("Locations.COPY.X");
	double y = plugin.getConfig().getDouble("Locations.COPY.Y");
	double z = plugin.getConfig().getDouble("Locations.COPY.Z");
        float yaw = plugin.getConfig().getInt("Locations.COPY.Yaw");
        float pitch = plugin.getConfig().getInt("Locations.COPY.Pitch");
        
        Location loc = new Location(w, x, y, z, yaw, pitch);
        return loc;
    }
    
    public void Veröffentlichen(Player p, String präfix) {
        Map world = Map.getMapByName(p.getWorld().getName(), plugin);
                        
        if(!world.getPrivacy().equals("Offentlich")) {
            world.setPrivacy("Offentlich");
            world.setPräfix(präfix);
            
            Firework firework = p.getWorld().spawn(p.getLocation(), Firework.class);
            FireworkEffect effect = FireworkEffect.builder().withColor(Color.AQUA).flicker(true).trail(true).withFade(Color.RED).with(FireworkEffect.Type.BALL_LARGE).build();
            FireworkMeta meta = firework.getFireworkMeta();
            meta.addEffect(effect);
            meta.setPower(1);
            firework.setFireworkMeta(meta);
                            
            p.sendMessage(BuildEmpire.prefix + "Die Welt wurde §aerfolgreich §7veröffentlicht!");
        }else{
            p.sendMessage(BuildEmpire.prefix + "§cDiese Welt ist schon öffentlich!");
        }
        p.closeInventory();
    }
    
}
