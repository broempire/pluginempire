/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Manager;

import de.broempire.buildempir.BuildEmpire;
import de.broempire.buildempire.APIs.ItemBuilder;
import de.broempire.buildempire.Utils.Generator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

/**
 *
 * @author Matti, Juli
 */
public class Map {
    
    private String name;
    private Generator generator;
    private String owner;
    private BuildEmpire plugin;
    private FileConfiguration config;
    private File file;
    
    public Map(String name, Generator generator, String owner, BuildEmpire plugin) {
        this.name = name;
        this.generator = generator;
        this.owner = owner;
        this.plugin = plugin;
        createFile();
    }
    
    private void createFile() {
        List<String> maps;
        if (plugin.getConfig().contains("Maps")) {
            maps = plugin.getConfig().getStringList("Maps");
        } else {
            maps = new ArrayList<>();
        }
        if (!maps.contains(name)) {
            maps.add(name);
        }
        plugin.getConfig().set("Maps", maps);
        plugin.saveConfig();
        file = new File(plugin.getDataFolder().getPath(), name + ".yml");
        
        if (!file.exists()) {
            try {
                FileWriter writer = new FileWriter(file);
                writer.write("Owner: " + owner + "\n");
                writer.write("Type: " + generator.getName() + "\n");
                writer.write("Name: " + name + "\n");
                writer.write("Privacy: Privat\n");
                writer.write("Praefix: Leer");
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        config = YamlConfiguration.loadConfiguration(file);
        
        saveConfig();
        
        //TODO Welt generieren!
        if(generator.equals(Generator.EMPTY)) {
            WorldCreator c = WorldCreator.name(name).generateStructures(false);
            Bukkit.createWorld(c);
        }else if(generator.equals(Generator.FLAT)) {
            WorldCreator c = WorldCreator.name(name).type(WorldType.FLAT).generateStructures(false);
            Bukkit.createWorld(c);
        }
        
    }
    
    public static Map getMapByName(String name, BuildEmpire plugin) {
        File file = new File(plugin.getDataFolder().getPath(), name + ".yml");
        if (file.exists()) {
            FileConfiguration config = YamlConfiguration.loadConfiguration(file);
            return new Map(name, Generator.valueOf(config.getString("Type")), config.getString("Owner"), plugin);
        } else {
            return null;
        }
    }
    
    public static boolean exists(String name, BuildEmpire plugin) {
        File file = new File(plugin.getDataFolder().getPath(), name + ".yml");
        return file.exists();
    }
    
    public void saveConfig() {
        try {
            config.save(file);
        } catch (IOException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public File getFile() {
        return file;
    }
    
    public FileConfiguration getConfig() {
        return config;
    }
    
    public Generator getGenerator() {
        return generator;
    }
    
    public String getOwner() {
        return owner;
    }
    
    public void setName(String newName) {    
        config.set("Name", newName);
        saveConfig();
    }
    
    public void setFileName(String newName) {
        File newFile = new File(plugin.getDataFolder().getPath(), newName + ".yml");
        
        if(file.exists()) {
            file.renameTo(newFile);
        }
    }
    
    public void setWorldName(Location loc, String path/*, String newName*/) {
        config.set("Locations." + path + ".World", loc.getWorld().getName());
        saveConfig();
        
        /*File newFile = new File(plugin.getDataFolder().getPath(), newName + ".yml");
        
        plugin.getServer().getWorld(name).getWorldFolder().renameTo(newFile);*/
    }
    
    public String getName() {
        return name;
    }
    
    public void setPrivacy(String privacy) {
        config.set("Privacy", privacy);
        saveConfig();
    }
    
    public String getPrivacy() {
        return config.getString("Privacy");
    }
    
    public void setPräfix(String präfix) {
        config.set("Praefix", präfix);
        saveConfig();
    }
    
    public String getPräfix() {
        return config.getString("Praefix");
    }
    
    public void setLocation(Location loc, String path) {
        
        config.set("Locations." + path + ".World", loc.getWorld().getName());
        config.set("Locations." + path + ".X", loc.getX());
        config.set("Locations." + path + ".Y", loc.getY() + 1);
        config.set("Locations." + path + ".Z", loc.getZ());
        config.set("Locations." + path + ".Yaw", loc.getYaw());
        config.set("Locations." + path + ".Pitch", loc.getPitch());
        
        saveConfig();
    }
    
    public Location getLocation(String path) {
        if(file.exists()) {
            
            String world = config.getString("Locations." + path + ".World");
            double x = config.getDouble("Locations." + path + ".X");
            double y = config.getDouble("Locations." + path + ".Y");
            double z = config.getDouble("Locations." + path + ".Z");
            float yaw = config.getInt("Locations." + path + ".Yaw");
            float pitch = config.getInt("Locations." + path + ".Pitch");
            
            return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
        }else{
            return null;
        }
    }
    
    public Location getSpawn() {
        return null;
    }
}
