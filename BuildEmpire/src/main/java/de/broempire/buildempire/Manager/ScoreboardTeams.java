/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Manager;

import com.nametagedit.plugin.NametagEdit;
import de.broempire.buildempire.APIs.ScoreboardTeam;
import de.broempire.buildempir.BuildEmpire;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author Fabian
 */
public class ScoreboardTeams {
    
    private BuildEmpire plugin;
    
    private static Map<String, ScoreboardTeam> teams = new HashMap<>();
    
    public ScoreboardTeams(BuildEmpire plugin) {
        this.plugin = plugin;
    }
    
    public static void scoreboardteamsinit() {
        
        ScoreboardTeam admin = new ScoreboardTeam("00ADMIN");
        admin.setPrefix(BuildEmpire.teamadminprefix);
        teams.put("ADMIN", admin);
        ScoreboardTeam developer = new ScoreboardTeam("01DEVELOPER");
        developer.setPrefix(BuildEmpire.teamdeveloperprefix);
        teams.put("DEVELOPER", developer);
        ScoreboardTeam leaddeveloper = new ScoreboardTeam("02LEADDEVELOPER");
        leaddeveloper.setPrefix(BuildEmpire.teamleaddeveloperprefix);
        teams.put("LEADDEVELOPER", leaddeveloper);
        ScoreboardTeam moderator = new ScoreboardTeam("03MODERATOR");
        moderator.setPrefix(BuildEmpire.teammoderatorprefix);
        teams.put("MODERATOR", moderator);
        ScoreboardTeam jrmoderator = new ScoreboardTeam("04JRMODERATOR");
        jrmoderator.setPrefix(BuildEmpire.teamjrmoderatorprefix);
        teams.put("JRMODERATOR", jrmoderator);
        ScoreboardTeam leadmoderator = new ScoreboardTeam("05LEADMODERATOR");
        leadmoderator.setPrefix(BuildEmpire.teamleadmoderatorprefix);
        teams.put("LEADMODERATOR", leadmoderator);
        ScoreboardTeam forenmod = new ScoreboardTeam("06FORENMOD");
        forenmod.setPrefix(BuildEmpire.teamforenmodprefix);
        teams.put("FORENMOD", forenmod);
        ScoreboardTeam leadforenmod = new ScoreboardTeam("07LEADFORENMOD");
        leadforenmod.setPrefix(BuildEmpire.teamleadforenmodprefix);
        teams.put("LEADFORENMOD", leadforenmod);
        ScoreboardTeam builder = new ScoreboardTeam("08BUILDER");
        builder.setPrefix(BuildEmpire.teambuilderprefix);
        teams.put("BUILDER", builder);
        ScoreboardTeam leadbuilder = new ScoreboardTeam("09LEADBUILDER");
        leadbuilder.setPrefix(BuildEmpire.teamleadbuilderprefix);
        teams.put("LEADBUILDER", leadbuilder);
        ScoreboardTeam vip = new ScoreboardTeam("10VIP");
        vip.setPrefix(BuildEmpire.teamvipprefix);
        teams.put("VIP", vip);
        ScoreboardTeam premium = new ScoreboardTeam("11PREMIUM");
        premium.setPrefix(BuildEmpire.teampremiumprefix);
        teams.put("PREMIUM", premium);
        ScoreboardTeam player = new ScoreboardTeam("12PLAYER");
        player.setPrefix(BuildEmpire.teamplayerprefix);
        teams.put("PLAYER", player);
        
    }
    public void updatePlayer(Player p) {
        String group = plugin.getBro().getGroup(p.getUniqueId().toString());
        ScoreboardTeam team = teams.get(group);
        p.setPlayerListName(team.getPrefix() + p.getName());
        p.setDisplayName(team.getPrefix() + p.getName());
        
        switch(plugin.getGroupManager().getGroup(p)) {
            case PLAYER:
                p.setCustomName("§e" + p.getName());
                break;
            case PREMIUM:
                p.setCustomName("§6" + p.getName());
                break;
            case VIP:
                p.setCustomName("§5" + p.getName());
                break;
            case BUILDER:
                p.setCustomName("§9" + p.getName());
                break;
            case LEADBUILDER:
                p.setCustomName("§9" + p.getName());
                break;
            case MODERATOR:
                p.setCustomName("§c" + p.getName());
                break;
            case JRMODERATOR:
                p.setCustomName("§c" + p.getName());
                break;
            case LEADMODERATOR:
                p.setCustomName("§c" + p.getName());
                break;
            case DEVELOPER:
                p.setCustomName("§b" + p.getName());
                break;
            case LEADDEVELOPER:
                p.setCustomName("§b" + p.getName());
                break;
            case ADMIN:
                p.setCustomName("§4" + p.getName());
                break;
            case FORENMOD:
                p.setCustomName("§2" + p.getName());
                break;
            case LEADFORENMOD:
                p.setCustomName("§2" + p.getName());
                break;
        }
        
        new BukkitRunnable() {
            @Override
            public void run() {
                NametagEdit.getApi().setPrefix(p, team.getPrefix());
                NametagEdit.getApi().setSuffix(p, team.getSuffix());
            }
        }.runTaskAsynchronously(plugin);
    }
    
    public void renewScoreboard() {
        for (Player p : plugin.getServer().getOnlinePlayers()) {
            updatePlayer(p);
        }
    }
}
	
