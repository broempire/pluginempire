/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Listener;

import de.broempire.buildempir.BuildEmpire;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 *
 * @author Juli
 */
public class ChatListener implements Listener {
    
    BuildEmpire plugin;
    
    public ChatListener(BuildEmpire plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onChatEvent(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        
        e.setFormat(p.getDisplayName() + " §8» §r" + e.getMessage());
    }
    
}
