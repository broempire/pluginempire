/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Listener;

import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

/**
 *
 * @author Juli
 */
public class Listeners implements Listener {
    
    @EventHandler
    public void onSpawn(EntitySpawnEvent e) {
        if(!e.getEntityType().equals(EntityType.ARMOR_STAND)) {
            e.setCancelled(true);
        }
    }
    
    /*@EventHandler
    public void onDrop(PlayerDropItemEvent e) {
    
    
        
    }*/
    
    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        
        if(p.getGameMode() == GameMode.CREATIVE) {
            e.setCancelled(false);
        }else{
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        
        if(p.getGameMode() == GameMode.CREATIVE) {
            if(p.getItemInHand() != null && p.getItemInHand().hasItemMeta()) {
            if(!p.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase("§7>>§6Einstellungen§7<<")) {
                e.setCancelled(false);
            }else{
              e.setCancelled(true);  
            }
            }
        }else{
            e.setCancelled(true);
        }
    }
}
