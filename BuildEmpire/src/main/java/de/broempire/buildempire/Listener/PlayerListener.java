/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Listener;

import de.broempire.buildempir.BuildEmpire;
import de.broempire.buildempire.APIs.ItemBuilder;
import de.broempire.buildempire.Manager.Map;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author Matti
 */
public class PlayerListener implements Listener {
    
    private BuildEmpire plugin;

    public PlayerListener(BuildEmpire plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler (priority = EventPriority.HIGHEST)
    public void onJoin (PlayerJoinEvent e) {
        Player p = e.getPlayer();
        
        e.setJoinMessage("§7[§2+§7] " + p.getName());
        p.sendMessage("§aViel Spaß beim Bauen!");
        plugin.readyPlayer(p);
        
        p.teleport(plugin.getLocation("LOBBY"));
        p.getInventory().setItem(8, new ItemBuilder(Material.REDSTONE_COMPARATOR, 1).setDisplayName("§7>>§6Menü§7<<").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
        
        for (Player online : plugin.getServer().getOnlinePlayers()) {
                if (online != p) {
                    online.hidePlayer(p);
                    p.hidePlayer(online);
                }
            }
            new BukkitRunnable() {
                @Override
                public void run() {
                    for (Player online : plugin.getServer().getOnlinePlayers()) {
                        online.showPlayer(p);
                        p.showPlayer(online);
                    }
                }
            }.runTaskLater(plugin, 5L);
        
        plugin.getScoreboardTeams().updatePlayer(p);
        plugin.getScoreboardManager().sendScoreboard(p);
    }
    
    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        Map world = Map.getMapByName(p.getName(), plugin);
        
        e.setQuitMessage("§7[§4-§7] " + p.getName());
        
        if(world != null) {
            if(world.getOwner().equalsIgnoreCase(p.getName())) {
                plugin.getBuild().removePlayers();
                for(Player all : Bukkit.getWorld(world.getName()).getPlayers()) {
                    plugin.readyPlayer(p);
                    p.setGameMode(GameMode.SURVIVAL);
                    all.teleport(plugin.getLocation("LOBBY"));
                    
                    all.getInventory().setItem(8, new ItemBuilder(Material.REDSTONE_COMPARATOR, 1).setDisplayName("§7>>§6Menü§7<<").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
                }
            }else if(plugin.getBuild().hasPlayer(p)) {
                plugin.getBuild().removeFromBuild(p);
            }
        }
    }
}
