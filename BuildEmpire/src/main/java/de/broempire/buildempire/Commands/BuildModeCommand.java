/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Commands;

import de.broempire.buildempir.BuildEmpire;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Fabian
 */
public class BuildModeCommand implements CommandExecutor {

    BuildEmpire plugin;
    
    public BuildModeCommand(BuildEmpire plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            if(args.length == 1){
                Player target = Bukkit.getPlayer(args[0]);
                if (target != null) {
                    if (plugin.getBuildMode().isBuildmode(target) == true) {
                        plugin.getBuildMode().setBuildmodeFalse(target);
                        String msg = plugin.getBuildMode().isbuildmodeoff.replace("[Player]", target.getCustomName());
                        sender.sendMessage(BuildEmpire.prefix + msg);
                    } else {
                        plugin.getBuildMode().setBuildmodeTrue(target);
                        String msg = plugin.getBuildMode().isbuildmodeon.replace("[Player]", target.getCustomName());
                        sender.sendMessage(BuildEmpire.prefix + msg);
                    }
                } else {
                    sender.sendMessage(BuildEmpire.prefix + plugin.getBuildMode().notonline);
                }
            }
        } else if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("BroEmpire.build")) {
                switch (args.length) {
                    case 0:
                        if (plugin.getBuildMode().isBuildmode(p) == true) {
                            plugin.getBuildMode().setBuildmodeFalse(p);
                            p.sendMessage(BuildEmpire.prefix + "Du bist nun nicht mehr im §cBuildmode§7!");
                        } else {
                            plugin.getBuildMode().setBuildmodeTrue(p);
                            p.sendMessage(BuildEmpire.prefix + "Du bist nun im §aBuildmode§7!");
                        }
                        break;
                    case 1:
                        Player target = Bukkit.getPlayer(args[0]);
                        if (target != null) {
                            if (plugin.getBuildMode().isBuildmode(target) == true) {
                                plugin.getBuildMode().setBuildmodeFalse(target);
                                String msg = plugin.getBuildMode().isbuildmodeoff.replace("[Player]", target.getCustomName());
                                sender.sendMessage(BuildEmpire.prefix + msg);
                                msg = plugin.getBuildMode().isbuildmodeoff.replace("Der Spieler [Player] §7ist", "Du bist");
                                target.sendMessage(BuildEmpire.prefix + ((Player) sender).getDisplayName() + " §8» " + msg);
                                //Target soll vor der Message der Name des Senders angezeigt werden (brauche dafür die richtige Formatierung)
                                //Fabi
                            } else {
                                plugin.getBuildMode().setBuildmodeTrue(target);
                                String msg = plugin.getBuildMode().isbuildmodeon.replace("[Player]", target.getCustomName());
                                sender.sendMessage(BuildEmpire.prefix + msg);
                                msg = plugin.getBuildMode().isbuildmodeon.replace("Der Spieler [Player] §7ist", "Du bist");
                                target.sendMessage(BuildEmpire.prefix + ((Player) sender).getDisplayName() + " §8» "  + msg);
                                //same here
                            }
                        } else {
                            sender.sendMessage(BuildEmpire.prefix + plugin.getBuildMode().notonline);
                        }
                        break;
                    case 2:
                        if(args[1].equalsIgnoreCase("on") || args[1].equalsIgnoreCase("off") || args[1].equalsIgnoreCase("get")) {
                            target = Bukkit.getPlayer(args[0]);
                            if(target != null) {
                                switch (args[1]) {
                                    case "on":
                                        plugin.getBuildMode().setBuildmodeTrue(target);
                                        String msg = plugin.getBuildMode().isbuildmodeon.replace("[Player]", target.getCustomName());
                                        sender.sendMessage(BuildEmpire.prefix + msg);
                                        msg = plugin.getBuildMode().isbuildmodeon.replace("Der Spieler [Player] §7ist", "Du bist");
                                        target.sendMessage(BuildEmpire.prefix + ((Player) sender).getDisplayName() + " §8» "  + msg);
                                        //same here
                                        break;
                                    case "off":
                                        plugin.getBuildMode().setBuildmodeFalse(target);
                                        msg = plugin.getBuildMode().isbuildmodeoff.replace("[Player]", target.getCustomName());
                                        sender.sendMessage(BuildEmpire.prefix + msg);
                                        msg = plugin.getBuildMode().isbuildmodeoff.replace("Der Spieler [Player] §7ist", "Du bist");
                                        target.sendMessage(BuildEmpire.prefix + ((Player) sender).getDisplayName() + " §8» "  + msg);
                                        //same here
                                        break;
                                    case "get":
                                        if(plugin.getBuildMode().isBuildmode(target)==true) {
                                            msg = plugin.getBuildMode().isbuildmodeon.replace("ist nun", "befindet sich").replace("[Player]", target.getCustomName());
                                            sender.sendMessage(BuildEmpire.prefix + msg);
                                        } else {
                                            msg = plugin.getBuildMode().isbuildmodeoff.replace("ist nun", "befindet sich").replace("mehr ","").replace("[Player]", target.getCustomName());
                                            sender.sendMessage(BuildEmpire.prefix + msg);
                                        }
                                        break;
                                }
                            } else {
                                sender.sendMessage(BuildEmpire.prefix + plugin.getBuildMode().notonline);
                            }
                        } else {
                            plugin.sendUsage("/buildmode [Spieler] [on|off|get]", "/buildmode Phottor on", p);
                        }
                        break;
                    default:
                        plugin.sendUsage("/buildmode [Spieler] [on|off|get]", "/buildmode Phottor", p);
                        break;
                    }
            } else {
                sender.sendMessage(BuildEmpire.noPerms);
            }
        }
        return true;
    }

    
}
