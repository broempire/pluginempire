/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Commands;

import de.broempire.buildempir.BuildEmpire;
import de.broempire.buildempire.APIs.ItemBuilder;
import de.broempire.buildempire.Manager.Map;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class BuildCommand implements CommandExecutor {
    
    BuildEmpire plugin;
    
    public BuildCommand(BuildEmpire plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player)sender;
        Map world = Map.getMapByName(p.getWorld().getName(), plugin);
        
        switch (args.length) {
            case 2:
                switch(args[0]) {
                    case "accept":
                        if(!plugin.getBuild().hasPlayer(p)) {
                            if(plugin.getBuild().hasInvitation(p)) {
                                if(Bukkit.getPlayer(args[1]) != null) {
                                    Player t = Bukkit.getPlayer(args[1]);
                                    plugin.getBuild().addToBuild(p, t);
                                }else{
                                    p.sendMessage(BuildEmpire.prefix + "§cDer Spieler ist nicht online!");
                                }
                            }else{
                                p.sendMessage(BuildEmpire.prefix + "§cDu hast keine Einladung erhalten!");
                            }
                        }else{
                            p.sendMessage(BuildEmpire.prefix + "§cDu bist bereits in einem Bautrupp!");
                        }
                        break;
                    case "deny":
                        if(plugin.getBuild().hasInvitation(p)) {
                            if(Bukkit.getPlayer(args[1]) != null) {
                                Player t = Bukkit.getPlayer(args[1]);
                                p.sendMessage(BuildEmpire.prefix + "Du hast die Bauanfrage §cabgelehnt§7!");
                                t.sendMessage(BuildEmpire.prefix + p.getDisplayName() + " §7hat die Bauanfrage §cabgelehnt§7!");
                                plugin.getBuild().removeInvitation(p);
                            }else{
                                p.sendMessage(BuildEmpire.prefix + "§cDer Spieler ist nicht online!");
                            }
                        }else{
                            p.sendMessage(BuildEmpire.prefix + "§cDu hast keine Einladung erhalten!");
                        }
                        break;
                    case "kick":
                        if(plugin.getBuild().hasPlayer(p)) {
                            if(world != null) {
                                if(p.getName().equals(world.getOwner())) {
                                    if(Bukkit.getPlayer(args[1]) != null) {
                                        Player t = Bukkit.getPlayer(args[1]);
                                        if(plugin.getBuild().getPlayers().contains(t)) {
                                            plugin.getBuild().removeFromBuild(t);
                                            p.sendMessage(t.getDisplayName() + " §7wurde §crausgeworfen§7!");
                                            t.sendMessage(BuildEmpire.prefix + "§cDu wurdest aus dem Bautrupp gekickt!");
                                            
                                            plugin.readyPlayer(t);
                                            t.setGameMode(GameMode.SURVIVAL);
                                            t.teleport(plugin.getLocation("LOBBY"));
                                            t.getInventory().setItem(8, new ItemBuilder(Material.REDSTONE_COMPARATOR, 1).setDisplayName("§7>>§6Menü§7<<").setLore("§7↳ §8Rechtsklick zum Benutzen!").build());
                                        }else{
                                            p.sendMessage(BuildEmpire.prefix + "§cDer Spieler ist nicht in deiner Allianz!");
                                        }
                                    }else{
                                        p.sendMessage(BuildEmpire.prefix + "§cDer Spieler ist nicht online!");
                                    }
                                }else{
                                    p.sendMessage(BuildEmpire.prefix + "§cDu bist nicht der Bauleiter!");
                                }
                            }
                        }else{
                            p.sendMessage(BuildEmpire.prefix + "§cDu bist nicht in einem Bautrupp!");
                        }
                        break;
                    default:
                        sendHelp(p);
                        break; 
                }
                break;
            default:
                sendHelp(p);
                break; 
        }
        return true;
    }
    
    private void sendHelp(Player p) {
            p.sendMessage(BuildEmpire.prefix + "§bBefehle:");
            p.sendMessage("§8● §b/world accept <Spieler> §7Bauanfrage annehmen");
            p.sendMessage("§8● §b/world deny <Spieler> §7Bauanfrage ablehnen");
            //p.sendMessage(new TextComponent("§8● §b/alliance list §7Alle Allianzmitglieder auflisten"));
            //p.sendMessage(new TextComponent("§8● §b/alliance leave §7Allianz verlassen"));
            p.sendMessage("§8● §b/world kick <Spieler> §7Spieler aus der Welt werfen");
            //p.sendMessage(new TextComponent("§8● §b/alliance promote <Spieler> §7Neuen Bündnisleiter bestimmen"));
            //p.sendMessage(new TextComponent("§8● §b/alliance c <Nachricht> §7Schreibe deiner Allianz"));
        }
}
