/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Commands;

import de.broempire.buildempir.BuildEmpire;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class SetLobbyCommand implements CommandExecutor {
    
    BuildEmpire plugin;
    
    public SetLobbyCommand(BuildEmpire plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        
        Player p = (Player)sender;
        
        if(p.hasPermission("BuildEmpire.setlobby")) {
            if(args.length == 0) {
                plugin.setLocation(p.getLocation(), "LOBBY");
                if(plugin.getConfig().getString("Locations.LOBBY.world") != null) { 
                    p.sendMessage(BuildEmpire.prefix + "Du hast den §6Lobby-Spawn §7erfolgreich gesetzt!");
                }
            }else{
                plugin.sendUsage("/setlobby", null, p);
            }
            
        }else{
            p.sendMessage(BuildEmpire.noPerms);
        }
        return true;
    }   
}
    
