/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Commands;

import de.broempire.buildempir.BuildEmpire;
import de.broempire.buildempire.APIs.ActionAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class SpawnCommand implements CommandExecutor {
    
    BuildEmpire plugin;
    
    public SpawnCommand(BuildEmpire plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            switch(args.length) {
                case 0:
                    p.teleport(plugin.getLocation("LOBBY"));
                    ActionAPI.sendActionBar(p, "§7Du bist nun am §aSpawn§7!");
                    break;
                case 1:
                    if (p.hasPermission("BuildEmpire.spawn")) {
                        if (args[0].equalsIgnoreCase("all")) {
                            plugin.getServer().getOnlinePlayers().forEach((online) ->{
                                online.teleport(plugin.getLocation("LOBBY"));
                                ActionAPI.sendActionBar(online, "§7Du bist nun am §aSpawn§7!");
                            });
                        } else if (Bukkit.getPlayer(args[0]) != null) {
                            Bukkit.getPlayer(args[0]).teleport(plugin.getLocation("SPAWN"));
                            ActionAPI.sendActionBar(Bukkit.getPlayer(args[0]), "§7Du bist nun am §aSpawn§7!");
                        } else {
                            plugin.sendUsage("/spawn [Spieler|all]", "/spawn Juligum", p);
                        }
                    } else {
                        p.sendMessage(BuildEmpire.noPerms);
                    }
            }
        }
        return true;
    }
}
