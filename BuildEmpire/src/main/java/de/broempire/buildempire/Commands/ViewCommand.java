/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.Commands;

import de.broempire.buildempir.BuildEmpire;
import de.broempire.buildempire.Manager.Map;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Juli
 */
public class ViewCommand implements CommandExecutor {
    
    BuildEmpire plugin;
    List<String> worldName = new ArrayList<>();
    
    public ViewCommand(BuildEmpire plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player)sender;
        
        switch (args.length) {
            case 0:
                for(Map world : plugin.getWorlds()) {
                    worldName.add(world.getName());
                }
                p.sendMessage(BuildEmpire.prefix + "§9Alle Maps: §a" + worldName);
                
                for(Map world : plugin.getWorlds()) {
                    worldName.remove(world.getName());
                }
                break;
            case 2:
                switch(args[0]) {
                    case "info":
                        if(Map.getMapByName(args[1], plugin) != null) {
                            Map world = Map.getMapByName(args[1], plugin);
                            
                            p.sendMessage(BuildEmpire.prefix + "Name: §a" + world.getName());
                            p.sendMessage(BuildEmpire.prefix + "Owner: §a" + world.getOwner());
                            p.sendMessage(BuildEmpire.prefix + "Generator: §a" + world.getGenerator());
                            p.sendMessage(BuildEmpire.prefix + "Geheimhaltung: §a" + world.getPrivacy());
                        }else{
                            p.sendMessage(BuildEmpire.prefix + "§cDiese Welt gibt es nicht!");
                        }
                        break;
                    case "tp":
                        if(Map.getMapByName(args[1], plugin) != null) {
                            Map world = Map.getMapByName(args[1], plugin);
                            
                            p.teleport(world.getLocation("SPAWN"));
                            p.setGameMode(GameMode.CREATIVE);
                            plugin.readyPlayer(p);
                            
                            p.sendMessage(BuildEmpire.prefix + "§cWICHTIG:");
                            p.sendMessage(BuildEmpire.prefix + "Es ist als Teammitglied verboten, eine Welt eines anderen §c§nOHNE§r §7Einverständnis zu bearbeiten! Wenn ein Spieler so etwas meldet, werden Maßnahmen getroffen und die Rechte verändert werden müssen!");
                        }else{
                            p.sendMessage(BuildEmpire.prefix + "§cDiese Welt gibt es nicht!");
                        }
                        break;
                    default:
                        plugin.sendHelp(p);
                        break;
                }
                break;
            default:
                plugin.sendHelp(p);
                break;
        }
        return true;
    }
    
}
