/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.broempire.buildempire.APIs;

import com.google.common.collect.Lists;
import java.util.List;

/**
 *
 * @author Matti
 */
public class ScoreboardTeam {

    private final String name;
    private String prefix;
    private String suffix;
    private List<String> players;
    
    public ScoreboardTeam(String name) {
        this.name = name;
        this.players = Lists.newArrayList();
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }
    
    public List<String> getPlayerNameSet() {
        return players;
    }

    public String getName() {
        return name;
    }

    public void setPlayerNameSet(List<String> players) {
        this.players = players;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
    
    
}
